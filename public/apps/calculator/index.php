<html>

<head>
  <style>
    .header {
      background: transparent!important;
    }

    .wrapper .contenedor .dd_screen {
      min-height: calc(100% - 64px);
      display: inline-flex;
    }

    .calc {
      margin: auto;
      width: 100%;
      max-width: 300px;
      /*height: 480px;*/
      overflow: hidden;
      border-radius:1em;
      box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
    }
    
    .calc * {
      font-family:Product Sans, Roboto!important;
    }

    .calc .result {
      position: relative;
      background: white;
      padding: 1rem;
      box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);

    }

    .calc .result .entered {
    width: 100%;
    text-align: right;
    padding: 0.5rem;
    }

    .calc .result .entered p {
      color: #c5c5c5;
      display: inline;
      font-size: 1.5rem;
    }

    .calc .result .entered p:first-child,
    .calc .result .entered p:nth-child(2),
    .calc .result .entered p:nth-child(3) {
      margin-right: 3px;
    }

    .calc .result .current {
    width: 100%;
    text-align: right;
    padding: 0.5rem;
      padding-bottom:0;
    }

    .calc .result .current h1 {
      font-size: 2.5em;
      color: #2e2e2e;
      font-weight: 300;
    }

    .calc .buttons {
      height: 350px;
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-pack: justify;
      -ms-flex-pack: justify;
      justify-content: space-between;
      -webkit-box-align: end;
      -ms-flex-align: end;
      align-items: flex-end;
      -ms-flex-wrap: wrap;
      flex-wrap: wrap;
      background: #f1f2f4;
      border-right: 0.5rem solid #2c78e4;
    }

    .calc .buttons div {
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      align-items: center;
      width: 69px;
      height: 69px;
      cursor: pointer;
      border-radius:1em;
    }

    .calc .buttons div.pressed {
      background: rgba(58, 70, 84, 0.4);
    }

    .calc .buttons div:nth-child(1) p {
    color: #2c78e4;
    font-size: 2.5em;
    }

    .calc .buttons div:nth-child(2) p,
    .calc .buttons div:nth-child(3) p,
    .calc .buttons div:nth-child(4) p,
    .calc .buttons div:nth-child(8) p,
    .calc .buttons div:nth-child(12) p,
    .calc .buttons div:nth-child(16) p {
      color: #2c78e4;
    }

    .calc .buttons div:nth-child(4),
    .calc .buttons div:nth-child(8),
    .calc .buttons div:nth-child(12),
    .calc .buttons div:nth-child(16),
    .calc .buttons div:nth-child(20) {
      width: 70px;
    }

    .calc .buttons div:nth-child(20) p {
      color: #2c78e4;
      font-size: 3rem;
    }

    .calc .buttons div p {
    font-size: 1.5em;
    color: #4a4c4e;
    }
    
    @media (max-width: 75em) {
      .calc {
        max-width:100%;
      }
    }
    
  </style>
</head>

<body>
  <div class="calc PF">
    <div class="result">
      <div class="entered">
        <p class="f_entered"></p>
        <p class="operator"></p>
        <p class="l_entered"></p>
        <p>=</p>
      </div>
      <div class="current">
        <h1>0</h1>
      </div>
    </div>
    <div class="buttons">
      <div data-key="clear" class="ripple">
        <p>c</p>
      </div>
      <div data-key="inv" class="ripple">
        <p>+/ -</p>
      </div>
      <div data-key="sqrt" class="ripple">
        <p>√</p>
      </div>
      <div data-key="*" class="ripple">
        <p>x</p>
      </div>
      <div data-key="7" class="ripple">
        <p>7</p>
      </div>
      <div data-key="8" class="ripple">
        <p>8</p>
      </div>
      <div data-key="9" class="ripple">
        <p>9</p>
      </div>
      <div data-key="/" class="ripple">
        <p>/</p>
      </div>
      <div data-key="4" class="ripple">
        <p>4</p>
      </div>
      <div data-key="5" class="ripple">
        <p>5</p>
      </div>
      <div data-key="6" class="ripple">
        <p>6</p>
      </div>
      <div data-key="-" class="ripple">
        <p>-</p>
      </div>
      <div data-key="1" class="ripple">
        <p>1</p>
      </div>
      <div data-key="2" class="ripple">
        <p>2</p>
      </div>
      <div data-key="3" class="ripple">
        <p>3</p>
      </div>
      <div data-key="+" class="ripple">
        <p>+</p>
      </div>
      <div data-key="0" class="ripple">
        <p>0</p>
      </div>
      <div data-key="comma" class="ripple">
        <p>,
        </p>
      </div>
      <div data-key="pi" class="ripple">
        <p>π</p>
      </div>
      <div data-key="equals" class="ripple">
        <p>=</p>
      </div>
    </div>
  </div>
  <script src="//dumdarac.com/resources/js/stopExecutionOnTimeout.js"></script>
  <script>
    class Calculator {
      constructor() {
        this.current = 0;
        this.entered = 0;
        this.answer = 0;
        this.decimal = false;
        this.operator = '';
        this.states = {
          'inv': false,
          'comma': false
        }
        this.options = [
          ['equals',
            this.processEquals.bind(this)
          ],
          ['clear',
            this.processClear.bind(this)
          ],
          ['sqrt',
            this.processSqrt.bind(this)
          ],
          ['inv',
            this.processInv.bind(this)
          ],
          ['comma',
            this.processComma.bind(this)
          ],
          ['pi',
            this.processPi.bind(this)
          ]
        ];
        this.firstEnteredOutput = document.querySelector('.f_entered');
        this.lastEnteredOutput = document.querySelector('.l_entered');
        this.operatorOutput = document.querySelector('.operator');
        this.currentOutput = document.querySelector('.current > h1');
        this.buttons = document.querySelectorAll('.buttons > div');
        for (var i = 0, n = this.buttons.length; i < n; i++) {
          if (window.CP.shouldStopExecution(1)) {
            break;
          }
          var calc = this;
          var button = this.buttons[i];
          button.addEventListener('mousedown', function() {
            var _this = this;
            calc.processAction(_this.getAttribute('data-key'));
            _this.classList.add('pressed');
            setTimeout(function() {
              _this.classList.remove('pressed');
            }, 400);
          });
          button.addEventListener('mouseup', function() {
            var _this = this;
            _this.classList.remove('pressed');
          });
        }
        window.CP.exitedLoop(1);
      }
      processAction(a) {
        for (var i = 0, n = this.options.length; i < n; i++) {
          if (window.CP.shouldStopExecution(2)) {
            break;
          }
          var option = this.options[i];
          if (a === option[0]) {
            option[1]();
            return;
          }
        }
        window.CP.exitedLoop(2);
        if (a === '+' || a === '-' || a === '/' || a === '*') {
          this.processASDM(a);
          return;
        } else {
          this.processNumber(a);
          return;
        }
      }
      processEquals() {
        if (!!this.operator) {
          this.displayNumber(this.current, this.lastEnteredOutput);
          this.answer = eval(this.entered + this.operator + this.current);
          this.displayNumber(this.answer, this.currentOutput);
          this.current = this.answer;
        }
      }
      processClear() {
        this.current = 0;
        this.displayNumber(this.current, this.currentOutput);
        this.entered = 0;
        this.operator = '';
        this.firstEnteredOutput.innerHTML = '';
        this.lastEnteredOutput.innerHTML = '';
        this.operatorOutput.innerHTML = '';
      }
      processSqrt() {
        this.current = Math.sqrt(this.current);
        this.displayNumber(this.current, this.currentOutput);
      }
      processInv() {
        this.current = this.current * -1;
        this.displayNumber(this.current, this.currentOutput);
      }
      processComma() {
        if (!this.decimal) {
          this.current += '.';
          this.currentOutput.innerHTML = this.current;
        }
        this.decimal = true;
      }
      processPi() {
        this.current = Math.PI;
        this.displayNumber(this.current, this.currentOutput);
      }
      processASDM(a) {
        if (!!this.entered && !!!this.answer) {
          return;
        }
        if (this.answer) {
          this.lastEnteredOutput.innerHTML = '';
        }
        this.decimal = false;
        this.operator = a;
        this.entered = this.current;
        this.displayNumber(this.entered, this.firstEnteredOutput);
        a === '*' ? this.operatorOutput.innerHTML = 'x' : this.operatorOutput.innerHTML = this.operator;
        this.current = 0;
        this.displayNumber(this.current, this.currentOutput);
      }
      processNumber(n) {
        this.current === 0 ? this.current = n : this.current += n;
        this.displayNumber(this.current, this.currentOutput);
      }
      displayNumber(n, location) {
        location.innerHTML = String(n).substring(0, 10);
      }
    }

    var Calc = new Calculator();
  </script>
</body>

</html>