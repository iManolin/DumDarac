<html>

<head>
  <style>
    .container_ico {
      max-width: 40rem;
      margin: 80px auto 0 auto;
    }

    .container_ico p {
      padding: 0.5rem;
      word-break: break-word;
      text-align: justify;
      clear: both;
    }

    .container_ico p.intro {
    font-size: 1.2em;
    }

    .container_ico b {
      font-weight: 500;
    }

    .container_ico ul {
      padding: 0.5rem;
    }

    .container_ico ul li {
    padding: 0.2rem 0.5rem;
    text-align: justify;
    }

    .container_ico ul li b {
    }

    .container_ico blockquote {
    color: #4285f4;
    font-size: 1.2em;
    background: #eee;
    padding: 1rem;
    border-radius: 1em;
    margin: 0.5rem 0;
    padding-top: 1.2rem;
    }

    .container_ico h1,
    .container_ico .container_ico h2,
    .container_ico h3,
    .container_ico h4,
    .container_ico h5,
    .container_ico h6 {
      font-weight: 500;
      text-transform: uppercase;
      color: #4285f4;
    }

    .container_ico h1 {
      font-size: 3em;
    }

    .container_ico h2 {
      font-size: 2.4em;
    }

    .container_ico h3 {
      font-size: 2.2em;
    }

    .container_ico h4 {
      font-size: 1.8em;
    }

    .container_ico h5 {
      font-size: 1.4em;
    }

    .container_ico h6 {
      font-size: 1.4em;
    }

    .container_ico h6.small {
      font-size: 16px;
      font-weight: 300;
      margin-bottom: 20px;
    }

    .container_ico header.info {
      overflow: hidden;
      margin-bottom: 20px;
    }

    .container_ico header.info h2 {
    font-size: 2em;
    color: #4285f4;
    background-clip: text;
    margin-bottom: 2rem;
      padding:1rem;
    }

    .container_ico header.info img {}

    section.meta {
      margin-bottom: 40px;
    }

    section.meta span {
      color: #888;
    }

    section.meta a {
      margin-right: 5px;
    }

    section.meta img {
      width: 40px;
      border-radius: 100%;
      display: inline-block;
      position: relative;
      top: 12px;
      margin: 0 5px 0 10px;
    }

    .container_ico img {
      max-width: 100%;
      border-radius: 1em;
    }

    img+span {
      color: #888;
      font-size: 0.9em;
      padding: 1rem;
      display: block;
    }

    footer.navigation {
      width: 100%;
      padding: 20px 25px;
      margin-top: 0.5rem;
      border-top: solid 3px #4285f4;
      background: #fff;
      -webkit-transition: .3s;
      position: relative;
      z-index: 1;
    }

    footer.navigation header {
      width: 100%;
      overflow: hidden;
      margin-bottom: 20px;
    }

    footer.navigation header h6 {
      margin: 0;
      float: left;
    }

    footer.navigation header span.ss-icon {
      display: block;
      float: right;
      margin-top: -4px;
      color: #4285f4;
    }

    footer.navigation h4 {
      color: transparent;
      background-image: url(http://media-cache-ak0.pinimg.com/236x/ac/2f/cf/ac2fcfad5efdd3c7d27c9c85a18c56c4.jpg);
      -webkit-background-clip: text;
      background-clip: text;
      margin-bottom: 10px;
      -webkit-animation: 8s movebg infinite alternate;
      -webkit-animation-timing-function: ease;
      -webkit-animation-play-state: paused;
    }

    footer.navigation .meta {
      font-size: 0.9em;
    }

    footer.navigation .meta a:hover {
      border-bottom: none;
    }

    footer.navigation .meta img {
      width: 30px;
      top: 9px;
    }

    footer.navigation:hover {
      margin-top: -40px;
    }

    footer.navigation:hover h4 {
      -webkit-animation-play-state: running;
    }

    @-webkit-keyframes movebg {
      to {
        background-position: right;
      }
    }
  </style>
</head>

<body>
  <div class="container_ico">
    <header class="info">
      <h2>A Free Social Platform with many services and applications, edited collaboratively.</h2>
      <img src="https://img.dumdarac.com/logo/wallpaper.png">
      <span>Logo of DumDarac (dumdarac.com)</span>
    </header>
    <p class="intro">Among all the applications that are: Fit, Games, Movies, Music, Books, News, DumDarac Hey (chat), Documents, Cloud, Calendar, Save, Space, Travel Assistance, TV Series, Celebrities and also has an online desktop, etc... Many aplications are in process, but let see what each one:</p>
    <p>
      Social Network, the main DumDarac service, which integrates different services:
      <ul>
        <li><b>Circles</b> allows users organize people into groups or ready to share.</li>
        <li><b>Interests</b> allows users identify topics that might be interested in sharing with others. "Featured interests" it is also available, based on other global issues that they find interesting. Interests helps keep users posted on the latest
          updates on topics of interest.</li>
        <li><b>Communities</b> is a feature that allows groups gather real world as your basketball teammates, classmates or your family. Share things usually with the same group of people, communities are a fast, fun way to keep in touch more easily.</li>
        <li><b>People</b> allows users organize contacts in groups to share, through various products and services DumDarac. Even though other users can see a list of people in the collection of circles, they can not see the names of those circles. Privacy
          settings also allow users hide users in their circles, as well as those who have in their circle.</li>
        <li><b>Explore</b> users see updates from people in their circles. The entry box allows users enter a status update or use icons to upload and share photos and videos. Developments can be filtered to show only messages from specific circles.</li>
        <li><b>Hashtags</b> allow the use of words or phrases (without spaces) preceded by the # symbol that can display related information.</li>
      </ul>
    </p>
    <p>
      <b>Hey</b> is an application similar to Google Allo messenger, Whatsapp or Messenger including your virtual assistant Kamiku via chat (in development).
    </p>
    <p>
      <b>DumDarac Play</b> offers movies, games, music, books and other download options immediately software on some products DumDarac in most regions, with the payment option exchange Bitcoin, Faircoin, Paypal and other payment methods,well that in
      some products you can get a demo version and then purchase a full version although most are free.
    </p>
  
  <p>
    <b>News</b> is an application to view the news including the personal assistant Kamiku, whose artificial intelligence help the user find the news you most interested.
  </p>
  
  <p>
    <b>Music</b> You can listen to music, create playlists and even have your own social network.
  </p>
  
  <p>
    <b>Celebritys</b> is an application that shows you all the information you ask him about known or famous people. 
  </p>
  
  <blockquote>All services offered by DumDarac are free.</blockquote>
  
  <h2>How the idea came about to create the platform DumDarac</h2>
    <p>
The idea of ​​doing DumDarac arose because Albert (the founder) was a boy of only 12 years old who when trying to listen to music or use a social network or other services all had the same problem: Advertising or selling your data. Even paying.</p>
  <blockquote>The founder and principal developer is Albert Isern Alvarez. <br>A platform non-profit organization whose funding is based on donations. They founded in 2012 with 12 years.</blockquote>
    <p>
That's why Albert decided to create his own platform. A free social platform that anyone can use for free without having to pay for anything or receive advertisements or sell their data.</p>
  <a class="PF PF-button ripple opendd" href="/">Open DumDarac</a>
    <footer class="navigation">
      <a onclick="windowdd('./resources/window/donar.php', 'fit');" >
        <h4 style="cursor:pointer;" >Would you like to make a donation?</h4>
      </a>
    </footer>
  </div>

  <script>
    $('.header').addClass('noborder');
  </script>

</body>

</html>