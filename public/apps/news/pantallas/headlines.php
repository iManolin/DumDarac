
<div class="PF-tabs">
  <div class="PF PF-tabbar shadow">
    <div class="container center">
      <ul>
        <li class="ripple active" data-for="tabsnewstopics-1" data-taburl="./apps/<?=$app?>/resources/load-feed.php?topic=lastest&<?=$server_querystring?>"><span t-dd>Lastest</span></li>
        <li class="ripple" data-for="tabsnewstopics-2" data-taburl="./apps/<?=$app?>/resources/load-feed.php?topic=world&<?=$server_querystring?>"><span t-dd>World</span></li>
        <li class="ripple" data-for="tabsnewstopics-3" data-taburl="./apps/<?=$app?>/resources/load-feed.php?topic=bussiness&<?=$server_querystring?>"><span t-dd>Business</span></li>
        <li class="ripple" data-for="tabsnewstopics-4" data-taburl="./apps/<?=$app?>/resources/load-feed.php?topic=technology&<?=$server_querystring?>"><span t-dd>Technology</span></li>
        <li class="ripple" data-for="tabsnewstopics-5" data-taburl="./apps/<?=$app?>/resources/load-feed.php?topic=entertainment&<?=$server_querystring?>"><span t-dd>Entertainment</span></li>
        <li class="ripple" data-for="tabsnewstopics-6" data-taburl="./apps/<?=$app?>/resources/load-feed.php?topic=sports&<?=$server_querystring?>"><span t-dd>Sports</span></li>
        <li class="ripple" data-for="tabsnewstopics-7" data-taburl="./apps/<?=$app?>/resources/load-feed.php?topic=science&<?=$server_querystring?>"><span t-dd>Science</span></li>
        <li class="ripple" data-for="tabsnewstopics-8" data-taburl="./apps/<?=$app?>/resources/load-feed.php?topic=health&<?=$server_querystring?>"><span t-dd>Health</span></li>
      </ul>
      <div class="slider"></div>
    </div>
  </div>
  <div class="tabs">
    <div class="tab" data-name="tabsnewstopics-1"></div>
    <div class="tab" data-name="tabsnewstopics-2"></div>
    <div class="tab" data-name="tabsnewstopics-3"></div>
    <div class="tab" data-name="tabsnewstopics-4"></div>
    <div class="tab" data-name="tabsnewstopics-5"></div>
    <div class="tab" data-name="tabsnewstopics-6"></div>
    <div class="tab" data-name="tabsnewstopics-7"></div>
    <div class="tab" data-name="tabsnewstopics-8"></div>
  </div>
</div>