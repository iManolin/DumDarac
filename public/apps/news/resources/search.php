<?php
include_once(__DIR__."/../../../variables.php");
$idioma = $_COOKIE['idioma'];
$source = $_GET['source'];
$q = $_GET['q'];
$categoria = $_GET['categoria'];
$ultimafecha = $_GET['ultimafecha'];
$hoy = date("Y-m-d H:i:s");

function imagennews_exists($url){
   $headers=get_headers($url);
   return stripos($headers[0],"200 OK")?true:false;
}

$select_noticias = "SELECT * FROM kiosco_news WHERE idioma='$idioma' ";

if($q != null){
  $select_noticias .= " AND (titulo LIKE LOWER('%$q%') OR descripcion LIKE '%$q%') ";
}

if($categoria == ''){
  $select_noticias .= " AND categoria='portada' ";
} else {
  $select_noticias .= " AND categoria='$categoria' ";
}

if($ultimafecha != ''){
  $select_noticias .= " AND fecha<'$ultimafecha' ";
}

if($first != '' and $categoria == '' and $source == ''){
  $select_noticias .= " AND imagen!='' ";
}

  if($source != ''){
    $source = str_replace('--', ' ', $source);
    $select_noticias .= " AND fuente='$source' "; 
  }
  
if($source == ''){

  if($first == ''){
    $select_noticias .= " AND LENGTH(texto)>500 ";
  } else {
    $select_noticias .= " AND LENGTH(texto)>1200 ";
  }
}
  
$randomnumbernewsarray = array("5", "7", "11", "13");
$randomnumbernewsarray = $randomnumbernewsarray[array_rand($randomnumbernewsarray)];
$limitpublicaciones = " LIMIT $randomnumbernewsarray";

$kiosco_news = mysqli_query($con,"$select_noticias ORDER BY fecha DESC $limitpublicaciones");
while($row_kiosco_news = mysqli_fetch_array($kiosco_news))
{
  $id_kiosco_news = $row_kiosco_news["id"];
	$titulo_kiosco_news = $row_kiosco_news["titulo"];
	$fecha_kiosco_news = $row_kiosco_news["fecha"];
	$categoria_kiosco_news = $row_kiosco_news["categoria"];
  $imagen_kiosco_news = $row_kiosco_news["imagen"];
  $etiquetas_kiosco_news = $row_kiosco_news["etiquetas"];
	$fuente_kiosco_news = $row_kiosco_news["fuente"];
  $autor_kiosco_news = $row_kiosco_news["autor"];
  $descripcion_kiosco_news = $row_kiosco_news["descripcion"];
  
  $titulo_kiosco_news = preg_replace("/$q/i", "<b>$q</b>", $titulo_kiosco_news);
  $descripcion_kiosco_news = preg_replace("/$q/i", "<b>$q</b>", $descripcion_kiosco_news);
  
  if($categoria_kiosco_news == ''){$etiquetas_categoria_kiosco_news = $etiquetas_kiosco_news;} else { $etiquetas_categoria_kiosco_news = $categoria_kiosco_news; }
  $kiosco_news_smartcard_2 = mysqli_query($con,"SELECT * FROM kiosco_fuentes WHERE nombre='$fuente_kiosco_news' LIMIT 1");
  while($row_kiosco_news_smartcard_2 = mysqli_fetch_array($kiosco_news_smartcard_2))
  {
    $id_kiosco_news_img = $row_kiosco_news_smartcard_2["id"];
  ?>

<div class="PF PF-card <?php if($imagen_kiosco_news == ''){?> border <?}?>" opendd-href="?app=news&id=<?=$id_kiosco_news?>">
  <div class="fechanew" style="display:none;" ><?=$fecha_kiosco_news?></div>
  <?php if($imagen_kiosco_news != '' and imagennews_exists($imagen_kiosco_news) == true){?><div class="imagen_noticia" onclick="openarticle('<?=$id_kiosco_news?>');" style="background-image:url('//dumdarac.com/resources/resize-images/resize.php?w=400&url=<?=$imagen_kiosco_news?>');" ></div><?}?>
  <div class="elements" >
    
  <div class="content" >
    <div class="iconos" >
      <div class="fuente" >
        <p><?=$fuente_kiosco_news?></p>
      </div>
    </div>
    
    <div class="info" onclick="openarticle('<?=$id_kiosco_news?>');" >
      <h1><?=$titulo_kiosco_news?></h1>
    </div>
  
  </div>
    </div>
  
  <div class="rest" >
        <p><?php echo $descripcion_kiosco_news; ?></p>
      </div>
  
  <div class="footer" >
      <p prettydate ><?=date('Y-m-d H:i:s', strtotime($fecha_kiosco_news . ' + 7 hours'));?></p>
      <i class="material-icons" onclick="downmenudd('./apps/news/resources/menu/noticia.php', '#noticia<?=$id_kiosco_news?>');">&#xE5D4;</i>
    </div>
  
</div>

<?} }?>