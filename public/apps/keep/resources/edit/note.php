<div class="PF PF-toolbar">
	<div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
	<h1 t-dd>Edit note</h1>
</div>
<form class="PF PF-form" id="form-note-update" action="./apps/keep/resources/update/note.php?<?=$server_querystring?>" method="post" enctype="multipart/form-data">
	<label class="PF-textfield filled">
		<textarea placeholder=" " name="text" ><?=$note_text?></textarea>
		<span t-dd>Text</span>
	</label>
	<button class="PF-button" style="margin-left: auto;" t-dd>Save</button>
</form>

<script>

  $("#form-note-update").ajaxForm({
    beforeSubmit: function (arr, $form, options) {
      $('.header .PF-progress.loading').show();
      alertdd.show('Saving note');
    },
    success: function (data) {
      $('.header .PF-progress.loading').hide();
        eval(data);
    }
  });
  
</script>