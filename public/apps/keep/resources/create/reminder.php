<div class="PF PF-toolbar">
	<div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
	<h1 t-dd>Create reminder</h1>
</div>
<form class="PF PF-form" id="form-reminder-create" action="./apps/keep/resources/insert/reminder.php?<?=$server_querystring?>" method="post" enctype="multipart/form-data">
	<label class="PF-textfield filled">
		<textarea placeholder=" " name="text" ><?=$reminder_text?></textarea>
		<span t-dd>Text</span>
	</label>
	<button class="PF-button" style="margin-left: auto;" t-dd>Create</button>
</form>

<script>

  $("#form-reminder-create").ajaxForm({
    beforeSubmit: function (arr, $form, options) {
      $('.header .PF-progress.loading').show();
      alertdd.show('Creating reminder');
    },
    success: function (data) {
      $('.header .PF-progress.loading').hide();
        eval(data);
    }
  });
  
</script>