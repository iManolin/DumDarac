<div class="PF PF-button" onclick="windowdd('./apps/keep/resources/create/note.php?<?=$server_querystring?>', 'fit');" t-dd>Create note</div>
<div class="PF PF-grid" style="grid-template-columns: repeat(auto-fill, minmax(300px, 1fr)); " id="keep-notes">
<?php
$notes_select = mysqli_query($con,"SELECT * FROM keep_notes WHERE usuario_id='$usuario_mismo_id' ORDER BY id DESC LIMIT 25");
while($row_notes = mysqli_fetch_array($notes_select))
  {
  	$notes_id = $row_notes['id'];
  	$notes_text = $row_notes['text'];
  	?>

<div class="PF PF-card" keep-note="<?=$notes_id?>" onclick="windowdd('./apps/keep/resources/edit/note.php?note=<?=$notes_id?>&<?=$server_querystring?>', 'fit');">
	<div class="info">
		<p keep-note-data-text><?=replaceemojis($notes_text)?></p>
	</div>
</div>
<?}?>
</div>