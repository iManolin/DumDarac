<html>

<head>
  <style>

  .keep_container {
  	display: flex;
  	flex-direction: column;
  	flex: 1 auto;
  }

  .keep_container>.PF-tabs>.tabs>.tab>.PF-button {
  	margin: .5em auto;
  }

.keep_container .PF-grid {
	margin: auto;
	max-width: 60em;
}

.keep_container .PF-grid .PF-card .info p {
	-webkit-line-clamp: 8;
}
  
  </style>
</head>

<body>

	<div class="keep_container">

  <div class="PF-tabs">
    <div class="tabs">
      <div class="tab" data-name="tabskeep-1"></div>
      <div class="tab" data-name="tabskeep-2"></div>
    </div>

    <div class="PF PF-tabbar bottom shadow">
      <div class="container center">
        <ul id="bottombar">
          <li class="ripple active" data-for="tabskeep-1" data-taburl="./apps/<?=$app?>/pantallas/notes.php?<?=$server_querystring?>" onclick="keepnotes();">
            <i class="material-icons bd">local_library</i>
            <span t-dd>Notes</span>
          </li>

          <li class="ripple" data-for="tabskeep-2" data-taburl="./apps/<?=$app?>/pantallas/reminders.php?<?=$server_querystring?>" onclick="keepreminders();">
            <i class="material-icons">language</i>
            <span t-dd>Reminders</span>
          </li>
        </ul>
        <div class="slider"></div>
      </div>
    </div>
  </div>
  </div>

  <script> 
  keepnotes();

  function keepnotes(){
    $("body[class*='PFC-']").removeClass (function (index, css) { return (css.match (/(^|\s)PFC-\S+/g) || []).join(' '); });
    $('body').addClass('PFC-yellow');
  }

  function keepreminders(){
    $("body[class*='PFC-']").removeClass (function (index, css) { return (css.match (/(^|\s)PFC-\S+/g) || []).join(' '); });
    $('body').addClass('PFC-purple');
  }
  </script>

</body>

</html>