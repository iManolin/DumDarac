<?php

$img_id = $_GET['id'];
$dir_img = __DIR__."/../../";

$filesmanager = mysqli_query($con,"SELECT * FROM files WHERE fileid='$img_id' LIMIT 1");
if($row_filesmanager = mysqli_fetch_array($filesmanager))
{
  $id_filesmanager = $row_filesmanager['id'];
  $usuarioid_filesmanager = $row_filesmanager['usuario_id'];
  $mime_filesmanager = $row_filesmanager['mime'];
  $mime_filesmanager = str_replace("image/", "", $mime_filesmanager);
  
  $img = "usuario/" . $usuarioid_filesmanager . "/images/".$img_id.'.'.$mime_filesmanager;
  
  if (!file_exists($dir_img.$img)) {
    //transparent - In bd but not exist the file
    $img = "usuario/logo-noimage.png";
  }

} else {
  //Yellow - No in BD
  $img = "usuario/logo-noimage-2.png";
}

if ($img_id == 'usuario.svg') {
    //Default avatar
    $img = "usuario/usuario.svg";
    $img_mime = "image/svg";
}

if(!$usuario_mismo_id){
  //Green Water - Client is not logged in
  $img = "usuario/logo-noimage-3.png";
}

if($img){
  
  $img_mime = mime_content_type($dir_img.$img);
  if($img_mime === 'image/svg'){ $img_mime = "image/svg+xml"; }
  if($img_mime === 'image/svg+xml'){
    header('Content-Type: '.$img_mime);
    readfile($dir_img.$img);
  } else {
    $requested_uri = "/".$img;
    include_once($dir_img."resources/adaptive-images/adaptive-images.php");
  }
  
}
?>