<html>

<head>

  <style>

    .slider {
      max-width: 500px;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      margin:auto;
    }

    .slider .logo {
      width: 96px;
      height: 96px;
      position: relative;
    }

    .slider .logo:after {
      content: '';
      display: block;
      background-color: rgb(var(--PF-color-on-surface), .3);
      position: absolute;
      top: 20%;
      left: 0;
      width: 100%;
      height: 100%;
      z-index: 0;
      border-radius: 50%;
      filter: blur(20px)
    }

    .slider .logo:before {
      content: '';
      display: block;
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      background-image: url(//img.dumdarac.com/logo/logo.svg);
      background-size: contain;
      background-position: center;
      background-repeat: no-repeat;
      z-index: 1;
    }

    .slider .h1 {
      font-size: calc( (12vmin + 4em) / 3);
      text-align: center;
      margin: 0.5em 0 0.5em 0;
      color: rgb(var(--PF-color-on-surface));
      font-weight: 600;
    }

    .slider .h2 {
      font-size: 1.5em;
      text-align: center;
      color: #939393;
      font-weight: 500;
    }

    .slider .signin {
      margin: 3em 0 0 0;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: center;
      font-size: calc( (2vmin + 2em) / 3);
    }

    .slider .yes {
      font-size: 1.5em;
      height: 3rem;
      padding-bottom: 12px;
      padding-top: 12px;
      text-align: center;
      white-space: nowrap;
      width: 256px;
      background-color: #1a73e8;
      flex-shrink: 0;
      font-weight: 500;
      margin: 0;
      padding: 8px 16px;
      text-decoration: none;
      text-transform: none;
      border-radius: 4px;
      color: white;
      align-items: center;
      display: inline-grid;
      text-align: center;
    }

    .slider .no {
      margin: 1em 0 0 0;
      color: #5d5d5d;
      cursor: pointer;
      font-weight: 600;
      font-size: 1.2em;
    }

    .slider a {
      color: inherit;
      text-decoration: inherit;
    }
  </style>
</head>

<body>
  <div class="slider">
    <div class="logo"></div>
    <div class="h1">Make Dive your own</div>
    <div class="h2">Set up your browser in a few simple steps</div>
    <div class="signin">
      <a href="https://dumdarac.com/?p=login&continuar=https://dumdarac.com/?app=navegador" target="_blank">
        <div class="yes">Get Started</div>
      </a>
      <div class="no">Not Now</div>
    </div>
  </div>

</body>

</html>