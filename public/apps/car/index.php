<html><head>
<style>
  
  .headerbajo {
        min-height: calc(100% - 15rem);
        display:block;
  }
  
  .wrapper .contenedor {
    padding-top:2rem;
  }

  .contenedor_eventos {
    background: white;
    width: 100%;
    max-width: 70rem;
    margin: 0 auto;
    border-radius: 2px;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
    overflow: hidden;
  }
  
  .contenedor_eventos .nav {
    width: 100%;
    background: <?=$color_app_dd_2?>;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
  }
  
  .contenedor_eventos .nav h1 {
    color:white;
    padding:1rem;
    font-size:1.5rem;
    width: calc(100% - 1rem);
  }
	
	.contenedor_eventos .nav .selects {
		display:inline-flex;
		align-items:center;
    width:100%;
	}
  
  .contenedor_eventos .nav .selects p {
    padding:0.5rem 1rem;
    font-size:1rem;
    color:black;
    background:white;
    border-right:1px #ddd solid;
  }
	
	.contenedor_eventos .nav .selects input {
		padding:0.5rem;
		width:100%;
		font-size:1rem;
    outline:none;
    border:none;
    background:#fff;
	}
  
  .contenedor_eventos .nav .categorias {
    padding: 0.5rem;
    display: inline-flex;
    align-items: center;
    width: calc(100% - 1rem);
    background: rgba(0, 0, 0, 0.15);
    overflow-x: auto;
  }
	
	.contenedor_eventos .nav .categorias::-webkit-scrollbar {
    display:none;
}
  
  .contenedor_eventos .nav .categorias .categoria {
    padding:0.5rem;
  }
  
  .contenedor_eventos .nav .categorias .categoria .circulo {
    color: <?=$color_app_dd_2?>;
    border-radius: 50px;
    background: white;
    padding: 0.5rem 1rem;
    display: inline-block;
    text-align: center;
    cursor:pointer;
  }
  
  .contenedor_eventos .nav .destinos {
    padding: 0.5rem;
    color: white;
    font-size: 1.2rem;
    background:rgba(0, 0, 0, 0.25);
  }
  
  .contenedor_eventos .eventos {
    padding:0.5rem;
    width:calc(100% - 1rem);
  }
  
  .contenedor_eventos .eventos span.titulo-container {
    padding:0.5rem;
    display:block;
    width:calc(100% - 1rem);
  }
  
  .contenedor_eventos .eventos .eventos_container {
    padding: 0.5rem;
    max-width: calc(100% - 1rem);
    display: block;
    overflow-x: auto;
    text-align: left;
    padding-top: 0;
    column-count:auto;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento {
    width: calc(100% - 1rem);
    max-width: 20rem;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    background-color: #0D47A1;
    border-radius: 2px;
    overflow: hidden;
    color: white;
    background-image: url('https://static1.squarespace.com/static/52f3ba1fe4b0932b3f775421/t/530e5fa1e4b07a55c9b3d6ad/1393450915317/Beer+Die+Events.jpg?format=1500w');
    background-position: center;
    background-size: cover;
    margin: 0.5rem;
    display: inline-block;
    text-align: left;
	  transition:0.5s;
  }
	
  .contenedor_eventos .eventos .eventos_container .evento .price {
    padding: 0.5rem 0.7rem;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    background: white;
    border-radius: 2px;
    margin: 0.5rem;
    display: inline-block;
    color: black;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .imagen {
    padding-top: 2rem;
    width: 100%;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .imagen .info {
    padding: 1rem;
    display: block;
    width: calc(100% - 2rem);
    background: -webkit-linear-gradient(top, rgba(255, 255, 255, 0), #000);
    padding-top: 3rem;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .imagen .info span.titulo {
    font-size: 1.8rem;
    display: block;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .imagen .info span.sub_titulo i {
    font-size: 1rem;
    display: inline-block;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .informacion {
    padding: 0.5rem;
    background: <?=$color_app_dd?>;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .informacion span.titulo {
    display: block;
    padding: 0.5rem;
    font-size: 1.2rem;
	  display:inline-flex;
		align-items:center;
  }
	
	.contenedor_eventos .eventos .eventos_container .evento .informacion span.titulo i {
		margin:0 0.5rem;
	}
  
  .contenedor_eventos .eventos .eventos_container .evento .informacion span.fecha {
    display: block;
    padding: 0 0.5rem;
    font-size: 0.8rem;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .informacion span.ubicacion {
    display: block;
    padding: 0.5rem;
    font-size: 0.8rem;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .botones {
    background: <?=$color_app_dd?>;
    padding: 0.2rem;
    padding-top: 0;
    display: inline-flex;
    align-items: center;
    width: 100%;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .botones .boton {
    padding: 0.5rem 0.7rem;
    margin: 0.4rem;
    display: inline-block;
    background: rgba(0, 0, 0, 0.48);
    border-radius: 2px;
    display:inline-flex;
    align-items:center;
    cursor:pointer;
    cursor:pointer;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .botones .boton i {
    margin-right:0.5rem;
  }
  
</style></head><body>
<div class="contenedor_eventos">
  <div class="nav">
    <h1>¿Donde quieres ir?</h1>
		<div class="selects" >
      <p>Salida</p>
			<input type="text" placeholder="Salida" class="salidainput" >
      <p>Destino</p>
			<input type="text" placeholder="Destino" class="destinoinput" >
		</div>
    <p class="destinos" >Destinos</p>
    <div class="categorias" >
      <a class="categoria" onclick="$('.destinoinput').val('Barcelona');" ><div class="circulo" >Barcelona</div></a>
      <a class="categoria" onclick="$('.destinoinput').val('Valencia');" ><div class="circulo" >Valencia</div></a>
      <a class="categoria" onclick="$('.destinoinput').val('Paris');" ><div class="circulo" >Paris</div></a>
      <a class="categoria" onclick="$('.destinoinput').val('Madrid');" ><div class="circulo" >Madrid</div></a>
      <a class="categoria" onclick="$('.destinoinput').val('Milan');" ><div class="circulo" >Milan</div></a>
      <a class="categoria" onclick="$('.destinoinput').val('Lyon');" ><div class="circulo" >Lyon</div></a>
      <a class="categoria" onclick="$('.destinoinput').val('Bilbao');" ><div class="circulo" >Bilbao</div></a>
    </div>
  </div>
  
  <div class="eventos">
    <span class="titulo-container" >Todos</span>
    <div class="eventos_container">
      
  <?php 
	$usuario = mysqli_query($con,"SELECT * FROM car ORDER BY fecha DESC LIMIT 100");
while($row_evento = mysqli_fetch_array($usuario))
  { 
    $id_evento=$row_evento['id'];
	  $salida_evento=$row_evento['salida'];
    $destino_evento=$row_evento['destino'];
    $precio_evento=$row_evento['precio'];
    $fecha_evento=$row_evento['fecha'];
    $sitio_evento=$row_evento['sitio'];
    $imagen_evento=$row_evento['imagen'];
    $tipo_evento=$row_evento['tipo'];
    
?>
      <div class="evento" id="evento<?=$id_evento?>">
        <span class="price" ><?=$precio_evento?>€</span>
        <div class="imagen">
          <div class="info">
            <span class="sub_titulo"><i class="material-icons">&#xEB4A;</i> <i class="material-icons">&#xE405;</i> <i class="material-icons">&#xE91D;</i> <i class="material-icons">&#xE0CA;</i> </span>
            <span class="titulo"><?=$destino_evento?></span>
          </div>
        </div>
        <div class="informacion">
          <span class="titulo"><?=$salida_evento?> <i class="material-icons">&#xE260;</i> <?=$destino_evento?></span>
          <span class="fecha">Salida de <?=$salida_evento?> a las 18:20h con llegada a <?=$destino_evento?> a las 12:34h</span>
          <span class="ubicacion"><?=$sitio_evento?></span>
        </div>
        <div class="botones">
          <span class="boton">Reservar</span>
          <span class="boton">Compartir</span>
        </div>
      </div>
  
  <?}?>
      
    </div>
    </div>

</div>
</body></html>