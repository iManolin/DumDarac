<html>

<head>
	<style>
		.department_container {
			width: 100%;
		}
		
		.department_container>.head {
    width: calc(100% - 2em);
    background-color: rgba(var(--PF-color-primary), .2);
    border-radius: 1em;
    display: flex;
    flex-direction: row;
    overflow: hidden;
    margin: 1em;
		}
		
		.department_container>.head>h1 {
			flex: 1 auto;
			padding: 2em;
			padding-top: 2.2em;
			color: rgb(var(--PF-color-primary));
		}
	</style>
</head>

<body>
	<div class="content">
	<div class="department_container">
		<div class="head">
			<div class="PF PF-image" style="background-image:url('./apps/shopping/imgs/departments/<?=$department_id?>.jpg');" rgdd></div>
			<h1 t-dd><?=$department_name?></h1>
		</div>

<?php

$department_topics = array();
$topic_sql = mysqli_query($con,"SELECT * FROM topics WHERE LOWER(title) IN(SELECT related FROM topics_related WHERE LOWER(title) IN(LOWER('$department_name')) AND language IN('$language') ORDER BY id DESC) ORDER BY demand DESC LIMIT 20");
while($row_topic_sql = mysqli_fetch_array($topic_sql))
{
	$department_topics[] = $row_topic_sql["title"];
}
?>

		
		<div class="PF-tabs">
      <div class="PF PF-tabbar transparent" style="border-bottom: 1px solid #ddd">
        <div class="container">
          <ul>
          	<li class="ripple active" data-for="tabdepartment" data-taburl="./apps/shopping/pantallas/department/topic.php?<?=$server_querystring?>"><span>All products</span></li>
          	<?php foreach ($department_topics as &$value) {?>
            	<li class="ripple" data-for="tabdepartmenttopic-<?=$value?>" data-taburl="./apps/shopping/pantallas/department/topic.php?tags=<?=$value?>&<?=$server_querystring?>"><span><?=$value?></span></li>
						<?}?>
            </ul>
          <div class="slider"></div>
        </div>
      </div>
      <div class="tabs">
      	<div class="tab" data-name="tabdepartment"></div>
      	<?php foreach ($department_topics as &$value) {?>
        	<div class="tab" data-name="tabdepartmenttopic-<?=$value?>"></div>
        <?}?>
      </div>
    </div>
	</div>
	</div>

	<script> <?php if($department_color){?>$('body').addClass('PFC-<?=$department_color?>'); <?}?> </script>
	
</body>

</html>