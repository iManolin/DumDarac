<html>
	<head>
		<style>
			.product_page .product-info {
			width: 100%;
			padding: .5em;
			text-align: center;
			background-color: rgb(var(--PF-color-background));
			z-index: 5;
			position: relative;
		}
		
		.product_page .product-info .PF-image {
			width: 5em;
			height: 5em;
			margin: auto;
		}
		</style>
	</head>
<body>
<?php
if($product_id){?>
<div class="product_page" data-productid="<?=$product_id?>" data-storeid="<?=$store_id?>">
<div class="PF product-info">
  <div class="container">
    <div class="PF-image circle ripple <?=$product_avatar?>" style="background-image:url('<?=$product_avatar?>');" rgdd></div>
    <?php if($product_name){?><h1 data-product="name"><?=$product_name?></h1><?}?>
    <?php if($store_name){?><p data-product-store="name" opendd-href="?app=shopping&section=store&store=<?=$store_id?>"><?=$store_name?></p><?}?>
    <div class="PF PF-button" t-dd>Add to cart</div>
  </div>
</div>


<div class="PF-tabs">
			<div class="PF PF-tabbar shadow">
				<div class="container">
					<ul>
						<li class="ripple active" data-for="producttabs-about"><span t-dd>About</span></li>
						<li class="ripple" data-for="producttabs-media" data-taburl="./apps/shopping/pantallas/product/media.php?<?=$server_querystring?>"><span t-dd>Media</span></li>
						<li class="ripple" data-for="producttabs-discussion" data-taburl="./apps/shopping/pantallas/product/reviews.php?<?=$server_querystring?>"><span t-dd>Reviews</span></li>
						<?php if($product_usuario_id === $usuario_mismo_id){?><li class="ripple" data-for="producttabs-edit" data-taburl="./apps/shopping/pantallas/product/edit.php?<?=$server_querystring?>"><span t-dd>Edit</span></li><?}?>
					</ul>
					<div class="slider"></div>
				</div>
			</div>
			<div class="tabs">
				<div class="tab" data-name="producttabs-about">
					<?php include(__DIR__."/about.php"); ?>
				</div>
				<div class="tab" data-name="producttabs-media"></div>
				<div class="tab" data-name="producttabs-discussion"></div>
				<?php if($product_usuario_id === $usuario_mismo_id){?><div class="tab" data-name="producttabs-edit"></div><?}?>
			</div>
		</div>
</div>
<?}?>
</body></html>