<html>

<head>
	<style>
		.shopping_container {
			width: 100%;
		}
		
		.shopping_container .shopping_top {
			width: 100%;
			background-position: 50% 10px;
			background-repeat: no-repeat;
			margin-top: 5em;
			margin-bottom: 3.5em;
		}
		
		.shopping_container .shopping_top h1 {
			font-size: 2.8rem;
			padding: 0;
			font-weight: 500;
			text-align: center;
		}
		
		.shopping_container .shopping_top p {
			font-size: 20px;
			padding: 0;
			font-weight: 400;
			text-align: center;
		}
		
		.shopping_container .shopping_top .shopping_logo {
			width: fit-content;
			height: fit-content;
			margin: auto;
			margin-bottom: 1em;
			padding: .5em .8em;
			background-color: rgba(var(--PF-color-surface));
			border-radius: 28px;
			box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .26);
			box-sizing: border-box;
			display: flex;
		}
		
		.shopping_container .content {
			max-width: 60rem;
			width: 100%;
			margin: auto;
		}
		
		.shopping_container .content .PF-card {
			flex-basis: calc(100%/3 - 1rem);
		}
		
		.shopping_container .content .PF-grid.selector .PF-card .PF-image {
			padding-bottom: 70%;
		}
		
		.shopping_container .content .PF-grid .PF-card .info p {
			-webkit-line-clamp: initial;
		}

		.PF-grid.selector.departments .PF-card .info {
			padding-top: 40%;
		}
	</style>
</head>

<body>

	<div class="shopping_container">
		<?php
		if(!$section){
		$section = "departments";
		?>
		<div class="content">
			<div class="shopping_top">
				<div>
					<h1><span t-dd>Let's go shopping</span><?php if($usuario_mismo_nombre){?>, <?=$usuario_mismo_nombre?><?}?></h1>
				</div>
			</div>
			<div class="PF PF-grid selector departments" style="grid-template-columns: repeat(auto-fill, minmax(300px, 1fr)); ">
				<?php include(__DIR__.'/resources/departments.php'); ?>
			</div>
		</div>
			<?
			} else {
				include_once(__DIR__.'/pantallas/'.$section."/index.php");
				}?>
	</div>


</body>

</html>