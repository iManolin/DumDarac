<html><head>
	
<style>
.wrapper_mails {
  width: 66.66%;
  max-width: 800px;
  margin-left: auto;
  margin-right: auto;
  padding-top: 1rem;
}

.icon {
  height: 28px;
  width: 28px;
  border-radius: 14px;
  background: seagreen;
}

.message {
  background: #fff;
}

.message__summary {
    display: inline-flex;
    cursor: pointer;
    align-items: center;
    width: calc(100% - 1rem);
    padding: 0.5rem;
	  overflow:hidden;
	  text-overflow:ellipsis;
}
.message__summary .message__summary__icon {
  padding: 0.5rem;
}
.message__summary .message__summary__title {
	white-space:nowrap;
	overflow:hidden;
	  text-overflow:ellipsis;
	max-width:60%;
}
.message__summary .message__summary__body {
  padding-left: 16px;
  color: gray;
	white-space:nowrap;
	overflow:hidden;
	  text-overflow:ellipsis;
}
.message__summary:before {
  bottom: 0;
  box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
  content: '';
  display: block;
  left: 0;
  pointer-events: none;
  position: absolute;
  right: 0;
  top: 0;
}

.message__details {
  line-height: 1.5;
  width: 100%;
}
.message__details .message__details__header {
    display: flex;
    align-items: center;
    background: #fff;
    cursor: pointer;
    padding: 0;
    width: 100%;
}
.message__details .message__details__header:before {
  bottom: 0;
  box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
  content: '';
  display: block;
  left: 0;
  pointer-events: none;
  position: absolute;
  right: 0;
  top: 0;
}
.message__details .message__details__header .h1 {
  line-height: 1;
  margin: 0;
	padding:1rem;
}
.message__details .message__details__body {
  border-top: 1px solid #e5e5e5;
}
.message__details .message__details__body__icon {
  float: left;
  width: 40px;
  height: 40px;
  margin: 16px 20px;
}
.message__details .message__details__body__content {
  margin: 0 20px 16px 80px;
  padding: 1rem 16px 16px;
  text-align: justify;
}
	
	.message__details .message__details__body__content iframe {
		border:none;
		width:100%;
	}

/*******************************************************************
 * FIXED / ABSOLUTE STYLES
 ******************************************************************/
.message {
  position: relative;
}

.pusher {
  display: none;
}

.message--fixed .pusher,
.message--absolute .pusher {
  display: block;
}

.message--fixed .message__details__header {
  position: fixed;
  top: 64px;
}

.message--absolute .message__details__header {
  position: absolute;
  bottom: 0;
}

/*******************************************************************
 * ANIMATION STYLES
 ******************************************************************/
.message__details {
  display: none;
}

.grower {
  height: 0;
  background: #fff;
  will-change: height;
  transition: height 150ms;
}

.message {
  transition: margin 150ms;
}

.message--opening,
.message--open {
    margin-left: -1rem;
    margin-right: -1rem;
    margin-bottom: 1rem;
    margin-top: 1rem;
	  box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
	  overflow:hidden;
}

.message--opening .message__details {
  display: block;
  position: absolute;
  left: -50000px;
  width: 100%;
}
.message--opening .message__summary {
  animation: fade-out 150ms forwards;
}
.message--opening .message__summary:before {
  display: none;
}

.message--open .message__summary {
  display: none;
}
.message--open .grower {
  display: none;
}
.message--open .message__details {
  display: block;
  position: static;
  opacity: 0.0001;
  animation-name: fade-in;
  animation-duration: 150ms;
  animation-fill-mode: forwards;
}

.message--closing .message__summary {
  display: flex;
  opacity: 0;
  animation-name: fade-in;
  animation-duration: 150ms;
  animation-fill-mode: forwards;
}
.message--closing .message__summary:before {
  display: block;
  opacity: 0;
  animation-name: fade-in;
  animation-duration: 150ms;
  animation-fill-mode: forwards;
}
.message--closing .grower {
  animation: shrink 37.5ms forwards;
}

/*******************************************************************
 * KEYFRAMES
 ******************************************************************/
@keyframes fade-out {
  0%,33% {
    opacity: 0.9999;
  }
  100% {
    opacity: 0.0001;
  }
}
@keyframes fade-in {
  0%,33% {
    opacity: 0.0001;
  }
  100% {
    opacity: 0.9999;
  }
}
@keyframes shrink {
  to {
    height: 0;
  }
}
	
</style></head><body>
<div class="wrapper_mails" id="wrapper_mails">
	
	
	<?php include('./resources/apps/correo/emails.php'); ?>
	
	
  <div class="message">
    <div class="message__summary">
      <div class="message__summary__icon">
        <div class="icon"></div>
      </div>
      <div class="message__summary__title">The Title 1</div>
      <div class="message__summary__body">The body 1</div>
    </div>
    <div class="grower"></div>
    <div class="message__details">
      <div class="pusher"></div>
      <div class="message__details__header">
        <div class="h1">The Title 1</div>
      </div>
      <div class="message__details__body">
        <div class="message__details__body__icon">
          <div class="icon"></div>
        </div>
        <div class="message__details__body__content">
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
        </div>
      </div>
    </div>
  </div>
  <div class="message">
    <div class="message__summary">
      <div class="message__summary__icon">
        <div class="icon"></div>
      </div>
      <div class="message__summary__title">The Title 2</div>
      <div class="message__summary__body">The body 2</div>
    </div>
    <div class="grower"></div>
    <div class="message__details">
      <div class="pusher"></div>
      <div class="message__details__header">
        <div class="h1">The Title 2</div>
      </div>
      <div class="message__details__body">
        <div class="message__details__body__icon">
          <div class="icon"></div>
        </div>
        <div class="message__details__body__content">
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
        </div>
      </div>
    </div>
  </div>
  <div class="message">
    <div class="message__summary">
      <div class="message__summary__icon">
        <div class="icon"></div>
      </div>
      <div class="message__summary__title">The Title 3</div>
      <div class="message__summary__body">The body 3</div>
    </div>
    <div class="grower"></div>
    <div class="message__details">
      <div class="pusher"></div>
      <div class="message__details__header">
        <div class="h1">The Title 3</div>
      </div>
      <div class="message__details__body">
        <div class="message__details__body__icon">
          <div class="icon"></div>
        </div>
        <div class="message__details__body__content">
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
        </div>
      </div>
    </div>
  </div>
  <div class="message">
    <div class="message__summary">
      <div class="message__summary__icon">
        <div class="icon"></div>
      </div>
      <div class="message__summary__title">The Title 4</div>
      <div class="message__summary__body">The body 4</div>
    </div>
    <div class="grower"></div>
    <div class="message__details">
      <div class="pusher"></div>
      <div class="message__details__header">
        <div class="h1">The Title 4</div>
      </div>
      <div class="message__details__body">
        <div class="message__details__body__icon">
          <div class="icon"></div>
        </div>
        <div class="message__details__body__content">
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
        </div>
      </div>
    </div>
  </div>
  <div class="message">
    <div class="message__summary">
      <div class="message__summary__icon">
        <div class="icon"></div>
      </div>
      <div class="message__summary__title">The Title 5</div>
      <div class="message__summary__body">The body 5</div>
    </div>
    <div class="grower"></div>
    <div class="message__details">
      <div class="pusher"></div>
      <div class="message__details__header">
        <div class="h1">The Title 5</div>
      </div>
      <div class="message__details__body">
        <div class="message__details__body__icon">
          <div class="icon"></div>
        </div>
        <div class="message__details__body__content">
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
        </div>
      </div>
    </div>
  </div>
  <div class="message">
    <div class="message__summary">
      <div class="message__summary__icon">
        <div class="icon"></div>
      </div>
      <div class="message__summary__title">The Title 6</div>
      <div class="message__summary__body">The body 6</div>
    </div>
    <div class="grower"></div>
    <div class="message__details">
      <div class="pusher"></div>
      <div class="message__details__header">
        <div class="h1">The Title 6</div>
      </div>
      <div class="message__details__body">
        <div class="message__details__body__icon">
          <div class="icon"></div>
        </div>
        <div class="message__details__body__content">
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
        </div>
      </div>
    </div>
  </div>
  <div class="message">
    <div class="message__summary">
      <div class="message__summary__icon">
        <div class="icon"></div>
      </div>
      <div class="message__summary__title">The Title 7</div>
      <div class="message__summary__body">The body 7</div>
    </div>
    <div class="grower"></div>
    <div class="message__details">
      <div class="pusher"></div>
      <div class="message__details__header">
        <div class="h1">The Title 7</div>
      </div>
      <div class="message__details__body">
        <div class="message__details__body__icon">
          <div class="icon"></div>
        </div>
        <div class="message__details__body__content">
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
        </div>
      </div>
    </div>
  </div>
  <div class="message">
    <div class="message__summary">
      <div class="message__summary__icon">
        <div class="icon"></div>
      </div>
      <div class="message__summary__title">The Title 8</div>
      <div class="message__summary__body">The body 8</div>
    </div>
    <div class="grower"></div>
    <div class="message__details">
      <div class="pusher"></div>
      <div class="message__details__header">
        <div class="h1">The Title 8</div>
      </div>
      <div class="message__details__body">
        <div class="message__details__body__icon">
          <div class="icon"></div>
        </div>
        <div class="message__details__body__content">
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
        </div>
      </div>
    </div>
  </div>
  <div class="message">
    <div class="message__summary">
      <div class="message__summary__icon">
        <div class="icon"></div>
      </div>
      <div class="message__summary__title">The Title 9</div>
      <div class="message__summary__body">The body 9</div>
    </div>
    <div class="grower"></div>
    <div class="message__details">
      <div class="pusher"></div>
      <div class="message__details__header">
        <div class="h1">The Title 9</div>
      </div>
      <div class="message__details__body">
        <div class="message__details__body__icon">
          <div class="icon"></div>
        </div>
        <div class="message__details__body__content">
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
        </div>
      </div>
    </div>
  </div>
  <div class="message">
    <div class="message__summary">
      <div class="message__summary__icon">
        <div class="icon"></div>
      </div>
      <div class="message__summary__title">The Title 10</div>
      <div class="message__summary__body">The body 10</div>
    </div>
    <div class="grower"></div>
    <div class="message__details">
      <div class="pusher"></div>
      <div class="message__details__header">
        <div class="h1">The Title 10</div>
      </div>
      <div class="message__details__body">
        <div class="message__details__body__icon">
          <div class="icon"></div>
        </div>
        <div class="message__details__body__content">
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="//production-assets.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.3.0/lodash.js"></script>
<script>'use strict';

/**
 * Some bare-bones Javascript
 *
 * Main goals here are to:
 * - Add top-level classes to each message to indicate open/closed and
 *   animations states.
 * - Add fixed/absolute position when scrolling an open message
 *
 * I like to use functional techniques where I can so you'll see some of that
 * below.
 */
(function () {

    // Throttle from lodash will limit our scroll handler calls, which is
    // generally good practice.
    //
    // I like to call lodash's `flowRight` `compose` locally because it's more
    // familiar to me.
    var _ref = _;
    var throttle = _ref.throttle;
    var compose = _ref.flowRight;

    var messages = Array.from(document.querySelectorAll('.message'));

    // We'll only have one scroll listener at a time so we store it globally
    var listener = null;

    /**
     * Side effecty stuff
     */

    function cleanMessage(message) {
        message.classList.remove('message--fixed');
        message.classList.remove('message--absolute');

        return message;
    }

    function bindScrollEvent(message) {
        listener = handleScroll.bind(null, message);

        document.addEventListener('scroll', listener);

        return message;
    }

    function unBindScrollEvent(message) {
        if (listener) {
            document.removeEventListener('scroll', listener);
        }

        return message;
    }

    // Small helper for listening for an animation/transition end event.
    function listenForEndEvent(el, endEventType, cb) {

        var onEnd = function onEnd() {
            cb();
            el.removeEventListener(endEventType, onEnd);
        };

        el.addEventListener(endEventType, onEnd);
    }

    /**
     * Transition a message from closed to open.  There can only be one open
     * message at a time.  So we first close any existing open messages and once
     * those are closed, we proceed with opening the clicked message.
     */
    function transitionToOpen(message) {
        var details = message.querySelector('.message__details');
        var grower = message.querySelector('.grower');
        var startingRectTop = message.getBoundingClientRect().top;

        // Is there a message already open?
        var existingOpenMessage = document.querySelector('.message--open');

        // Internal function for actually opening the message
        function open() {

            // We now try to save the scroll position.
            // We've saved the top position of the message we're trying to open
            // before closing or opening any other message.  After closing a
            // different message, this message may now be above the viewport, so
            // we move the scroll position back by that amount.  We also move
            // back by the amount of the starting top position.
            var newRectTop = message.getBoundingClientRect().top;

            if (newRectTop < 0) {
                var newScroll = document.body.scrollTop - Math.abs(newRectTop) - startingRectTop;

                document.body.scrollTop = newScroll;
            }

            // Start the opening transition by adding this class here
            message.classList.add('message--opening');

            // The grower should be the same height as the incoming details,
            // which are now displayed, but offscreen so we can still get the
            // height
            grower.style.height = details.offsetHeight + 'px';

            // When the grower has stopped height-transitioning, we remove the
            // animation class and add the open class
            listenForEndEvent(grower, 'transitionend', function () {
                message.classList.remove('message--opening');
                message.classList.add('message--open');
            });
        }

        // If there's an open message, first close it then proceed with opening
        // this message.  Closing first seems to help with more accurate scroll
        // positions
        if (existingOpenMessage) {
            transitionToClosed(existingOpenMessage, open);
        } else {
            open();
        }
    }

    /**
     * Transition a message from open to closed
     */
    function transitionToClosed(message, cb) {
        var grower = message.querySelector('.grower');

        // Remove any open class and add the closing animation class
        message.classList.remove('message--open');
        message.classList.add('message--closing');

        // We're animating the grower out now, so we listen to the `animationend`
        // event.  After that, remove any style artifacts from other animations
        listenForEndEvent(grower, 'animationend', function () {
            message.classList.remove('message--closing');

            grower.removeAttribute('style');

            if (typeof cb === 'function') {
                cb();
            }
        });
    }

    /**
     * Throttled scroll handler
     *
     * When the handler is called with the message on a scroll event, we grab
     * the coordinates of the message via `getBoundingClientRect`.  The absolute
     * class should be applied when the header has reached the point where its
     * bottom position lines up with the bottom of the message container.  The
     * fixed class should be applied when the header has been scrolled past the
     * viewport but not yet reached the absolute trigger point.
     */
    var handleScroll = throttle(function (message) {
        var header = message.querySelector('.message__details__header');
        var pusher = message.querySelector('.pusher');

        var _message$getBoundingC = message.getBoundingClientRect();

        var top = _message$getBoundingC.top;
        var bottom = _message$getBoundingC.bottom;
        var width = _message$getBoundingC.width;

        var height = header.offsetHeight;

        var shouldBeSticky = top < 0 && bottom - height > 0;
        var shouldBeAbsolute = bottom - height <= 0;

        pusher.style.height = height + 'px';

        if (shouldBeSticky) {
            message.classList.remove('message--absolute');
            message.classList.add('message--fixed');
            header.style.width = width + 'px';
        } else if (shouldBeAbsolute) {
            message.classList.remove('message--fixed');
            message.classList.add('message--absolute');
        } else {
            message.classList.remove('message--fixed');
            message.classList.remove('message-absolute');
        }
    }, 50);

    function handleMessage(message) {
        var summary = message.querySelector('.message__summary');
        var header = message.querySelector('.message__details__header');

        var openHandler = compose(transitionToOpen, bindScrollEvent, cleanMessage);

        var closeHandler = compose(transitionToClosed, unBindScrollEvent, cleanMessage);

        summary.addEventListener('click', openHandler.bind(null, message));
        header.addEventListener('click', closeHandler.bind(null, message));
    }

    messages.forEach(handleMessage);
})();
</script>
</body></html>