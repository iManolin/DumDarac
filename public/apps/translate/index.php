<html>

<head>
  <style>
    .traductor_container {
      width: 100%;
      margin: auto;
    }
    
    .traductor_container .translate_translated {
      width: 100%;
      min-height: 108px;
      display: flex;
    }
    
    .traductor_container .translate_translated .box {
      flex: 1 auto;
      width: 100%;
      min-height: 12rem;
      font-size: 2rem;
      display: flex;
      flex-direction: column;
      user-select: text;
      background-color: rgba(var(--PF-color-surface), .5);
    }

    .traductor_container .translate_zone .PF-tabbar {
      background-color: rgb(var(--PF-color-surface));
    }
    
    .traductor_container .translate_translated .box:first-child {
      border-right: 1px solid rgba(0, 0, 0, 0.12);
    }
    
    .traductor_container .translate_translated .box:last-child {
      color: rgba(var(--PF-color-primary));
      background: rgba(var(--PF-color-primary), 0.22);
    }
    
    .traductor_container .translate_translated .box .textarea {
      padding: 1rem;
      flex: 1;
    }
    
    .translate_zone {
      margin: auto;
      max-width: 80rem;
      border-radius: 1em;
      overflow: hidden;
      margin-top: .5em;
      position: relative;
    }
    
    .translate_zone .PF-buttons {
      padding: .5em;
      position: relative;
      z-index: 4;
      background: rgb(var(--PF-color-surface));
    }
    
    @media (max-width: 75em) {
      .traductor_container .translate_translated {
        flex-direction: column;
      }
      .traductor_container .translate_translated .box:first-child {
        border-right: 0;
        border-bottom: 1px solid rgba(0, 0, 0, 0.12);
      }
    }
  </style>
</head>

<body>

  <div class="traductor_container">
    <div class="PF shadow translate_zone">
      <div class="PF PF-buttons full align-left">
        <button class="PF PF-button semi ripple border"><i class="material-icons">camera_alt</i>
          <p t-dd>Camera</p>
        </button>
        <button class="PF PF-button semi ripple border"><i class="material-icons">text_fields</i>
          <p t-dd>Text</p>
        </button>
        <button class="PF PF-button semi ripple border"><i class="material-icons">attachment</i>
          <p t-dd>Documents</p>
        </button>
        <button class="PF PF-button semi ripple border" opendd-href="?p=contributors&contribute=translate"><i class="material-icons">record_voice_over</i>
          <p t-dd>Contribute</p>
        </button>
      </div>
      <div class="PF PF-tabbar full shadow">
        <div class="container">
          <ul>
            <li class="ripple active"><span>Detect language</span></li>
            <li class="ripple"><span>English</span></li>
            <li class="ripple"><span>Spanish</span></li>
            <li class="ripple"><span>French</span></li>
            <div class="PF PF-icon ripple"><i class="material-icons">expand_more</i></div>
          </ul>
          <div class="slider"></div>
        </div>
        <div class="PF PF-icon ripple"><i class="material-icons">swap_horiz</i></div>
        <div class="container">
          <ul>
            <li class="ripple active"><span>English</span></li>
            <li class="ripple"><span>Spanish</span></li>
            <li class="ripple"><span>Arabic</span></li>
            <div class="PF PF-icon ripple"><i class="material-icons">expand_more</i></div>
          </ul>
          <div class="slider"></div>
        </div>
      </div>

      <div class="translate_translated">
        <div class="box">
          <div contenteditable="true" id="atraducir" class="textarea"></div>
        </div>
        <div class="box">
          <div class="textarea" id="traducidotexto"></div>
        </div>
      </div>
    </div>

    <input id="language1" name="language1" type="text" value="es-ES" style="display:none;" />
    <input id="language2" name="language2" type="text" value="en-US" style="display:none;" />
    <script>
      $('#atraducir').on("keyup", function() {
        traducirtexto();
      });
      $('#language1').change(function() {
        traducirtexto();
      });
      $('#language2').change(function() {
        traducirtexto();
      });
      $('#traducidotexto').on("keyup", function() {
        edittext();
      });

      var traducirtextotimeout;

      function traducirtexto() {
        var language1 = $('#language1').val();
        var language2 = $('#language2').val();
        var texttranslate = $('#atraducir').text();
        var texttranslatetraducido = $('#traducidotexto').text();
        clearTimeout(traducirtextotimeout);
        traducirtextotimeout = setTimeout(function() {
          if (texttranslate != '') {
            $("#traducidotexto").addClass("escribiendo");

            $.ajax({
              url: "./apps/translate/resources/translate.php",
              type: "POST",
              data: {
                from: language1,
                to: language2,
                text: texttranslate
              },
              success: function(data) {
                var data = JSON.parse(data);
                if (data.translation) {
                  $("#traducidotexto").empty().text(data.translation).show();
                } else {
                  $("#traducidotexto").empty().text(data.originaltext).show();
                }
                setTimeout(function() {
                  $("#traducidotexto").removeClass("escribiendo");
                }, 1000);
              },
              error: function() {
                estetranslate.attr('t-dd', 'no');
              }
            });

          }
          return false;
        }, 1000);
      }

      function edittext() {
        var editartexto_language1 = $('#language1').val();
        var editartexto_language2 = $('#language2').val();
        var editartexto_texttranslate = $('#atraducir').text();
        var editartexto_texttranslatetraducido = $('#traducidotexto').text();
        var editartexto_dataString = 'textedit=' + editartexto_texttranslatetraducido;

        if (editartexto_texttranslatetraducido != '') {
          $.ajax({
            type: "POST",
            url: "./apps/traductor/resources/editar.php?from=" + editartexto_language1 + "&to=" + editartexto_language2 + "&text=" + editartexto_texttranslate,
            data: editartexto_dataString,
            cache: false,
            success: function(htmledit) {
              $('#traducidotextoedit').empty().html(htmledit);
            }
          });
        }
        return false;
      }
    </script>
  </div>

</body>

</html>