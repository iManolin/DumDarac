<html><head>
<style>
  
  .headerbajo {
        min-height: calc(100% - 15rem);
        display:block;
  }
  
  .wrapper .contenedor {
    padding-top:2rem;
  }

  .contenedor_eventos {
    background: white;
    width: 100%;
    max-width: 70rem;
    margin: 0 auto;
    border-radius: 2px;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
    overflow: hidden;
  }
  
  .contenedor_eventos .nav {
    width: 100%;
    background: <?=$color_app_dd_2?>;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
  }
  
  .contenedor_eventos .nav h1 {
    color:white;
    padding:1rem;
    font-size:1.5rem;
    width: calc(100% - 1rem);
  }
  
  .contenedor_eventos .nav .categorias {
    padding: 0.5rem;
    display: inline-flex;
    align-items: center;
    width: calc(100% - 1rem);
    background: rgba(0, 0, 0, 0.15);
    overflow-x: auto;
  }
	
	.contenedor_eventos .nav .categorias::-webkit-scrollbar {
    display:none;
}
  
  .contenedor_eventos .nav .categorias .categoria {
    padding:0.5rem;
  }
  
  .contenedor_eventos .nav .categorias .categoria .circulo {
    color: <?=$color_app_dd_2?>;
    border-radius: 50px;
    background: white;
    padding: 0.5rem;
    width: 3rem;
    display: inline-block;
    text-align: center;
    cursor:pointer;
  }
  
  .contenedor_eventos .eventos {
    padding:0.5rem;
    width:calc(100% - 1rem);
  }
  
  .contenedor_eventos .eventos span.titulo-container {
    padding:0.5rem;
    display:block;
    width:calc(100% - 1rem);
  }
  
  .contenedor_eventos .eventos .eventos_container {
    padding: 0.5rem;
    max-width: calc(100% - 1rem);
    display: block;
    overflow-x: auto;
    text-align: left;
    padding-top: 0;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento {
    width: calc(100% - 1rem);
    max-width: 20rem;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    background-color: #0D47A1;
    border-radius: 2px;
    overflow: hidden;
    color: white;
    background-image: url('https://static1.squarespace.com/static/52f3ba1fe4b0932b3f775421/t/530e5fa1e4b07a55c9b3d6ad/1393450915317/Beer+Die+Events.jpg?format=1500w');
    background-position: center;
    background-size: cover;
    margin: 0.5rem;
    display: inline-block;
    text-align: left;
	  transition:0.5s;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento:nth-child(3n+2)
{
    max-width: 32%;
}
	
	.contenedor_eventos .eventos .eventos_container .evento:nth-child(2n+1)
{
    max-width: 64%;
}
  
  .contenedor_eventos .eventos .eventos_container .evento:first-child
{
    max-width: 64%;
}
	
	.contenedor_eventos .eventos .eventos_container .evento.abierto {
		max-width:100%;
	}
  
  .contenedor_eventos .eventos .eventos_container .evento .price {
    padding: 0.5rem 0.7rem;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    background: white;
    border-radius: 2px;
    margin: 0.5rem;
    display: inline-block;
    color: black;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .imagen {
    padding-top: 2rem;
    width: 100%;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .imagen .info {
    padding: 1rem;
    display: block;
    width: calc(100% - 2rem);
    background: -webkit-linear-gradient(top, rgba(255, 255, 255, 0), #000);
    padding-top: 3rem;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .imagen .info span.titulo {
    font-size: 1.2rem;
    display: block;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .imagen .info span.sub_titulo {
    font-size: 1rem;
    display: block;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .informacion {
    padding: 0.5rem;
    background: <?=$color_app_dd?>;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .informacion span.titulo {
    display: block;
    padding: 0.5rem;
    font-size: 1.2rem;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .informacion span.fecha {
    display: block;
    padding: 0 0.5rem;
    font-size: 0.8rem;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .informacion span.ubicacion {
    display: block;
    padding: 0.5rem;
    font-size: 0.8rem;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .botones {
    background: <?=$color_app_dd?>;
    padding: 0.2rem;
    padding-top: 0;
    display: inline-flex;
    align-items: center;
    width: 100%;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .botones .boton {
    padding: 0.5rem 0.7rem;
    margin: 0.4rem;
    display: inline-block;
    background: rgba(0, 0, 0, 0.48);
    border-radius: 50px;
    display:inline-flex;
    align-items:center;
    cursor:pointer;
    cursor:pointer;
  }
  
  .contenedor_eventos .eventos .eventos_container .evento .botones .boton i {
    margin-right:0.5rem;
  }
  
</style></head><body>
<div class="contenedor_eventos">
  <div class="nav">
    <h1>Cerca de <span><?=$ciudadactual?></span></h1>
    <div class="categorias" >
      <a class="categoria" ><div class="circulo" ><i class="material-icons">&#xE53F;</i> </div></a>
      <a class="categoria" ><div class="circulo" ><i class="material-icons">&#xE566;</i> </div></a>
      <a class="categoria" ><div class="circulo" ><i class="material-icons">&#xE54D;</i> </div></a>
      <a class="categoria" ><div class="circulo" ><i class="material-icons">&#xE54C;</i> </div></a>
      <a class="categoria" ><div class="circulo" ><i class="material-icons">&#xEB44;</i> </div></a>
      <a class="categoria" ><div class="circulo" ><i class="material-icons">&#xEB43;</i> </div></a>
      <a class="categoria" ><div class="circulo" ><i class="material-icons">&#xEB3F;</i> </div></a>
      <a class="categoria" ><div class="circulo" ><i class="material-icons">&#xEB40;</i> </div></a>
      <a class="categoria" ><div class="circulo" ><i class="material-icons">&#xEB47;</i> </div></a>
      <a class="categoria" ><div class="circulo" ><i class="material-icons">&#xE56C;</i> </div></a>
      <a class="categoria" ><div class="circulo" ><i class="material-icons">&#xE326;</i> </div></a>
    </div>
  </div>
  
  <div class="eventos">
    <span class="titulo-container" >Todos</span>
    <div class="eventos_container">
      
  <?php
$blog = mysqli_query($con,"SELECT * FROM blogs WHERE usuario_id='$usuario_id_mismo'");
while($row_blog = mysqli_fetch_array($blog))
  { 
	$nombre_blog = $row_blog["nombre"];
  $descripcion_blog = $row_blog["descripcion"];
  $categoria_blog = $row_blog["categoria"];
  $eslogan_blog = $row_blog["eslogan"];
  $etiquetas_blog = $row_blog["etiquetas"];
  $color_blog = $row_blog["color"];
  $usuario_blog = $row_blog["usuario_id"];
  if($color_blog == ''){$color_blog = '#607D8B';}
  
  ?>
      <div class="evento" onclick="javascript: $(this).toggleClass('abierto'); setTimeout(function(){ scrollToElemento('#evento<?=$id_evento?>'); }, 500);" id="evento<?=$id_evento?>">
        <span class="price" >Free</span>
        <div class="imagen">
          <div class="info">
            <span class="sub_titulo"><?=$tipo_evento?></span>
            <span class="titulo"><?=$nombre_blog?></span>
          </div>
        </div>
        <div class="informacion">
          <span class="titulo"><?=$eslogan_blog?></span>
          <span class="fecha">16 de Enero de 2015</span>
          <span class="ubicacion"><?=$sitio_evento?></span>
        </div>
        <div class="botones">
          <span class="boton">Asistir</span>
          <span class="boton">Compartir</span>
        </div>
      </div>
  
  <?}?>
      
    </div>
    </div>

</div>
</body></html>