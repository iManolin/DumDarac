<?php $blog_id = $_GET['b_id']; ?>
<html>

<head>
  <style>
    .blogs_container {
      width: 100%;
      margin: auto;
      max-width: 50rem;
    }

    .blogs_container .secciones .seccion {
      display: none;
      width: 100%;
    }

    .blogs_container .secciones .seccion.visible {
      display: block;
    }

    .blogs_container .secciones .seccion h1 {
      width: 100%;
      font-size: 1.5rem;
      padding: 1rem;
      padding-bottom: 0;
      margin: 0;
    }

    .blogs_container .secciones .seccion .head_seccion {
    width: 100%;
    padding: 1rem;
    padding-bottom: 0.5rem;
    display: flex;
    align-items: center;
    }

    .blogs_container .secciones .seccion .head_seccion h1 {
font-size: 1rem;
    width: 100%;
    padding: 0;
    }

    .blogs_container .secciones .seccion .head_seccion .boton {
      padding: 0.5rem;
      background: #222;
      border-radius: 2px;
    }

    .blogs_container .secciones .seccion .contenido {
      width: calc(100% - 1rem);
      min-height: 5rem;
      box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.4);
      background: white;
      margin: 0.5rem;
      margin-top: 0;
      border-radius: 1em;
      display: inline-block;
    }

    .blogs_container .secciones .seccion .contenido .lista_blogs {
      width: 100%;
    }

    .blogs_container .secciones .seccion .contenido .lista_blogs li {
      width: 100%;
      padding: 0.5rem;
      border-bottom: 1px solid white;
      display: inline-flex;
      align-items: center;
      transition: 0.5s;
      cursor: pointer;
    }

    .blogs_container .secciones .seccion .contenido .lista_blogs li:hover {
    background-color: #f6f7f9;
    }

    .blogs_container .secciones .seccion .contenido .lista_blogs li p {
      width: 100%;
      padding: 0.5rem;
    }

    .blogs_container .secciones .seccion .contenido .lista_blogs li a {
      padding: 0.5rem;
      display: none;
    }

    .blogs_container .secciones .seccion .contenido .lista_blogs li:hover a {
      display: block;
    }

    .blogs_container .secciones .seccion .contenido .lista_blogs li a i {
      font-size: 1.3rem;
    }
  </style>
</head>

<body>

  <div class="blogs_container">

    <div class="secciones">

      <?php 
				
				if($blog_id == ''){?>

      <div class="seccion blogs visible">

        <div class="head_seccion">
          <h1>Tus blogs</h1>
        </div>
        <div class="contenido">
          <ul class="lista_blogs">
            <?php
$blog = mysqli_query($con,"SELECT * FROM blogs WHERE usuario_id='$usuario_id_mismo'");
while($row_blog = mysqli_fetch_array($blog))
  { 
	$id_blog = $row_blog["id"];
	$nombre_blog = $row_blog["nombre"];
  $descripcion_blog = $row_blog["descripcion"];
  $categoria_blog = $row_blog["categoria"];
  $eslogan_blog = $row_blog["eslogan"];
  $etiquetas_blog = $row_blog["etiquetas"];
  $color_blog = $row_blog["color"];
  $usuario_blog = $row_blog["usuario_id"];
  if($color_blog == ''){$color_blog = '#607D8B';}
  ?>
              <li>
                <p>
                  <?=$nombre_blog?>
                </p>
                <a href="?app=<?=$app?>&b_id=<?=$id_blog?>"><i class="material-icons">&#xE254;</i></a>
                <a href="#"><i class="material-icons">&#xE250;</i></a>
                <a href="#"><i class="material-icons">&#xE89E;</i></a>
                <a href="#"><i class="material-icons">&#xE8B8;</i></a>
              </li>
              <?}?>

          </ul>
        </div>

      </div>

      <?} else {
				if($pantalla_blog == ''){
					include('pantallas/dashboard.php');
				} else {
					
				}
				}?>

    </div>

  </div>

</body>

</html>