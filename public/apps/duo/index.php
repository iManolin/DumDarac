<html>

<head>
  <style>
    .messages-container {
      display: flex;
      flex: 1 auto;
    }

    .messages-list {
      width: 350px;
      flex: 1 auto;
      max-width: 25rem;
      display: -webkit-box;
      display: -moz-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      -webkit-flex-direction: column;
      flex-direction: column;
      overflow: hidden;
    }

    .messages-duo {
      flex: 1 auto;
      display: flex;
      flex-direction:column;
      background-color:rgb(var(--PF-color-primary));
      margin: .5em;
      border-radius: 1em;
    }

    .messages-list-header {
      display: flex;
      align-items: center;
      -webkit-justify-content: space-between;
      justify-content: space-between;
      height: 55px;
      margin: 6px 0 0 16px;
      padding: 0 0 0 8px;
    }

    .messages-list-content {
display: flex;
    flex-direction: column;
    overflow: auto;
    flex: 1;
    }

    .messages-list-new .PF-icon {
      -webkit-box-shadow: 0 0 2px rgba(0, 0, 0, 0.12), 0 2px 2px rgba(0, 0, 0, 0.24);
      box-shadow: 0 0 2px rgba(0, 0, 0, 0.12), 0 2px 2px rgba(0, 0, 0, 0.24);
      background-color: var(--PF-color-bg-second-default);
    }

    .messages-no-duo {
      -webkit-align-items: center;
      align-items: center;
      display: -webkit-box;
      display: -moz-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      -webkit-flex-direction: column;
      flex-direction: column;
      -webkit-box-flex: 1;
      -webkit-flex: 1;
      flex: 1 auto;
      -webkit-justify-content: center;
      justify-content: center;
      margin: 0 24px;
      overflow: auto;
    }

    .messages-no-duo img {
      height: 256px;
      width: 256px;
    }

    .messages-no-duo h3 {
      color: rgba(0, 0, 0, .87);
      margin: 32px auto 8px;
    }

    .messages-no-duo p {
      color: rgba(0, 0, 0, .74);
      max-width: 420px;
      text-align: center;
    }

    @media screen and (max-width: 560px) {
      .messages-list {
        max-width: 100%;
        width: 100%;
        border-right: 0;
      }
      .messages-duo {
        width: 100%;
        display: none;
        border-right: 0;
        height: 100%;
        left: 0;
        top: 0;
        position: absolute;
      }
    }
    
    
    
    
    .messages-container .PF-textarea {
      flex:1;
      margin-bottom:0;
    }
    
    
    
    /*CONVERSATION*/
    
    .messages-duo .conversation {
    flex: 1 auto;
    width: 100%;
    padding: 0.5em;
    margin-bottom: 0;
    height: fit-content;
    align-self: flex-end;
    }
    
    .clearfix:after, .messages-group:after {
  content: "";
  display: table;
  clear: both;
  height: 0;
  visibility: hidden;
}

.messages-group {
  margin-bottom: 1em;
}
.messages--received .message {
  float: left;
  background-color: var(--PF-color-original-default);
  color:white;
  border-bottom-left-radius: 5px;
  border-top-left-radius: 5px;
}
.messages--received .message:first-child {
  border-top-left-radius: 1.5em;
}
.messages--received .message:last-child {
  border-bottom-left-radius: 1.5em;
}
.messages--sent .message {
    float: right;
    background: var(--PF-color-bg-first-default);
    color: var(--PF-color-original-default);
    border-bottom-right-radius: 5px;
    border-top-right-radius: 5px;
}
.messages--sent .message:first-child {
  border-top-right-radius: 1.5em;
}
.messages--sent .message:last-child {
  border-bottom-right-radius: 1.5em;
}

.message {
  display: inline-block;
  margin-bottom: 2px;
  clear: both;
  padding: 7px 13px;
  font-size: 14px;
  border-radius: 1.5em;
  line-height: 1.4;
}
.message--thumb {
  background-color: transparent !important;
  padding: 0;
  margin-top: 5px;
  margin-bottom: 10px;
  width: 20px;
  height: 20px;
  border-radius: 0px !important;
}

  </style>
</head>

<body>
  <div class="messages-container">
    <div class="messages-list">
      <div class="PF PF-toolbar">
        <h1>Duo</h1>
      </div>
      <div class="messages-list-content">
        <ul class="PF PF-list">
          <li class="messages-list-new" onclick="$('#conversations').load('./apps/duo/resources/contacts.php');">
            <div class="PF PF-icon ripple"><i class="material-icons">add</i></div>
            <div class="data">
              <h1>New Conversation</h1>
            </div>
          </li>
        </ul>
        <div class="messages-list-conversations" id="conversations">
          <ul class="PF PF-list">
            <li class="ripple">
              <div class="image circle"></div>
              <div class="data">
                <h1>Jake Leichtling</h1>
                <p>See you soon.</p>
              </div>
              <div class="data"><span class="meta">Wed</span></div>
            </li>
            <li class="ripple">
              <div class="image circle"></div>
              <div class="data">
                <h1>Katie Novak</h1>
                <p>No Problem 😀</p>
              </div>
              <div class="data"><span class="meta">Wed</span></div>
            </li>
            <li class="ripple">
              <div class="image circle"></div>
              <div class="data">
                <h1>Jake Leichtling</h1>
                <p>See you soon.</p>
              </div>
              <div class="data"><span class="meta">Wed</span></div>
            </li>
            <li class="ripple">
              <div class="image circle"></div>
              <div class="data">
                <h1>Jake Leichtling</h1>
                <p>See you soon.</p>
              </div>
              <div class="data"><span class="meta">Wed</span></div>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="messages-duo" id="conversation"></div>
  </div>
  <script>
    
    $('#conversations').load('./apps/duo/resources/conversations.php');
    
    $('#conversation').load('./apps/duo/pantallas/call.php');
  </script>
</body>

</html>