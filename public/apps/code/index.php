<html><head>
<style>
  
  body {
    background-color:<?=$color_app_dd?>;
  }
  
  .code_contenedor {
    width: 100%;
    max-width: 70rem;
    margin: auto;
    column-count: auto;
    column-width: 20rem;
    column-gap: 0;
    position:relative;
  }

.code_contenedor .pen {
    position: relative;
    max-width: 380px;
    width: calc(100% - 2rem);
    padding: 0.5rem;
    color: #999;
    background-color: #2f2f31;
    border-radius: 0.125em;
    -webkit-box-shadow: 0 0.25em 0.25em -0.125em rgba(0,0,0,0.5);
    box-shadow: 0 0.25em 0.25em -0.125em rgba(0,0,0,0.5);
    margin: 0.5rem;
    display:inline-block;
}

.code_contenedor .add-to-collection {
    position: absolute;
    top: 0.7rem;
    right: 0.5rem;
    overflow: hidden;
    color: #fff;
    font-size: 1.375em;
    line-height: 1.375em;
    text-align: right;
    text-decoration: none;
    opacity: 0;
    -webkit-transition: 0.2s;
    transition: 0.2s;
    z-index: 9;
    display: inline-flex;
    align-items: center;
    padding: 0.5rem;
    padding-right: 1rem;
    padding-left: 1rem;
}

.code_contenedor .pen:hover .add-to-collection { background-color: rgba(0,0,0,0.5);
    border-radius: 1em; opacity: 1; }

.code_contenedor .add-to-collection:hover {
  background-color: rgba(0,0,0,1);
}

.code_contenedor .add-to-collection::before {
    content: 'Add to collection';
    right: 1em;
    padding: 0;
    font-size: 0.625em;
    white-space: nowrap;
    display:none;
    -webkit-transition: 0.2s;
    transition: 0.2s;
    z-index: 9;
    margin-right: 0.5rem;
}

.code_contenedor .add-to-collection:hover::before { display: block; }

.code_contenedor .pen-frame {
  display: block;
  width: 100%;
  height: 0;
  padding-bottom: 56.25%;
  position:relative;
}
  
.code_contenedor .pen-frame iframe {
      width: 201%;
    height: 201%;
    border: 0;
    position: absolute;
    top: 0;
    left: 0;
    background: white;
    -webkit-transform: scale(0.5);
    transform: scale(0.5);
    -webkit-transform-origin: top left;
    transform-origin: top left;
  z-index:0;
  transition:0.5s;
  opacity:0.9;
  }
  
  .code_contenedor .pen:hover .pen-frame iframe {
    opacity:0.3;
  }

.code_contenedor .meta {
  padding: 0.5em 0.625em 0.25em;
  position:relative;
  
}

.code_contenedor .meta h3 {
  margin: 0.25em 0 0.75em;
  font-weight: 900;
  font-size: 0.9375em;
}

.code_contenedor .meta a {
  color: inherit;
  text-decoration: none;
  -webkit-transition: 0.2s;
  transition: 0.2s;
}

.code_contenedor .meta a:hover {
  color: #ccc;
}

.code_contenedor .meta .user {
  font-size: 0.8125em;
}

.code_contenedor .meta .user img {
  display: inline-block;
  width: 1.4375em;
  height: 1.4375em;
  margin-right: 0.1875em;
  vertical-align: middle;
}

.code_contenedor .meta .badge {
  margin-left: 0.2308em;
  font-size: 0.7692em;
}

.code_contenedor .meta .stats {
  position: absolute;
  right: 0.3076em;
  bottom: 1em;
  font-size: 0.8125em;
}

.code_contenedor .meta .stats a {
  padding: 0 0.3076em;
}

.code_contenedor .badge {
  padding: 0.0625em 0.3125em;
  color: #000;
  font-weight: 900;
  text-transform: uppercase;
  border-radius: 0.3125em;
}

.code_contenedor .badge-pro { background-color: #fcd000; }


/* CSS image preloading - just for demo */
body::after {
  position: absolute; width: 0; height: 0; overflow: hidden; z-index: -1;
  content: url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/38273/submit_button.gif);
}</style></head><body>
  
  <?php 
  $id_code_html = $_GET['id'];
  if($id_code_html != ''){
  include('pantallas/editor.php');
  } else {?>

<div class="code_contenedor" >
  
  <?php
include('../../../acceso_db.php');
		$code_html_list = mysqli_query($con,"SELECT * FROM code_html ORDER BY id DESC");
while($row_code_html_list = mysqli_fetch_array($code_html_list))
  { 
	$id_code_html = $row_code_html_list["id"];
  $titulo_code_html = $row_code_html_list["titulo"];
	$html_code_html = $row_code_html_list["html"];
  $css_code_html = $row_code_html_list["css"];
  $js_code_html = $row_code_html_list["js"];?>
  
<div class="pen">
  <a href="#0" class="add-to-collection">+</a>
  
  <a href="#0" class="pen-frame">
    <iframe src="<?=$httpshttp?>://dumdarac.com/apps/<?=$app?>/pantallas/html.php?id=<?=$id_code_html?>" id="codeiframeeditor" frameborder="0"></iframe>
  </a>
  
  <div class="meta">
    <h3><a href="?app=<?=$app?>&id=<?=$id_code_html?>"><?=$titulo_code_html?></a></h3>
    
    <a href="#0" class="user">
      <img src="//s3-us-west-2.amazonaws.com/s.cdpn.io/38273/codepen-user-avatar-80x80.png" alt="">
      Guy Dostuff
      <span class="badge badge-pro">Pro</span>
    </a>
    
    <div class="stats">
      <a href="?app=<?=$app?>&id=<?=$id_code_html?>" class="views">
        1364
        <svg class="icon-eye" width="17px" height="12px">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#eye"></use>
        </svg>
      </a>
      
      <a href="?app=<?=$app?>&id=<?=$id_code_html?>" class="comments">
        2
        <svg class="icon-comment" width="12px" height="12px">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#comment"></use>
        </svg>
      </a>
      
      <a href="?app=<?=$app?>&id=<?=$id_code_html?>" class="loves">
        93
        <svg class="icon-heart" width="12px" height="12px">
          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#heart"></use>
        </svg>
      </a>
    </div>
  </div>
</div>
  
  <?}?>
  
  </div>
  
  <?}?>

</body></html>