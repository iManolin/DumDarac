<?php
include('../../../acceso_db.php');
		$code_html = mysqli_query($con,"SELECT * FROM code_html WHERE id='$id_code_html'");
while($row_code_html = mysqli_fetch_array($code_html))
  {
	$id_code_html = $row_code_html["id"];
  $titulo_code_html = $row_code_html["titulo"];
	$html_code_html = $row_code_html["html"];
  $css_code_html = $row_code_html["css"];
  $js_code_html = $row_code_html["js"];
	
	$html_code_html = urlencode($html_code_html);
	$html_code_html = htmlentities(urldecode($html_code_html));
?>

<html><head>
<style>
  
  .header {
    background-color:black;
  }

.code_editor_code {
  display: grid;
  height: calc(100vh - 64px);
  grid-template-rows: auto 1fr 1fr auto;
  color: white;
  overflow: auto;
}

.code_editor_code > * {
  border: 1px solid black;
}

.code {
  display: grid;
 /* display: -webkit-box;
  display: -ms-flexbox;
  display: flex;*/
  border-bottom: 15px solid #343436;
   grid-template-columns: repeat(3, minmax(min-content, 1fr)); 
  resize: both;
  height:auto;
}

.editor {
  -webkit-box-flex: 1;
      -ms-flex: 1 0 auto;
          flex: 1 0 auto;
  resize: none;
  height: 100%;
}

/* Header */
.pen {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: end;
      -ms-flex-pack: end;
          justify-content: flex-end;
  background: black;
  border-bottom: 5px solid #343436;
  -webkit-box-shadow: 0 1px 1px black;
          box-shadow: 0 1px 1px black;
  padding: 10px;
}

.pen__author {
  margin: 0;
  text-transform: uppercase;
  color: grey;
  font-size: 12px;
}

.button {
  background: #343436;
  border: 0;
  color: white;
  padding: 10px;
  border-radius: 3px;
  margin-right: 10px;
  font-size: 15px;
  position: relative;
  cursor:pointer;
}

.button--small {
  font-size: 12px;
  padding: 4px;
}

.button--black {
  background: black;
}

.button--dirty:before {
  display: block;
  content: "";
  height: 2px;
  width: calc(100% - 6px);
  top: 4px;
  background: #ffc600;
  position: absolute;
  z-index: 2;
  left: 3px;
}

.pen__details {
  -webkit-box-flex: 1;
      -ms-flex: 1;
          flex: 1;
}

/* Footer */
.settings {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

/* Editor */
.editor {
  border-left: 15px solid #343436;
  border-right: 1px solid #424242;
  -webkit-box-shadow: inset 1px 0 0 rgba(0, 0, 0, 0.2);
          box-shadow: inset 1px 0 0 rgba(0, 0, 0, 0.2);
  background: #1d1f20;
  display: grid;
  grid-template-rows: auto 1fr;
}

.editor__header {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  background: rgba(0, 0, 0, 0.1);
  border-bottom: 1px solid rgba(255, 255, 255, 0.05);
  color: grey;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  -webkit-box-pack: center;
      -ms-flex-pack: center;
          justify-content: center;
  padding: 5px;
}

.editor__heading {
  -webkit-box-flex: 1;
      -ms-flex: 1;
          flex: 1;
  font-weight: 900;
  font-size: 20px;
}

.editor__code {
  font-family: "Operator Mono", Courier New, Courier, monospace;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  font-size: 16px;
  overflow: auto;
  max-height:100%;
}

.editor__number {
  padding: 0 10px;
  display: block;
  line-height: 26px;
}

.editor__input {
  width: 100%;
  resize: none;
  background: none;
  border: 0;
  color: grey;
  font-size: 16px;
  line-height: 25px;
}

/* Preview iFrame */

.preview {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

.preview iframe {
  -webkit-box-flex: 1;
      -ms-flex: 1;
          flex: 1;
}

.settings {
  background: #343436;
  border-top: 1px solid #3e3e3f;
  padding: 10px;
}
</style></head><body>
  <div class="code_editor_code">
    <header class="pen">
      <div class="pen__details">
        <h1><?=$titulo_code_html?></h1>
        <p class="pen__author">A code by Wes Bos</p>
      </div>
      <button class="button button--dirty">☁️ Save</button>
      <button class="button">️️☁️ Save As Private</button>
      <button class="button">⚙ Settings</button>
      <button class="button">👀 Change View</button>
    </header>
    <section class="code" id="codeeditors">
      <div class="editor">
        <header class="editor__header">
          <button class="button button--small editor__settings">⚙</button>
          <h3 class="editor__heading">HTML</h3>
          <button class="button button--small editor__settings">⌄</button>
        </header>
        <div class="editor__code">
          <div class="editor__gutter">
            <span class="editor__number">1</span>
          </div>

          <div contenteditable="true" class="editor__input"><?=$html_code_html?></div>
        </div>
      </div>
      <div class="editor">
        <header class="editor__header">
          <button class="button button--small editor__settings">⚙</button>
          <h3 class="editor__heading">CSS</h3>
          <button class="button button--small editor__settings">⌄</button>
        </header>
        <div class="editor__code">
          <div class="editor__gutter">
            <span class="editor__number">1</span>
            <span class="editor__number">2</span>
            <span class="editor__number">3</span>
          </div>

          <div contenteditable="true" class="editor__input"><?=$css_code_html?></div>
        </div>

      </div>
      <div class="editor">
        <header class="editor__header">
          <button class="button button--small editor__settings">⚙</button>
          <h3 class="editor__heading">JS</h3>
          <button class="button button--small editor__settings">⌄</button>
        </header>
        <div class="editor__code">
          <div class="editor__gutter">
            <span class="editor__number">1</span>
          </div>
          <div contenteditable="true" class="editor__input"><?=$js_code_html?></div>
        </div>

      </div>
    </section>
    <section class="preview">
      <iframe src="<?=$httpshttp?>://dumdarac.com/apps/<?=$app?>/pantallas/html.php?id=<?=$id_code_html?>" id="codeiframeeditor" frameborder="0"></iframe>
    </section>
    <footer class="settings">
      <button class="button button--small button--black">Console</button>
      <button class="button button--small button--black">Assets</button>
      <button class="button button--small button--black">⌘</button>
    </footer>
  </div>
  
  <script>
  
$(function() {
  
  $( "#codeeditors" ).resizable();
  
});
    
  </script>

  
</body></html>
<?}?>