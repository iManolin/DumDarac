<form class="PF PF-form" action="./apps/pay/resources/transfer/send.php?<?=$server_querystring?>" method="post" id="paysend">

  <label class="PF-textfield outlined">
    <input placeholder=" " type="text" name="concept">
    <span t-dd>Concept</span>
  </label>

  <label class="PF-textfield outlined">
    <input placeholder=" " type="number" min="0.01" step="0.01" max="<?=$pay_total?>" name="amount" required>
    <span t-dd>Amount</span>
  </label>

  <input type="number" name="recipient" value="<?=$usuario_id?>" style="display:none;" />

  <div class="PF PF-buttons center" onclick="$(this).parent('form').submit();">
    <div class="PF PF-button ripple border" t-dd>Send</div>
  </div>

  </div>
</form>

<script>
  $('#paysend').ajaxForm({
    beforeSubmit: function() {
      $('#header .PF-progress.loading').show();
    },
    success: function(data) {
      $('#header .PF-progress.loading').hide();
      eval(data);
    }
  });
</script>