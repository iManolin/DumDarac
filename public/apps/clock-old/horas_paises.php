<html>

<head>
  <style>
    .clocks {
      width: 100%;
      background: var(--PF-color-bg-second-default);
      text-align: left;
      overflow:hidden;
      max-width: 40rem;
      margin: auto;
      border-radius: 1em 1em 0 0;
      box-shadow: 0px 1px 3px 1px rgba(60, 64, 67, .15);
      margin-top: 60vh;
      transition:0.5s;
    }
    
    .clocks .PF-bottombar {
      border-bottom: solid 1px #ddd;
      box-shadow: none;
    }
    
    .clocks #content {
      width: 100%;
      padding: 0.5rem;
    }

    .timezone {
      position: relative;
      overflow: hidden;
      padding: 0.5rem;
      margin: 0;
      border-bottom: solid 1px #ddd;
      display: inline-flex;
      width: 100%;
      align-items: center;
      color: var(--PF-color-original-default);
    }

    .timezone .first {
      width: 100%;
    }

    .timezone attr {
      font-size: 0.9em;
    }

    .timezone__location {
      margin: 0;
      font-size: 2.5em;
      font-weight: 200;
    }

    .time {
      font-size: 2em;
      font-weight: 200;
      white-space: nowrap;
    }

    .time small {
      font-size: 0.6em;
    }
  </style>
</head>

<body>
  <div class="clocks">
    <div class="PF PF-bottombar"><ul><li class="ripple">
  <i class="material-icons">alarm</i>
  <span t-dd>Alarm</span>
</li>

<li class="ripple active">
  <i class="material-icons">access_time</i>
  <span t-dd>Clock</span>
</li>

<li class="ripple">
  <i class="material-icons">restore</i>
  <span t-dd>Timer</span>
</li>

<li class="ripple">
  <i class="material-icons">timer</i>
  <span t-dd>Stopwatch</span>
</li></ul></div>
    <div id="content"  >
      
    </div>
  </div>

  <script type="text/template" id="timezone">
    <div class="timezone">
      <div class="first">
        <h1 class="timezone__location">{{name}}</h1>
        <attr><strong>{{when}}</strong></attr>
      </div>
      <div class="time" id="time-{{name}}">{{time}}</div>
    </div>
  </script>
  <script src="//dumdarac.com/apps/clock/js/moment.min.js"></script>
  <script src="//dumdarac.com/apps/clock/js/moment-timezone-with-data-2012-2022.js"></script>
  <script src="//dumdarac.com/apps/clock/js/mustache.min.js"></script>
  <script>
    var timezones = [{
        name: 'Madrid',
        timezone: 'Europe/Madrid'
      },
      {
        name: 'Athens',
        timezone: 'Europe/Athens'
      },
      {
        name: 'Seattle',
        timezone: 'America/Los_Angeles'
      },
      {
        name: 'Chicago',
        timezone: 'America/Chicago'
      },
      {
        name: 'London',
        timezone: 'Europe/London'
      },
      {
        name: 'Paris',
        timezone: 'Europe/Paris'
      },
      {
        name: 'Shanghai',
        timezone: 'Asia/Shanghai'
      },
      {
        name: 'Sydney',
        timezone: 'Australia/Sydney'
      }
    ];

    var template = $('#timezone').html();
    Mustache.parse(template);

    var display = function() {
      var totalcontent = "";
      var content = $('#content');
      for (var i = 0; i < timezones.length; i++) {
        var tz = timezones[i];
        var m = moment();
        tz.time = m.tz(tz.timezone).format('h:mm a');
        tz.when = m.format('dddd');
        totalcontent = totalcontent + (Mustache.render(template, tz));
      }
      content.html(totalcontent);
      content.css("min-height", content.height() + "px");
      var today = new Date();
      var h = today.getHours();
      var m = today.getMinutes();
      var s = today.getSeconds();
      // add a zero in front of numbers<10
      m = checkTime(m);
      s = checkTime(s);
      //document.getElementById('timeclock').innerHTML = h + ":" + m + ":" + s;
    }

    display();
    setInterval(display, 1000);

    function checkTime(i) {
      if (i < 10) {
        i = "0" + i;
      }
      return i;
    }
  </script>
</body>

</html>