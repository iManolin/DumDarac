<html>
<head>
  <title>./dev/login/process</title>
  <style>
  
    #./dev/login/process_form h1 {
  text-align: center;
}
#./dev/login/process_form {
  width: 37%;
  margin: 100px auto;
  padding-bottom: 30px;
  border: 1px solid #918274;
  border-radius: 5px;
  background: white;
}
#./dev/login/process_form input {
  width: 80%;
  height: 35px;
  margin: 5px 10%;
  font-size: 1.1em;
  padding: 4px;
  font-size: .9em;
}
#reg_btn {
  height: 35px;
  width: 80%;
  margin: 5px 10%;
  color: white;
  background: #3B5998;
  border: none;
  border-radius: 5px;
}
/*Styling for errors on form*/
.form_error span {
  width: 80%;
  height: 35px;
  margin: 3px 10%;
  font-size: 1.1em;
  color: #D83D5A;
}
.form_error input {
  border: 1px solid #D83D5A;
}

/*Styling in case no errors on form*/
.form_success span {
  width: 80%;
  height: 35px;
  margin: 3px 10%;
  font-size: 1.1em;
  color: green;
}
.form_success input {
  border: 1px solid green;
}
#error_msg {
  color: red;
  text-align: center;
  margin: 10px auto;
}

  
  </style>
</head>
<body>
 <form id="./dev/login/process_form">
      <h1>./dev/login/process</h1>
      <div id="error_msg"></div>
	  <div>
	 	<input type="text" name="usuario_nombre" placeholder="usuario_nombre" id="usuario_nombre" >
	    <span></span>
	  </div>
	  <div>
	    <input type="usuario_email" name="usuario_email" placeholder="usuario_email" id="usuario_email">
		<span></span>
	  </div>
	  <div>
	   <input type="usuario_clave" name="usuario_clave" placeholder="usuario_clave" id="usuario_clave">
	  </div>
	  <div>
	 	<button type="button" name="./dev/login/process" id="reg_btn">./dev/login/process</button>
	  </div>
	</form>
  
  <script>
  
    $('document').ready(function(){
 var usuario_nombre_state = false;
 var usuario_email_state = false;
 $('#usuario_nombre').on('blur', function(){
  var usuario_nombre = $('#usuario_nombre').val();
  if (usuario_nombre == '') {
  	usuario_nombre_state = false;
  	return;
  }
  $.ajax({
    url: './dev/login/process.php',
    type: 'post',
    data: {
    	'usuario_nombre_check' : 1,
    	'usuario_nombre' : usuario_nombre,
    },
    success: function(response){
      if (response == 'taken' ) {
      	usuario_nombre_state = false;
      	$('#usuario_nombre').parent().removeClass();
      	$('#usuario_nombre').parent().addClass("form_error");
      	$('#usuario_nombre').siblings("span").text('Sorry... usuario_nombre already taken');
      }else if (response == 'not_taken') {
      	usuario_nombre_state = true;
      	$('#usuario_nombre').parent().removeClass();
      	$('#usuario_nombre').parent().addClass("form_success");
      	$('#usuario_nombre').siblings("span").text('usuario_nombre available');
      }
    }
  });
 });		
  $('#usuario_email').on('blur', function(){
 	var usuario_email = $('#usuario_email').val();
 	if (usuario_email == '') {
 		usuario_email_state = false;
 		return;
 	}
 	$.ajax({
      url: './dev/login/process.php',
      type: 'post',
      data: {
      	'usuario_email_check' : 1,
      	'usuario_email' : usuario_email,
      },
      success: function(response){
      	if (response == 'taken' ) {
          usuario_email_state = false;
          $('#usuario_email').parent().removeClass();
          $('#usuario_email').parent().addClass("form_error");
          $('#usuario_email').siblings("span").text('Sorry... usuario_email already taken');
      	}else if (response == 'not_taken') {
      	  usuario_email_state = true;
      	  $('#usuario_email').parent().removeClass();
      	  $('#usuario_email').parent().addClass("form_success");
      	  $('#usuario_email').siblings("span").text('usuario_email available');
      	}
      }
 	});
 });

 $('#reg_btn').on('click', function(){
 	var usuario_nombre = $('#usuario_nombre').val();
 	var usuario_email = $('#usuario_email').val();
 	var usuario_clave = $('#usuario_clave').val();
 	if (usuario_nombre_state == false || usuario_email_state == false) {
	  $('#error_msg').text('Fix the errors in the form first');
	}else{
      // proceed with form submission
      $.ajax({
      	url: './dev/login/process.php',
      	type: 'post',
      	data: {
      		'save' : 1,
      		'usuario_email' : usuario_email,
      		'usuario_nombre' : usuario_nombre,
      		'usuario_clave' : usuario_clave,
      	},
      	success: function(response){
      		alert('user saved');
      		$('#usuario_nombre').val('');
      		$('#usuario_email').val('');
      		$('#usuario_clave').val('');
      	}
      });
 	}
 });
});
  
  </script>
  
</body>
</html>