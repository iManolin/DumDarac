
var norepeatedactionssimpleform = null;
(function($) {

  $.fn.simpleform = function(options) {

    var self = this;

    // Start index counter
    var i = 0;

    // Parameters (options) relevant to the plugin
    // and its functionality.
    var params = {
      next: 'Next',
      previous: 'Previous',
      submit: 'Submit',
      transition: 'slide',
      speed: 300,
      validate: false,
      progressBar: true,
      showProgressText: true,
    };

    // Get all fieldsets of the current form
    var $target = self.find('.content');

    // Active fieldset height
    var targetHeight;

    // Height of active fieldset, progress bar
    // and controls
    var totalHeight;

    var $controls = {
      container: undefined,
      next: undefined,
      previous: undefined,
      submit: undefined
    };

    var controlsHeight;

    // Set progress bar height to 0 incase the
    // user requests the plugin not to use it, that way
    // it's height is not calculated
    var progressBarHeight = 0;

    // This determines how many sections there are
    var totalSections = $target.length;

    // This is our total percentage which we devide
    // by totalSections
    var fullProgress = 100;

    // Used for whether fieldset is valid after validation
    var isFormValid;

    //  If options are passed, override the default values
    if (options) {
      $.extend(params, options);
    }

    //  If progressBar is set true, then add the Progress Bar
    //  which returns the height used later to calculate totalHeight
    if (params.progressBar === true) {
      addProgressBar();
    } else if(params.progressBar){
              $progressBar = $(params.progressBar);
    }

    // After the form controls have been added, store their
    // values of this.form (self);
    $controls.container = self.find('.PF-buttons');
    $controls.previous = self.find('.previous');
    $controls.next = self.find('.next');
    $controls.submit = self.find('.submit');
    progressBarHeight = $progressBar.outerHeight(true);

    // If not set in CSS already, hide any of the fieldsets that
    // are not the first.
    $target.not(':first').addClass('hided');

    // Go Back click event
    $controls.previous.click(function(e) {
      if (norepeatedactionssimpleform == null) {
        norepeatedactionssimpleform = 'nonull';
        // If the form is NOT animating, pass the new index
        // to go back 1 fieldset.
        if (!isAnimating()) {
          i--;
          changeTarget(i, 'slideback');
        }
      }
      e.preventDefault();
    });

    // Go Forward click event
    $controls.next.click(function(e) {
      if (norepeatedactionssimpleform == null) {
        norepeatedactionssimpleform = 'nonull';
        // Before moving forward, does the user have Validation
        // turned on?
        if (params.validate) {

          // Store the result of the form validation by calling
          // the validateForm function outside of this plugin,
          // manually created by the user which contains the validation.
          // The function needs to return true on the validation
          // for us to continue.
          isFormValid = validateForm(self);

          // If Validation was successful and the form isn't animating, continue.
          if (isFormValid && !isAnimating()) {
            i++;
            changeTarget(i);
            history.pushState({
              url: "next"
            }, null, null);
          }
          e.preventDefault();
        }

        // Otherwise just continue the regular route.
        else if (!isAnimating()) {
          i++;
          changeTarget(i);
          history.pushState({
            url: "next"
          }, null, null);
        }
      }
    });

    $controls.submit.click(function(e) {
      if (norepeatedactionssimpleform == null) {
        norepeatedactionssimpleform = 'nonull';
        // Before moving forward, does the user have validation
        // turned on?
        if (params.validate) {
          isFormValid = validateForm(self);
        }

        if (!isFormValid) e.preventDefault();
      }
    });

    
    // Checks to see if the current form is being animating
    function isAnimating() {
      return self.is(':animated');
    }

    // Function controlling the changing of each fieldset including
    // the transition effect requested by the user
    function changeTarget(index, transition) {
      if (transition == null) {
        transition = params.transition;
      }
      // Get the total heights of each element that need to be
      // calculated. This will ensure that the animation of the
      // form's height is accurate to accommodate its elements.
      targetHeight = $target.eq(index).outerHeight(true);
      controlsHeight = $controls.container.outerHeight(true);
      totalHeight = targetHeight + controlsHeight + progressBarHeight;

      switch (transition) {
        case 'slide':
          $target.eq(index - 1).addClass('contentleft');
          setTimeout(function() {
            $target.eq(index - 1).addClass('hided').removeClass('contentleft');
            $controls.container.css('visibility', 'hidden');
            self.removeAttr('style');
            $target.eq(index).addClass('contentright').removeClass('hided').removeClass('contentright');
            $controls.container.css('visibility', 'visible');
            showControls(index);
          }, 900);
          break;

        case 'slideback':
          $target.eq(index + 1).addClass('contentright');
          setTimeout(function() {
            $target.eq(index + 1).addClass('hided').removeClass('contentright');
            $controls.container.css('visibility', 'hidden');
            self.removeAttr('style');
            $target.eq(index).addClass('contentleft').removeClass('hided').removeClass('contentleft');
            $controls.container.css('visibility', 'visible');
            showControls(index);
          }, 900);
          break;

        default:
          $target.addClass('hided');
          $target.eq(index).removeClass('hided');
          showControls(index);
      }

      // Activate the progress bar's new value and position
      progressBarSetIndex(index);
      setTimeout(function() {
        norepeatedactionssimpleform = null;
      }, 1500);
    }

    // Show the controls dependent on the forms position.
    function showControls(index) {

      // If our index is less than or equal to zero, we dont
      // need to display the Previous button.
      if (index <= 0) {
        $controls.previous.hide();
      } else {
        $controls.previous.show();
      }

      // If we're more than or equal to the total sections,
      // display the submit button and remove the Next button
      if (index >= totalSections - 1) {
        $controls.next.hide();
        self.find('#submit-button').show();
      } else {
        $controls.next.show();
        self.find('#submit-button').hide();
      }

    }

    // Progress bar is activated if true in our parameters. This
    // prepends the bar at the top of the form.
    function addProgressBar() {
      self.prepend('<progress class="PF-progress linear" value="0" max="100"></progress>');

      $progressBar = self.find('.PF-progress');
    }

    // Our progress bar animation
    function progressBarSetIndex(index) {

      // If true in parameters, display the navigation information
      if (params.showProgressText) {
        self.find('.progress-text').text((index + 1) + "/" + totalSections);
      }

      $progressBar.val((fullProgress / totalSections) * (index + 1));
    }

    progressBarSetIndex(0);

  };

})(jQuery);