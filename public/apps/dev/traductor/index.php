<html>
  <head>
    <style>
      
      .headerbajo {
        height:20rem;
        display:block;
      }
      
      .traductor {
    width: 90%;
    max-width: 60rem;
    margin: 0 auto;
      }
      
      table {
    width: 100%;
    border-radius: 3px;
    border-spacing: 0;
      }

      td {
        padding:1rem;
            vertical-align: top;
      }

    textarea {
    width: 100%;
    height: 100%;
    min-height: 15rem;
    border: none;
    padding: 1rem;
    font-size: 1rem;
    resize: none;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    border-radius: 3px;
    text-align: left;
    background: white;
    outline: none;
      }
      
      select {
    padding: 0.5rem;
    border-radius: 3px;
    background: #FFFFFF;
    margin-bottom: 1rem;
    color: <?=$color_app_dd?>;
    box-shadow: 0 1px 4px 0 rgba(0,0,0,0.14);
    outline: none;
    border: none;
    font-size: 1rem;
      }
      
      select option {
    padding: 0.5rem;
    border-radius: 3px;
    background: #FFFFFF;
    margin-bottom: 1rem;
    color: <?=$color_app_dd?>;
    box-shadow: 0 1px 4px 0 rgba(0,0,0,0.14);
    outline: none;
    border: none;
    font-size: 1rem;
      }
      
     
      
      .escribiendo {color:#ABABAB;}
      
  
      
      .seccion_traducir {
        text-align:center;
      }
    </style>
  </head>
  <body>
    
    
    <div class="traductor" >
      
      
      <table>
    <tbody><tr class="seccion_traducir" >
      <td>
  <select name="idioma1" id="idioma1" >
  <option value="es" selected>Español</option>
  <option value="en">English</option>
  <option value="fr">Français</option>
  <option value="ca">Catala</option>
  <option value="ku">kurmanji</option>
  <option value="ru">Русо</option>
  </select>
        <textarea id="atraducir" ></textarea>
      <div class="significado_traducciones" >
      </div>
      </td>
    <td>
        <select name="idioma2" id="idioma2" >
  <option value="es" selected>Español</option>
  <option value="en">English</option>
  <option value="fr">Français</option>
  <option value="ca">Catala</option>
  <option value="ku">kurmanji</option>
  <option value="ru">Русо</option>
  </select>
      <textarea id="traducidotexto" class="traducido" ></textarea>
      <div id="traducido" ></div>
      <div id="traducidoeditar" ></div>
            <div class="significado_traducciones" >
      </div>
      </td>
    </tr>
    <tr style="background:#ddd;     color: #795548;" >
      <td>
        <p>
          Definiciones de: <b id="textodefinicion" > </b>
        </p>
      </td>
      <td>
      <p>
        Traducciones de: <b id="textotraducciones" > </b>
        </p>
      </td>
      </tr>
  </tbody></table>
      
    </div>
    
     <script>
      $('#atraducir').keyup(function() {  
        setTimeout(function(){
        
var idiomaescogido = $('#idioma1').val();        
var idiomaatraducir = $('#idioma2').val();        
var searchid = $('#atraducir').val();
var dataString = 'tr='+ searchid;
          
          document.getElementById("textodefinicion").innerHTML = searchid;
          document.getElementById("textotraducciones").innerHTML = searchid;
          
          var urlcargarrapido = "./resources/apps/traductor/traducir.php?l=" + idiomaatraducir + "&lesc=" + idiomaescogido + "&tr=" + searchid;
          
         setTimeout(function(){  $("#traducidoeditar").load(urlcargarrapido); }, 1000);
          
if(searchid!='')
{
  $("#traducidotexto").addClass("escribiendo");
  
    $.ajax({
    type: "POST",
    url: "./resources/apps/traductor/traducir.php?l=" + idiomaatraducir + "&lesc=" + idiomaescogido,
    data: dataString,
    cache: false,
    success: function(html)
    {
    $("#traducido").html(html).show();
      setTimeout(function(){ $("#traducidotexto").removeClass("escribiendo"); }, 1000);
    }
    });
}return false;    
          
          }, 2000);
});
      
      
    
          $('#traducidotexto').keyup(function() {  
        
var idiomaescogido = $('#idioma1').val();        
var idiomaatraducir = $('#idioma2').val(); 
var searchidtraducido = $('#traducidotexto').val();
var searchid = $('#atraducir').val();
var dataString = 'treditar='+ searchidtraducido;
if(searchid!='')
{
  
    $.ajax({
    type: "POST",
    url: "./resources/apps/traductor/editar.php?l=" + idiomaatraducir + "&lesc=" + idiomaescogido + "&tr=" + searchid,
    data: dataString,
    cache: false,
    success: function(html)
    {
    $("#traducidoeditar").html(html).show();
      
    }
    });
}return false;    
});
    
    </script>
    
    
  </body>
</html>