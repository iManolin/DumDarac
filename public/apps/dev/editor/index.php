<html class=""><head><script src="//production-assets.codepen.io/assets/editor/live/console_runner-079c09a0e3b9ff743e39ee2d5637b9216b3545af0de366d4b9aad9dc87e26bfd.js"></script><script src="//production-assets.codepen.io/assets/editor/live/events_runner-73716630c22bbc8cff4bd0f07b135f00a0bdc5d14629260c3ec49e5606f98fdd.js"></script><script src="//production-assets.codepen.io/assets/editor/live/css_live_reload_init-2c0dc5167d60a5af3ee189d570b1835129687ea2a61bee3513dee3a50c115a77.js"></script><meta charset="UTF-8"><meta name="robots" content="noindex"><link rel="shortcut icon" type="image/x-icon" href="//production-assets.codepen.io/assets/favicon/favicon-8ea04875e70c4b0bb41da869e81236e54394d63638a1ef12fa558a4a835f1164.ico"><link rel="mask-icon" type="" href="//production-assets.codepen.io/assets/favicon/logo-pin-f2d2b6d2c61838f7e76325261b7195c27224080bc099486ddd6dccb469b8e8e6.svg" color="#111"><link rel="canonical" href="https://codepen.io/adrifolio/pen/GvXVgP">


<style class="cp-pen-styles">/** Read the guide at: http://bit.ly/2xIdHu0 **/
/** Variables & Mixins **/
.ellipsis {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}
/** Scafolding --------------------**/
body {
  margin: 0;
}
.page {
  display: grid;
  grid-template-rows: auto 30px;
  grid-template-columns: 300px 1fr;
  height: calc(100% - 64px);
}
/** Panel Module **/
.panel {
  display: grid;
  grid-template-rows: 30px auto;
  height: calc(100% - 30px);
}
.panel-body {
  overflow: auto;
}
/** Tabs Module **/
.tabs {
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  height: 100%;
}
.tab-item {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  text-align: center;
  padding: 6px;
}
/** Editor **/
.editor {
  display: grid;
}
.editor .window-2 {
  display: none;
}
.editor.editor--2-windows-layout {
  grid-template-columns: 1fr 1fr;
}
.editor.editor--2-windows-layout .window-2 {
  display: grid;
}
/** Footer **/
.footer {
  display: grid;
  grid-template-columns: 1fr auto;
  grid-column-start: 1;
  grid-column-end: 3;
}
.footer-file-info {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}
.footer-controls {
  display: flex;
  height: 100%;
  justify-content: flex-end;
}
.footer-controls a {
  height: 100%;
  display: flex;
  align-items: center;
}
/** Theme --------------------- **/
/** Icons 
  * From https://octicons.github.com/
**/
@font-face {
  font-family: 'octicons';
  src: url("data:application/x-font-ttf;charset=utf-8;base64,AAEAAAALAIAAAwAwT1MvMg8SBSwAAAC8AAAAYGNtYXAXVtKPAAABHAAAAFRnYXNwAAAAEAAAAXAAAAAIZ2x5Zq+OrE4AAAF4AAAFQGhlYWQO9XO3AAAGuAAAADZoaGVhB8IDzgAABvAAAAAkaG10eCHAAKwAAAcUAAAANGxvY2EHRAkAAAAHSAAAABxtYXhwABcAaQAAB2QAAAAgbmFtZcgccnQAAAeEAAABknBvc3QAAwAAAAAJGAAAACAAAwL6AZAABQAAApkCzAAAAI8CmQLMAAAB6wAzAQkAAAAAAAAAAAAAAAAAAAABEAAAAAAAAAAAAAAAAAAAAABAAADpCAPA/8AAQAPAAEAAAAABAAAAAAAAAAAAAAAgAAAAAAADAAAAAwAAABwAAQADAAAAHAADAAEAAAAcAAQAOAAAAAoACAACAAIAAQAg6Qj//f//AAAAAAAg6QD//f//AAH/4xcEAAMAAQAAAAAAAAAAAAAAAQAB//8ADwABAAAAAAAAAAAAAgAANzkBAAAAAAEAAAAAAAAAAAACAAA3OQEAAAAAAQAAAAAAAAAAAAIAADc5AQAAAAACACwAAAQAA4AARgBTAAABIg4CFRQWBzQuAiM2JjEwBgcuATEHMAYVHgE3FgYHBiYjIhQzMhYzDgEeATEjIgYxITI+AjU0JicmNhcWNjU0LgIjASImNTQ2MzIWFRQGIwMANV1GKBgYOlFXHgIhDwQNFwh2CWcuKgMJKU8wMDAwMGBKEyU4QDAQAYBIdlQuJRs1UCUlmyhGXTX9oA0TEw0NExMNA4AdM0QnXZaSbJpiLhgSCwsPAyZSfBATCAMzCig7QEAdV1I6QBk2VDspVSRGWyEhEZA1XUYo/sATDQ0TEw0NEwAHAAD/wAMAA8AAAwAHAAsADwAlAC0AMQAAASM1MzUjFTM1IxUzNSMVMyURFAYjIRUnBzUjIiY1ETQ2MyEyFhUDIRUzNTMVIREhESEBAEBAQEBAQEBAAgAmGv7AYGCAGiYmGgKAGiZA/YCAwAFA/cACQAGAQIBAwEDAQID9ABomgGBggCYaAwAaJiYa/YCAQEADAP3AAAAAAAQAAAAAAoADgAA+AEsAWABmAAABNCYjIgYVFBYXFQ4BBw4BBw4BBxE+ATU0JiMiBhUUFhcRDgEVFBYzMjY1NCYnPgE3PgEzPgE3PgE3Iz4BNTElMhYVFAYjIiY1NDYzESImNTQ2MzIWFRQGIwEiJjU0NjMyFhUUBiMxAoBLNTVLJBwBFBMULBknQBkdI0o2NUokHBwkSzU1SxIQBB0FDB4SMlgmJigCAR0k/gAgLS4fHy4uHyAtLh8fLi4fAYAgLS4fHy4uHwKANUtLNSM7EBMZLBQTFAEBDg4BMRA6JDVLSzUjOxD+XBE6IzVLSzUZLREDGAMFBgIoJiZpMhI6I80uHx8uLh8fLvzmLR8gLS0gHy0CAC0fIC0tIB8tAAAAAAIAAABAA4ADQAAVABkAAAEhNTQmIyEiBhURFBYzITI2NRE0JiMpATUhA0D+gB8h/sAaJiYaAwAaJiYa/kD+wAFAAsBAICAmGv2AGiYmGgIAGiZAAAYAAAAAAwADgAADAAcACwAPAB0AIgAAASE1IQEhNSEVITUhFSE1IQERFAYjISImNRE0NjMhEychESEBgP8AAQD/AAHA/kABwP5AAcD+QAKAJhr9gBomJhoB4KDA/kACgAKAQP8AQMBAwEABoP2gGiYmGgMAGib/AMD9AAAFAAD/wANAA8AACwAPAB0AIgAqAAABMxUjFSM1IzUzNTMDITUhARcRFAYjISImNRE0NjMBJyERIQMhFSEBETMRAYCAgECAgEDAAUD+wAEg4CYa/cAaJiYaAkDA/oACQGD+oAFAAQBAAgBAgIBAgP4AQAKA4P2gGiYmGgMAGib/AMD9AAPAQP8A/gACIAAAAAAJAEAAQAQAA0AAAwAHAAsADwATABcALgAzADgAABMhFSEVITUhFSE1IQEhFSEVIRUhFSEVIRMRFAYjIQcnISImNRE0NjMhFzchMhYVBSchESEBIQcRIcABAP8AAQD/AAEA/wACwP8AAQD/AAEA/wABAIAmGv6gQED+oBomJhoBYEBAAWAaJv4AIP6gAYABwP6gIAGAAoBAgEDAQAEAQEBAQEABwP3AGiZAQCYaAkAaJkBAJhogIP3AAkAg/eAAAAABAAABAAKAAqAABQAACQE3FzcXAUD+wGDg4GABAAFAYPDwYAABAEAAgAHgAwAABQAACQEnNyc3AeD+wGDw8GABwP7AYODgYAABAAAABhmagmMIIV8PPPUACwQAAAAAANXoitAAAAAA1eiK0AAA/8AEAAPAAAAACAACAAAAAAAAAAEAAAPA/8AAAAQAAAAAAAQAAAEAAAAAAAAAAAAAAAAAAAANBAAAAAAAAAAAAAAAAgAAAAQAACwDAAAAAoAAAAOAAAADAAAAA0AAAAQAAEACgAAAAgAAQAAAAAAACgAUAB4AkADcAWwBlgHUAhwCfAKOAqAAAQAAAA0AZwAJAAAAAAACAAAAAAAAAAAAAAAAAAAAAAAAAA4ArgABAAAAAAABAAgAAAABAAAAAAACAAcAaQABAAAAAAADAAgAOQABAAAAAAAEAAgAfgABAAAAAAAFAAsAGAABAAAAAAAGAAgAUQABAAAAAAAKABoAlgADAAEECQABABAACAADAAEECQACAA4AcAADAAEECQADABAAQQADAAEECQAEABAAhgADAAEECQAFABYAIwADAAEECQAGABAAWQADAAEECQAKADQAsG9jdGljb25zAG8AYwB0AGkAYwBvAG4Ac1ZlcnNpb24gNi4xAFYAZQByAHMAaQBvAG4AIAA2AC4AMW9jdGljb25zAG8AYwB0AGkAYwBvAG4Ac29jdGljb25zAG8AYwB0AGkAYwBvAG4Ac1JlZ3VsYXIAUgBlAGcAdQBsAGEAcm9jdGljb25zAG8AYwB0AGkAYwBvAG4Ac0ZvbnQgZ2VuZXJhdGVkIGJ5IEljb01vb24uAEYAbwBuAHQAIABnAGUAbgBlAHIAYQB0AGUAZAAgAGIAeQAgAEkAYwBvAE0AbwBvAG4ALgAAAAMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=") format('truetype');
  font-weight: normal;
  font-style: normal;
}
[class^="icon-"],
[class*=" icon-"] {
  font-family: 'octicons' !important;
  speak: none;
  font-style: normal;
  font-weight: normal;
  font-variant: normal;
  text-transform: none;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}
.icon-squirrel:before {
  content: "\e900";
}
.icon-repo:before {
  content: "\e901";
}
.icon-git-branch:before {
  content: "\e902";
}
.icon-diff:before {
  content: "\e905";
}
/** Variables **/
/** Styles **/
body {
  background-color: #d7dae0;
  font-family: -apple-system, system-ui, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
  font-size: 0.8rem;
  color: #2c313a;
}
.sidebar {
  border-right: 1px solid #c5cad3;
}
.sidebar .tree-group {
  padding: 0;
  margin: 0;
}
.sidebar .tree-item {
  padding: 5px 0 5px 25px;
  list-style: none;
  position: relative;
}
.sidebar .tree-item:before {
  font-family: 'octicons' !important;
  position: absolute;
  left: 10px;
}
.sidebar .tree-item.tree-item--chevron-down:before {
  content: "\e907";
}
.sidebar .tree-item.tree-item--chevron-right:before {
  content: "\e908";
}
.sidebar .tree-item-label {
  display: flex;
}
.sidebar .tree-item-label:before {
  font-family: 'octicons' !important;
  font-size: 17px;
  margin-right: 10px;
}
.sidebar .tree-item-label.tree-item-label--repo:before {
  content: "\e901";
}
.sidebar .tree-item-label.tree-item-label--file-directory:before {
  content: "\e903";
}
.sidebar .tree-item-label.tree-item-label--file-text:before {
  content: "\e904";
}
.sidebar .tree-item-label.tree-item-label--file-book:before {
  content: "\e906";
}
.sidebar .panel-header {
  display: flex;
  align-items: center;
  justify-content: center;
}
.editor .tab-item {
  border-right: 1px solid #c5cad3;
  border-bottom: 1px solid #c5cad3;
  color: #5c6370;
}
.editor .tab-item.tab-item--selected {
  background-color: #2c313a;
  color: #dcdfe4;
  border-color: #2c313a;
}
.editor .panel-body {
  background-color: #2c313a;
  color: #dcdfe4;
  font-family: "Courier New";
  font-size: 1rem;
  height:calc(100% + 30px);
}
.editor .panel-body .editor-container {
  padding-left: 45px;
  margin-top: 0;
}
.editor .panel-body .editor-line {
  color: #5c6370;
}
.editor .panel-body .editor-content {
  color: #c5cad3;
}
.editor .window-2 {
  border-left: 1px solid #c5cad3;
}
.footer {
  align-items: center;
  border-top: 1px solid #c5cad3;
}
.footer-file-info .file-info-source {
  padding: 0 10px;
}
.footer-controls a {
  padding: 0 10px;
  color: initial;
  text-decoration: none;
}
.footer-controls a [class*="icon-"] {
  font-size: 15px;
  margin-right: 5px;
}
.footer-controls a:hover {
  background-color: #c5cad3;
}
.footer-controls a .icon-squirrel {
  color: #1f96ff;
}
/** Control Bar for demo purposes **/
.control-bar {
  position: fixed;
  bottom: 50px;
  right: 20px;
}
#window-1 {
  display: none;
}
  
  #kamiku {
    display:none!important;
  }
</style></head><body>
<div class="page">
  
  <!--Sidebar-->
  <div class="sidebar">
    <div class="panel">
     <div class="panel-header ">Project</div>

		  <!--Tree-->
      <div class="panel-body">
        
		    <ul class="tree-group">
          
          <li class="tree-item tree-item--chevron-down">
            <span class="tree-item-label tree-item-label--repo">vintage-shop</span>
            <ul class="tree-group">
              <li class="tree-item tree-item--chevron-right">
                <span class="tree-item-label tree-item-label--file-directory">git</span>
              </li>
              <li class="tree-item tree-item--chevron-right">
                <span class="tree-item-label tree-item-label--file-directory">dist</span>
              </li>
              <li class="tree-item tree-item--chevron-right">
                <span class="tree-item-label tree-item-label--file-directory">node_modules</span>
              </li>
              <li class="tree-item tree-item--chevron-right">
                <span class="tree-item-label tree-item-label--file-directory">src</span>
              </li>
              <li class="tree-item">
                <span class="tree-item-label tree-item-label--file-text">.editconfig</span>
              </li>
              <li class="tree-item">
                <span class="tree-item-label tree-item-label--file-text">build.js</span>
              </li>
              <li class="tree-item">
                <span class="tree-item-label tree-item-label--file-text">development.html</span>
              </li>
              <li class="tree-item">
                <span class="tree-item-label tree-item-label--file-text">LICENSE</span>
              </li>
              <li class="tree-item">
                <span class="tree-item-label tree-item-label--file-text">package-lock.json</span>
              </li>
              <li class="tree-item">
                <span class="tree-item-label tree-item-label--file-text">production.html</span>
              </li>
              <li class="tree-item">
                <span class="tree-item-label tree-item-label--file-book">README.md</span>
              </li>
              <li class="tree-item">
                <span class="tree-item-label tree-item-label--file-text">test.html</span>
              </li>
            </ul>
          </li>
         </ul>
       </div>

    </div>
  </div><!--Sidebar ends-->

  
  <div class="editor">
    
    <!--Editor 1 -->
    <div class="panel">
      
      <div class="panel-header">
        <div class="tabs">
          <div class="tab-item">scafolding.less</div>
          <div class="tab-item tab-item--selected">styles.less</div>
          <div class="tab-item">cart-item.stash</div>
        </div>
      </div>

      <div class="panel-body">
        <ol class="editor-container">
          <li class="editor-line"></li>
          <li class="editor-line"><span class="editor-content">Hey there!</span></li>
          <li class="editor-line"></li>
         </ol>
       </div>
     </div>

     <!--Editor 2 -->
     <div class="panel window-2">
         
       <div class="panel-header">
         <div class="tabs">
           <div class="tab-item tab-item--selected">styles.less</div>
         </div>
       </div>

       <div class="panel-body">
         <ol class="editor-container">
           <li class="editor-line"></li>
           <li class="editor-line"><span class="editor-content">Hey there!</span></li>
           <li class="editor-line"></li>
         </ol>
       </div>
       
      </div>
    
  </div><!--Editor ends-->

	<!--Footer-->
	<div class="footer">
    <div class="footer-file-info">
      <span class="file-info-source">src/styles.less</span>
      <span class="file-info-location">2:5</span>
      </div>
    <div class="footer-controls">
      <a href="">LF</a>
      <a href="">UTF-8</a>
      <a href="">Less</a>
      <a href=""><span class="icon-git-branch"></span>master</a>
      <a href="">⬇︎ 5 ⬆︎</a>
      <a href=""><span class="icon-diff"></span>1 file</a>
      <a href=""><span class="icon-squirrel"></span></a>
    </div>
  </div>
  
</div><!--Grid structure ends-->

<!-- Control Bar for demo purposes-->
<div class="control-bar">
  <button id="window-1">Close Tabs to the Right</button>
	<button id="window-2">Split Right</button>
</div>
<script src="//production-assets.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>$( "#window-2" ).click(function (){
	$( ".editor" ).addClass( "editor--2-windows-layout" );
  $( "#window-1" ).show();
  $( "#window-2" ).hide();
});

$( "#window-1" ).click(function (){
	$( ".editor" ).removeClass( "editor--2-windows-layout" );
  $( "#window-2" ).show();
  $( "#window-1" ).hide();
});

//# sourceURL=pen.js
</script>
</body></html>