<html>

<head>
	<link rel="stylesheet" type="text/css" href="/resources/css/jquery.convform.css">
  
  <style>
  
    .conv-form-wrapper {
  position: relative;
    width: 100%;
    padding: 0;
    display: flex;
    flex-direction: column;
    align-items: flex-end;
    background: white;
    background-size: cover;
    background-position: center;
    background-attachment: fixed;
    flex: 0 1 auto;
    max-height: 100%;
    height: fit-content;
    margin: auto;
    margin-bottom: .5em!important;
    max-width: 30rem;
  }
  
  </style>
  
</head>

<body>

	<div id="chatform" class="conv-form-wrapper">
		<form action="./resources/comprobar.php" method="POST" class="hidden" id="registro">
			<select conv-question="¡Hola! Soy Kamiku, tu asistente. ¿Puedo mostrarte algunas características?">
	                                    <option value="yes">Si</option>
	                                    <option value="sure">Claro!</option>
	                                </select>
			<input type="text" name="name" conv-question="¡Bien! Primero, dime tu nombre completo, por favor.|¡De acuerdo! Por favor, dime tu nombre primero.">
			<input type="text" conv-question="¡Hola, {name}:0! Es un placer conocerte." no-answer="true">
			<select name="programmer" callback="storeState" conv-question="Así que, dime {name}:0 quieres que te haga unas preguntas personales? (Así podre recomendarte lo que mas te guste).">
	                                    <option value="yes">Yes</option>
	                                    <option value="no">No</option>
	                                </select>
			<div conv-fork="programmer">
				<div conv-case="yes">
					<input type="text" conv-question="¿Qué tipo de música te gusta?" no-answer="true">
					<select name="multi[]" conv-question="Así podre recomendarte musica que te guste." multiple>
	                                    <option value="Rock">Rock</option>
	                                    <option value="Pop">Pop</option>
	                                    <option value="Country">Country</option>
	                                    <option value="Classic">Classic</option>
	                                </select>
					<select name="multi[]" conv-question="¿Qué genero de peliculas prefieres ver?" multiple>
	                                    <option value="Rock">Accion</option>
	                                    <option value="Pop">Comedia</option>
	                                    <option value="Country">Ninguna</option>
	                                    <option value="Classic">Yo mismo</option>
	                                </select>
				</div>
				<div conv-case="no">
					<select name="callbackTest" conv-question="You can do some cool things with callback functions. Want to rollback to the 'programmer' question before?">
	                                    <option value="yes" callback="rollback">Yes</option>
	                                    <option value="no" callback="restore">No</option>
	                                </select>
				</div>
			</div>
			<select name="thought" conv-question="Have you ever thought about learning? Programming is fun!">
		                                    	<option value="yes">Yes</option>
		                                    	<option value="no">No..</option>
		                                    </select>
			<input type="text" conv-question="convForm also supports regex patterns. Look:" no-answer="true">
			<input conv-question="Type in your e-mail" pattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$" id="email" type="email" name="email" required placeholder="What's your e-mail?">
			<input conv-question="Now tell me a secret (like a password)" type="password" data-minlength="6" id="senha" name="password" required placeholder="password">
			<select conv-question="Selects also support callback functions. For example, try one of these:">
											<option value="google" callback="google">Google</option>
											<option value="bing" callback="bing">Bing</option>
									</select>
			<select name="callbackTest" conv-question="You can do some cool things with callback functions. Want to rollback to the 'programmer' question before?">
	                                    <option value="yes" callback="rollback">Yes</option>
	                                    <option value="no" callback="restore">No</option>
	                                </select>
			<select conv-question="This is it! If you like me, consider donating! If you need support, contact me. When the form gets to the end, the plugin submits it normally, like you had filled it." id="">
	                                    <option value="">Mola!</option>
	                                </select>
		</form>
	</div>


	<script type="text/javascript" src="/resources/js/jquery.convform.js"></script>
	<script type="text/javascript" src="/resources/js/autosize.min.js"></script>

	<script>
		function google() {
			window.open("https://google.com");
		}

		function bing() {
			window.open("https://bing.com");
		}
		var rollbackTo = false;
		var originalState = false;

		function storeState(stateWrapper) {
			rollbackTo = stateWrapper.current;
			console.log("storeState called: ", rollbackTo);
		}

		function rollback(stateWrapper) {
			console.log("rollback called: ", rollbackTo, originalState);
			console.log("answers at the time of user input: ", stateWrapper.answers);
			if (rollbackTo != false) {
				if (originalState == false) {
					originalState = stateWrapper.current.next;
					console.log('stored original state');
				}
				stateWrapper.current.next = rollbackTo;
				console.log('changed current.next to rollbackTo');
			}
		}

		function restore(stateWrapper) {
			if (originalState != false) {
				stateWrapper.current.next = originalState;
				console.log('changed current.next to originalState');
			}
		}
	</script>
</body>

</html>