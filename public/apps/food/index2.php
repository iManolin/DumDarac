<html>
  <head>
    <style>
      
      body {
        background-color:<?=$color_app_dd?>;
      }
    
      .comida_container {
        width:100%;
        max-width:55rem;
        margin:auto;
      }
      
      .comida_container .comida_block {
    width: calc(100% - 1rem);
    border-radius: 2px;
    background: white;
    margin: 0.5rem;
    margin-top: 1rem;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
      }
      
      .comida_container .comida_block p {
font-size: 1rem;
    padding: 1rem;
    padding-bottom: 0.5rem;
      }
      
      .comida_container .comida_block .comida_content_carrousel {
        width:calc(100% - 1rem);
        padding:0.5rem;
        display:inline-flex;
        align-items:center;
        overflow:auto;
      }
      
      .comida_container .comida_block .comida_content_carrousel .element {
    text-align: center;
    border-radius: 2px;
    padding: 0.5rem;
      }
      
      .comida_container .comida_block .comida_content_carrousel .element p {
    padding: 0.5rem;
    white-space: nowrap;
      }
      
            .comida_container .comida_block .comida_content_carrousel .element .ball {
    display: inline-flex;
    align-items: center;
    padding: 1rem;
    background: #E91E63;
    border-radius: 50px;
    color: white;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    margin: 0.5rem;
      }
      
      .comida_container .comida_block .comida_content_carrousel .element .ball i {
     font-size: 1.5rem;
    width: 1.5rem;
    height: 1.5rem;
      }
      
      
      .comida_container .comida_block .comida_content {
        width:100%;
        display:inline-block;
      }
      
      .comida_container .comida_block .comida_content .card {
        background:white;
      }
      
      .comida_container .comida_block .comida_content .card .img {
        width:100%;
        height:10rem;
      }
      
    </style>
  </head>
  <body>
    
    <div class="comida_container" >
      <div class="comida_block" >
        <p>Elige lo que mas te guste</p>
        <div class="comida_content_carrousel" >
          <div class="element" >
            <div class="ball" style="background: -webkit-linear-gradient(top, #E91E63, #F44336);" >
              <i class="pe-is-f-burger-1"></i>
            </div>
            <p>Hamburger</p>
          </div>
          <div class="element" >
            <div class="ball" style="background: -webkit-linear-gradient(top, #3F51B5, #00BCD4);" >
              <i class="pe-is-f-pizza-2"></i>
            </div>
            <p>Pizza</p>
          </div>
          <div class="element" >
            <div class="ball" style="background: -webkit-linear-gradient(top, #E91E63, #9C27B0);" >
              <i class="pe-is-f-chinese-food-53"></i>
            </div>
            <p>Chinese Food</p>
          </div>
          <div class="element" >
            <div class="ball" style="background: -webkit-linear-gradient(top, #FFC107, #FF5722);" >
              <i class="pe-is-f-burger-1"></i>
            </div>
            <p>Hot Dog</p>
          </div>
          <div class="element" >
            <div class="ball" style="background: -webkit-linear-gradient(top, #4CAF50, #E91E63);" >
              <i class="pe-is-f-kebab"></i>
            </div>
            <p>Kebab</p>
          </div>
        </div>
      </div>
      <div class="comida_block" >
        <p>Elige entre miles de platos</p>
        <div class="comida_content" >
          <div class="card" >
            <div class="img" >
              
            </div>
          </div>
        </div>
      </div>
    </div>
    
  </body>
</html>