<html lang="en" class=""><head><script src="//production-assets.codepen.io/assets/editor/live/console_runner-079c09a0e3b9ff743e39ee2d5637b9216b3545af0de366d4b9aad9dc87e26bfd.js"></script><script src="//production-assets.codepen.io/assets/editor/live/events_runner-73716630c22bbc8cff4bd0f07b135f00a0bdc5d14629260c3ec49e5606f98fdd.js"></script><script src="//production-assets.codepen.io/assets/editor/live/css_live_reload_init-2c0dc5167d60a5af3ee189d570b1835129687ea2a61bee3513dee3a50c115a77.js"></script><meta charset="UTF-8"><meta name="robots" content="noindex"><link rel="shortcut icon" type="image/x-icon" href="//production-assets.codepen.io/assets/favicon/favicon-8ea04875e70c4b0bb41da869e81236e54394d63638a1ef12fa558a4a835f1164.ico"><link rel="mask-icon" type="" href="//production-assets.codepen.io/assets/favicon/logo-pin-f2d2b6d2c61838f7e76325261b7195c27224080bc099486ddd6dccb469b8e8e6.svg" color="#111"><link rel="canonical" href="https://codepen.io/xmark/pen/hovaB?q=shopping+list&amp;limit=all&amp;type=type-pens">

<link rel="stylesheet prefetch" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css"><link rel="stylesheet prefetch" href="//fonts.googleapis.com/css?family=Roboto:400|Roboto+Condensed:400|Roboto+Slab:100,300,700">
<style class="cp-pen-styles">body {
  background: #fff;
  font-size: 100%;
  line-height: 1.75;
  font-family: Roboto;
  color: #333;
  padding-bottom: 3em;
}
*,
*:before,
*:after {
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}
.cf:before,
.cf:after {
  content: " ";
  /* 1 */
  display: table;
  /* 2 */
}
.cf:after {
  clear: both;
}
.container {
  max-width: 1024px;
  margin: 0 auto;
  padding: 1em 2em;
}
.container > div {
  float: left;
  padding-right: 20px;
}
.ingredients {
  width: 37%;
  margin-right: 3%;
}
.ingredients li {
  font-family: "Roboto Condensed", sans-serif;
}
.instructions {
  width: 60%;
}
.details {
  width: 100%;
  background: #fff;
  padding: 1em 0 2em;
}
dl,
dt,
dd {
  float: left;
}
dl {
  width: 33%;
  padding: .5em 0;
}
dt {
  font-family: "Roboto Condensed";
  padding-right: 5px;
  color: #C15B45;
}
dd {
  color: #000;
}
h1 {
  font-family: "Roboto Slab";
  font-size: 3.5em;
  line-height: 1;
  color: #fff;
  margin: 0;
  padding: 1em 0;
  display: block;
  font-weight: 100;
}
.recipe-title {
  background: #C15B45;
}
h2 {
  font-family: Roboto;
  font-size: 2em;
  line-height: 1;
  margin: 1em 0 .5em;
  padding-bottom: .25em;
  font-weight: 400;
  color: #C15B45;
  border-bottom: 1px solid #db9e91;
}
ol {
  counter-reset: my-counter;
  list-style-type: none;
}
ol li {
  counter-increment: my-counter;
  margin-bottom: 1.5em;
}
ol li:before {
  content: "Step " counters(my-counter, ".") ": ";
  font-weight: 700;
  text-transform: uppercase;
}
/* do the shopping list */
.panel-two {
  clear: both;
}
.shopping-list {
  margin-top: 2em;
}
.shopping-list li {
  height: 44px;
  border-bottom: 1px dotted #E5A36F;
  padding-top: 6px;
}
.delme {
  text-decoration: line-through;
}
.panel {
  display: none;
}
/* do the tabs */
.simpletab {
  background: #E5A36F;
  margin-bottom: 3em;
}
.simpletabs {
  max-width: 1024px;
  margin: 0 auto;
  padding: .5em 2em 0;
}
.simpletabs li {
  float: left;
  width: 32%;
  height: 48px;
  padding-top: 8px;
  color: #fff;
  text-align: center;
  border-radius: 4px 4px 0 0;
  -webkit-transition: all .5s linear;
  transition: all .5s linear;
  cursor: pointer;
  margin-right: 1%;
  background: #e39d66;
}
.simpletabs li:hover {
  background: #dd8744;
}
.simpletabs li.selected {
  background: #fff;
  color: #dd8744;
}
.simpletab-wrapper {
  background: #fff;
}
/* do the cooking list */
.cooking-list {
  counter-reset: cook-counter;
  list-style-type: none;
  margin: 2em auto 0;
  font-size: 1.75em;
}
.cooking-list li {
  counter-increment: cook-counter;
  margin-bottom: 1.5em;
  -webkit-transition: all .5s linear;
  transition: all .5s linear;
}
.cooking-list li:before {
  content: "Step " counters(cook-counter, ".") ": ";
  font-weight: 700;
  text-transform: uppercase;
}
.fade-me {
  color: #ddd;
  -webkit-transition: all .5s linear;
  transition: all .5s linear;
}
@media screen and (max-width: 800px) {
  .container > div {
    width: 100%;
    padding-right: 0;
  }
  dl {
    display: block;
    width: 100%;
    padding: 0;
  }
  dt {
    padding-right: 10px;
  }
}
</style></head><body>
<div class="recipe-title">
  <div class="container">
    <h1 id="recipe-title" class="saver" contenteditable="true">idjdsado jjpodjpoajd</h1>
  </div>
</div>

<div class="simpletab">
  <ul class="simpletabs cf">
    <li class="selected">Full</li>
    <li class="">Shop</li>
    <li class="">Cook</li>
  </ul>
  <div class="simpletab-wrapper">

    <div class="panel panel-one" style="display: block;">

      <div class="details cf">
        <div class="container">
          <h2>Details</h2>
          <dl>
            <dt>Prep time: </dt>
            <dd id="prep-time" class="saver" contenteditable="true">10 minutes</dd>
          </dl>
          <dl>
            <dt>Total time: </dt>
            <dd id="total-time" class="saver" contenteditable="true">50 minutes</dd>
          </dl>
          <dl>
            <dt>Skill level: </dt>
            <dd class="saver" contenteditable="true">Easy</dd>
          </dl>
          <dl>
            <dt>Serves: </dt>
            <dd id="skill-level" class="saver" contenteditable="true">6</dd>
          </dl>
          <dl>
            <dt>Healthy: </dt>
            <dd id="healthy" class="saver" contenteditable="true">Yes</dd>
          </dl>
          <dl>
            <dt>Freezable: </dt>
            <dd id="freezable" class="saver" contenteditable="true">Yes</dd>
          </dl>
        </div>
      </div>


      <div class="container">  

        <div class="ingredients">
          <h2>Ingredients</h2>
          <ul id="ingredients-list" class="saver" contenteditable="true">
            <li>1 tbsp olive oil, plus extra for drizzling</li>
            <li>200g cooking chorizo, peeled and diced</li>
            <li>1 large onion, chopped</li>
            <li>2 carrots, chopped</li>
            <li>pinch of cumin seeds</li>
            <li>3 garlic cloves, chopped</li>
            <li>1 tsp smoked paprika, plus extra for sprinkling</li>
            <li>pinch of golden caster sugar</li>
            <li>small splash red wine vinegar</li>
            <li>250g red lentils</li>
            <li>2 x 400g cans chopped tomato</li>
            <li>850ml chicken stock</li>
            <li>plain yogurt, to serve</li>
          </ul>
        </div>

        <div class="instructions">
          <h2>Method</h2>
          <ol id="method-list" class="saver" contenteditable="true">
            <li>Heat the oil in a large pan. Add the chorizo and cook until crisp and it has released its oils. Remove with a slotted spoon into a bowl, leaving the fat in the pan. Fry the onion, carrots and cumin seeds for 10 mins until soft and glistening, then add the garlic and fry for 1 min more. Scatter over the paprika and sugar, cook for 1 min, then splash in the vinegar. Simmer for a moment, then stir in the lentils and pour the tomatoes and chicken stock over.</li>
            <li>Give it a good stir, then simmer for 30 mins or until the lentils are tender. Blitz with a hand blender until smooth-ish but still chunky. Can be made several days ahead or frozen for 6 months at this point. Serve in bowls, drizzled with yogurt and olive oil, scattered with the chorizo and a sprinkling of paprika.</li>
          </ol>
        </div>
      </div>
    </div>

    <div class="panel panel-two" style="display: none;">
      <div class="container">
        <ul class="shopping-list">
            <li>1 tbsp olive oil, plus extra for drizzling</li>
            <li>200g cooking chorizo, peeled and diced</li>
            <li>1 large onion, chopped</li>
            <li>2 carrots, chopped</li>
            <li>pinch of cumin seeds</li>
            <li>3 garlic cloves, chopped</li>
            <li>1 tsp smoked paprika, plus extra for sprinkling</li>
            <li>pinch of golden caster sugar</li>
            <li>small splash red wine vinegar</li>
            <li>250g red lentils</li>
            <li>2 x 400g cans chopped tomato</li>
            <li>850ml chicken stock</li>
            <li>plain yogurt, to serve</li>
          </ul>
      </div> 
    </div>

    <div class="panel panel-three" style="display: none;">
      <div class="container">
        <ol class="cooking-list">
            <li>Heat the oil in a large pan. Add the chorizo and cook until crisp and it has released its oils. Remove with a slotted spoon into a bowl, leaving the fat in the pan. Fry the onion, carrots and cumin seeds for 10 mins until soft and glistening, then add the garlic and fry for 1 min more. Scatter over the paprika and sugar, cook for 1 min, then splash in the vinegar. Simmer for a moment, then stir in the lentils and pour the tomatoes and chicken stock over.</li>
            <li>Give it a good stir, then simmer for 30 mins or until the lentils are tender. Blitz with a hand blender until smooth-ish but still chunky. Can be made several days ahead or frozen for 6 months at this point. Serve in bowls, drizzled with yogurt and olive oil, scattered with the chorizo and a sprinkling of paprika.</li>
          </ol>
      </div> 
    </div>


  </div>

</div>

<script src="//production-assets.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js"></script><script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>(function (jQuery) {
  jQuery.mark = {
    makecookinglist: function (options) {
      var defaults = {
        selector: '.cooking-list'
      };
      if (typeof options == 'string') defaults.selector = options;
      var options = jQuery.extend(defaults, options);
      return jQuery(options.selector).each(function () {
        var cooklistobj = jQuery(this);
        var getTheId = "method-list";
        if (localStorage.getItem(String('cooking-list'))) {
          cooklistobj.html(localStorage.getItem('cooking-list'));
        } else if (localStorage.getItem(String(getTheId)))  {           
          cooklistobj.html(localStorage.getItem(getTheId));
        } else {
          cooklistobj.html(jQuery('#method-list').html());
        }
      })
    },
    makeshoppinglist: function (options) {
      var defaults = {
        selector: '.shopping-list'
      };
      if (typeof options == 'string') defaults.selector = options;
      var options = jQuery.extend(defaults, options);
      return jQuery(options.selector).each(function () {
        var shoplistobj = jQuery(this);
        var getTheId = "ingredients-list";
        if (localStorage.getItem(String('shopping-list'))) {
          shoplistobj.html(localStorage.getItem('shopping-list'));
        } else if (localStorage.getItem(String(getTheId)))  {           
          shoplistobj.html(localStorage.getItem(getTheId));
        } else {
          shoplistobj.html(jQuery('#ingredients-list').html());
        }
      })
    },
    cookcheck: function (options) {
      var defaults = {
        selector: '.cooking-list'
      };
      if (typeof options == 'string') defaults.selector = options;
      var options = jQuery.extend(defaults, options);
      return jQuery(options.selector).each(function () {
        var cookobj = jQuery(this);
        cookobj.on( "click", "li", function() {
          jQuery(this).toggleClass("fade-me");
          var getcooking = jQuery(this).parent().html();
          localStorage.setItem('cooking-list', getcooking);
        });
      })
    },
    listcheck: function (options) {
      var defaults = {
        selector: '.shopping-list li'
      };
      if (typeof options == 'string') defaults.selector = options;
      var options = jQuery.extend(defaults, options);
      return jQuery(options.selector).each(function () {
        var shopobj = jQuery(this).closest('ul');
        shopobj.on( "click", "li", function() {
          jQuery(this).toggleClass("delme");
          var getshopping = jQuery(this).parent().html();
          localStorage.setItem('shopping-list', getshopping);
        });
      })
    },
    shopping: function (options) {
      var defaults = {
        selector: '.shop'
      };
      if (typeof options == 'string') defaults.selector = options;
      var options = jQuery.extend(defaults, options);
      return jQuery(options.selector).each(function () {
        var shopobj = jQuery(this);
        shopobj.click(function(event){

        });
      })
    },
    simpletabs: function (options) {
      var defaults = {
        selector: '.simpletab'
      };
      if (typeof options == 'string') defaults.selector = options;
      var options = jQuery.extend(defaults, options);
      return jQuery(options.selector).each(function () {
        var tabobj = jQuery(this);
        var doSimpletab = "0";
        if (localStorage.getItem('simpletabnumber')) {
          doSimpletab = localStorage.getItem('simpletabnumber');
        }
        jQuery('.simpletabs li').eq(doSimpletab).addClass('selected');
        jQuery('.simpletab-wrapper > div').eq(doSimpletab).show();
        var clicker = jQuery('.simpletabs li');

        clicker.click(function(event){
          jQuery('.simpletabs li').removeClass("selected");
          jQuery(this).addClass("selected");
          var triggerID = jQuery('.simpletabs').find('li').index(this);
          localStorage.setItem('simpletabnumber', triggerID);
          jQuery('.panel').hide();
          jQuery('.simpletab-wrapper > div').eq(triggerID).show();
          event.preventDefault(event);
        });
      })
    },
    saveinfo: function (options) {
      var defaults = {
        selector: '.saver',
      };
      if (typeof options == 'string') defaults.selector = options;
      var options = jQuery.extend(defaults, options);
      return jQuery(options.selector).each(function () {
        var obj = jQuery(this);
        var getTheId = obj.attr('id');
        jQuery( document ).on( "blur", "#" + getTheId, function() {
          localStorage.setItem(getTheId, this.innerHTML);
          if (getTheId == "ingredients-list") {
            localStorage.setItem('shopping-list', this.innerHTML);
            jQuery('.shopping-list').html(this.innerHTML);
          }
          if (getTheId == "method-list") {
            localStorage.setItem('cooking-list', this.innerHTML);
            jQuery('.cooking-list').html(this.innerHTML);
          }
        });
      })
    },
    getinfo: function (options) {
      var defaults = {
        selector: '.saver',
      };
      if (typeof options == 'string') defaults.selector = options;
      var options = jQuery.extend(defaults, options);
      return jQuery(options.selector).each(function () {
        var obj = jQuery(this);
        var getTheId = obj.attr('id');
        if (localStorage.getItem(String(getTheId))) {
          obj.html(localStorage.getItem(getTheId));
        }
      })
    },

  }
})(jQuery);

jQuery(function(){	
  jQuery.mark.makecookinglist();
  jQuery.mark.makeshoppinglist();
  jQuery.mark.cookcheck();
  jQuery.mark.listcheck();
  jQuery.mark.shopping();
  jQuery.mark.simpletabs();
  jQuery.mark.saveinfo();
  jQuery.mark.getinfo();
});
//# sourceURL=pen.js
</script>
</body></html>