<html>
  <head>
    <style>
      
      .cesta_compra {
        display:block!important;
      }
      
      .headerbajo {
        height:31rem;
        display:block;
      }
      
      .express_container * {
        font-family: One Day;
      }
      
      .express_container {
        padding-top:1.5rem;
    max-width: 70rem;
    margin: auto;
    width: calc(100% - 2rem);
        font-family: One Day;
      }

      .express_container .shops {
        width:calc(100% - 2rem);
        padding:1rem;
        color:white;
      }
      
      .express_container .shops h1 {
    width: 100%;
    font-size: 2rem;
    font-weight: 100;
      }
      
      .express_container .shops .balls {
        width:100%;
        align-items:center;
        overflow:auto;
        margin:auto;
        
            display: flex;
    box-sizing: border-box;
    flex-direction: row;
    transform: translate3d(0px, 0px, 0px);
        margin-top:0.5rem;
        }
      
      .express_container .shops .balls::-webkit-scrollbar { 
    display: none; 
}
      
      .express_container .shops .balls .ball {
    text-align: center;
    margin: 0.5rem;
    overflow: hidden;
    position: relative;
    min-width: 6rem;
    cursor:pointer;
      }
      
      .express_container .shops .balls .ball .icon {
    font-size: 2rem;
    color: white;
    margin: 0.5rem;
        border-radius: 50px;
        padding:1rem;
        min-width:3rem;
        height:3rem;
        display:inline-flex;
        align-items:center;
        text-align:center;
        box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
      }
      
      .express_container .shops .balls .ball .icon i {
        margin:auto;
        font-family:'pe-icon-set-food';
      }
      
      .express_container .shops .balls .ball p {
        font-size:1rem;
        margin:auto;
        text-align:center;
            white-space: nowrap;
        padding:0.5rem;
      }
    
      .express_container .products_lists {
        width:100%;
        box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
        background:white;
        border-radius:5px;
      }
      
      .express_container .products_lists .products_section {
        width:calc(100% - 2rem);
        border-bottom:1px solid #eee;
        padding:1rem;
      }
      
      .express_container .products_lists .products_section h1 {
    padding: 0.5rem;
    width:calc(100% - 1rem);
    font-size: 2rem;
    font-weight: 900;
    padding-top:1rem;
      }
      
      .express_container .products_lists .products_section .products {
        width:100%;
        align-items:center;
        overflow:auto;
        margin:auto;
        
            display: flex;
    box-sizing: border-box;
    flex-direction: row;
    transform: translate3d(0px, 0px, 0px);
        }
      
      .express_container .products_lists .products_section .products::-webkit-scrollbar { 
    display: none; 
}
      
      .express_container .products_lists .products_section .products .cover {
        background-size:cover;
        background-repeat:no-repeat;
        background-image:url('https://lh6.googleusercontent.com/proxy/4jdK_vxzeaBsOMNAq_Pt8Ig1CqOvYNBTxR4kj8hCzem7pELSLZZ5khAvarH4oz8ibmgUcH_Rl8uj_9xV0zzD-4OeaqpxprGRNLcurWVb0eS6w9mham2i7RW9=w440-e365');
        width:25rem;
        height:20rem;
        min-width:25rem;
        margin-right:0.5rem;
        border-radius:5px;
      }
      
      .express_container .products_lists .products_section .products .product {
        margin:0.5rem;
        border:1px solid #eee;
        background:white;
        transition:0.5s;
        border-radius:2px;
    display: flex;
    box-sizing: border-box;
    flex-direction: column;
    min-width: 189.6px;
    width: 189.6px;
    overflow:hidden;
        font-family: Open Sans;
        font-weight:100;
      }
      
      .express_container .products_lists .products_section .products .product * {
        font-family: Open Sans;
        font-weight:100;
      }
      
      .express_container .products_lists .products_section .products .product:hover {
        box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
      }
      
      .express_container .products_lists .products_section .products .product .imagen {
        width:100%;
        height:10rem;
        background:#eee;
        background-size:cover;
        background:<?=$color_app_dd_2?>;
      }
      
      .express_container .products_lists .products_section .products .product .minidata {
    padding: 0.5rem;
    width: calc(100% - 1rem);
      }
      
      .express_container .products_lists .products_section .products .product .minidata .price {
        display:inline-flex;
        align-items:center;
        width:100%;
      }
      
      .express_container .products_lists .products_section .products .product .minidata .price p {
        font-size:1rem;
      }
      
      .express_container .products_lists .products_section .products .product .minidata .price span {
        font-size:0.8rem;
        padding-bottom:0.2rem;
        padding-left:0.2rem;
        text-decoration: line-through;
      }
      
      .express_container .products_lists .products_section .products .product .minidata .delivery .top {
        display:inline-flex;
        align-items:center;
            color: rgba(0,0,0,0.54);
    font-size: 12px;
    line-height: 16px;
      }
      
      .express_container .products_lists .products_section .products .product .minidata .delivery p {
            font-weight: bold;
    color: #0D904F;
         font-size: 12px;
    line-height: 16px;
      }
      
      .express_container .products_lists .products_section .products .product .minidata .delivery span {
        padding-left:0.2rem;
      }
      
            .express_container .products_lists .products_section .products .product button {
    box-sizing: border-box;
    position: relative;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    cursor: pointer;
    outline: none;
    border: none;
    -webkit-tap-highlight-color: transparent;
    display: inline-block;
    white-space: nowrap;
    text-decoration: none;
    vertical-align: baseline;
    text-align: center;
    margin: 0;
    min-width: 88px;
    line-height: 36px;
    padding: 0 16px;
    border-radius: 2px;
    border: 1px solid rgba(0,0,0,0.38);
    text-transform:uppercase;
    margin-top: 12px;
    background:white;
    color: rgba(0,0,0,0.54);
    margin:0.5rem;
      }
      
      .express_container .products_lists .products_section .products .product .p {
    -ms-hyphens: auto;
    -moz-hyphens: auto;
    -webkit-hyphens: auto;
    hyphens: auto;
    line-height: 1.43em;
    overflow-wrap: break-word;
    -ms-word-break: break-all;
    word-break: break-word;
    word-wrap: break-word;
    color: rgba(0,0,0,0.54);
    font-size: 14px;
    padding-left: 0.5rem;
      }
      
      @media (max-width: 75em) { 
        .express_container .products_lists {
          border-radius:0;
        }
        .express_container .products_lists .products_section .products .cover {
          width:calc(100vw - 2rem);
          min-width:calc(100vw - 2rem);
        }
        .headerbajo {
          height:100vh;
        }
        .express_container {
          width:100%;
        }
        .express_container .shops {
          padding:0;
          width:100%;
        }
        .express_container .shops h1 {
          width:calc(100% - 2rem);
          padding:1rem;
          padding-bottom:0;
        }
        .express_container .shops p {
          width:calc(100% - 2rem);
          padding:1rem;
          padding-top:0;
        }
      }
      
    </style>
  </head>
  <body>
    
    <div class="express_container" >
      
      <div class="shops" >
        <h1>Categorias</h1>
        <p>Elige lo que mas te guste</p>
        <div class="balls" >
          <div class="ball" >
              <div class="icon" style="background: -webkit-linear-gradient(top, #E91E63, #F44336);" >
                <i class="pe-is-f-burger-1"></i> 
            </div>
              <p>Hamburger</p>
          </div>
          
          <div class="ball" >
            <div class="icon" style="background: -webkit-linear-gradient(top, #3F51B5, #00BCD4);" >
              <i class="pe-is-f-pizza-2" ></i>
            </div>
            <p>Pizza</p>
          </div>
          
          <div class="ball" >
            <div class="icon" style="background: -webkit-linear-gradient(top, #E91E63, #9C27B0);" >
              <i class="pe-is-f-chinese-food-53"></i>
            </div>
              <p>Chinese Food</p>
          </div>
          
          <div class="ball" >
            <div class="icon" style="background: -webkit-linear-gradient(top, #FFC107, #FF5722);" >
              <i class="pe-is-f-hot-dog"></i>
            </div>
              <p>Hot Dog</p>
          </div>
          
          <div class="ball" >
            <div class="icon" style="background: -webkit-linear-gradient(top, #4CAF50, #E91E63);" >
              <i class="pe-is-f-kebab"></i>
            </div>
              <p>Kebab</p>
          </div>
          
        </div>
      </div>
      
      <div class="products_lists" >
        
        <?php 
        
        $nombre_seccion = "china";
        include('seccion.php'); 
        
        $nombre_seccion = "arabe";
        include('seccion.php'); 
        
        $nombre_seccion = "mexicana";
        include('seccion.php'); 
        
        ?>
      
    </div>
    
  </body>
</html>