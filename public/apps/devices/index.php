<html><head>
<style>
  
  body {
    background: <?=$color_app_dd?>;
  }

.site-wrapper {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
      -ms-flex-direction: column;
          flex-direction: column;
  width: 100%;
}

.topbar {
  padding: 8px;
  background: <?=$color_app_dd?>;
  text-align:center;
}
.topbar article,
.topbar input {
  display: inline-block;
  vertical-align: top;
}
.topbar button {
  vertical-align: top;
  border: none;
  background: transparent;
  color: #aaa;
  -webkit-transition: all 0.125s;
  transition: all 0.125s;
  padding: 12px 12px 8px;
  margin-right: 4px;
  line-height: 24px;
  font-weight: 500;
}
.topbar button .material-icons {
  font-size: 24px;
  line-height: 1;
}
.topbar button:hover, .topbar button:focus {
  color: white;
  background: rgba(0, 0, 0, 0.25);
  outline: none;
}
.topbar .navbar-right {
  margin:auto;
}

select {
  font-size: 18px;
  padding: 14px 0;
}.topbar .navbar-right
select:focus {
  color: white;
  outline: none;
}
select option {
  background: trnsparent;
}

.device-chooser {
  margin: 0;
  padding-left: 0;
}
.device-chooser li {
  display: inline-block;
  vertical-align: top;
}
.device-chooser .device-size {
  min-width: 7em;
  padding: 0 8px;
  text-align: right;
  font-size: 18px;
  line-height: 48px;
  color: #aaa;
}

.brand a .material-icons {
  position: relative;
  top: 4px;
}

.content {
  -webkit-box-flex: 1;
      -ms-flex: 1;
          flex: 1;
  position: relative;
}
.content article {
  padding: 2em;
  text-align: center;
}
.content article .device-wrapper {
    display: inline-block;
    position: relative;
    padding: 0;
    padding-bottom: 4em;
    background-color: #fff;
    border-radius: 1em;
  overflow:hidden;
    box-shadow: 2px 2px 8px 4px rgba(0, 0, 0, 0.25);
    max-width: calc(100% - 5rem);
}
.content article .device-wrapper:before {
    content: "";
    position: absolute;
    bottom: 0.8rem;
    left: 50%;
    margin-left: -1em;
    width: 2.5rem;
    height: 2.5rem;
    border-radius: 50%;
    background-color: rgb(238, 238, 238);
    background-image: url(http://img.dumdarac.com/logo/logo.png);
    background-size: 1.5rem;
    background-position: center;
    background-repeat: no-repeat;
}
.content article .device-wrapper:after {
  position: absolute;
  display: block;
  display: none;
  content: "";
  left: 2em;
  right: 2em;
  top: 2em;
  bottom: 4em;
  background: rgba(255, 255, 255, 0.25);
  z-index: 1000;
}
.content.content--maximized article {
  position: absolute;
  width: 100%;
  height: 100%;
  padding: 0;
}
.content.content--maximized article .device-wrapper {
    position: absolute;
    display: block;
    padding: 0;
    border-radius: 0;
    box-shadow: none;
    width: 100%;
    max-width: 100%;
    height: 100%;
    max-height: calc(100% - 4rem);
}
.content.content--maximized article .device-wrapper:before {
  display: none;
}
.content.content--maximized article .device-wrapper iframe {
    position: absolute;
    display: block;
    min-width: 100%;
    width: 100%;
    height: 100%!important;
    top: 0;
    bottom: 0;
    top: 0;
    bottom: 0;
}

iframe {
    border: none;
    background: #ddd;
    -webkit-transition: all 0.5s ease-in-out;
    transition: all 0.5s ease-in-out;
    max-height: calc(100% - 17rem);
    max-width: 100%;
}
</style></head><body>
<div class="site-wrapper">

<!----  <section class="topbar">

    <div class="navbar-right">

      <article>
        <button class="btn-maximize"><i class="material-icons">crop_free</i></button>
        <button class="btn-fullscreen"><i class="material-icons">open_with</i></button>
        <button class="btn-3d">3D</button>
        <button class="btn-close"><i class="material-icons">close</i></button>
      </article>
    </div>
  </section>---->

  <section class="content">
    <article>
      <div class="device-wrapper">
        <iframe src="//dumdarac.com/" ></iframe>
      </div>
    </article>
  </section>

</div>

  <script src="/resources/js/stopExecutionOnTimeout.js"></script>
  <script>(function($, window, document) {

  var breakpoints = {
    "xxs":  {"width": 320, "height": 320, "icon" : "watch"},
    "xs":   {"width": 480, "height": 640, "icon" : "phone_iphone"},
    "s":    {"width": 768, "height": 1024, "icon" : "tablet_mac"},
    "m":    {"width": 1024, "height": 768, "icon" : "laptop_mac"},
    "l":    {"width": 1200, "height": 1024, "icon" : "desktop_windows"},
    "xl":   {"width": 1600, "height": 1200, "icon" : "tv"}
  }
  
  $(function() {
    
    $('.header .PF.PF-tabbar .container').addClass('center');
    
    // The DOM is ready!

    // Membas
    
    var deviceCurrent;
    
    // DOM cache
    var $deviceChooser = $('#topbar'),
        $deviceSize = $('.device-size'),
        // $device = $('.device-wrapper'),
        $content = $('.content'),
        $iframe = $('iframe'),
        $urlBar = $('.url-bar input'),
        $btnMaximize = $('.btn-maximize');

    // URL
    
    $urlBar.on('change', function() {
      $iframe.attr("src", $urlBar.val());
    })

    $iframe.on('change', function() { // TODO
      console.log('foo');
      $urlBar.val(this.attr('src'));
    })
    
    // DEVICES
    
    $btnMaximize.on('click', function() {
      $content.toggleClass('content--maximized')
    });
    
    for(key in breakpoints){if (window.CP.shouldStopExecution(1)){break;}

      var btn = $(
        '<li class="' + 
        breakpoints[key]["icon"] + 
        '" ><i class="material-icons">' + 
        breakpoints[key]["icon"] + 
        '</i></li>');

      btn.on('click', changeDevice(key, breakpoints[key]["width"], breakpoints[key]["height"]));
      $deviceChooser.append(btn);
 
      //console.log(breakpoints[key]);
    }
window.CP.exitedLoop(1);

       
    
    function changeDevice(key, w, h) {
      return function() {
        $iframe.css('width', w + 'px').css('height', h + 'px');
        deviceCurrent = key;

        $deviceSize.text(w + 'x' + h + 'px');
      };
    }
    
    var initDeviceType = "s"
    var initDevice = changeDevice(initDeviceType, breakpoints[initDeviceType]["width"], breakpoints[initDeviceType]["height"]);
    initDevice();
    $('.header .PF.PF-tabbar li.tablet_mac').addClass('active');
    updatetabSlider();
    screendd_updateheight();
  });

}(window.jQuery, window, document));

</script>
</body></html>