<?php
$clase_id = $_GET['clase']; $clase_subp = $_GET['subp']; ?>
<div id="paginaeducacion" >
<html>
	<head>
		<style>
			<?php if($clase_id != ''){?>
			.wrapper {margin-top:0;}
			
			.header {
				background:transparent;
			}
			<?}?>
			
			.cards_educacion {
				position:relative;
    text-align: center;
    width: 100%;
			}
			
			.card_educacion {
    -webkit-box-shadow: 0 .1rem .2rem rgba(0,0,0,0.12),0 0 .1rem rgba(0,0,0,0.12);
    box-shadow: 0 .1rem .2rem rgba(0,0,0,0.12),0 0 .1rem rgba(0,0,0,0.12);
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    background-clip: padding-box;
    background-color: #fff;
    height: auto;
    margin-bottom: 2.4rem;
    margin-right: 2.4rem;
    position: relative;
    text-align: left;
    width: 100%;
    max-width: 20rem;
    min-height: 15rem;
    margin: 1rem;
    display: inline-flex;
    margin-right: 0.5rem;
    border-radius: 2px;
			}
			
			.card_educacion_header {
    width: 100%;
    height: 6rem;
    background: transparent;
    padding: 1rem;
    z-index: 1;
			}
			
			.card_educacion_header_bg {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 8rem;
    overflow: hidden;
    z-index: 0;
			}
			
			.card_educacion_header_bg_img {
    background-size: cover;
    background-position: center;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 0;
			}
			
			.card_educacion_header_bg_color {
    background-color: #DB4437;
    opacity: 0.70;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 1;
			}
			
			.card_educacion_header h1 {
				    margin: 0;
    padding: 0;
    color: white;
    font-size: 1.5rem;
    font-family: Roboto;
    font-weight: 100;
			}
			
			.card_educacion_header p {
    margin: 0;
    padding: 0;
    color: white;
    font-size: 1rem;
    font-family: Roboto;
    font-weight: 100;
    margin-top: 3.4rem;
			}
			
			.card_educacion_deberes {
    position: relative;
    width: 100%;
    position: absolute;
    left: 1rem;
    top: 8rem;
    width: 13rem;
			}
			
			.educacion_classe_header {
    width: 100%;
    height: 30rem;
    overflow: hidden;
    position: relative;
			}
			
			.educacion_classe_header_imagen {
				position:absolute;
				top:0;
				left:0;
				    width: 101%;
    height: 101%;
    background-color: #4B4B4B;
		background-size:cover;
		background-position:top;
		  -webkit-filter: blur(5px);
  -moz-filter: blur(5px);
  -o-filter: blur(5px);
  -ms-filter: blur(5px);
  filter: blur(5px);
	margin-left:-1%;
			}
			
			.capa_color_header_clase {
    background-color: #DB4437;
    opacity: 0.70;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 1;
			}
			
			.clase_info {
    text-align: center;
    position: relative;
    top: 0;
    left: 0;
    margin-top: 15rem;
    right: 0;
    width: 100%;
    height: 10rem;
    z-index: 2;
    color: white;
    padding-top: 1rem;
			}
			
			.clase_info h1 {
    color: white;
    margin: 0;
    padding: 0;
    font-family: Roboto;
    font-weight: 100;
    text-align: center;
    font-size: 3rem;
			}
			
			.table_clase {
				    width: 100%;
    max-width: 76rem;
    display: inline-table;
			}
			
			td {
    padding: 0;
			}
			
			td {
    vertical-align: top;
    margin: 0;
    padding: 0;
			}
			
			.table_clase .bloque {
    width: 15rem;
    height: auto;
    background: white;
    border-radius: 3px;
    margin: 1rem;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.26);
    min-height: 14rem;
		text-align:center;
			}
			
			.table_clase .pag_bloque {
    width: 100%;
    height: auto;
    background: white;
    border-radius: 3px;
    margin: 1rem;
    max-width: 55rem;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.26);
    min-height: 14rem;
    display: inline-block;
			}
			
			.table_clase .bloque p {
				    font-family: Roboto;
    font-weight: 100;
    font-size: 1rem;
		padding:1rem;
		padding-bottom:0.5rem;
		margin:0;
			}
			
			.table_clase .bloque .p_profesor {
    color: #929292;
    padding-top: 0;
			}
			
			.table_bloque {
				    width: 100%;
    padding: 0;
    border-spacing: 0;
    margin: 0;
    border-bottom: #EEEEEE solid 1px;
				
			}
			
			.table_bloque_avatar {
    width: 2rem;
    height: 2rem;
    border-radius: 100%;
    display: inline-block;
    float: left;
    margin: 1rem;
			}
			
			.table_bloque_nombre {
				    width: 10rem;
    padding: 1.4rem;
    display: inline-block;
    float: left;
    margin: 0;
    font-family: Roboto;
    font-weight: 100;
		padding-left:0;
			}
			
			.table_bloque_icono_left {
				background-color:teal; width:2rem; height:2rem; padding:1rem; display: inline-block; float: left;
			}
			
			.table_bloque_icono_right {
			 height:2rem; padding:1rem; display: inline-block; float: right;
			}
			
			.table_bloque_icono_right svg {
    fill: teal;
    width: 2rem;
    height: 2rem;
			}
			
			.table_bloque_icono_right p {
    padding: 0.5rem;
    margin: 0;
    float: left;
    font-family: Roboto;
    font-weight: 100;
		color:#607D8B;
			}
			
			.pag_bloque .titulo_publicacion_clase {
				font-size: 2rem;
    font-family: Roboto;
    font-weight: 100;
    margin: 1rem;
    padding: 0;
			}
			
		.pag_bloque .texto_publicacion_clase {
    margin: 0;
    padding: 0;
    font-family: Roboto;
    font-weight: 100;
    font-size: 1rem;
    margin-left: 1rem;
			}
			
			.menu_clase {
				    height: 5rem;
    width: 100%;
    background: #607D8B;
			}
			
			.menu_clase ul {
				padding: 0;
    margin: 0;
    width: 100%;
    height: 100%;
    text-align: center;
    list-style: none;
			}
			
			.menu_clase ul li {
    display: inline-block;
    color: white;
    font-size: 1rem;
    font-family: Roboto;
    font-weight: 100;
    padding: 2rem;
    padding-bottom: 1.5rem;
    padding-left: 1rem;
    padding-right: 1rem;
    margin: 0;
    text-transform: uppercase;
    border-bottom: 5px solid transparent;
		cursor:pointer;
			}
			
			.menu_clase ul li:hover {
				border-bottom: 5px solid #fff;
			}
		</style>
	</head>
	<body>
		<?php 
		if($clase_id == ''){?>
		<div class="cards_educacion" >
			
			<?php
$educacion = mysqli_query($con,"SELECT * FROM educacion_classes WHERE FIND_IN_SET( '$usuario_id', alumnos ) ORDER BY id DESC");
while($row_educacion = mysqli_fetch_array($educacion))
  { 
	$nombre_educacion = $row_educacion["asignatura"];
  $id_educacion = $row_educacion["id"];
	$profesor_educacion = $row_educacion["profesor"];
	$color_educacion = $row_educacion["color"];
	$tipo_educacion = $row_educacion["tipo"];
	$deberes_educacion = $row_educacion["deberes"];
	
	$educacion_centro = mysqli_query($con,"SELECT * FROM educacion_centros WHERE id='1' ORDER BY id DESC");
	while($row_educacion_centro = mysqli_fetch_array($educacion_centro))
  { 
	$nombre_educacion_centro = $row_educacion_centro["nombre"];
  $id_educacion_centro = $row_educacion_centro["id"];
	}
	
	$educacion_centro_profesor = mysqli_query($con,"SELECT * FROM usuarios WHERE usuario_id='$profesor_educacion'");
	while($row_educacion_centro_profesor = mysqli_fetch_array($educacion_centro_profesor))
  { 
	$nombre_educacion_centro_profesor = $row_educacion_centro_profesor["nombre"];
	$apellidos_educacion_centro_profesor = $row_educacion_centro_profesor["apellidos"];
	$usuario_educacion_centro_profesor = $row_educacion_centro_profesor["usuario_nombre"];
	$avatar_educacion_centro_profesor = $row_educacion_centro_profesor["avatar"];
	if($nombre_educacion_centro_profesor != '' and $apellidos_educacion_centro_profesor != ''){ $nombre_completo_educacion_centro_profesor = $nombre_educacion_centro_profesor . ' ' . $apellidos_educacion_centro_profesor;} else { $nombre_completo_educacion_centro_profesor = $usuario_educacion_centro_profesor;}
  $id_educacion_centro_profesor = $row_educacion_centro_profesor["usuario_id"];
	}
?>
			
			<div class="card_educacion" >
				<div class="card_educacion_header_bg" >
					<div class="card_educacion_header_bg_img" style="background-image:url('./imgs/educacion/<?=$tipo_educacion?>.jpg');" ></div><div class="card_educacion_header_bg_color" style="background-color:<?=$color_educacion?>;" ></div></div>
			<div class="card_educacion_header" >
				<a href="?app=<?=$pagina_app?>&aplicacion=<?=$aplicacion?>&clase=<?=$id_educacion?>&iconono=no" ><h1><?=$nombre_educacion?></h1></a>
				<a href="?app=<?=$pagina_app?>&aplicacion=<?=$aplicacion?>&profesor=<?=$id_educacion_centro_profesor?>&iconono=no" ><p><?=$nombre_completo_educacion_centro_profesor?></p></a>
			</div>
			<a href="?app=<?=$pagina_app?>&aplicacion=<?=$aplicacion?>&profesor=<?=$id_educacion_centro_profesor?>&iconono=no" ><img src="./usuario/avatares/<?=$id_educacion_centro_profesor?>/<?=$avatar_educacion_centro_profesor?>" style="
    width: 5rem;
    height: 5rem;
    border-radius: 100%;
    position: absolute;
    right: 1rem;
    top: 5rem;
	z-index:1;
				"></a>
<a href="?app=drive&sub_app=educacion&carpeta=/<?=$pagina_app?>/<?=$nombre_educacion?>/" ><svg xmlns="http://www.w3.org/2000/svg" fill="#000000" height="24" viewBox="0 0 24 24" width="24" style="
    position: absolute;
    right: 1rem;
    bottom: 1rem;
    fill: #4B4B4B;
    width: 2rem;
    height: 2rem;
	  cursor:pointer;
">
    <path d="M10 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2h-8l-2-2z"></path>
    <path d="M0 0h24v24H0z" fill="none"></path>
</svg></a>
				
				<p class="card_educacion_deberes" >
					<?=$deberes_educacion?>
				</p>
				
			</div>
			
			<?}?>
			
		</div>
		<?}
		
		$educacion_clase = mysqli_query($con,"SELECT * FROM educacion_classes WHERE id='$clase_id' AND FIND_IN_SET( '$usuario_id', alumnos ) LIMIT 1");
while($row_educacion_clase = mysqli_fetch_array($educacion_clase))
  { 
	$nombre_educacion_clase = $row_educacion_clase["asignatura"];
  $id_educacion_clase = $row_educacion_clase["id"];
	$profesor_educacion_clase = $row_educacion_clase["profesor"];
	$color_educacion_clase = $row_educacion_clase["color"];
	$tipo_educacion_clase = $row_educacion_clase["tipo"];
	$deberes_educacion_clase = $row_educacion_clase["deberes"];
	
	$educacion_centro_profesor_clase = mysqli_query($con,"SELECT * FROM usuarios WHERE usuario_id='$profesor_educacion_clase'");
	while($row_educacion_centro_profesor_clase = mysqli_fetch_array($educacion_centro_profesor_clase))
  { 
	$nombre_educacion_centro_profesor_clase = $row_educacion_centro_profesor_clase["nombre"];
	$apellidos_educacion_centro_profesor_clase = $row_educacion_centro_profesor_clase["apellidos"];
	$usuario_educacion_centro_profesor_clase = $row_educacion_centro_profesor_clase["usuario_nombre"];
	$avatar_educacion_centro_profesor_clase = $row_educacion_centro_profesor_clase["avatar"];
	if($nombre_educacion_centro_profesor_clase != '' and $apellidos_educacion_centro_profesor_clase != ''){ $nombre_completo_educacion_centro_profesor_clase = $nombre_educacion_centro_profesor_clase . ' ' . $apellidos_educacion_centro_profesor_clase;} else { $nombre_completo_educacion_centro_profesor_clase = $usuario_educacion_centro_profesor_clase;}
  $id_educacion_centro_profesor_clase = $row_educacion_centro_profesor_clase["usuario_id"];
	}
		?>
		
		<div class="educacion_classe_header" >
		<div class="clase_info" ><h1><?=$nombre_educacion_clase?></h1></div>
		<div class="educacion_classe_header_imagen" style="background-image:url('./imgs/educacion/<?=$tipo_educacion_clase?>.jpg');" ></div>
		<div class="capa_color_header_clase" style="background-color:<?=$color_educacion_clase?>;" ></div>
		</div>
		<div class="menu_clase" style="background-color:<?=$color_educacion_clase?>;" >
			<ul>
				<a href="?app=<?=$pagina_app?>&aplicacion=<?=$aplicacion?>&clase=<?=$id_educacion_clase?>&iconono=no&subp=" ><li style="<?php if($clase_subp == ''){?>border-bottom: 5px solid #fff;<?}?>" >Tabla de actividades</li></a>
				<a href="?app=<?=$pagina_app?>&aplicacion=<?=$aplicacion?>&clase=<?=$id_educacion_clase?>&iconono=no&subp=companeros" ><li style="<?php if($clase_subp == 'companeros'){?>border-bottom: 5px solid #fff;<?}?>" >Compañeros de clase</li></a>
				<a href="?app=<?=$pagina_app?>&aplicacion=<?=$aplicacion?>&clase=<?=$id_educacion_clase?>&iconono=no&subp=informacion" ><li style="<?php if($clase_subp == 'informacion'){?>border-bottom: 5px solid #fff;<?}?>" >Informacion</li></a>
			</ul>
		</div>
		<?php if($clase_subp == 'informacion'){?>
		<div class="aligncenter" >
			<table class="table_clase" >
				<td style="width:15rem;" >
					
				<div class="bloque" >
				<img src="./usuario/avatares/<?=$id_educacion_centro_profesor_clase?>/<?=$avatar_educacion_centro_profesor_clase?>" style="
    width: 7rem;
    height: 7rem;
    border-radius: 100%;
    margin-top: 1.6rem;
">	
					<p><?=$nombre_completo_educacion_centro_profesor_clase?></p>
					<p class="p_profesor" >Profesor</p>
				</div>
					
				</td>
				<td>

					<div class="pag_bloque" >
				
					</div>
					</td>
					<td></td>
					</table>
					</div>	
						
						<h1 class="titulo_publicacion_clase" ><?=$titulo_educacion_clase_publicacion?></h1>
						<p class="texto_publicacion_clase" ><?=$descripcion_educacion_clase_publicacion?></p>
					</div>
					
				</td>
			</table>
		</div>
	<?php } elseif($clase_subp == ''){?>
		<div class="aligncenter" >
			<table class="table_clase" >
				<td style="width:15rem;" >
					
					<div class="bloque" ></div>
					
				</td>
				<td>
					<?php
			$educacion_clase_publicacion = mysqli_query($con,"SELECT * FROM educacion_publicaciones WHERE clase_id='$id_educacion_clase' AND alumno_id='$usuario_id' ORDER BY id DESC");
while($row_educacion_clase_publicacion = mysqli_fetch_array($educacion_clase_publicacion))
  { 
	$titulo_educacion_clase_publicacion = $row_educacion_clase_publicacion["titulo"];
	$descripcion_educacion_clase_publicacion = $row_educacion_clase_publicacion["descripcion"];
  $id_educacion_clase_publicacion = $row_educacion_clase_publicacion["id"];
	$estado_educacion_clase_publicacion  = $row_educacion_clase_publicacion["estado"];
	?>
					
					
					<div class="pag_bloque" >
				<div>
					<table class="table_bloque" >
					<td>
					<div class="table_bloque_icono_left" >
					<svg xmlns="http://www.w3.org/2000/svg" fill="#FFFFFF" height="2rem" viewBox="0 0 24 24" width="2rem"> <path d="M0 0h24v24H0z" fill="none"/> <path d="M19 3h-4.18C14.4 1.84 13.3 1 12 1c-1.3 0-2.4.84-2.82 2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7 0c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1zm2 14H7v-2h7v2zm3-4H7v-2h10v2zm0-4H7V7h10v2z"/></svg>	
					</div>
						<img class="table_bloque_avatar" src="http://dumdarac.com/usuario/avatares/108/albert.isern.alvarez.16_1454753082_43.jpg" >
						<p class="table_bloque_nombre" >Albert Isern Alvarez</p>
						<?php if($estado_educacion_clase_publicacion == 'completado'){?>
						<div class="table_bloque_icono_right" >
							<p>Completado</p>
					 <svg style="fill:#0EC70E;" xmlns="http://www.w3.org/2000/svg" fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
    <path d="M0 0h24v24H0z" fill="none"/>
    <path d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z"/>
</svg>
					</div><?} elseif($estado_educacion_clase_publicacion == 'acabar'){?>
						<div class="table_bloque_icono_right" >
							<p >Sin acabar</p>
					 <svg style="fill:#F44336;" xmlns="http://www.w3.org/2000/svg" fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
    <path d="M0 0h24v24H0z" fill="none"/>
    <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm1 15h-2v-2h2v2zm0-4h-2V7h2v6z"/>
</svg>
					</div><?} elseif($estado_educacion_clase_publicacion == 'revisar'){?>
						<div class="table_bloque_icono_right" >
							<p >Sin revisar</p>
					 <svg xmlns="http://www.w3.org/2000/svg" fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
    <path d="M0 0h24v24H0zm15.35 6.41l-1.77-1.77c-.2-.2-.51-.2-.71 0L6 11.53V14h2.47l6.88-6.88c.2-.19.2-.51 0-.71z" fill="none"/>
    <path d="M20 2H4c-1.1 0-1.99.9-1.99 2L2 22l4-4h14c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zM6 14v-2.47l6.88-6.88c.2-.2.51-.2.71 0l1.77 1.77c.2.2.2.51 0 .71L8.47 14H6zm12 0h-7.5l2-2H18v2z"/>
</svg>
					</div><?}?>
					</td>
					<td></td>
					</table>
					</div>	
						
						<h1 class="titulo_publicacion_clase" ><?=$titulo_educacion_clase_publicacion?></h1>
						<p class="texto_publicacion_clase" ><?=$descripcion_educacion_clase_publicacion?></p>
					</div>
				<?}?>
					
				</td>
			</table>
		</div>
		
		<?}?>
<a href="?app=<?=$pagina_app?>&aplicacion=<?=$aplicacion?>&iconono=no" ><svg style="position:fixed; left:2rem; bottom:2rem; width:2.5rem; height:2.5rem;" xmlns="http://www.w3.org/2000/svg" fill="#607D8B" height="24" viewBox="0 0 24 24" width="24">
    <path d="M0 0h24v24H0z" fill="none"/>
    <path d="M10.09 15.59L11.5 17l5-5-5-5-1.41 1.41L12.67 11H3v2h9.67l-2.58 2.59zM19 3H5c-1.11 0-2 .9-2 2v4h2V5h14v14H5v-4H3v4c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z"/>
</svg></a>
<?}?>
		
		<script>
			
			$(function(){
				
			});
			
			$('#wrapper').scroll(function(){
        if($('#wrapper').scrollTop() < 420){
         $('.header').css("background","transparent");
      } else {
          $('.header').css("background","<?=$color_educacion_clase?>");
      }
  }); 
			
		$(document).scroll(function() {
				$(".educacion_classe_header_imagen").css("background-position-y","+" + $(document).scrollTop()/2.5 + "px");
			});
		</script>
		
	</body>
</html>
	
	</div>