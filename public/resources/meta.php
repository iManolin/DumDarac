<!-----
                                        
                                        
              `.-::::--.                
          .:+oooooossssss+:.            
        -+oooooooosssssssssso-          
      .+ooooooooossssssssssssso.        
     -ooooooooooosssssssssssssys-       
    .ooooo:-+oooso::ossssssssyyyy.      
    /oooo:  `ooss.  -sssssssyyyyyo      
    ooooo:  `osss.  :sssssssyyyyyy      
    oooooo/:osssso//sssssssyyyyyyy`     
    /oooooooosssssssssssssyyyyyyys      
    .ooooooosssssssssssssyyyyyyyys/o    
     -ooooosssssssssssssyyyyyyyyyys:    
      .+oossssssssssssssyyyyyyyys:      
        -+sssssssssssssyyyyyys/.        
          .:+sssssssssyyyo/-`           
              .-:::/::-.                
                                        
                                        
DumDarac
dumdarac.com
Wow ! We guess that you are a developer.
Now DumDarac is recruiting a CRAZY developer.
Pleases contact us via e-mail: contact@dumdarac.com
------->
<?php
$cache_version = file_get_contents(__DIR__.'/../version.txt');
?>
<link rel="dns-prefetch" href="https://dumdarac.com">
<link rel="dns-prefetch" href="https://img.dumdarac.com">
<link rel="preconnect" href="https://dumdarac.com">
<link rel="preconnect" href="https://img.dumdarac.com">
<meta name="google-site-verification" content="L0T0_6VCNrT3slPROGy2XjzhW8lqVCYTBZ-l5XnEm1U" />
<meta http-equiv="content-language" content="<?=$language?>" />
<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
<link href="https://dumdarac.com/" rel="canonical">
<meta name="google" content="notranslate" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta property="og:type" content="website">
<link rel=”alternate” hreflang=”en” href=”//dumdarac.com/l/en” >
<link rel=”alternate” hreflang=”es” href=”//dumdarac.com/l/es” >
<link rel=”alternate” hreflang=”fr” href=”//dumdarac.com/l/fr” >
<link rel=”alternate” hreflang=”ca” href=”//dumdarac.com/l/ca” >
<link rel=”alternate” hreflang=”ar” href=”//dumdarac.com/l/ar” >
<link rel=”alternate” hreflang=”tr” href=”//dumdarac.com/l/tr” >
<link rel=”alternate” hreflang=”ku” href=”//dumdarac.com/l/ku” >
<link rel=”alternate” hreflang=”gr” href=”//dumdarac.com/l/gr” >
<?php if(isset($_COOKIE['darkmode'])){?>
<meta name="theme-color" content="#000000">
<?} else {?>
<meta name="theme-color" content="#ffffff">
<?}?>

<!--------SOCIAL----------->

<!-------TWITTER-------->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@DumDarac">

<!------ meta otros------->
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/style.css?version=<?=$cache_version?>">
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/dumdarac-internal.css?version=<?=$cache_version?>">
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/followbutton.css?version=<?=$cache_version?>" async>
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/more-less.css?version=<?=$cache_version?>" async>
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/memories.css?version=<?=$cache_version?>" async>
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/filters.css?version=<?=$cache_version?>" async>
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/animate.css?version=<?=$cache_version?>" async>
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/PFWalk.css?version=<?=$cache_version?>" async>
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/paperflower/colors.css?version=<?=$cache_version?>">
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/paperflower.css?version=<?=$cache_version?>">
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/paperflower/page.css?version=<?=$cache_version?>">
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/paperflower/cards.css?version=<?=$cache_version?>">
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/paperflower/select.css?version=<?=$cache_version?>">
  <link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/paperflower/button.css?version=<?=$cache_version?>">
  <link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/paperflower/checkbox.css?version=<?=$cache_version?>">
  <link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/paperflower/radio.css?version=<?=$cache_version?>">
  <link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/paperflower/switch.css?version=<?=$cache_version?>">
  <link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/paperflower/progress.css?version=<?=$cache_version?>">
  <link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/paperflower/textfield.css?version=<?=$cache_version?>">
  <link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/paperflower/grid.css?version=<?=$cache_version?>">
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/paperflower/chips.css?version=<?=$cache_version?>">
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/paperflower/collapsible.css?version=<?=$cache_version?>">
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/paperflower/collection.css?version=<?=$cache_version?>">
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/feed-multisource.css?version=<?=$cache_version?>">
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/smart-cards.css?version=<?=$cache_version?>">
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/reactions.css?version=<?=$cache_version?>" async>
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/highlight.css?version=<?=$cache_version?>" async>
<link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/glance.css?version=<?=$cache_version?>" async>