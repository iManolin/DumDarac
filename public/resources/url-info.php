<?php

function urlinfo($url, $tagname)
{
  $ch = curl_init();
  
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  
  $html = curl_exec($ch);
  curl_close($ch);
  
  $doc = new DOMDocument();
  @$doc->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
  $nodes = $doc->getElementsByTagName('title');
  
  $title = $nodes->item(0)->nodeValue;
  $metas = $doc->getElementsByTagName('meta');
  
  for ($i = 0; $i < $metas->length; $i++)
  {
    $meta = $metas->item($i);
    if($meta->getAttribute('name') == 'description')
      $description = $meta->getAttribute('content');
    if($meta->getAttribute('property') == 'og:image')
      $image = $meta->getAttribute('content');
    if($meta->getAttribute('name') == 'keywords')
      $keywords = $meta->getAttribute('content');
  }
  
  switch ($tagname) {
    case 'title':
        return $title;
        break;
      case 'description':
        return $description;
        break;
      case 'image':
        return $image;
        break;
      case 'keywords':
        return $keywords;
        break;
      case 'domain':
        return preg_replace('#^(http(s)?://)?w{3}\.#', '$1', $domain);
        break;
}
  
}

?>