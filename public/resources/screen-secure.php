<html>

<head>
  <style>
    .screensecure {
    cursor: pointer;
    color: var(--PF-color-original-default);
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    min-height: 100%;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    width: 100%;
    height: 100%;
    position: fixed;
    left: 0px;
    top: 0px;
    display: none;
    z-index: 99999;
      background-color:var(--PF-color-bg-second-default);
    }

    .screensecure.show {
      display: flex;
    }

    .screensecure * {
      transition: 0.1s;
    }

    .screensecure .tab {
    text-align: left;
    margin-bottom: 3rem;
    position: relative;
    color: var(--PF-color-original-default);
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    width: 13rem;
    height: 10rem;
    border: 0.3rem solid var(--PF-color-original-default);
    border-radius: 1em;
    display: grid;
    grid-template-rows: 2.2rem 1fr;
    background: var(--PF-color-semitransparent-default);
    }

    .screensecure .bar {
      position: relative;
      border-bottom: 0.3rem solid var(--PF-color-original-default);
      font-size: 2.6rem;
      padding-left: 0.5rem;
      letter-spacing: 0.3rem;
      line-height: 1.4rem;
    }


    .screensecure .zzz {
      position: absolute;
      top: -4rem;
      right: 1rem;
      height: 1.3rem;
      width: 3.5rem;
      transition: all 0.2s ease;
      opacity: 1;
    }

    .screensecure .zzz>span {
      font-weight: 900;
      position: absolute;
      width: 0;
      line-height: 0;
    }

    .screensecure .zzz>span:nth-of-type(1) {
      font-size: 1.8rem;
      top: 100%;
      left: 0;
    }

    .screensecure .zzz>span:nth-of-type(2) {
      font-size: 1.5rem;
      top: 50%;
      left: 50%;
    }

    .screensecure .zzz>span:nth-of-type(3) {
      font-size: 1.2rem;
      top: 0;
      left: 90%;
    }

    .screensecure .tab .spinner {
      position: absolute;
      top: 0.3rem;
      right: 0.5rem;
      height: 1.5rem;
      width: 1.5rem;
      opacity: 0;
    }

    .screensecure .tab .spinner::before {
      top: -0.1rem;
      left: 0;
      margin: 0;
      content: '';
      box-sizing: border-box;
      position: absolute;
      width: 100%;
      height: 100%;
      border-radius: 50%;
      border: 2px solid transparent;
      border-top-color: var(--PF-color-original-default);
      border-right-color: var(--PF-color-original-default);
      -webkit-animation: spinner 0.6s linear infinite;
      animation: spinner 0.6s linear infinite;
    }

    .screensecure.waking .eye-closed,
    .screensecure.waking .zzz {
      opacity: 0;
      -webkit-transform: translateY(-10px);
      transform: translateY(-10px);
    }

    .screensecure.waking .mouth {
      background: var(--PF-color-bg-first-default);
    }

    .screensecure.waking .eye-open {
      display: block;
      -webkit-transform: rotateX(0);
      transform: rotateX(0);
    }

    .screensecure.waking .spinner {
      opacity: 1;
    }

    .screensecure.waking .textWrap {
      opacity: 0;
    }

    .screensecure .instructions {
    font-weight: bold;
    font-size: 16px;
    margin-bottom: 0.5rem;
    text-transform: uppercase;
    }

    .screensecure .shortcut {
      font-size: 15px;
    }

    @-webkit-keyframes spinner {
      to {
        -webkit-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }

    @keyframes spinner {
      to {
        -webkit-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }
  </style>
</head>

<body class="">

  <div id="screensecure" class="screensecure">
    <div class="tab">
      <div class="bar">
        •••
        <div class="spinner"></div>
      </div>
      <div class="zzz">
        <span class="z">Z</span>
        <span class="z">Z</span>
        <span class="z">Z</span>
      </div>
    </div>

    <div class="textWrap" style="text-align: center;">
      <div class="instructions">
        Refresh or click to reload
      </div>
      <div class="shortcut">DumDarac ScreenSecure</div>
    </div>
  </div>

  <script>
    $(document).on("click", ".screensecure", function(event) {
      $('.screensecure').addClass('waking');
      setTimeout(function() {
        stop_screensecure();
        $('.screensecure').removeClass('waking');
      }, 1500);
    });
  </script>

  <script>
    var mousetimeout;
    var screensecure_active = false;
    var idletime = 5;

    function show_screensecure() {
      $('#screensecure').addClass('show');
      screensecure_active = true;
    }

    function stop_screensecure() {
      $('#screensecure').removeClass('show');
      screensecure_active = false;
    }

    $(document).mousemove(function() {
      clearTimeout(mousetimeout);
      mousetimeout = setTimeout(function() {
        show_screensecure();
      }, 1000 * idletime); // 5 secs			
    });
  </script>
</body>

</html>