<?php
$usuario_mismo_weather = mysqli_query($con,"SELECT currently FROM weather WHERE usuario_id='$usuario_mismo_id' LIMIT 1");
while($row_usuario_mismo_weather = mysqli_fetch_array($usuario_mismo_weather))
  {

    $usuario_mismo_weather_array = json_decode($row_usuario_mismo_weather['currently'], true);
    $usuario_mismo_weather_temperature = $usuario_mismo_weather_array['temperature'];
    $usuario_mismo_weather_temperature = substr($usuario_mismo_weather_temperature, 0, strpos($usuario_mismo_weather_temperature, "."));
    $usuario_mismo_weather_icon = $usuario_mismo_weather_array['icon'];
    $usuario_mismo_weather_icon_extension = ".png";
    if (in_array($usuario_mismo_weather_icon, array("clear-day", "partly-cloudy-day", "rain", "moon", "thunderstorms", "clear-night"))) {
      $usuario_mismo_weather_icon_extension = ".svg";
    }
  }
?>

<div class="PF glance block" data-id="<?=$_POST['id']?>">
  <div class="container">
    <h1 class="up text"></h1>
    <div class="down" ><p opendd-href="?p=genius&section=glance"><span class="date"></span></p><?php if($usuario_mismo_weather_temperature){?><p opendd-href="?app=weather"><span class="weather-icon" style="background-image: url('//img.dumdarac.com/weather/<?=$usuario_mismo_weather_icon.$usuario_mismo_weather_icon_extension?>');"></span><span><?=$usuario_mismo_weather_temperature?>ºC</span></p><?}?></div>
  </div>
</div>