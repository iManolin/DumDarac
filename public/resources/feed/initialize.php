<script>
  $.fn.isFeedPartiallyInViewport = function() {
    if($(this).length){
      var elementTop = $(this).offset().top;
      var elementBottom = elementTop + $(this).outerHeight();

      var viewportTop = $(window).scrollTop();
      var viewportBottom = viewportTop + $(window).height();

      return elementBottom > viewportTop && elementTop < viewportBottom;
    }
  };

  $.initialize("[feed]", function() {
    var thisfeed = $(this);
    var id = thisfeed.attr("data-id");
    var type = thisfeed.attr("data-type");
    var unique = null;
    unique = thisfeed.attr("data-unique");
    if (!unique) {
      unique = Math.floor((Math.random() * 1000000) + 1000);
    }
    var classes = thisfeed.attr("data-classes");
    var columns = thisfeed.attr("data-columns");
    var hideglance = thisfeed.attr("data-hideglance");
    var currenturl = thisfeed.attr("data-currenturl");
    var hideform = thisfeed.attr("data-hideform");
    if (thisfeed.attr("data-type") !== null) {
      xhr.push($.post("./resources/feed/feed.php" + currenturl, {
          id: id,
          type: type,
          unique: unique,
          classes: classes,
          columns: columns,
          hideglance: hideglance,
          currenturl: currenturl,
          hideform: hideform,
          reset: true
        })
        .done(function(data) {
          thisfeed.replaceWith(data);
          cargarmasfeed($(".feed-multisource[data-unique='" + unique + "']"));
          $(window).on('resize scroll', function() {
            if ($(".feed-multisource[data-unique='" + unique + "'] .block").last().isFeedPartiallyInViewport()) {
              cargarmasfeed($(".feed-multisource[data-unique='" + unique + "']"));
            }
          });
        }) );
    }
  });

  function cargarmasfeed(thisfeed) {
    var unique = null;
    unique = thisfeed.attr("data-unique");
    if (!thisfeed.attr("data-allowloadmore")) {
      thisfeed.attr("data-allowloadmore", "no");
      var currenturl = thisfeed.attr("data-currenturl");
      if(!$(".feed-multisource[data-unique='" + unique + "'] .no-content").is(':visible')){
        $('#loading').show();
        $(".feed-multisource[data-unique='" + unique + "'] .loading-content").fadeIn();
      }

      xhr.push( $.post("./resources/feed/block.php" + currenturl, {
          unique: unique,
          currenturl: currenturl,
        })
        .done(function(data) {
          $('#loading').hide();
          $(".feed-multisource[data-unique='" + unique + "'] .loading-content").hide();
          if (data) {
            $(".feed-multisource[data-unique='" + unique + "'] .no-content").fadeOut();
            $(".feed-multisource[data-unique='" + unique + "'] .blocks").append(data);
          } else {
            if(!$(".feed-multisource[data-unique='" + unique + "'] .no-content").is(':visible')){
              $(".feed-multisource[data-unique='" + unique + "'] .no-content").fadeIn();
            }
          }
          setTimeout(function(){ thisfeed.attr("data-allowloadmore", null); }, 250);
        }) );
    }
  }

  $.initialize("[post]", function() {
    var post = $(this);
    var id = post.attr("data-id");
    xhr.push( $.post("./resources/feed/items/post/post.php", { post: id }).done(function(data) {
      post.replaceWith(data);
    }) );
  });



  //POSTS
  function feeddeletepost(id) {
    $.get("./resources/feed/feeds/posts/resources/delete.php?id=" + id, function(data, status) {
      eval(data);
    });
  }

  function feedrestorepost(id) {
    $.get("./resources/feed/feeds/posts/resources/restore.php?id=" + id, function(data, status) {
      eval(data);
    });
  }

  function feedrepostpost(id) {
    $.get("./resources/feed/feeds/posts/resources/repost.php?id=" + id, function(data, status) {
      eval(data);
    });
  }
</script>