<?php

include_once(__DIR__.'/feeds-loaded.php');

$feed_unique = $_POST['unique'];

if(!$app and ($usuario_mismo_id or $usuario_id === '1' or $usuario_id === '639')){
include_once(__DIR__.'/feeds/posts/posts.php');
}

if($pagina == '' or $q or $pagina == 'topics' or $app == 'news'){
include_once(__DIR__.'/../../apps/news/feed/news.php');
}

include_once(__DIR__.'/feeds/smart-cards/smart-cards.php');

//include(__DIR__.'/feeds/twitter/twitter.php');

$feed_query = mysqli_query($con,"SELECT * FROM feed t1 WHERE feed_unique='$feed_unique' AND usuario_id='$usuario_mismo_id' ORDER BY weight DESC LIMIT 20");

if(mysqli_num_rows($feed_query)>0){?>
<div class="block">
	<?php
	$ids_feedsloaded_insert = array();
  while($row_feed = mysqli_fetch_array($feed_query)){
    $feed_item_class = null;
    $feed_item_userid = $row_feed["userid"];
    $feed_item = $row_feed["item"];
    $feed_item_id = $row_feed["item_id"];
    $feed_name = $row_feed["feed"];
    $feed_item_type = $row_feed["type"];
    $feed_item_title = $row_feed["title"];
    $feed_item_title = replaceemojis($feed_item_title);
    $feed_item_text = $row_feed["text"];
    $feed_item_text = replaceemojis($feed_item_text);
    $feed_item_text_links = $row_feed["text"];
    $feed_item_images = $row_feed["images"];
    if($feed_item_images){ list($feed_item_images_size_width, $feed_item_images_size_height) = getimagesize($feed_item_images); }
    $feed_item_source = $row_feed["source"];
    $feed_item_date = $row_feed["date"];
    $feed_item_contentid = $row_feed["contentid"];
    $feed_item_reply = $row_feed["reply"];

    if(!$feed_item_type){
      $feed_item_text = preg_replace("/https?\:\/\/[^\" ]+/i", "", $feed_item_text);
    }

    if($feed_name == 'news'){
      if($feed_item_images and $feed_item_images_size_width < 500){ $feed_item_class .= " flex "; }
      $feed_item_title_onclick = "windowdd('./apps/news/resources/article.php?id=" . $feed_item_id . "', 'fit');";
      $feed_item_image_onclick = $feed_item_title_onclick;
      $feed_item_text_onclick = $feed_item_title_onclick;
      $feed_item_span_onclick = "openddgo('?p=topics&topic=".$feed_item_source."');";
    }

      addfeedloaded($usuario_mismo_id, $feed_unique, $feed_name, $feed_item_id);

      switch ($feed_item) {
        case "post":
          include(__DIR__."/items/post/post-template.php");
          break;
        case "fullcoverage":
          include(__DIR__."/items/fullcoverage/fullcoverage.php");
          break;
        case "smart-card":
          include(__DIR__."/items/smart-cards/$feed_item_type.php");
          break;
        case "informative-cards":
          include(__DIR__."/items/informative-cards/card.php");
          break;
        default:
          include(__DIR__."/items/card/card.php");
    }

  }

mysqli_query($con, "DELETE FROM feed WHERE usuario_id='$usuario_mismo_id' AND feed_unique='$feed_unique'");

  ?>
</div>
<?}?>