<?php

preg_match_all('/https?\:\/\/[^\" ]+/i', $feed_item_text_links, $match_link_p);
if($match_link_p){
$links_implode = implode("' AND url LIKE '", $match_link_p[0]);
$links_query = mysqli_query($con, "SELECT * FROM links WHERE url LIKE '$links_implode' ORDER BY date DESC LIMIT 1");
  while($row_links = mysqli_fetch_array($links_query)){
  	$links_item_url = $row_links["url"];
  	$links_item_url_domain = str_ireplace('www.', '', parse_url($links_item_url, PHP_URL_HOST));
    $links_item_title = $row_links["title"];
    $links_item_description = $row_links["description"];
    $links_item_image = $row_links["image"];
    ?>
		<a class="link PF shadow" href="<?=$links_item_url?>" target="_blank">
			<?php if($links_item_url){?><div class="PF PF-image" style="background-image: url('<?=$links_item_image?>');"></div><?}?>
			<div class="data">
				<?php if($links_item_url_domain){?><div class="source"><p><?=$links_item_url_domain?></p></div><?}?>
				<?php if($links_item_title){?><h1><?=$links_item_title?></h1><?}?>
			</div>
		</a>
<?}
}?>