<div class="card <?=$feed_item_class?>">
  <div class="content" <?php if($feed_item_content_onclick){?>onclick="<?=$feed_item_content_onclick?>"<?}?>>
    <?php if($feed_item_images){?><div class="PF PF-image" style="background-image: url('<?=$feed_item_images?>');" <?php if($feed_item_image_onclick){?>onclick="<?=$feed_item_image_onclick?>"<?}?>></div><?}?>
    <div class="data">
      <div class="container"><span <?php if($feed_item_span_onclick){?>onclick="<?=$feed_item_span_onclick?>"<?}?>><?php if($feed_item_author){ echo $feed_item_author; } if($feed_item_author and $feed_item_source){ echo " · "; } if($feed_item_source){ echo $feed_item_source; }?></span></div>
      <?php if($feed_item_title){?><div class="container title" <?php if($feed_item_title_onclick){?>onclick="<?=$feed_item_title_onclick?>"<?}?>><h1><?=$feed_item_title?></h1></div><?}?>
      <?php if($feed_item_text){?><div class="container text" <?php if($feed_item_text_onclick){?>onclick="<?=$feed_item_text_onclick?>"<?}?>><p><?=$feed_item_text?></p></div><?}?>
    </div>
  </div>
  <div class="footer">
    <?php if($feed_item_date){?><div class="container"><span prettydate><?=$feed_item_date?></span></div><?}?>
    <div class="space"></div>
    <div more-less data-type="<?=$feed_name?>" data-id="<?=$feed_item_id?>" data-content="<?=$feed_item_title?> <?=$feed_item_text?>" ></div>
    <div class="PF PF-icon ripple"><i class="material-icons">more_vert</i></div>
  </div>
</div>