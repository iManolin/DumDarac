<?php
function readfeedloaded($usuario_mismo_id, $feed_unique, $feed_name){
	if($usuario_mismo_id){
		$readfeedsloaded = $_SESSION["users"]["$usuario_mismo_id"]['feed'][$feed_unique]['loaded'][$feed_name];
	}
	return implode("','", $readfeedsloaded);
}

function addfeedloaded($usuario_mismo_id, $feed_unique, $feed_name, $ids){
	if($ids and $usuario_mismo_id){
		$_SESSION['users']["$usuario_mismo_id"]['feed'][$feed_unique]['loaded'][$feed_name][] = $ids;
	}
}
?>