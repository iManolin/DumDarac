<?php
  $feed_unique = $_POST['unique'];
  $feedsloaded_date_delete = date("Y-m-d", strtotime($datetime));

  if($usuario_mismo_id){
    $_SESSION["users"]["$usuario_mismo_id"]['feed'][$feed_unique]['loaded'] = array();
  }
  
  mysqli_query($con, "DELETE FROM feed WHERE usuario_id='$usuario_mismo_id' ");
  mysqli_query($con, "DELETE FROM feeds_loaded WHERE usuario_id='$usuario_mismo_id' AND date<'$feedsloaded_date_delete' ");
?>

<div class="feed-multisource <?=$_POST['classes']?>" data-unique="<?=$_POST['unique']?>" data-currenturl="<?=$_POST['currenturl']?>">
  <?php if($usuario_mismo_id and (!$q and $pagina !== 'search' and !$_POST['hideform']) and ($pagina == 'perfil' and $usuario_mismo_id == $usuario_id or $pagina != 'perfil')){?>
    <div class="PF new-post-smallform shadow ripple" opendd-href="?p=new-post<?php if($topic){?>&topic=<?=$topic?><?}?>">
      <div class="PF-avatar ripple ddbg-avatar" rgdd></div>
      <p>
        <?php include(__DIR__."/../../pantallas/new-post/resources/text-form.php"); ?>
      </p>
      <div class="PF PF-icon ripple" opendd-href="?p=new-post&action=add-image"><i class="material-icons">image</i></div>
    </div>
  <?}?>
  <div class="blocks"></div>
    <div class="PF shadow feed-card loading-content">
      <div class="cover contain" style="background-image: url('//img.dumdarac.com/smart-cards/feed/loading.gif');"></div>
      <div class="head">
        <?php if($pagina === null or $app){?>
          <h1 t-dd>Generating your content</h1>
        <?} else {?>
          <h1 t-dd>Loading content</h1>
        <?}?>
      </div>
    </div>
    <div class="PF shadow feed-card no-content">
      <div class="cover contain" style="background-image: url('//img.dumdarac.com/smart-cards/feed/empty.png'); margin-top: 2em;"></div>
      <div class="head">
        <h1 t-dd>There's nothing to show</h1>
      </div>
    </div>
</div>