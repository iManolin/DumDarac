<html>

<head>
  <style>
    .PF-window .container.avatarbgcontainer {
      position: relative;
      overflow: hidden;
      background-color: var(--PF-color-bg-first-default);
    }

    .avatar-upload {
      position: relative;
      max-width: 10rem;
      margin: auto;
      text-align: center;
    }

    .avatar-upload .avatar-edit {
    position: absolute;
    right: 0.5rem;
    z-index: 1;
    bottom: 0.5rem;
    border-radius: 2em;
    background: rgb(var(--PF-color-primary));
    color: rgb(var(--PF-color-surface));
    padding: 0.5rem;
    box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .16), 0 2px 5px 0 rgba(0, 0, 0, .23);
    transition: 0.5s;
    cursor: pointer;
    }

    .avatar-upload .avatar-edit i {
      cursor: pointer;
    }

    .avatar-upload .avatar-edit:hover {
      box-shadow: 0 10px 10px 0 rgba(0, 0, 0, .19), 0 6px 3px 0 rgba(0, 0, 0, .23);
    }

    .avatar-upload .avatar-edit input {
      display: none;
    }

    .avatar-upload .avatar-edit input+label {
      display: inline-block;
      width: 2rem;
      height: 2rem;
      margin-bottom: 0;
      border-radius: 100%;
      cursor: pointer;
      transition: 0.5s;
    }

    .avatar-upload .avatar-edit input+label {
      text-align: center;
      margin: auto;
      display: inline-flex;
      align-items: center;
      text-align: center;
    }

    .avatar-upload .avatar-edit input+label i {
      margin: auto;
    }

    .avatar-upload .avatar-preview {
    width: 10rem;
    height: 10rem;
    position: relative;
    border-radius: 5em;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
    overflow: hidden;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    }
  </style>
</head>

<body>
  <div class="PF PF-toolbar">
    <div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
    <h1 t-dd>Change avatar</h1>
  </div>

  <div class="content">
    <div class="container full middle avatarbgcontainer">
      <div class="avatar-upload">
        <div class="avatar-edit">
          <form id="subiravatar" method="post" enctype="multipart/form-data" action="./pantallas/davatar/update.php">
            <input name="files[]" accept="image/*;capture=camera" type="file" id="imageUpload">
          </form>
          <label for="imageUpload"><i class="material-icons">&#xE2C3;</i></label>
        </div>
        <div class="avatar-preview ddbg-avatar imagePreview" id="imagePreview"></div>
      </div>
    </div>
    <div class="container">
      <div class="PF PF-buttons full">
        <button class="PF PF-button" onclick="$('#subiravatar').submit();" t-dd>
          <div class="inside">
            <p t-dd>Upload</p>
          </div>
        </button>
      </div>
    </div>
  </div>

  <script>
    
    
    $("#subiravatar").ajaxForm(function(data) {
      $('#ddwindow').removeClass('open');
      $('.closewindowdd').removeClass('show');
      $('body').removeClass('overflowhidden');
      if(data){
        $("#ddbg-avatar").load('./resources/ddbg-avatar.php');
        $(".ddbg-avatar").css("background-image", "");
        $(".ddbg-avatar").attr("data-bg", "");
        alertdd.show('Avatar updated correctly');
      } else {
        alertdd.show('The avatar cannot be updated');
      }
    });
    
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('.imagePreview').css('background-image', 'url(' + e.target.result + ')');
          $('.imagePreview').hide();
          $('.imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#imageUpload").change(function() {
        readURL(this);
      }

    );
  </script>
</body>

</html>