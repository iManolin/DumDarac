<html><head>
<style>.container {
  text-align: center;
}

.tooltip {
  position: absolute;
  padding: 5px;
  color: #006400;
  background-color: rgba(255, 255, 255, 0.9);
  border: 1px solid #006400;
  border-radius: 5px;
  pointer-events: none;
  font-family: sans-serif;
  font-size: 12px;
  text-align: left;
}
.tooltip > h3 {
  margin: 5px 0;
}

.node {
  stroke: #333;
  stroke-width: 1px;
}

.link {
  stroke-width: 1.5px;
}
</style></head><body>

<div class="container" id="root"></div>
  <script src="/resources/js/jquery-3.3.1.js"></script>
  <script src="/resources/js/jquery-ui.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.17/d3.min.js"></script>
  <script> /* global d3 */

var width = 1000;
var height = 800;

var color = d3.scale.category20b();

var force = d3.layout.force().
charge(-100).
linkDistance(function (d) {return d.linkDistance;}).
gravity(0.05).
size([width, height]);

var svg = d3.select('#root').append('svg').
attr('width', width).
attr('height', height);

var tooltip = d3.select('#root').append('div').
attr('class', 'tooltip').
style('opacity', 0);

var defs = svg.append('defs');

d3.json('https://dumdarac.com/resources/d3/data/users.php', function (error, data) {
  if (error) console.error(error);
  var usuario_nombreIndex = {};
  var nodes = [{ usuario_nombre: 'DumDarac', avatar: 'https://dumdarac.com/imgs/logo/logo.png', usuario_id: data.usuario_id, value: 25 }];
  var links = [];
  
  
  
  
  
  

  // build nodes and links
  data.forEach((d) => {
    const nodesLength = nodes.length;
    if (d.usuario_nombre in usuario_nombreIndex) {
      nodes[usuario_nombreIndex[d.usuario_nombre]].value += 1;
      links.push({ source: nodesLength, target: usuario_nombreIndex[d.usuario_nombre],
        linkDistance: 50, stroke: color(usuario_nombreIndex[d.usuario_nombre]) });
    } else {
      usuario_nombreIndex[d.usuario_nombre] = nodesLength;
        nodes.push({ usuario_nombre: d.usuario_nombre, avatar: null, value: 1 });
      links.push({ source: nodesLength, target: 0, linkDistance: 200, stroke: color(usuario_nombreIndex[d.usuario_nombre]), opacity: 0.5, strokeWidth: 1 });
    }
  });

  force.
  nodes(nodes).
  links(links).
  start();

  var link = svg.selectAll('.link').
  data(links).
  enter().append('line').
  attr('class', 'link');

  var node = svg.selectAll('.node').
  data(nodes).
  enter().append('a').
  attr('xlink:href', function (d) {return d.url;}).
  attr('target', '_blank').
  append('g').
  attr('class', 'node').
  call(force.drag);


  // create patterns to fill circles with avatars
  nodes.filter(function (d) {return d.avatar;}).forEach(function (d) {
    var r = Math.pow(d.value * 2000, 1 / 3);
    var size = r * 2;
    defs.append('pattern').
    attr('id', d.usuario_nombre.replace(/ /g, '_')).
    attr('patternUnits', 'userSpaceOnUse').
    attr('width', size).
    attr('height', size).
    attr('x', -r).
    attr('y', -r).
    append('image').
    attr('xlink:href', d.avatar).
    attr('width', size).
    attr('height', size).
    attr('x', 0).
    attr('y', 0);
  });

  // append circle filled with image pattern for avatars or color
  node.append('circle').
  attr('r', function (d) {return Math.pow(d.value * 2000, 1 / 3) || 5;}).
  style('fill', function (d) {return (
      d.avatar ? 'url(#' + d.usuario_nombre.replace(/ /g, '_') + ')' : color(usuario_nombreIndex[d.usuario_nombre]));}).
  on('mouseover', function (d) {
    tooltip.transition().
    style('opacity', 1);
    tooltip.html('<h3>' + d.usuario_nombre + '</h3>' + (d.title ? '<span>' + d.title + '</span>' : '')).
    style('left', d3.event.pageX + 20 + 'px').
    style('top', d3.event.pageY - 30 + 'px');
  }).
  on('mouseout', function () {
    tooltip.transition().
    style('opacity', 0);
  });

  force.on('tick', function () {
    link.attr('x1', function (d) {return d.source.x;}).
    attr('y1', function (d) {return d.source.y;}).
    attr('x2', function (d) {return d.target.x;}).
    attr('y2', function (d) {return d.target.y;}).
    style('stroke', function (d) {return d.stroke;}).
    style('opacity', function (d) {return d.opacity;}).
    style('stroke-width', function (d) {return d.strokeWidth;});

    node.attr('transform', function (d) {return 'translate(' + d.x + ', ' + d.y + ')';});
  });
}); // end d3.json
</script>
</body></html>