<script>

	var thismorelessbuttontemplate;
	$.post("./resources/more-less/button.php").done(function(data) {
		thismorelessbuttontemplate = data;
	});

	$.initialize("[more-less]", function() {
		var thismoreless = $(this);
		var id = thismoreless.attr("data-id");
		var type = thismoreless.attr("data-type");
		var content = thismoreless.attr("data-content");
		if (thismoreless.attr("data-id") != null && thismoreless.attr("data-type") != null) {
			thismoreless.replaceWith(function() {
				return $(thismorelessbuttontemplate).attr('data-id', id).attr('data-type', type).attr('data-content', content);
			});
		}
	});
/*
	$.initialize(".more-less", function() {
		morelessstate($(this));
	});*/

	$(document).on('click', '.more-less', function(e) {
		if($(this).hasClass('more-less') && $(this).hasClass('reset') && !$(this).hasClass('open')){
			morelessupdate($(this), 'reset');
		}
		$(this).toggleClass('open');
		e.preventDefault();
	});

	$(document).on('click scroll', function(e) {
		if(!$(e.target).closest('.more-less').length){
			$('.more-less.open').removeClass('open');
		}
	});

	$(document).on('click', '.more-less>.both>ul>li.more', function(e) {
		morelessupdate($(this).closest('.more-less'), 'more');
		e.preventDefault();
	});

	$(document).on('click', '.more-less>.both>ul>li.less', function(e) {
		morelessupdate($(this).closest('.more-less'), 'less');
		e.preventDefault();
	});

	function morelessupdate(thismoreless, moreless) {
		if(moreless !== 'reset'){
			$('#loading').show();
		}
		var id = thismoreless.attr("data-id");
		var type = thismoreless.attr("data-type");
		var content = thismoreless.attr("data-content");
		$.post("./resources/more-less/update-data.php", {
				id: id,
				type: type,
				moreless: moreless,
				content: content
			})
			.done(function(data) {
				if (data === false) {
					thismoreless.removeClass("more");
					thismoreless.removeClass("less");
				} else {
					morelesschangestate(thismoreless, data);
				}
				if(moreless !== 'reset'){
					$('#loading').hide();
					}
			});
	}

	function morelesschangestate(thismorelessbtn, moreless) {
		thismorelessbtn.removeClass("less");
		thismorelessbtn.removeClass("more");
		if (moreless == "more") {
			thismorelessbtn.addClass("more");
			alertdd.show('More like that coming your way');
		} else if (moreless == "less") {
			thismorelessbtn.addClass("less");
			thismorelessbtn.closest('.card, .post').hide();
			alertdd.show('You will get less like that');
		}
	}

</script>