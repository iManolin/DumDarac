<html><head>
<style>
  
@font-face {
  font-family: 'Material Icons';
  font-style: normal;
  font-weight: 400;
  src: url(../../fuentes/MaterialIcons/MaterialIcons-Regular.eot); /* For IE6-8 */
  src: local('Material Icons'),
       local('MaterialIcons-Regular'),
       url(../../fuentes/MaterialIcons/MaterialIcons-Regular.woff2) format('woff2'),
       url(../../fuentes/MaterialIcons/MaterialIcons-Regular.woff) format('woff'),
       url(../../fuentes/MaterialIcons/MaterialIcons-Regular.ttf) format('truetype');
}

.material-icons {
  font-family: 'Material Icons';
  font-weight: normal;
  font-style: normal;
  font-size: 24px;  /* Preferred icon size */
  display: inline-block;
  line-height: 1;
  text-transform: none;
  letter-spacing: normal;
  word-wrap: normal;
  white-space: nowrap;
  direction: ltr;

  /* Support for all WebKit browsers. */
  -webkit-font-smoothing: antialiased;
  /* Support for Safari and Chrome. */
  text-rendering: optimizeLegibility;

  /* Support for Firefox. */
  -moz-osx-font-smoothing: grayscale;

  /* Support for IE. */
  font-feature-settings: 'liga';
}
  
  html, body {
	position: fixed;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	margin: 0;
	font-family: Arial, sans-serif;
	-webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
#playercontainer {
	width: 100%;
	height: 100%;
	background: #111;
	overflow: hidden;
}
#player {
	width: 100%;
	height: 100%;
	cursor: pointer;
}
#player, #controls {
	pointer-events: none;
}
#controls {
	position: relative;
	bottom: 62px;
	padding-top: 12px;
	padding-left: 15px;
	padding-right: 15px;
	width: calc(100% - 30px);
	height: 50px;
	background: linear-gradient(rgba(0,0,0,0), rgba(0,0,0,0.6));
}
#progressholder {
	height: 5px;
	border-radius: 4px;
	background: rgba(61, 61, 61, 0.7);
	margin-bottom: 11px;
	cursor: pointer;
}
#buffered {
	height: 5px;
	border-radius: 4px;
	background: rgba(160, 160, 160, 0.4);
	width: 0;
}
#progress {
	position: relative;
	bottom: 5px;
	height: 5px;
	border-radius: 4px;
	background: rgba(255, 0, 0, 0.7);
	width: 0;
}
#progressholder:hover #progressorb {
	display: block;
}
#progressorb {
	position: relative;
	bottom: 14px;
	width: 14px;
	height: 14px;
	border-radius: 10px;
	background: rgba(255, 0, 0, 0.7);
	display: none;
}
#playpause {
	font-size: 14pt;
	color: #fff;
	margin-left: 10px;
	cursor: pointer;
  display: inline-block;
}
#playpause:hover {
	color: #d3d3d3;
}
#progresstime {
	position: relative;
	right: 45px;
	top: 3px;
	font-size: 10pt;
	color: #fff;
	font-weight: bold;
	float: right;
  display: inline-block;
}
#fullscreen {
	position: fixed;
	right: 25px;
	bottom: 10px;
	color: #fff;
	font-size: 14pt;
	cursor: pointer;
}</style></head><body>
<div id="playercontainer">
	<video id="player" src="http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_1080p_30fps_normal.mp4" style="cursor: pointer; pointer-events: auto;"></video>
	<div id="controls" style="pointer-events: auto; display: block;">
		<div id="progressholder">
			<div id="buffered" style="width: 0%;"></div>
			<div id="progress" style="width: 0%;"></div>
			<div id="progressorb" style="margin-left: 0px;"></div>
		</div>
		<div id="playpause" class="fa fa-play"><i class="material-icons">&#xE037;</i></div>
		<div id="progresstime">0:00 / 10:34</div>
		<div id="fullscreen" class="fa fa-arrows-alt"><i class="material-icons">&#xE5D0;</i></div>
	</div>
</div>
  
  <script src="../../js/jquery-1.11.3.js"></script>
  <script src="../../js/jquery.min.js"></script>
  
<script>
  
  var player = document.getElementById("player");

var fadeTime;
var over = false;

var full = false;

var loadCheck;

$("#player").on("loadeddata", updateTime);

loadCheck = setInterval(function(){
	if($("#player").get(0).duration > 0){
		$("#player, #controls").css("pointer-events", "auto");
		clearInterval(loadCheck);
	}
}, 100);

$("#player").click(toggleVideo);
$("#playpause").click(toggleVideo);

$("#playercontainer").mousemove(function() {
	$("#controls").show();
	$("#player").css("cursor", "pointer");
	clearTimeout(fadeTime);
	if(!player.paused) {
		fadeTime = setTimeout(function() {
			$("#controls").fadeOut("medium");
			$("#player").css("cursor", "none");
		}, 3000);
	}
});

$("#progressholder").hover(function(e) { over = true; }, function() { over = false; });

$("#progressholder").click(function(e) {
	var pos = e.pageX - 25;
	var prop = (pos + 1) / $("#progressholder").width();
	var prog = prop * player.duration;
	player.currentTime = prog;
	updateProgress();
});

$("#fullscreen").click(function() {
	if(!full) launchIntoFullscreen(document.getElementById("playercontainer"));
	else exitFullscreen(document.getElementById("playercontainer"));
	full = !full;
});

function launchIntoFullscreen(element) {
  if(element.requestFullscreen) {
    element.requestFullscreen();
  } else if(element.mozRequestFullScreen) {
    element.mozRequestFullScreen();
  } else if(element.webkitRequestFullscreen) {
    element.webkitRequestFullscreen();
  } else if(element.msRequestFullscreen) {
    element.msRequestFullscreen();
  }
}

function exitFullscreen() {
  if(document.exitFullscreen) {
    document.exitFullscreen();
  } else if(document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if(document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  }
}

$("#progressholder").mousemove(function(e) { updateOrb(e); });

function toggleVideo() {
	//We're playing
	if(!player.paused) {
		$("#controls").show();
		$("#player").css("cursor", "pointer");
		$("#playpause").attr("class", "fa fa-play");
		player.pause();
	} else {
		$("#controls").fadeOut("medium");
		$("#player").css("cursor", "none");
		$("#playpause").attr("class", "fa fa-pause");
		player.play();
	}
}

function updateProgress() {
	var bp = player.buffered.end(player.buffered.length-1) / player.duration;
	var bw = bp * 100;
	$("#buffered").css("width", bw + "%");
	
	var p = player.currentTime / player.duration;
	var w = p * 100;
	$("#progress").css("width", w + "%");
	
	updateTime();
	
	if(player.ended) {
		$("#playpause").attr("class", "fa fa-repeat");
		$("#controls").show();
		$("#player").css("cursor", "pointer");
		clearTimeout(fadeTime);
	}
}

function updateOrb(e) {
	var pos = e.pageX - 25;
	var prop = pos / $("#progressholder").width();
	var prog = prop * player.duration;
	$("#progressorb").css("margin-left", pos + "px");
}

function updateTime() {
	$("#progresstime").text(player.currentTime.toString().toHHMMSS() + " / " + player.duration.toString().toHHMMSS());
}

setInterval(updateProgress, 100);

String.prototype.toHHMMSS = function () {
    var sec_num = parseInt(this, 10);
    var minutes = Math.floor(sec_num / 60);
    var seconds = sec_num - (minutes * 60);
	
    if (seconds < 10) {seconds = "0"+seconds;}
    var time = minutes + ':' + seconds;
    return time;
};
</script>
</body></html>