<html class=""><head>
<title>Nueva Pestaña</title>
<style>
  
* {
  box-sizing: border-box;
  margin: 0;
  padding: 0;
  border: 0;
  font-family: Open Sans;
}
  
@font-face {
src: url(../../fuentes/opensans/OpenSans-Light.ttf);
font-family: Open Sans;
}

      @font-face {
  font-family: 'Material Icons';
  font-style: normal;
  font-weight: 400;
  src: url(../../fuentes/MaterialIcons/MaterialIcons-Regular.eot); /* For IE6-8 */
  src: local('Material Icons'),
       local('MaterialIcons-Regular'),
       url(../../fuentes/MaterialIcons/MaterialIcons-Regular.woff2) format('woff2'),
       url(../../fuentes/MaterialIcons/MaterialIcons-Regular.woff) format('woff'),
       url(../../fuentes/MaterialIcons/MaterialIcons-Regular.ttf) format('truetype');
}

.material-icons {
  font-family: 'Material Icons';
  font-weight: normal;
  font-style: normal;
  font-size: 24px;  /* Preferred icon size */
  display: inline-block;
  line-height: 1;
  text-transform: none;
  letter-spacing: normal;
  word-wrap: normal;
  white-space: nowrap;
  direction: ltr;

  /* Support for all WebKit browsers. */
  -webkit-font-smoothing: antialiased;
  /* Support for Safari and Chrome. */
  text-rendering: optimizeLegibility;

  /* Support for Firefox. */
  -moz-osx-font-smoothing: grayscale;

  /* Support for IE. */
  font-feature-settings: 'liga';
}
      
body {
  margin:0;
  background: -webkit-linear-gradient(280deg, #1D68BE, #FB6F8C);
  background: -moz-linear-gradient(280deg, #1D68BE, #FB6F8C);
  background: -o-linear-gradient(280deg, #1D68BE, #FB6F8C);
  background: linear-gradient(10deg, #1D68BE, #FB6F8C);
  height:100%;
  font-family: Open Sans;
  display: flex;
  align-items: center;
  justify-content: center;
}

a {
  color: #205081;
  text-decoration: none;
}
a:hover {
  color: #3983ce;
}

ul {
  list-style-type: none;
  margin: 0;
  padding-left: 0;
}

.wrapper {
  margin: 50px auto;
  max-width: 800px;
  min-width: 700px;
  width: 70%;
}

/* Jira Box */
.jira {
    background-color: rgba(255, 255, 255, 0.9);
    border-radius: 10px;
    overflow: hidden;
    color: black;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
}
.jira h1 {
  font-weight: 900;
  margin-top: 0;
  text-align: center;
}
.jira input {
    background-color: rgba(0, 0, 0, 0.2);
    border: 0 none;
    box-shadow: none;
    color: #FFFFFF;
    font-size: 26px;
    font-weight: 300;
    padding: 20px;
    width: 100%;
    outline: none;
}
  
  .jira input::-webkit-input-placeholder {
  color:white;
}
  
.jira input:focus {
  background-color: rgba(0, 0, 0, 0.3);
}
.jira a {
  color: #FFFFFF;
}

.recent-issues {
  padding: 0.5rem;
}
.recent-issues li {
  display: inline-block;
  font-size: 90%;
  margin: 4px;
}
.recent-issues li a {
  background-color: #296CA3;
  border-radius: 3px;
  padding: 3px 6px;
  opacity: 0.5;
  color:white;
}
.recent-issues li a:hover {
  opacity: 1;
}

.links {
    display: inline-block;
    width: 100%;
}
.links section {
  background-color: rgba(255, 255, 255, 0.14);
  border-radius: 5px;
  margin:0.5rem;
  padding: 0.5rem;
}
.links section h3 {
    font-weight: 900;
    color: white;
    padding: 0.5rem;
    border-bottom: solid 1px rgba(255, 255, 255, 0.31);
}
.links section li {
    font-weight: 100;
    margin: 0.5rem;
    color: white;
}
.links section:first-child:nth-last-child(1) {
  width: 100%;
}
.links section:first-child:nth-last-child(2),
.links section:first-child:nth-last-child(2) ~ section {
  width: 50%;
}
.links section:first-child:nth-last-child(3),
.links section:first-child:nth-last-child(3) ~ section {
width: calc(100% - 1rem);
    display: block;
}
.links section:first-child:nth-last-child(4),
.links section:first-child:nth-last-child(4) ~ section {
  width: 25%;
}
.links :last-child {
  margin-right: 0 !important;
}
</style></head><body>
  
  <script src="/resources/js/jquery-1.11.3.js"></script>
  <script src="/resources/js/jquery.min.js"></script>
  
    <div class="wrapper">
      <section class="jira">
        <input id="jira_issue" placeholder="<?=BUSCAR?>">
        <ul class="recent-issues">
          <li>Recent Issues:</li>
          <li><a href="">DEV-300</a></li>
          <li><a href="">DEV-301</a></li>
        </ul>
      </section>

      <section class="links">
        
        <section id="issues">
          <h3>Issues</h3>
          <ul>
            <li><a href="http://dumdarac.com/">DumDarac</a></li>
          </ul>
        </section>
        
        <section id="reference">
          <h3>Reference</h3>
          <ul>
            
          </ul>
        </section>
        
        <section id="resources">
          <h3>Resources</h3>
          <ul>
            
          </ul>
        </section>
      </section>
    </div>
    
<script>$("a[href^='http']").each(function() {
    $(this).css({
        background: "url(http://www.google.com/s2/favicons?domain=" + this.href + ") left center no-repeat",
        "padding-left": "26px"
    });
});
</script>
</body></html>