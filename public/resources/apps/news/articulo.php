<html>
  <head>
		<style>
			
				@font-face {
src: url(../../fuentes/roboto/Roboto-Light.ttf);
font-family: Roboto;
}
			
			* {
				text-decoration:none;
				font-family:Roboto;
			}
			
	p { color: #212121;
    font-family: Roboto;
    font-size: 14px;
	line-height: 20px;
          margin: 1rem;}
      h1 {
    margin: 1rem;
    font-family: Roboto;
    font-weight: 100;
      }
      h2 {
    margin: 1rem;
    font-family: Roboto;
    font-weight: 100;
    font-size: 1rem;
      }
			
			a {
				color: #2196F3;
			}
			
      body {
    background: white;
    color:#353535;
    padding-bottom:5rem;
	font-family: Roboto;
      }
      
      .header {
    width: 100%;
    height: 25rem;
    background-position: center;
    background-size: cover;
    position: fixed;
    top: 0;
    left: 0;
    background-color: #536dfe;
      }
      
			.thumbnail {display:none;}	
	
      img {
    width: auto;
    border-radius: 2px;
    max-width: 20rem;
    margin: 1rem;
      }
      
      .contenido_publicacion {
    position: absolute;
    top: 23rem;
    left: 0;
    right: 0;
    background: white;
    width: 90%;
    margin: auto;
    border-radius: 3px;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
      }
			
			.contenido_publicacion .fuente {
			text-transform:uppercase;
			}
			
			ul {
				    background: #eee;
    padding: 0;
    padding-top: 1rem;
    padding-bottom: 2rem;
    list-style: none;
			}
			
			li {
    margin-top: 10px;
    margin-left: 2rem;
}
    </style>
  </head>
  <body>
    <?php 
  include ("../../acceso_db.php");

  $id_n = $_GET['id'];
  $kiosco_news = mysqli_query($con,"SELECT * FROM kiosco_news WHERE id='$id_n'");
  while($row_kiosco_news = mysqli_fetch_array($kiosco_news))
  { 
  $id_kiosco_news = $row_kiosco_news["id"];
	$titulo_kiosco_news = $row_kiosco_news["titulo"];
	$fecha_kiosco_news = $row_kiosco_news["fecha"];
	$descripcion_kiosco_news = $row_kiosco_news["descripcion"];
  $imagen_kiosco_news = $row_kiosco_news["imagen"];
  $texto_kiosco_news = $row_kiosco_news["texto"];
  $fuente_kiosco_news = $row_kiosco_news["fuente"];
	$url_kiosco_news = $row_kiosco_news["url"];
	$texto_kiosco_news = str_replace("noticia original", "", $texto_kiosco_news);
	$texto_kiosco_news = str_replace("(", "", $texto_kiosco_news);	
	$texto_kiosco_news = str_replace(")", "", $texto_kiosco_news);	
	
	//miro si hay enlaces con solamente www, si es así le añado el http://
	$texto_kiosco_news= preg_replace("/href=\"www/", '', $texto_kiosco_news);
  ?>
    
  <div class="header" style="background-image:url('<?=$imagen_kiosco_news?>');" ></div>
    <div class="contenido_publicacion" >
        <h1><?=$titulo_kiosco_news?></h1>
  <h2><?=$fecha_kiosco_news?> | <a class="fuente" href="" ><?=$fuente_kiosco_news?></a></h2>
  <p><?=$texto_kiosco_news?></p>
    </div>

  <?}?>
		<script src="../../../resources/js/jquery-1.11.3.js"></script>
		<script>
			$(document).scroll(function() {
				$(".header").css("top","-" + $(document).scrollTop()/2 + "px");
			});
		</script>
  </body>
</html>