<html>
  <head>
    <style>
      .news_dashboard {
    width: 100%;
    column-count: auto;
    column-gap: 0;
    margin: 0;
    padding: 0;
    text-align: center;
    column-width: 19rem;
      }
      
      .news_dashboard .card_news {
    width: calc(100% - 1rem);
    height: auto;
    text-align: center;
    position: relative;
    padding: 0.5rem;
    padding-top: 1rem;
    background: linear-gradient(to top, rgb(0, 0, 0), rgba(0, 0, 0, 0.498039));
    display: inline-block;
      }
      
      .news_dashboard .card_news span {
    font-size: 1.2rem;
    color: black;
    padding: 0.5rem;
    margin: 0.5rem;
    background: #00ff8b;
    display: inline-block;
    font-weight: 900;
    font-family: DumDarac;
        box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);

      }
      
      .news_dashboard .card_news h1 {
    color: white;
    font-size: 1.5rem;
    text-align: center;
    padding: 0.5rem;
      }
      
      .news_dashboard .card_news .bg {
        position:absolute;
        top:0;
        left:0;
        width:100%;
        height:100%;
        background-size:cover;
        background-position:center;
        z-index:-1;
      }
    </style>
  </head>
  <body>
    <div class="news_dashboard" >
      
      <?php 
  include("../../../acceso_db.php");
  $numeronoticias = '80';

  $kiosco_news = mysqli_query($con,"SELECT * FROM kiosco_news WHERE idioma='es' AND categoria LIKE '%$categoria_kiosco%' ORDER BY id DESC LIMIT $numeronoticias");
  while($row_kiosco_news = mysqli_fetch_array($kiosco_news))
  { 
  $id_kiosco_news = $row_kiosco_news["id"];
	$titulo_kiosco_news = $row_kiosco_news["titulo"];
	$fecha_kiosco_news = $row_kiosco_news["fecha"];
	$categoria_kiosco_news = $row_kiosco_news["categoria"];
  $imagen_kiosco_news = $row_kiosco_news["imagen"];
	$texto_kiosco_news = $row_kiosco_news["texto"];
	$fuente_kiosco_news = $row_kiosco_news["fuente"];
  ?>
      <div class="card_news" onclick="javascript: $('.articulo').load('./resources/apps/news/article.php');" >
        <div class="bg" style="background-image:url('<?=$imagen_kiosco_news?>');" ></div>
        <span>Ultimas Noticias</span>
        <h1>
          <?=$titulo_kiosco_news?>
        </h1>
      </div>
      
      <?}?>
      
    </div>
  </body>
</html>