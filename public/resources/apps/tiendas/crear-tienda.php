<html>
  <head>
    <style>
      .ventana_creartienda h1 {
        width:calc(100% - 1rem);
        font-size:1.5rem;
        text-align:center;
        padding:0.5rem;
      }
      
      .ventana_creartienda .card_creartienda {
        width:calc(100% - 2rem);
        background:white;
        height:auto;
        padding:0.5rem;
        margin:0.5rem;
        border-radius:2px;
        box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
      }
      
      .ventana_creartienda .card_creartienda input {
    font-size: 1rem;
    padding: 0.5rem;
    width: calc(100% - 1rem);
    margin: 0.5rem;
    border-radius: 2px;
    border: none;
    background: #f5f8fa;
    outline: none;
      }
    </style>
  </head>
  <body>
      <h1>Crea tu tienda</h1>
      <div class="card_creartienda" >
        <form>
          <input type="text" name="firstname" placeholder="Nombre de la tienda"><br>
          <input type="text" name="lastname" placeholder="Eslogan">
        </form>
      </div>
  </body>
</html>