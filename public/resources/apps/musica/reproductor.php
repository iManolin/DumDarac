<html>
	<head>
		<style>
			.reproductor {
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 90px;
    background: white;
    box-shadow: 0 0 8px rgba(0,0,0,.4);
    display: flex;
		font-family:Product Sans;
		z-index:15;
			}
			
			.reproductor p {font-family:Product Sans;}
			.reproductor h1 {font-family:Product Sans;}
			
			.reproductor table {
    width: 100%;
    height: 90px;
    border-spacing: 0;
		font-family:Product Sans;
			}
			
			.reproductor table td {
    padding: 0;
    display: inline-flex;
		font-family:Product Sans;
			}
			
  .controles {
    width: 100%;
    text-align: center;
    padding: 0;
  }
			
			.reproductor img {width: 90px; float:left;
    height: 90px;}
  
  
  .controles svg {
    width: 2rem;
    height: 2rem;
    vertical-align: middle;
    cursor: pointer;
    margin: 0 0.2rem;
		    fill: #607D8B;
  }
			
			@media (max-width: 50em) {
				.info_controles {
					display:none!important;
				}
				.td_controles {width:100%!important;}
			}
			

		</style>
	</head>
	<body>


		<div class="reproductor" >
<div id="reproductor_progreso_fondo" style="
    width: 29%;
    position: absolute;
    left: 0;
    top: -5px;
    height: 5px;
    background: #C8C8C8;
    z-index: 1;
	  cursor:pointer;
">
				
			</div>

			
<div style="
    width: 100%;
    position: absolute;
    left: 0;
    top: -5px;
    height: 5px;
    background: #eee;
	  cursor:pointer;
    ">

	
<div id="reproductor_progreso_bola" class="drop_bola" style="
    background: #FF9800;
    width: 1rem;
    height: 1rem;
    border-radius: 100%;
    position: absolute;
    top: -5px;
    left: 29%;
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.45);
    cursor:pointer;
    z-index: 1;
">
				
	</div>
			</div>
			<audio id="musicaaudio" numero="" style="display:none;" >
 
  <source src="" type="audio/mpeg">
Your browser does not support the audio element.
</audio>
			<table>
				
				<td class="info_controles" style="    width: 45%; min-width:19rem;" >
					<a href="#" ><img id="imagenalbumreproduciendo" onclick="javascript:$('#paginamusica').load('./apps/musica.php?id_album=<?=$id_musica?>&p=<?=$pagina?>'); ocultarbotonredondo();"  src="http://androidayuda.com/app/uploads/2015/05/Google-Play-Music-All-Access-Portada.jpg" ></a>
				
<div style="overflow: hidden; padding: 1.2rem 0.8rem;">
					<h1 id="reproductortitulocancion" style="overflow: hidden;text-overflow: ellipsis;font-size: 16px;font-weight: 100;padding-top: 0.5rem;margin: 0;"> Nombre de la cancion
					</h1>
<p style="
    white-space: nowrap;
    width: auto;
    display: inline;
    color: #616161;
    font-size: 13px;
    font-weight: 400;
    line-height: 25px;
" >
	<span id="reproductortituloartista" onclick="javascript:$('#paginamusica').load('./apps/artistas.php?q_artista=<?=$artista_musica_buscar?>&p=<?=$pagina?>&tipo=1&prl=musica'); ocultarbotonredondo();" >Nombre del artista</span> - <span id="reproductortituloalbum" onclick="javascript:$('#paginamusica').load('./apps/musica.php?id_album=<?=$id_musica?>&p=<?=$pagina?>'); ocultarbotonredondo();" >Album</span>
	</p></div>
				
				</td>
				<td class="td_controles" style="
   width: 9.3rem;
">
				  <div class="controles">
<svg onclick="anteriorcancion()" xmlns="http://www.w3.org/2000/svg" fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
    <path d="M6 6h2v12H6zm3.5 6l8.5 6V6z"></path>
    <path d="M0 0h24v24H0z" fill="none"></path>
</svg><svg class="icono_playpause_MUSICA icono_playpause_MUSICA_play" style="    width: 4rem;
    height: 4rem;
    fill: #FF9800;
    /* background: white; */
    /* border-radius: 100%; */" xmlns="http://www.w3.org/2000/svg" fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
    <path d="M0 0h24v24H0z" fill="none"></path>
    <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z"></path>
</svg><svg class="icono_playpause_MUSICA icono_playpause_MUSICA_pause" style="    width: 4rem;
    height: 4rem;
    fill: #FF9800;
    /* background: white; */
    /* border-radius: 100%; */ display:none;" xmlns="http://www.w3.org/2000/svg" fill="#000000" height="24" viewBox="0 0 24 24" width="24">
    <path d="M0 0h24v24H0z" fill="none"/>
    <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-1 14H9V8h2v8zm4 0h-2V8h2v8z"/>
</svg><svg onclick="siguientecancion()" xmlns="http://www.w3.org/2000/svg" fill="#FFFFFF" height="24" viewBox="0 0 24 24" width="24">
    <path d="M6 18l8.5-6L6 6v12zM16 6v12h2V6h-2z"></path>
    <path d="M0 0h24v24H0z" fill="none"></path>
</svg>
  </div>
				</td>
				<td></td>
				
			</table>
<p style="
    position: absolute;
    right: 1rem;
">
				<span onclick="hellou()" id="duracion_actual">0:00</span> / <span id="duracion_total" >0:00</span>
			</p>

			</div>
			<div class="playlist">
			<div id="playlist" style="    overflow: hidden;
    overflow-y: scroll;
    height: 100%;">
				
		<ul style="    padding: 0;
    margin: 0;
    list-style: none;" id="playlist">
					
				    <?php $reproductor_playlist_musica = mysqli_query($con,"SELECT * FROM musica_canciones WHERE album='$id_musica_album' ORDER BY id DESC");
$numero_pelicula = 1;
		while($row_reproductor_playlist_musica = mysqli_fetch_array($reproductor_playlist_musica))
  { 
	$titulo_cancion_reproductor_playlist_musica = $row_reproductor_playlist_musica["titulo"];
	$id_cancion_reproductor_playlist_musica = $row_reproductor_playlist_musica["id"];
	$id_album_reproductor_playlist_musica = $row_reproductor_playlist_musica["album"];?>
					
    <li style="padding: 1rem; font-size: 1.1rem;" data-src="http://85.53.27.212/musica/<?=$id_album_reproductor_playlist_musica?>/<?=$id_cancion_reproductor_playlist_musica?>.mp3"><?=$titulo_cancion_reproductor_playlist_musica?></li>
					
	<?}?>
</ul></div>
		</div>
				<script src="../../js/jquery-1.11.3.js"></script>
		<script type="text/javascript">
			
			var musicaaudio = document.getElementById("musicaaudio");
    
			

    playlist = $('#playlist');

               playlist.on('click', 'li', function() {
                  playlist.find('.current').removeClass('current');
                  $(this).addClass('current');
                  musicaaudio.src = $(this).data('src');
                  musicaaudio.play();
		$('.icono_playpause_MUSICA_play').hide();
		$('.icono_playpause_MUSICA_pause').show();
               });

			

			
musicaaudio.addEventListener("timeupdate", function() {

		//DURACION TOTAL
    var durationtotal = document.getElementById('duracion_total');
    var stotal = parseInt(musicaaudio.duration % 60);
    var mtotal = parseInt((musicaaudio.duration / 60) % 60);
	if (stotal<'10')
		 durationtotal.innerHTML = mtotal + ':' + '0' + stotal;
	else
     durationtotal.innerHTML = mtotal + ':' + stotal ;

	
	//DURACION_ACTUAL
    var currentTime = musicaaudio.currentTime;
    var duration = musicaaudio.duration;
    $('#reproductor_progreso_fondo').stop(true,true).animate({'width':(currentTime +.25)/duration*100+'%'},250,'linear');
	  $('#reproductor_progreso_bola').stop(true,true).animate({'left':(currentTime +.25)/duration*100+'%'},250,'linear');
    var minutes = musicaaudio.currentTime;
	
	//DURACION ACTUAL
    var duration = document.getElementById('duracion_actual');
    var s = parseInt(musicaaudio.currentTime % 60);
    var m = parseInt((musicaaudio.currentTime / 60) % 60);
	
	
	if (s<'10')
		 duration.innerHTML = m + ':' + '0' + s;
	else
     duration.innerHTML = m + ':' + s ;
});
			

			
			//AL FINALIZAR
			musicaaudio.addEventListener("ended", function(){
     musicaaudio.currentTime = 0;
     playicon();
		 siguientecancion();
			});
			
			   $(function() { 
     $(".icono_playpause_MUSICA").click(function(e) {
			 playicon();
       e.preventDefault();

       // This next line will get the audio element
       // that is adjacent to the link that was clicked.
       var song = document.getElementById("musicaaudio");
       if (song.paused)
         song.play();
       else
         song.pause();
     });
   });
			
			function playicon() {
				$('.icono_playpause_MUSICA').toggle();
			}
			

			
			function siguientecancion() {
								numerocancionactual = document.getElementById("musicaaudio").getAttribute("numero");
				
		
				numerosiguientecancion=parseInt(numerocancionactual)+parseInt(1);
				
				clicknumerosiguientecancion = 'cancion_playlist_'+numerosiguientecancion;
				document.getElementById(clicknumerosiguientecancion).click();
			}
			
						function anteriorcancion() {
								numerocancionactualanterior = document.getElementById("musicaaudio").getAttribute("numero");
				
		
				numerosiguientecancionanterior = numerocancionactualanterior - 1;
				
				clicknumerosiguientecancionanterior = 'cancion_playlist_'+numerosiguientecancionanterior;
				document.getElementById(clicknumerosiguientecancionanterior).click();
							alert(clicknumerosiguientecancionanterior);
			}
</script>
		
	</body>
</html>