	<table class="table_canciones" style="
    width: 100%;
    height: auto;
    margin: 0;
    border-spacing: inherit;
    padding: 0;
">
<tr style="background:white!important;" ><td class="table_canciones_numero" >Nº </td><td class="nombre_td_canciones" >Nombre</td><td></td><td><svg xmlns="http://www.w3.org/2000/svg" fill="#607D8B" height="24" viewBox="0 0 24 24" width="24">
    <path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z"/>
    <path d="M0 0h24v24H0z" fill="none"/>
    <path d="M12.5 7H11v6l5.25 3.15.75-1.23-4.5-2.67z"/>
</svg></td><td class="table_canciones_likes" ><svg xmlns="http://www.w3.org/2000/svg" fill="#607D8B" height="24" viewBox="0 0 24 24" width="24">
    <path d="M12 6c0-.55-.45-1-1-1H5.82l.66-3.18.02-.23c0-.31-.13-.59-.33-.8L5.38 0 .44 4.94C.17 5.21 0 5.59 0 6v6.5c0 .83.67 1.5 1.5 1.5h6.75c.62 0 1.15-.38 1.38-.91l2.26-5.29c.07-.17.11-.36.11-.55V6zm10.5 4h-6.75c-.62 0-1.15.38-1.38.91l-2.26 5.29c-.07.17-.11.36-.11.55V18c0 .55.45 1 1 1h5.18l-.66 3.18-.02.24c0 .31.13.59.33.8l.79.78 4.94-4.94c.27-.27.44-.65.44-1.06v-6.5c0-.83-.67-1.5-1.5-1.5z"/>
    <path d="M0 0h24v24H0z" fill="none"/>
</svg></td></tr>
    
    <?php $pelicula = mysqli_query($con,"SELECT * FROM musica_canciones WHERE album='$id_musica_album' ORDER BY id DESC");
$numero_pelicula = 1;
		while($row_pelicula = mysqli_fetch_array($pelicula))
  { 
	$titulo_pelicula = $row_pelicula["titulo"];
	$id_pelicula = $row_pelicula["id"];
	$id_album_pelicula = $row_pelicula["album"];
	$titulo_pelicula_abrir = str_replace("'", "", $titulo_pelicula);
	$numero_pelicula_actual = $numero_pelicula++; ?>
<tr onclick='javascript: musicaaudio.setAttribute("numero", "<?php echo $numero_pelicula_actual?>"); document.getElementById("reproductortituloalbum").innerHTML = "<?=$nombre_musica_album?>"; document.getElementById("reproductortituloartista").innerHTML = "<?=$artista_musica_album?>"; document.getElementById("reproductortitulocancion").innerHTML = "<?=$titulo_pelicula_abrir?>"; document.getElementById("imagenalbumreproduciendo").src = "./media/musica/albumes/caratulas/<?=$id_album_pelicula?>.jpg"; document.getElementById("musicaaudio").src = "./media/musica/<?=$id_album_pelicula?>/<?=$id_pelicula?>.mp3"; playaudio(); $("#playlist").show(); $("#playlist").load("./resources/apps/musica/playlist.php?album=<?=$id_album_pelicula?>");' ><td class="table_canciones_numero" ><?php echo $numero_pelicula_actual;?></td><td class="nombre_td_canciones" ><p><?=$titulo_pelicula?></p></td><td></td><td>0:00</td><td class="table_canciones_likes" >
  <svg style="    margin-top: 23px;" xmlns="http://www.w3.org/2000/svg" fill="#9E9E9E" height="24" viewBox="0 0 24 24" width="24">
    <path d="M0 0h24v24H0z" fill="none"/>
    <path d="M1 21h4V9H1v12zm22-11c0-1.1-.9-2-2-2h-6.31l.95-4.57.03-.32c0-.41-.17-.79-.44-1.06L14.17 1 7.59 7.59C7.22 7.95 7 8.45 7 9v10c0 1.1.9 2 2 2h9c.83 0 1.54-.5 1.84-1.22l3.02-7.05c.09-.23.14-.47.14-.73v-1.91l-.01-.01L23 10z"/>
</svg>
  <svg style="    margin-top: 23px;" xmlns="http://www.w3.org/2000/svg" fill="#9E9E9E" height="24" viewBox="0 0 24 24" width="24">
    <path d="M0 0h24v24H0z" fill="none"/>
    <path d="M15 3H6c-.83 0-1.54.5-1.84 1.22l-3.02 7.05c-.09.23-.14.47-.14.73v1.91l.01.01L1 14c0 1.1.9 2 2 2h6.31l-.95 4.57-.03.32c0 .41.17.79.44 1.06L9.83 23l6.59-6.59c.36-.36.58-.86.58-1.41V5c0-1.1-.9-2-2-2zm4 0v12h4V3h-4z"/>
</svg></td></tr>
    <?}?>
	</table>