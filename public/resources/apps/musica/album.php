<?php
	$id_album = $_GET['id_album']; $q_artista = $_GET['q_artista']; 
	include('../../../acceso_db.php');
	include('../../../idiomas/idiomas.php'); 
?>
<div id="paginamusica" >
<html><head><meta charset="UTF-8">
<style>

	

	
	
  .contenedor {
    width: 88%;
    max-width: 100rem;
	  height:auto;
	}
  
  .carddos {
    overflow: visible;
    white-space: nowrap;
    display: inline-block;
    position: relative;
    background-color: white;
    width: calc((100%/6) - 20px);
    margin: 8px;
    border-radius: 2px;
    font-weight: 300;
    cursor: default;
    min-width: 9rem;
	  max-width: 12rem;
    vertical-align: top;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    cursor: pointer;
    box-shadow: 0 1px 4px 0 rgba(0,0,0,0.37);
    transition: box-shadow 0.28s cubic-bezier(0.4,0,0.2,1);
}
	
	.cardhover:hover {box-shadow: 0 2px 2px 0 rgba(0,0,0,0.2),0 6px 10px 0 rgba(0,0,0,0.3);}
	
	  .cardtres {
    display: inline-block;
    border-radius: 100%;
    position: relative;
    overflow: hidden;
    transform-origin: top left;
    height: 100%;
    max-height: 10rem;
    margin: 10px;
    max-width: 10rem;
    width: 100%;
    min-width: 10rem;
    min-height: 10rem;
    box-shadow: 0 1px 4px 0 rgba(0,0,0,0.14);
    color: white;
    background-image: url('./imgs/otros/artista.png');
    background-size: cover;
    transition: 0.5s;
    display: inline-flex;
}

.contentContainer {background:rgba(0, 0, 0, 0);}


.cards {
  position:relative;
  left:auto;
  width:100%;
  height:auto;
  top:0rem;
	margin-bottom: 5rem;
	text-align:center;
	
}


.info {
margin-left: 1rem;
    margin-bottom: 5px;
}

.titulo {
    text-align: left;
    font-size: 16px;
    line-height: 20px;
    max-height: 40px;
    overflow: hidden;
    text-overflow: ellipsis;
    margin: 0;
    color: #383838;
    font-family: Roboto,Arial,Helvetica,sans-serif;
}
.encomun {
    color: #383838;
    font-size: 14px;
    vertical-align: middle;
    text-align: left;
    margin: 0;
	overflow:hidden;
}

	

.icn {
position:relative;
top:-5px;
}

.icono {
  position:absolute; 
  top:5rem; 
  right:2.4rem;
  background-color:#fff;
  border-radius:50%;
  cursor:pointer;
  z-index:1;
display:none;
}


.usuario {
top:6rem;
position:relative;
margin-right:60px;
z-index:999;
}

.usuario {
  top:6.5rem;
  position:relative;
  overflow:hidden;
  box-shadow: 0 2px 20px rgba(0, 0, 0, 0.49);
  background:#fff;
  border-radius:1%;
}
.usuario_a { z-index:99; color:#000;}
.informacion {
padding:15px;
background-color:#f7f7f7;
}
.ver {display:block;}


svg.micro {
    fill: #666666;
}
	

  
  .capa_imagen_coleccion {
	    width: 100%;
    height: 12rem;
    margin: 0px auto 12px;
    display: block;
    overflow: hidden;
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;}
  
  .cerrar_coleccion {
  display:none;
  position: absolute;
  right: 0.5rem;
  top: 0.5rem;}

  
	.minititulo_explorar {
    font-size: 2rem;
    font-weight: 100;
    color: #9C27B0;
	  margin: 1rem 1rem 0.5rem 1rem;
	  display:none;
	    text-align: left;
	}
	
	.pag {
		margin-top:42%;
		    width: 100%;
    min-height: <?php if($id_album != ''){?>40rem<?}else{?>5rem<?}?>;
    background: white;
    border-radius: 2px;
    box-shadow: 0 1px 4px 0 rgba(0,0,0,0.14);
		display:none;
	}
	
	.pag_album {
		margin-top:calc(50vw - 64px - 132px);
		text-align:center;

	}
	
	.pag_album .divpadart {
padding: 1rem;
    padding-top: 1.5rem;
    margin: 0;
    width: 100%;
    background: rgba(0, 0, 0, 0.1);
    max-height: 10rem;
	}

	.pag_album h1 {
    font-weight: 100;
    font-family: Roboto;
    text-align: left;
    margin: 0;
    margin-left: 1rem;
    float: left;
	}
	
	.pag_album img {
		    position: relative;
    float: left;
    width: 100%;
    max-width: 264px;
		max-height:264px;
	}
	
	.pag_album .table_canciones td {
    height: 64px;
    line-height: 64px;
    max-height: 64px;
    min-height: 64px;
    font-size: 16px;
    font-weight: 400;
    color: #616161;
    white-space: nowrap;
    overflow: hidden;
	  text-align:center;
	  border-spacing: 0;
    margin: 0;
    padding: 0.5rem;
}
	
	.pag_album .album_tabla {
    border-spacing: 0;
    margin: 0;
    padding: 0;
    width: 100%;
    top: 0;
    position: relative;
    display: inline-table;
    background: #607d8b;
    color: white;
	}
	
	.pag_album .album_tabla tr {
		display: flex;
	}
	
		.pag_album .album_tabla td {
    border-spacing: 0;
    margin: 0;
    padding: 0;
    vertical-align: top;
    width: 100%;
    display: inline-flex;
	}
	
	@media (max-width: 44em){
.pag_album .album_tabla td {
    display: flex;
	} }
	
	.pag_album .table_canciones tr:hover {background:#eee;}

		.pag_album .nombre_td_canciones {
    text-align: left!important;
	}
	
	.pag_album .nombre_td_canciones p {
    text-align: left!important;
    white-space: initial;
    height: 64px;
    overflow: hidden;
    display: block;
	}
	
	.pag_album td.table_canciones_numero {
		    width: 3rem;
	}
	
		.pag_album td.table_canciones_likes {
		    width: 4rem;
	}
	
	
	
		@media (max-width: 80em) {
			.pag {margin-top: 30%;}}
	
	@media (max-width: 56em) {
		.pag {margin-top: 119%;}
	.contentContainer {    top: 3rem;}

	.ap-item {height:4rem!important;}

	
}
	
	.capa_imagen_artista {
height: 7.5rem;
    width: 7.5rem;
    background-size: cover;
    border-radius: 100%;
    overflow: hidden;
    position: absolute;
    left: 0;
    top: 0;
    bottom: 0;
    right: 0;
    margin: auto;
    background-position: center;
	}
	
	.playlist {
		max-width: 30rem;
    width: 100%;
    height: 80%;
    background: #eee;
    position: fixed;
    right: 0;
    left: 0;
    margin: auto;
    bottom: 0;
    border-radius: 2px;
    z-index: 99999;
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.45);
		display:none;
	}
	
	.playlist .header_playlist {    height: 12rem;
    width: 100%;
    background: rebeccapurple;
    background-position: 0 20%;
    background-size: cover;}
	
	
	.top-subir {bottom:6.2rem!important;}
	@media (max-width: 56em) {
	.capa_imagen_coleccion {height:9rem;}
	}
<?php if($id_album != '' or $q_artista != ''){

	?>
	.header {
	background:-webkit-linear-gradient(rgba(0, 0, 0, 0.5), rgba(255, 255, 255, 0));
  box-shadow:none;
	}
	
	.pag {display:block;}
	
	li:hover {background:#eee;}
	
	<?php 	
	if($q_artista == ''){
		$musica_artista_bg = mysqli_query($con,"SELECT * FROM musica_albumes WHERE id='$id_album' LIMIT 1");
while($row_musica_artista_bg = mysqli_fetch_array($musica_artista_bg))
  { 
	$nombre_musica_artista_bg = $row_musica_artista_bg["artista"];
	}}
	$artista_bg = mysqli_query($con,"SELECT * FROM artistas WHERE nombre_de_artista LIKE '%$q_artista%' AND nombre_de_artista LIKE '%$nombre_musica_artista_bg%' LIMIT 1");
while($row_artista_bg = mysqli_fetch_array($artista_bg))
  { 
	$id_artista_bg = $row_artista_bg["id"];
	?>
	body {background-image:url('../../../media/artistas/<?=$id_artista_bg?>.jpg'); background-attachment:fixed; background-repeat:no-repeat; background-size:cover; background-position:center;} <?}}?>
</style>

</head>
<body <?php if($q_artista != ''){ $q_artista_q_web = urlencode($q_artista); ?>onload="javascript:$('#paginamusica').load('./apps/artistas.php?q_artista=<?=$q_artista_q_web?>&p=&tipo=1&prl=musica'); ocultarbotonredondo();"<?}?> >
<?php 
	$genero_Musica = $_GET['genero'];
	$titulo_Musica_categoria_genero = $_GET['genero'];
	
	
		if($id_album != '' AND $q_artista == ''){
		
$musica_album = mysqli_query($con,"SELECT * FROM musica_albumes WHERE id='$id_album'");
while($row_musica_album = mysqli_fetch_array($musica_album))
  { 
	$nombre_musica_album = $row_musica_album["titulo"];
  $id_musica_album = $row_musica_album["id"];
	$fecha_musica_album = $row_musica_album["fecha"];
	$genero_musica_album = $row_musica_album["genero"];
	$color_musica_album = $row_musica_album["color"];
	$precio_musica_album = $row_musica_album["precio"];
	$artista_musica_album = $row_musica_album["artista"];
	$artista_musica_buscar_album = urlencode($artista_musica_album);
	$q_artista = $artista_musica_album;
		
		if($color_musica_album == '#607D8B'){
			$color_2_musica_album = "#ff5722";
			} elseif($color_musica_album == '#E91E63' or $color_musica_album == '#e91e63'){
			$color_2_musica_album = "#9e9e9e";
			} elseif($color_musica_album == '#3f51b5'){
			$color_2_musica_album = "#E91E63";
			}
		
		if($color_2_musica_album == ''){$color_2_musica_album = "red";}
	?>
	
			<script>
			
			$(document).ready(function(){
    $(this).scrollTop(300);
});
				
				    $(function(){
      $('#buscador').attr('placeholder','<?=$nombre_musica_album?>');
    });
		</script>
	
	<div class="pag pag_album" id="pagalbum" >

		<div onclick='javascript: document.getElementById("reproductortituloalbum").innerHTML = "<?=$nombre_musica_album?>"; document.getElementById("reproductortituloartista").innerHTML = "<?=$artista_musica_album?>"; document.getElementById("reproductortitulocancion").innerHTML = "<?=$nombre_musica_album?>"; document.getElementById("imagenalbumreproduciendo").src = "./media/musica/albumes/caratulas/<?=$id_musica_album?>.jpg"; document.getElementById("musicaaudio").src = "./media/musica/7/18.mp3"; playaudio(); $("#playlist").load("./resources/apps/musica/playlist.php");' class="" 
			style="background:<?=$color_2_musica_album?>;
						 position: relative;
    right: 3rem;
    float: right;
    margin-top: -4rem;
    top: 14.5rem;
    width: 1.5rem;
    padding: 1rem;
    height: 1.5rem;
    border-radius: 100%;
    cursor: pointer;
    z-index: 1;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
">
		<svg viewBox="0 0 24 24" preserveAspectRatio="xMidYMid meet" class="style-scope iron-icon" style="pointer-events: none;display: block;width: 100%;height: 100%;fill: white;"><g class="style-scope iron-icon">
  <path d="M3.1489987,1.62721508 C3.1489987,0.748830668 4.11108566,0.208210665 4.86145441,0.667097589 L21.835347,11.0393207 C22.5532194,11.4785129 22.5532194,12.5213482 21.835347,12.9605404 L4.86145441,23.3327636 C4.11108566,23.7916505 3.1489987,23.2520152 3.1489987,22.3726461 L3.1489987,1.62721508 Z" class="style-scope iron-icon"></path>
</g></svg></div>
		<table class="album_tabla" style="background:<?=$color_musica_album?>;" >
			<td style="    width: 17rem;"><img src="./media/musica/albumes/caratulas/<?=$id_musica_album?>.jpg" ></td>
			<td>
					<div class="divpadart" >
	<h1><?=$nombre_musica_album?></h1>
<p style="
    margin: 1rem;
	  cursor:pointer;
    margin-top: 3rem;
"><span onclick="javascript: $('#paginamusica').load('./apps/artistas.php?q_artista=<?=$artista_musica_buscar_album?>&p=<?=$pagina_app?>&aplicacion=<?=$aplicacion?>&tipo=1&prl=musica'); ocultarbotonredondo(); loadingocultartempo();"  ><?=$artista_musica_album?></span></p>
		
				</div>
			</td>
		</table>
	
	<?php include('../../../resources/apps/musica/canciones.php'); ?>
	</div>	
	<?}}?>
	



<script type="text/javascript">
	
			$(document).scroll(function() {
				$("body").css("background-position-y","-" + $(document).scrollTop()/3 + "px");
			});
	

		</script>

</body></html>
