<?php

include ("../../../resources/acceso_db.php");
include ("../../../resources/datos-usuario.php");
session_start();
$publicacion_usuario = $_SESSION['usuario_id'];
$publicacion_texto = $_POST['texto'];
$publicacion_texto = addslashes($publicacion_texto);
$string = $publicacion_texto;
$publicacion_tipo = $_POST['tipo'];
$atras = $_SERVER['HTTP_REFERER'];


if ($publicacion_tipo == 'imagen'){
	
  	$ultimo_id_publicacion_bd = mysqli_query($con,"SELECT MAX(id) AS id FROM recordatorios");
	while($ultimo_id_publicacion_bd_row = mysqli_fetch_array($ultimo_id_publicacion_bd))
  { 
	$ultimo_id_publicacion = trim($ultimo_id_publicacion_bd_row[0]);

	}
	
$target_dir = "../usuario/imagenes/$usuario_id_mismo/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
	header("Location: $atras&n=Ha habido un error al subir la imagen.&nb=Ok");
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 9999999999) {
    echo "Sorry, your file is too large.";
	header("Location: $atras&n=Ha habido un error al subir la imagen.&nb=Ok");
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "png" && $imageFileType != "gif" && $imageFileType != "webp" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
	header("Location: $atras&n=Ha habido un error al subir la imagen.&nb=Ok");
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
	header("Location: $atras&n=Ha habido un error al subir la imagen.&nb=Ok");
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
		rename("$target_file", "$target_dir$ultimo_id_publicacion.jpg");
			mysqli_query($con, "INSERT INTO recordatorios 
(texto, tipo, usuario_id, hashtags) VALUES('$publicacion_texto', '$publicacion_tipo', '$publicacion_usuario', '$keywords' ) ") 
or die(mysqli_error());
		header("Location: ../dumdarac.php?p=perfil");
    } else {
        echo "Sorry, there was an error uploading your file.";
		header("Location: $atras&n=Ha habido un error al subir la imagen.&nb=Ok");
    }
}
	
}


$color_aleatorio_card_recordatorios_colores = array("#f44336", "#3f51b5", "#ff5722", "#607d8b", "#4caf50", "white");
$color_aleatorio_card_recordatorios = array_rand($color_aleatorio_card_recordatorios_colores, 1);
$color_aleatorio_card_recordatorios = $color_aleatorio_card_recordatorios_colores[$color_aleatorio_card_recordatorios];



if ($publicacion_tipo == 'default'){
	if($publicacion_texto != ''){
		mysqli_query($con, "INSERT INTO recordatorios (texto, usuario_id, color) VALUES('$publicacion_texto', '$publicacion_usuario', '$color_aleatorio_card_recordatorios') ") or die(mysqli_error());
	} else { }

} 



	
$recordatorio = mysqli_query($con,"SELECT * FROM recordatorios WHERE usuario_id='$usuario_id_mismo'  ORDER BY id DESC");
	while($row_recordatorio = mysqli_fetch_array($recordatorio))
  { 
	$titulo_recordatorio = $row_recordatorio["texto"];
  $id_recordatorio = $row_recordatorio["id"]; 
  $color_recordatorio = $row_recordatorio["color"];
  $eliminado_recordatorio = $row_recordatorio["eliminado"];
		
if($eliminado_recordatorio == ''){
	?>

		<div class="note <?php if($color_recordatorio != 'white'){echo ' acolor '; } if(strlen($titulo_recordatorio) > 100){echo ' textopequeno ';}?>" id="recordatorio<?=$id_recordatorio?>" >
			<div class="note-inner" style="background-color:<?=$color_recordatorio?>;">
				<p><?=$titulo_recordatorio?></p>
				<div class="menu_recordatorio" >
					<i class="material-icons">&#xE7FE;</i>
					<i class="material-icons">&#xE3B7;</i>
					<i class="material-icons">&#xE80D;</i>
					<i class="material-icons" onclick="javascript: $('#addrecordatorios').load('./resources/apps/recordatorios/borrar.php?id=<?=$id_recordatorio?>'); $('#addrecordatoriosd').load('./resources/apps/recordatorios/borrados.php');">&#xE149;</i>
					<i class="material-icons" onclick="javascript:$('#addrecordatorios').load('./resources/apps/recordatorios/borrar-completo.php?id=<?=$id_recordatorio?>'); $('#addrecordatoriosd').load('./resources/apps/recordatorios/borrados.php');" >&#xE872;</i>
				</div>
			</div>
		</div>

	<?}}?>