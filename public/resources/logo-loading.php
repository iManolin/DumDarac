<div class="logo-loading" id="logoloading">
  <html>

  <head>
    <style>
      body {
        overflow: hidden;
      }

      .logo-loading {
        margin: auto;
        text-align: center;
        background-color: white;
        background-color: rgba(var(--PF-color-background), .9);
        position: fixed;
        left: 0;
        top: 0;
        right: 0;
        bottom: 0;
        width: 100%;
        height: 100%;
        z-index: 9998;
        display: flex;
        align-items: center;
        flex-direction: column;
      }

      .logo-loading .group {
        margin: auto;
        width: fit-content;
        height: fit-content;
        <?php if($client and device_agent('mobile')){?> display: none; <?}?>
      }

      .logo-loading .group .fantasma {
        width: 5em;
        height: 5em;
        margin: auto;
        background-size: contain;
        background-position: center;
        background-repeat: no-repeat;
        transform: rotate(0deg);
        -webkit-animation: animationfantasma 1.5s infinite ease;
        -moz-animation: animationfantasma 1.5s infinite ease;
        -ms-animation: animationfantasma 1.5s infinite ease;
        -o-animation: animationfantasma 1.5s infinite ease;
        animation: animationfantasma 1.5s infinite ease;
      }

      .logo-loading .group .loading {
        width: 6em;
        display: block;
        height: 2px;
        margin: 12px auto;
        border-radius: 2px;
        background-color: rgba(var(--PF-color-on-surface), .1);
        position: relative;
        overflow: hidden;
        z-index: 1;
        border-radius: 1em;
      }

      .logo-loading .group .loading:before {
        content: '';
        height: 3px;
        width: 5rem;
        position: absolute;
        -webkit-transform: translate(-34px, 0);
        -moz-transform: translate(-34px, 0);
        -ms-transform: translate(-34px, 0);
        -o-transform: translate(-34px, 0);
        transform: translate(-34px, 0);
        background-color: #0073b1;
        background: -webkit-linear-gradient(left, #E91E63, #2196F3);
        border-radius: 2px;
        -webkit-animation: animation 1.5s infinite ease;
        -moz-animation: animation 1.5s infinite ease;
        -ms-animation: animation 1.5s infinite ease;
        -o-animation: animation 1.5s infinite ease;
        animation: animation 1.5s infinite ease;
      }

      @keyframes animation {
        0% {
          left: 0;
        }
        50% {
          left: 100%;
        }
        100% {
          left: 0;
        }
      }
      
      @keyframes animationfantasma {
        0% {
          transform: rotate(0deg);
        }
        25% {
          transform: rotate(5deg);
        }
        50% {
          transform: rotate(-5deg);
        }
        100% {
          transform: rotate(0deg);
        }
      }
      
    </style>
  </head>

  <body>
    <div class="group">
      <div style="background-image:url('//img.dumdarac.com/logo/logo.svg');" class="fantasma"></div>
      <span class="loading"></span>
    </div>
    
    <script>
    $('#logoloading').delay(1500).fadeOut( "slow", function() {
    $(this).remove();
  });
    </script>
    
  </body>

  </html>
</div>