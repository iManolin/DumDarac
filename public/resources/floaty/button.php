<html>

<head>


  <style>
    
    .floaty {
    position: fixed;
    bottom: 2rem;
    right: 2rem;
    display: flex;
    flex-direction: column;
    align-items: center;
    text-align: center;
    }

    .floaty-list {
    margin: auto;
    margin-bottom: 0.5rem;
    padding: 0;
    list-style: none;
    opacity: 0;
    transition: opacity .2s ease-out;
    display: flex;
    flex-direction: column;
    }

    .floaty:hover .floaty-list {
      opacity: 1;
    }

    .floaty-list-item {
    position: relative;
    width: 40px;
    height: 40px;
    margin: 0.5rem;
    cursor: pointer;
    background-color: var(--PF-color-bg-first-default);
    color: var(--PF-color-original-default);
    border-radius: 5em;
    box-shadow: 0 4px 8px rgba(0, 0, 0, .25);
    display: flex;
    align-items: center;
    text-align: center;
    background-size: cover;
    background-position: center;
    }

    .floaty-list-item-label {
    position: absolute;
    top: 0;
    bottom: 0;
    margin: auto;
    display: inline-table;
    right: calc(100% - 1.5rem);
    padding: 0.5rem 1rem;
    padding-right:2rem;
    font-size: 15px;
    font-weight: 500;
    border-radius: 2em;
    opacity: 0;
    pointer-events: none;
    white-space: nowrap;
    transition: opacity .2s ease-out;
    z-index: -1;
    color: var(--PF-color-bg-first-default);
    background-color: var(--PF-color-original-default);
      box-shadow: 0 4px 8px rgba(0, 0, 0, .25);
    }

    .floaty-list-item:hover>.floaty-list-item-label {
      opacity: 1;
    }

    .floaty-btn {
position: relative;
    width: 56px;
    height: 56px;
    cursor: pointer;
    color: var(--PF-color-bg-first-default);
    background-color: var(--PF-color-original-default);
    border-radius: 50%;
    box-shadow: 0 4px 8px rgba(0, 0, 0, .25);
    display: inline-flex;
    align-items: center;
    text-align: center;
    margin: auto;
    }

    .floaty-btn:hover .floaty-btn-label {
      opacity: 1;
    }

    .floaty-btn-label {
position: absolute;
    top: 0;
    bottom: 0;
    margin: auto;
    display: inline-table;
    right: calc(100% - 1.5rem);
    padding: 0.5rem 1rem;
    padding-right:2rem;
    font-size: 15px;
    font-weight: 500;
    border-radius: 2em;
    opacity: 0;
    pointer-events: none;
    white-space: nowrap;
    transition: opacity .2s ease-out;
    z-index: -1;
    color: var(--PF-color-original-default);
    background-color: var(--PF-color-bg-second-default);
      box-shadow: 0 4px 8px rgba(0, 0, 0, .25);
    }

    .floaty i {
      transition: all .2s;
      margin:auto;
    }
  </style>
</head>

<body>
    <div class="PF floaty">
      <ul class="floaty-list">
        <li class="floaty-list-item">
          <span class="floaty-list-item-label">Barack (whitehouse.gov)</span>
        </li>
        <li class="floaty-list-item">
          <span class="floaty-list-item-label">George (hotmail.com)</span>
        </li>
        <li class="floaty-list-item">
          <span class="floaty-list-item-label">Reminder</span>
          <i class="material-icons">add</i>
        </li>
        <li class="floaty-list-item">
          <span class="floaty-list-item-label">Invite to Inbox</span>
          <i class="material-icons">add</i>
        </li>
      </ul>
      <div class="floaty-btn">
        <span class="floaty-btn-label">
        Compose
      </span>
        <i class="material-icons">add</i>
      </div>
    </div>
  
  <script>
    var $floaty = $('.floaty');

    $floaty.on('mouseover click', function(e) {
      $floaty.addClass('is-active');
      e.stopPropagation();
    });

    $floaty.on('mouseout', function() {
      $floaty.removeClass('is-active');
    });

    $('.container').on('click', function() {
      $floaty.removeClass('is-active');
    });
  </script>
</body>

</html>