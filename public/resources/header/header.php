<html>

<head>
  <style>
    .header {
      display: flex;
      flex-direction: column;
      position: sticky;
      z-index: 998;
      top: 0;
      right: 0;
      width: 100%;
      color: var(--PF-color-text-first-default);
      transition: .3s;
      background-color: rgb(var(--PF-color-background));
      max-height: 500px;
    }

    .header * {
      transition: .2s;
    }

    .header.hidden {
      max-height: 500px;
      width: 100%;
      opacity: 1;
      transition: .3s;
    }

    .header.hidden {
      max-height: 0;
    opacity: 0;
    overflow: hidden;
    }

    @media (min-width: 50em){
    body[scrolling='true'] .header:not(.search) {
      box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.1);
      background-color: rgba(var(--PF-color-surface), .95);
    }
    }
    
    .header>.container {
      display: flex;
      flex-direction: row;
      margin: 0;
      width: 100%;
      max-width: 100vw;
      transition: .2s;
      overflow: hidden;
    }
    
    .header>.container>.flex-container {
      display: flex;
      flex-direction: row;
      flex: 1 auto;
    }
    
    .header>.container>.flex-container>.flex {
      display: flex;
      flex-direction: row;
      flex: 1;
      align-items: center;
    }

    .header>.container>.flex-container>.flex>.flex-content {
      display: flex;
      flex-direction: row;
    }

    .header>.container>.flex-container>.flex.flex-center>.flex-content {
      flex: 1 auto;
    }

    .header>.container>.flex-container>.flex.flex-right {
      direction: rtl;
    }

    .header>.container>.flex-container>.flex.flex-right>.flex-content>.login-button {
      margin-right: .5em;
    }
    
    .header>.container>.flex-container>.flex.flex-center {
      flex: 100%;
      max-width: 40em;
    }
    
    .header>.container>.flex-container>.flex.flex-right>.flex-content .icons {
      display: flex;
      flex-direction: row;
    }
    
    .header>.container>.flex-container>.flex>.flex-content>.search {
      flex: 1;
      display: flex;
      flex-direction: row;
    }
    
    .header>.container>.flex-container>.flex>.flex-content>.search>.input {
      flex: 1;
      width: 100%;
      position: relative;
      border-radius: 2em;
      background-color: rgba(var(--PF-color-primary), 0.1);
      border-radius: 3em;
      min-height: fit-content;
      overflow: hidden;
      display: flex;
      flex-direction: row;
      left: 0;
      top: 0;
      width: 100%;
      transition: 0.3s;
      z-index: 1;
      padding: .2rem;
      margin: 0 .5rem;
    }
    
    .header>.container>.flex-container>.flex>.flex-content>.search>.input>i {
      padding: 0.5rem;
      position: relative;
      /* opacity: 0; */
      transition: .3s;
      margin: auto;
    }
    
    .header>.container>.flex-container>.flex>.flex-content>.search>.input>input {
      border: none;
      outline: none;
      border: none;
      color: rgb(var(--PF-color-on-surface));
      padding: .5em;
      width: 100%;
      font-size: inherit;
      display: block;
      text-align: left;
      display: inline-flex;
      transition: 0.2s;
      font-weight: 500;
      background-color: transparent;
    }
    
    .header>.container>.flex-container>.flex>.flex-content>.search>.input>input:not(:focus):not(:valid) {
      text-align: center;
    }

    .header>.container>.flex-container>.flex>.flex-content>.search>.input>.genius_search {
    padding: .5em;
    border-radius: 1.5em;
    background-color: rgba(var(--PF-color-surface), .9);
    display: flex;
    align-items: center;
    cursor: pointer;
    max-width: 65%;
    transition: .2s;
    }

    .header>.container>.flex-container>.flex>.flex-content>.search>.input>.genius_search:hover,
    .header>.container>.flex-container>.flex>.flex-content>.search>.input>.genius_search.active {
      padding-right: .8em;
      box-shadow: 0px 1px 3px 1px rgba(0,0,0, 0.1);
    }

    .header>.container>.flex-container>.flex>.flex-content>.search>.input>.genius_search:before {
    content: ' ';
    width: 1.5em;
    height: 1.5em;
    display: inline-block;
    margin-bottom: -.45em;
    background-image: url(//img.dumdarac.com/iconos/kamiku.svg);
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
    margin: auto;
    margin-right: .5em;
    transition: .2s;
    }

    .header>.container>.flex-container>.flex>.flex-content>.search>.input>.genius_search:not(:hover):not(.active):before {
      margin-right: 0;
    }

    .header>.container>.flex-container>.flex>.flex-content>.search>.input>.genius_search p {
    max-width: 50em;
    text-overflow: ellipsis;
    overflow: hidden;
    transition: .5s;
    width: fit-content;
    transition: .2s;
    font-weight: 500;
    white-space: nowrap;
    }

    .header>.container>.flex-container>.flex>.flex-content>.search>.input>.genius_search:not(:hover):not(.active) p {
      max-width: 0em;
    }
    
    .header>.container>.flex-container>.flex>.flex-content .logo {
      width: 40px;
      height: 40px;
      background-size: 2.5rem;
      background-position: center;
      background-repeat: no-repeat;
      background-image: url(//img.dumdarac.com/logo/logo.svg);
      border-radius: 1em;
      margin: auto 0;
    }
    
    @media (max-width: 50em) {

      .header>.container>.flex-container>.flex>.flex-content .logo,
      .header>.container>.flex-container>.flex>.flex-content>.search>.input>.genius_search {
        display: none;
      }
      
      .header:not(.search) {
        box-shadow: none;
        background-color: transparent;
      }

      body[scrolling='true'] .header:not(.search) {
        box-shadow: none;
        background-image: linear-gradient(rgb(var(--PF-color-background)), transparent);
      }
      
      .header.search {
        box-shadow: 0px 1px 3px 1px rgba(0, 0, 0, 0.1);
      }
      .header>.container {
        background-color: rgba(var(--PF-color-surface));
      }
      .header:not(.search)>.container {
        width: calc(100% - 2em);
        box-shadow: 0px 1px 3px 1px rgba(0, 0, 0, 0.1);
        border-radius: 1em;
        max-width: 50em;
        margin: 1em auto;
      }
      .header>.container>.flex-container>.flex>.flex-content>.search>.input {
        padding-left: 0;
        padding-right: 0;
        background-color: transparent;
        border-radius: 0;
        padding: .5rem;
        margin: 0 auto;
      }
      .header.search>.container>.flex-container>.flex.flex-left,
      .header.search>.container>.flex-container>.flex.flex-right {
        display: none;
      }
      .header:not(.search)>.container>.flex-container>.flex>.flex-content>.search>.input>i {
        display: none;
      }
      .header>.container>.flex-container>.flex.flex-right>.flex-content .icons {
        display: none;
      }
    }
  </style>
</head>

<body>

  <div class="PF header" id="header">
    <div class="container">
      <div class="flex-container">
        <div class="flex flex-left">
          <div class="flex-content">
            <div class="PF PF-icon margin ripple header_boton_menu"><i class="material-icons">menu</i></div>
            <div class="logo opendd" href="/"></div>
          </div>
        </div>
        <div class="flex flex-center">
          <div class="flex-content">
            <div class="search">
              <div class="input"><i class="material-icons search" id="buscariconoheader">&#xE8B6;</i>
                <input autocomplete="off" type="text" id="buscador" name="header-search" value="" placeholder="Search" required="" t-dd>
                <div class="PF genius_search d-o" opendd-href="?p=genius">
                  <p t-dd>Hi! How can I help you?</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="flex flex-right">
          <div class="flex-content">
            <div class="icons">
              <?php if($usuario_mismo_id) {?>
              <div class="PF PF-icon ripple d-o" title="DumDarac Pay" opendd-href="?app=pay"><i class="material-icons">money</i></div>
              <!----question_answer---->
              <div class="PF PF-icon ripple d-o opennotifications" title="DumDarac Center"><i class="material-icons">notifications_none</i></div>
              <?}?>
              <div class="PF PF-icon ripple applications-pageelementhide" opendd-href="?p=applications">
                <i class="material-icons" ><svg class="material-icons" viewBox="0 0 24 24" data-icon-id="apps" style="height: 24px; width: 24px;"><path d="M6 8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm6 12c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm-6 0c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0-6c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm6 0c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm4-8c0 1.1.9 2 2 2s2-.9 2-2-.9-2-2-2-2 .9-2 2zm-4 2c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm6 6c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm0 6c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2z"></path></svg></i>
              </div>
            </div>
            <?php if($usuario_mismo_id) {?>
              <div class="PF-avatar ripple ddbg-avatar" opendd-href="?p=perfil" rgdd></div>
            <?} else {?>
              <div class="PF PF-icon ripple login-button" openloginmodal opendd-href="?p=login" ><svg class="material-icons" style="height: 24px; width: 24px;" focusable="false" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path d="M0 0h24v24H0z" fill="none"></path><path clip-rule="evenodd" d="M19.07 4.93a10.01 10.01 0 0 0-14.15 0 9.99 9.99 0 1 0 14.15 0zM6.34 17.66c.86-.8 3.06-2.16 5.65-2.16s4.64 1.24 5.65 2.16a8 8 0 0 1-11.3 0zm12.56-1.64a10.7 10.7 0 0 0-13.81 0 8 8 0 0 1 12.56-9.68l-.01.01a7.97 7.97 0 0 1 1.26 9.67zM12 6a3 3 0 1 1 0 6 3 3 0 0 1 0-6z" fill-rule="evenodd"></path></svg></div>
            <?}?>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    /*$(document).on('click', '#buscador', function(e) {
      if ($('.search_container').length === 0) {
        openddgo('?p=search&f=trends');
      }
    });*/

    $(document).on('focus', '#buscador', function(e) {
      $('#header').addClass('search');
    });

    $(document).on('focusout', '#buscador', function(e) {
      $('#header').removeClass('search');
    });

    $('.buscador').click(function() {
        $('.buscador').attr('placeholder', 'Search');
        $('.buscador').css('background-image', 'none');
      });

    var timeoutloadsearch = null;
    $(document).on('keypress keyup keydown', '#buscador', function(e) {
      var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode == '13') {
          searchdd($(this).val());
        }
    });

  </script>

</body>

</html>