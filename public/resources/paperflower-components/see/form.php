<form class="PF PF-form">

  <div class="PF PF-input input">
    <input class="input-item" type="text" required="" id="input-field1">
    <label>First Name</label>
    <span class="bar"></span>
  </div>

  <div class="PF PF-input input">
    <input class="input-item" type="text" required="" id="input-field">
    <label>Last Name</label>
    <span class="bar"></span>
  </div>

  <div class="PF PF-textarea">
    <textarea class="input-item" type="text" required="" id="input-field"></textarea>
    <label>Textarea</label>
    <span class="bar"></span>
  </div>

  <div class="PF PF-input">
    <input class="input-item" type="email" id="email" required="" id="input-field">
    <label>Last Name</label>
    <span class="bar"></span>
  </div>

  <div class="PF PF-input">
    <select class="input-item">
        <option disabled="" selected=""></option>
        <option value="1">Option 1</option>
        <option value="2">Option 2</option>
        <option value="3">Option 3</option>
        <option value="4">Option 4</option>
      </select>
    <label>Make a selection</label>
    <span class="bar"></span>
  </div>

  <div class="PF-toggle">
    <input type="checkbox" id="toggle" name="toggle" checked=true class="switch" />
    <label for="toggle" class=""></label>
  </div>


  <div class="PF PF-buttons">
    <button class="PF PF-button ripple"><div class="PF-badge" >16</div>
    <div class="inside">
      <p>Send</p>
    </div>
  </button>

    <button class="PF PF-button ripple" disabled>
    <div class="inside">
      <p>Send</p>
    </div>
  </button>

    <button class="PF PF-button ripple transparent">
    <div class="inside">
      <p>Send</p>
    </div>
  </button>

    <button class="PF PF-button ripple border">
    <div class="inside">
      <p>Send</p>
    </div>
  </button>

    <div class="PF PF-button ripple border">
      <div class="inside">
        <p>Send</p>
      </div>
    </div>

  </div>
</form>