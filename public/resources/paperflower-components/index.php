<html><head>
  <?php include_once('../meta.php'); ?>
<style>

body {
    margin: 0;
    font-family: "Roboto", "Segoe UI", BlinkMacSystemFont, system-ui, -apple-system;
    font-size: 16px;
    color: rgb(var(--PF-color-on-surface));
    background-color: rgb(var(--PF-color-surface));
}

header {
    z-index: 1;
    position: -webkit-sticky;
    position: sticky;
    top: 0;
    padding: 16px;
    color: rgb(var(--PF-color-on-primary));
    background-color: rgb(var(--PF-color-primary));
    text-align: center;
    font-size: 36px;
    box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
}

nav {
  padding: 8px 0;
  text-align: center;
  color: rgb(var(--PF-color-surface));
  background-color: rgb(var(--PF-color-on-surface));
}

nav > a {
  color: rgb(var(--PF-color-primary));
}

.grid {
    display: grid;
    margin: 16px auto 64px;
    max-width: 720px;
    grid-template-columns: 200px minmax(240px, 1fr) minmax(240px, 1fr);
    grid-column-gap: 24px;
    grid-row-gap: 32px;
}

.column {
  font-size: 20px;
}

.title {
    padding-left: 40px;
    color: inherit;
    font-size: 18px;
}

.checkboxes > label,
.radios > label {
    display: block;
    margin: 16px 0;
}

.switches > label {
    width: 120px;
}

.switches > label:first-child {
    margin: 0 0 16px;
}

.textfields > label {
    margin: 16px 0;
}

.checkboxes > label:first-child,
.radios > label:first-child,
.textfields > label:first-child {
    margin: 0 0 16px;
}

.checkboxes > label:last-child,
.radios > label:last-child,
.textfields > label:last-child {
    margin: 16px 0 0;
}

.progresses > progress:first-child {
    margin: 0 0 16px;
}

.progresses > progress:last-child {
    margin: 16px 0 0;
}

footer {
    padding: 24px;
    text-align: center;
}

a {
    font-size: 18px;
    color: rgb(var(--PF-color-primary));
    text-decoration: none;
}

</style></head><body>
<div class="PF PF-toolbar center">
  <div class="PF PF-icon ripple"><i class="material-icons">&#xE5CD;</i></div>
  <h1>PaperFlower Components Showcase</h1>
</div>
<nav>UPDATE! This is being open-sourced here: <a href="https://gitlab.com/DumDarac/PaperFlower" target="_blank">https://gitlab.com/DumDarac/PaperFlower</a></nav>
<main class="grid">
    <div></div>
    <div class="column">Enabled</div>
    <div class="column">Disabled</div>

    <a class="title">Button - Contained</a>
    <div><button class="PF-button contained">Contained</button></div>
    <div><button class="PF-button contained" disabled="">Contained</button></div>

    <a class="title">Button - Outlined</a>
    <div><button class="PF-button outlined">Outlined</button></div>
    <div><button class="PF-button outlined" disabled="">Outlined</button></div>

    <a class="title">Button - Text</a>
    <div><button class="PF-button text">Text</button></div>
    <div><button class="PF-button text" disabled="">Text</button></div>

    <a class="title">Checkbox</a>
    <div class="checkboxes">
        <label class="PF-checkbox">
            <input type="checkbox">
            <span>Unchecked</span>
        </label>
        <label class="PF-checkbox">
            <input type="checkbox" checked="">
            <span>Checked</span>
        </label>
        <label class="PF-checkbox">
            <input type="checkbox">
            <span>Indeterminate</span>
        </label>
    </div>
    <div class="checkboxes">
        <label class="PF-checkbox">
            <input type="checkbox" disabled="">
            <span>Unchecked</span>
        </label>
        <label class="PF-checkbox">
            <input type="checkbox" checked="" disabled="">
            <span>Checked</span>
        </label>
        <label class="PF-checkbox">
            <input type="checkbox" disabled="">
            <span>Indeterminate</span>
        </label>
    </div>

    <a class="title">Radio Buttons</a>
    <div class="radios">
        <label class="PF-radio">
            <input type="radio" name="group-enabled">
            <span>Unchecked</span>
        </label>
        <label class="PF-radio">
            <input type="radio" name="group-enabled" checked="">
            <span>Checked</span>
        </label>
    </div>
    <div class="radios">
        <label class="PF-radio">
            <input type="radio" name="group-disabled" disabled="">
            <span>Unchecked</span>
        </label>
        <label class="PF-radio">
            <input type="radio" name="group-disabled" checked="" disabled="">
            <span>Checked</span>
        </label>
    </div>

    <a class="title">Switch</a>
    <div class="switches">
        <label class="PF-switch">
            <input type="checkbox">
            <span>Off</span>
        </label>
        <br>
        <label class="PF-switch">
            <input type="checkbox" checked="">
            <span>On</span>
        </label>
    </div>
    <div class="switches">
        <label class="PF-switch">
            <input type="checkbox" disabled="">
            <span>Off</span>
        </label>
        <br>
        <label class="PF-switch">
            <input type="checkbox" checked="" disabled="">
            <span>On</span>
        </label>
    </div>

    <a class="title">Slider</a>
    <div>
        <label class="PF-slider">
            <input type="range" min="0" max="100">
            <span>Slider</span>
        </label>
    </div>
    <div>
        <label class="PF-slider">
            <input type="range" min="0" max="100" disabled="">
            <span>Slider</span>
        </label>
    </div>

    <a class="title">Progress - Circular</a>
    <div><progress class="PF-progress circular"></progress></div>
    <div></div>

    <a class="title">Progress - Linear</a>
    <div class="progresses">
        <progress class="PF-progress linear" value="90" max="100"></progress>
        <progress class="PF-progress linear"></progress>
    </div>
    <div></div>

    <a class="title">Textfield - Filled</a>
    <div class="textfields">
        <label class="PF-textfield filled">
            <input placeholder=" ">
            <span>Empty</span>
        </label>
        <label class="PF-textfield filled">
            <input placeholder=" " value="Some text">
            <span>Non-empty</span>
        </label>
        <label class="PF-textfield filled">
            <textarea placeholder=" "></textarea>
            <span>Empty Textarea</span>
        </label>
        <label class="PF-textfield filled">
            <textarea placeholder=" ">Some text</textarea>
            <span>Non-empty Textarea</span>
        </label>
    </div>
    <div class="textfields">
        <label class="PF-textfield filled">
            <input placeholder=" " disabled="">
            <span>Empty</span>
        </label>
        <label class="PF-textfield filled">
            <input placeholder=" " value="Some text" disabled="">
            <span>Non-empty</span>
        </label>
        <label class="PF-textfield filled">
            <textarea placeholder=" " disabled=""></textarea>
            <span>Empty Textarea</span>
        </label>
        <label class="PF-textfield filled">
            <textarea placeholder=" " disabled="">Some text</textarea>
            <span>Non-empty Textarea</span>
        </label>
    </div>

    <a class="title">Textfield - Outlined</a>
    <div class="textfields">
        <label class="PF-textfield outlined">
            <input placeholder=" ">
            <span>Empty</span>
        </label>
        <label class="PF-textfield outlined">
            <input placeholder=" " value="Some text">
            <span>Non-empty</span>
        </label>
        <label class="PF-textfield outlined">
            <textarea placeholder=" "></textarea>
            <span>Empty Textarea</span>
        </label>
        <label class="PF-textfield outlined">
            <textarea placeholder=" ">Some text</textarea>
            <span>Non-empty Textarea</span>
        </label>
    </div>
    <div class="textfields">
        <label class="PF-textfield outlined">
            <input placeholder=" " disabled="">
            <span>Empty</span>
        </label>
        <label class="PF-textfield outlined">
            <input placeholder=" " value="Some text" disabled="">
            <span>Non-empty</span>
        </label>
        <label class="PF-textfield outlined">
            <textarea placeholder=" " disabled=""></textarea>
            <span>Empty Textarea</span>
        </label>
        <label class="PF-textfield outlined">
            <textarea placeholder=" " disabled="">Some text</textarea>
            <span>Non-empty Textarea</span>
        </label>
    </div>

    <a class="title">Textfield - Standard</a>
    <div class="textfields">
        <label class="PF-textfield standard">
            <input placeholder=" ">
            <span>Empty</span>
        </label>
        <label class="PF-textfield standard">
            <input placeholder=" " value="Some text">
            <span>Non-empty</span>
        </label>
        <label class="PF-textfield standard">
            <textarea placeholder=" "></textarea>
            <span>Empty Textarea</span>
        </label>
        <label class="PF-textfield standard">
            <textarea placeholder=" ">Some text</textarea>
            <span>Non-empty Textarea</span>
        </label>
    </div>
    <div class="textfields">
        <label class="PF-textfield standard">
            <input placeholder=" " disabled="">
            <span>Empty</span>
        </label>
        <label class="PF-textfield standard">
            <input placeholder=" " value="Some text" disabled="">
            <span>Non-empty</span>
        </label>
        <label class="PF-textfield standard">
            <textarea placeholder=" " disabled=""></textarea>
            <span>Empty Textarea</span>
        </label>
        <label class="PF-textfield standard">
            <textarea placeholder=" " disabled="">Some text</textarea>
            <span>Non-empty Textarea</span>
        </label>
    </div>

</main>
<footer>
    <a style="font-family: DumDarac Logo; font-weight: 900;" >DumDarac</a>
</footer>
<?php include_once(__DIR__.'/theming.php'); ?>

</body></html>