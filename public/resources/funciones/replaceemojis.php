<?php

function replaceemojis($text){
	//DumDarac
$text = str_replace (":DumDarac:", "<img alt=' DumDarac ' class='emoji' src='//img.dumdarac.com/logo/logo.svg'/>", $text);
$text = str_replace (":Dumdarac:", "<img alt=' DumDarac ' class='emoji' src='//img.dumdarac.com/logo/logo.svg'/>", $text);
$text = str_replace (":dumdarac:", "<img alt=' DumDarac ' class='emoji' src='//img.dumdarac.com/logo/logo.svg'/>", $text);
$text = str_replace (":DUMDARAC:", "<img alt=' DumDarac ' class='emoji' src='//img.dumdarac.com/logo/logo.svg'/>", $text);
$text = str_replace (":DD:", "<img alt=' DumDarac ' class='emoji' src='//img.dumdarac.com/logo/logo.svg'/>", $text);
$text = str_replace (":dd:", "<img alt=' DumDarac ' class='emoji' src='//img.dumdarac.com/logo/logo.svg'/>", $text);

$text = str_replace (":Kamiku:", "<img alt=' Kamiku ' class='emoji' src='//img.dumdarac.com/iconos/kamiku.svg'/>", $text);
$text = str_replace (":kamiku:", "<img alt=' Kamiku ' class='emoji' src='//img.dumdarac.com/iconos/kamiku.svg'/>", $text);
$text = str_replace (":KAMIKU:", "<img alt=' Kamiku ' class='emoji' src='//img.dumdarac.com/iconos/kamiku.svg'/>", $text);

//CONVERSION

$text = str_replace (":)", "😀", $text);
$text = str_replace (":-)", "😀", $text);
$text = str_replace (";)", "😉", $text);
$text = str_replace (";-)", "😉", $text);
$text = str_replace (":D", "😀", $text);
$text = str_replace (":-D", "😀", $text);
$text = str_replace (":|", "😐", $text);
$text = str_replace (":-|", "😐", $text);
$text = str_replace ("XD", "😆", $text);
$text = str_replace ("xD", "😆", $text);
$text = str_replace ("xD", "😆", $text);
$text = str_replace ("<3", "❤️", $text);
$text = str_replace ("&lt;3", "❤️", $text);
$text = str_replace (":'D", "😂", $text);
$text = str_replace (":')", "😂", $text);
$text = str_replace (":'(", "😢", $text);
$text = str_replace (":(", "☹️", $text);
$text = str_replace (":-(", "☹️", $text);
$text = str_replace ("=(", "☹️", $text);
$text = str_replace (":-*", "😘", $text);
$text = str_replace (":*", "😘", $text);
$text = str_replace ("=)", "😁", $text);
$text = str_replace (":loco:", "😵", $text);
$text = str_replace (":OMG:", "😱", $text);
$text = str_replace (":omg:", "😱", $text);
$text = str_replace (":O", "😮", $text);
$text = str_replace (":O", "😮", $text);

return $text;
}

?>