var MediaStream = null;
  var openddajax = null;
  var firstopendd = null;
  var lasturlopendd = null;

  function cleanopendd() {
    $('.contenedor').empty();
    $('#otherheader').empty();
    $('#topbar').empty();
    $('#bottombar').empty();
    $('#menuleft').empty();
    $('.header .logo span#titlepage').empty();
    $('.PF-hidedcontent.open').removeClass('open');
    $('#header').removeClass('shadow');
    $('#body').removeClass('scroll-start');
    $('#body').removeClass('surface-enabled');
    $('.PF-tabbar.headerbajando').removeClass('headerbajando');
    $('.PF-tabbar.full').removeClass('full');
    $('.PF-tabbar.flex').removeClass('flex');
    $('.header').removeClass('hidden');
    $('#loading').hide();
    $('#downmenudd').removeClass('open');

    if($('.search_container').length === 0){
      $('#buscador').val('');
    }

    if(typeof closeloginmodal === "function"){
      closeloginmodal();
    }
    
    //WINDOWDD
    $('#body').removeClass('overflowhidden');
    $('.side-menu-overlay').removeClass('show').removeClass('white').removeClass('closewindowdd');
    $('#side-menu-left').removeClass('active');
    $('#ddwindow .PF-progress.loading').show();
    $('#ddwindow').removeClass('open');
    //WINDOWDD
    
    $("body[class*='PFC-']").removeClass (function (index, css) {
      return (css.match (/(^|\s)PFC-\S+/g) || []).join(' ');
    });
    if (MediaStream !== null) {
      MediaStream.stop();
    }
  }

  function opendd(queabrir, click) {
    $('#loading').show();
    if(navigator.onLine){
    if (queabrir === '/') {
      queabrir = "";
    }
    openddajax = $.ajax({
      url: "./screen.php" + queabrir,
      type: "get",
      cache: false,
      contentType: false,
      processData: false,
      beforeSend: function() {
        $('#loading').show();
        if (openddajax != null) { openddajax.abort(); }
        for( var x = 0; x < xhr.length; x++ ){ xhr[x].abort(); xhr.splice(x, 1); }
      },
      success: function(data) {
        $('html').animate({
          scrollTop: 0
        }, "slow", function() {
          $('#loading').hide();
          cleanopendd();
          $('.contenedor').off().empty().html(data).find("script").each(function(i) {
            eval($(this).text());
            return false;
          });
        });
        
      },
      error: function (xhr, ajaxOptions, thrownError) {
        $('#loading').hide();
        if(thrownError !== 'abort' && thrownError){
          alertdd.show('Error: ' + thrownError);
          window.history.back();
          openddgo('?p=hole');
        }
      }
    });
    }
    lasturlopendd = queabrir;
    return false;
  }

  $(document).on("click", ".opendd, [opendd-href]", function(e) {

    e.preventDefault();

    var $this = $(this),
      title = $this.text();

    if ($(this).attr('href')) {
      var url = $(this).attr('href');
    } else {
      var url = $(this).attr('opendd-href');
    }
    if(!url){ url = "/"; }
    if(lasturlopendd !== url){
      history.pushState({ url: url, title: "title" }, "title", url);
    }

    opendd(url);
    return false;
  });
  
  var timeoutloadopenddgo = null;
  function openddgo(url){
    if(!url){ url = "/"; }
    clearTimeout(timeoutloadopenddgo);
    timeoutloadopenddgo = setTimeout(function() {
      if(lasturlopendd !== url){
      history.pushState({ url: url, title: "title" }, "title", url);
      }
      opendd(url);
      return false;
    }, 500);
  }

  
  var timeoutloadpopstate = null;
  window.onpopstate = function( e ) {
    clearTimeout(timeoutloadpopstate);
    timeoutloadpopstate = setTimeout(function() {
      var state = e.state;
      if(state !== null) {
        opendd(state.url);
      } else {
        opendd("");
      }
    }, 500);
  }