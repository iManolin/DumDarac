var timeouttddcue;
var tddcuearray = [];

$.initialize("[t-dd]:not([t-dd='no'])", function() {
	tddcuearray.push($(this));
	clearTimeout(timeouttddcue);
	timeouttddcue = setTimeout(tddcue, 500);
});


$(document).on("DOMSubtreeModified", "[t-dd='update']", function() {
		tddcuearray.push($(this));
		clearTimeout(timeouttddcue);
		timeouttddcue = setTimeout(tddcue, 500);
});

function tddcue(){
	if(language !== "en-US"){
	var tddcuearraytexts = [];
	tddcuearray.forEach(function(item){
		if(item.text()){
			if (!item.attr("tdd-original")) {
				item.attr("tdd-original", item.text());
			}
			if (!item.attr("tdd-type")) {
				item.attr("tdd-type", "text");
			}
			tddcuearraytexts.push(item.text());
		}
		if(item.attr('placeholder')){
			if (!item.attr("tdd-original")) {
				item.attr("tdd-original", item.attr('placeholder'));
			}
			if (!item.attr("tdd-type")) {
				item.attr("tdd-type", "placeholder");
			}
			tddcuearraytexts.push(item.attr('placeholder'));
		}
	});
		$.ajax({
				url: "./apps/translate/resources/translatearray.php",
				type: "POST",
				data: {
					from: "en-US",
					to: language,
					arraytexts: tddcuearraytexts
				},
				success: function(data) {
					if(data){
					var data = JSON.parse(data);
					if (data) {
						data.forEach(function(item){
							if(item.translation){
								$('[t-dd][tdd-type="text"][tdd-original="'+item.original+'"]').text(item.translation);
								$('[t-dd][tdd-type="placeholder"][tdd-original="'+item.original+'"]').attr("placeholder", item.translation);
							} else {
								$('[t-dd][tdd-original="'+item.original+'"]').attr('t-dd', 'no');
							}
						});
					}
					}
				},
				error: function() {
					estetranslate.attr('t-dd', 'no');
				}
			});
	tddcuearray = [];
	}
}
