/*RELOADS THE IMAGE WITH THE WIDTH EXACT OF THE DIV*/
$.initialize("[rgdd]:not(.resized), img:not(.resized)", function() {
  rgdd($(this));
});


function rgdd(este){
  
  var pregpre = "?";
  var src = este.attr("src");
  var background = este.css('background-image').replace('url(', '').replace(')', '').replace(/\"/gi, "");
  
  if(src && src.includes("dumdarac.com/") || background && background.includes("dumdarac.com/")){

  if(src && este.innerWidth()) {
    este.attr('data-src', src);
    este.attr("src", '');
    if(src.includes("?")){
      pregpre = "&";
    }
    if(este.attr('data-src')){
      este.attr("src", este.attr('data-src') + pregpre + 'w=' + este.innerWidth() + "px" + '&disabledh=' + este.innerHeight() + "px").addClass('resized');
    }
    
  }
  
  
  if(background && este.innerWidth() && background !== 'none') {
    este.attr('data-bg', background);
    este.css('background-image', '');
    if(background.includes("?")){
      pregpre = "&";
    }
    
    if(este.attr('data-bg')){
      este.css('background-image', 'url(' + este.attr('data-bg') + pregpre + 'w=' + este.innerWidth() + "px" + '&disabledh=' + este.innerHeight() + "px" + ')').addClass('resized');
    }
    
  }
}
}