$(document).ready(function() {
  var androidTapSound = document.createElement("audio");
  androidTapSound.src = "//dumdarac.com/resources/sounds/Effect_Tick.ogg";
  androidTapSound.volume = 0.2;
  androidTapSound.autoPlay = false;
  androidTapSound.preLoad = true;
  androidTapSound.controls = true;

  $(document).on("click", "[opendd-href], .PF-button, .PF-avatar, .PF-icon, .PF-chip, .PF-textfield, li", function() {
    androidTapSound.play();
    // androidTapSound.pause();
  });
});