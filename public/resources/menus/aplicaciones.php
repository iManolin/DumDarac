<html>

<head>
  <style>
    #aplicacioneslauncher {
      position: relative;
    }

    #aplicacioneslauncher:before {
      width: 0;
      height: 0;
      border-left: 10px solid transparent;
      border-right: 10px solid transparent;
      content: ' ';
      border-bottom: 10px solid #ffffff;
      position: absolute;
      top: 4px;
      right: 1.1rem;
      z-index: 99;
    }

    .menu_aplicaciones {
      position: fixed;
      bottom: 0;
      background: white;
      border-radius: 0px;
      padding: 0.5rem;
      width: calc(100% - 1rem);
      height: calc(100% - 64px - 1rem);
      z-index: 99999;
      overflow: hidden;
      overflow-y: auto;
      display: none;
      transition: 0.5s;
    }

    @media screen and (min-width: 769px) {
      #aplicacioneslauncher:before {
        top: 10px;
      }
      .menu_aplicaciones {
        position: fixed;
        top: 4.5rem;
        right: 1rem;
        background: white;
        border-radius: 2px;
        max-width: 22rem;
        max-height: 20rem;
        box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .16), 0 2px 5px 0 rgba(0, 0, 0, .23);
      }
    }

    .menu_aplicaciones .buscador-aplicaciones {
      width: calc(100% - 1rem);
      padding: 0.5rem;
      display: inline-flex;
      align-items: center;
    }

    .menu_aplicaciones .buscador-aplicaciones input {
      padding: 0.5rem;
      width: calc(100% - 1rem);
      font-size: 1rem;
      border: none;
    }

    .menu_aplicaciones .buscador-aplicaciones i {
      padding: 0.5rem;
      font-size: 1.5rem;
      color: #3490e8;
      cursor: pointer;
    }

    .menu_aplicaciones .conjunto {
      margin: 0.5rem;
      padding: 0.5rem;
      width: calc(100% - 2rem);
      height: auto;
      border-radius: 2px;
      background: rgba(238, 238, 238, 0.35);
    }

    .menu_aplicaciones .conjunto .nombreconjunto {
      font-size: 1rem;
      padding: 0.5rem;
    }

    .menu_aplicaciones .conjunto .contenedor {
      padding: 0;
      text-align: left;
      width: 100%;
    }

    .menu_aplicaciones .conjunto .contenedor .app {
      width: 3rem;
      height: 3rem;
      border: solid 2px #eee;
      border-radius: 2px;
      background: white;
      background-size: 2rem;
      background-position: center;
      background-repeat: no-repeat;
      margin: 0.2rem;
      display: inline-block;
    }

    .menu_aplicaciones .conjunto .contenedor .app.play {}
  </style>
</head>

<body>

  <div class="menu_aplicaciones">
    <div class="buscador-aplicaciones">
      <i class="material-icons">&#xE8B6;</i>
      <input type="text" placeholder="<?=BUSCAR?>" />
      <a href="?p=configuracion&open=applauncher"><i class="material-icons">&#xE8B8;</i></a>
    </div>
    <div class="conjunto">
      <h1 class="nombreconjunto">
        <?=APLICACIONES?>
      </h1>
      <div class="contenedor">
        <?php
	$menu_left_publicaciones = mysqli_query($con,"SELECT * FROM aplicaciones_dd WHERE eliminada='' AND tipo='' ORDER BY tipo ASC, $idioma ASC");
	while($row_menu_left_publicaciones = mysqli_fetch_array($menu_left_publicaciones))
		{ 
			$id_menu_left_publicaciones = $row_menu_left_publicaciones["id"];
      $name_menu_left_publicaciones = $row_menu_left_publicaciones["nombre"];
      $tipo_menu_left_publicaciones = $row_menu_left_publicaciones["tipo"];
			$nombreapp_menu_left_publicaciones = $row_menu_left_publicaciones[$idioma];
    if($tipo_menu_left_publicaciones == ''){$tipo_menu_left_publicaciones = 'app';}
            ?>
          <a href="?<?=$tipo_menu_left_publicaciones?>=<?=$name_menu_left_publicaciones?>" alt="<?=$nombreapp_menu_left_publicaciones?>"><div class="app" style="background-image:url('//img.dumdarac.com/iconos/<?=$name_menu_left_publicaciones?>.png');" ></div></a>
          <?}?>
      </div>
    </div>

    <!---------------
      <div class="conjunto" >
        <h1 class="nombreconjunto" >Accesorios</h1>
        <div class="contenedor" >
<a href="?app=news" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/news.png');" ></div></a>
          <a href="?app=comida" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/comida.png');" ></div></a>
          <a href="?app=blogs" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/blogs.png');" ></div></a>
          <a href="?app=now" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/now.png');" ></div></a>
          <a href="?app=revisores" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/revisores.png');" ></div></a>
          <a href="?app=traductor" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/traductor.png');" ></div></a>
          <a href="?app=express" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/express.png');" ></div></a>
          <a href="?app=eventos" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/eventos.png');" ></div></a>
          <a href="?app=transporte" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/transporte.png');" ></div></a>
          <a href="?app=fotos" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/fotos.png');" ></div></a>
          <a href="?app=fit" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/fit.png');" ></div></a>
          <a href="?app=car" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/car.png');" ></div></a>
          <a href="?app=alertas" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/alertas.png');" ></div></a>
          <a href="?app=tendencias" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/tendencias.png');" ></div></a>
          <a href="?app=wallet" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/wallet.png');" ></div></a>
          <a href="?app=educacion" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/educacion.png');" ></div></a>
          <a href="?app=cloud" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/cloud.png');" ></div></a>
          <a href="?app=ropa" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/ropa.png');" ></div></a>
          <a href="?app=viajes" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/viajes.png');" ></div></a>
          <a href="?app=spinner" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/spinner.png');" ></div></a>
          <a href="?p=timelines" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/timelines.png');" ></div></a>

          <a href="?app=calendario" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/calendario.png');" ></div></a>
          <a href="?app=camara" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/camara.png');" ></div></a>
          <a href="?app=contactos" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/contactos.png');" ></div></a>
          <a href="?app=recordatorios" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/recordatorios.png');" ></div></a>
          <a href="?app=calculadora" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/calculadora.png');" ></div></a>
          <a href="?app=reloj" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/reloj.png');" ></div></a>
          <a href="?app=wifi" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/wifi.png');" ></div></a>
          <a href="?app=devices" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/devices.png');" ></div></a>
        </div>
      </div>
      
      <div class="conjunto" >
        <h1 class="nombreconjunto" >Oficina</h1>
        <div class="contenedor" >
          <a href="?app=documentos" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/documentos.png');" ></div></a>
          <a href="?app=presentaciones" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/presentaciones.png');" ></div></a>
          <a href="?app=dibujos" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/dibujos.png');" ></div></a>
          <a href="?app=excel" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/excel.png');" ></div></a>
          <a href="?app=formularios" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/formularios.png');" ></div></a>
          <a href="?app=negocios" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/negocios.png');" ></div></a>
          <a href="?app=trabajo" ><div class="app" style="background-image:url('//img.dumdarac.com/iconos/trabajo.png');" ></div></a>
        </div>
      </div>
      ------------->
    <div class="conjunto">
      <h1 class="nombreconjunto">Play Store</h1>
      <div class="contenedor">
        <?php
	$menu_left_publicaciones = mysqli_query($con,"SELECT * FROM aplicaciones_dd WHERE eliminada='' AND tipo='play' ORDER BY $idioma DESC");
	while($row_menu_left_publicaciones = mysqli_fetch_array($menu_left_publicaciones))
		{ 
			$id_menu_left_publicaciones = $row_menu_left_publicaciones["id"];
      $name_menu_left_publicaciones = $row_menu_left_publicaciones["nombre"];
      $tipo_menu_left_publicaciones = $row_menu_left_publicaciones["tipo"];
			$nombreapp_menu_left_publicaciones = $row_menu_left_publicaciones[$idioma];
    if($tipo_menu_left_publicaciones == ''){$tipo_menu_left_publicaciones = 'app';}
            ?>
          <a href="?<?=$tipo_menu_left_publicaciones?>=<?=$name_menu_left_publicaciones?>" alt="<?=$nombreapp_menu_left_publicaciones?>"><div class="app" style="background-image:url('//img.dumdarac.com/iconos/<?=$name_menu_left_publicaciones?>.png');" ></div></a>
          <?}?>
      </div>
    </div>
  </div>

</body>

</html>