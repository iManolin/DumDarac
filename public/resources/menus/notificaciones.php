<html>

<head>
  <style>
    .notif-content p.title_notificaciones {
      font-size: 1rem;
      padding: 1rem;
      margin: 0;
      background: #167ac6;
    }

    .notif-content img {
      Max-height: 100%;
      Max-width: 100%;
      float: right;
    }

    .notif-content ul,
    li {
      margin: 0;
      padding: 0;
      List-style-type: none;
    }

    .notif-content a {
      color: inherit;
      text-decoration: none;
    }

    .noselect {
      -webkit-touch-callout: none;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }

    .tagme {
      color: #FF8000;
    }

    .next,
    .button {
      display: none;
    }

    .button {
      text-align: left;
      padding-top: 5px;
      text-transform: uppercase;
      font-weight: 500;
      color: #808080;
    }

    .notif-content {
      Background-color: transparent;
      width: 100%;
      max-width: 28rem;
      Margin: auto;
      Position: fixed;
      right: 0;
      top: -100%;
      left: 0;
      z-index: 999;
      Color: white;
      Text-align: center;
      transition: 0.5s;
      overflow-x: hidden;
      cursor: pointer;
      line-height: 2rem;
      max-height: 0;
    }

    .notif-contentVisible {
      top: 0px;
      max-height: 90%;
    }

    .notif-content li.notif {
      Border-radius: 0px;
      Padding: 1rem;
      Text-align: left;
      Background-color: #fff;
      Position: relative;
      transition: 0.5s;
      border-top: solid 1px #eee;
    }

    .notif-content li.notif .notif_img {
      Position: absolute;
      Height: 40px;
      Width: 40px;
      Border-radius: 50px;
      Background-color: #eee;
      Text-align: center;
      Color: #eee;
      -webkit-touch-callout: none;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      /*overflow: hidden;*/
      align-items: center;
      display: inline-flex;
      background-size: cover;
      background-position: center;
    }

    .notif-content li.notif .notif_img img {}

    .notif-content li.notif .notif_img .img_aplicacion {
      max-width: 1.5rem;
      margin: 0.5rem;
    }

    .notif-content li.notif .notif_img i {
      Line-height: 40px;
    }

    .notif-content li.notif .notif_img .object {
      display: inline-block;
      height: 15px;
      width: 15px;
      border-radius: 50px;
      background-color: inherit;
      position: absolute;
      bottom: 0px;
      right: 0px;
      overflow: hidden;
    }

    .notif-content li.notif .notif_img .object.tag {
      background-color: transparent;
      line-height: 10px;
    }

    .notif-content li.notif .notif_img .object.message {
      background-color: #428bca;
    }

    .notif-content li.notif .notif_img .object.message i {
      font-size: 12px;
      line-height: 20px;
    }

    .notif-content li.notif .notif_img .object.reply i {
      font-size: 14px;
      line-height: 18px;
    }

    .notif-content li.notif>.text {
      vertical-align: top;
      Padding-left: 50px;
    }

    .notif-content li.notif>.text .title {
      max-height: 80px;
      line-height: 20px;
      color: black;
      padding-right: 50px;
      text-overflow: ellipsis;
      overflow: hidden;
      width: 100%;
    }

    .notif-content li.notif>.text .title .titulo {
      text-overflow: ellipsis;
      /*white-space: nowrap;*/
      overflow: hidden;
    }

    .notif-content li.notif>.text .title span {
      Float: right;
      Font-size: 14px;
      Color: #595959;
      margin-right: -50px;
      display: none;
    }

    .notif-content li.notif>.text .subtitle {
      Padding-top: 2px;
      Font-size: 14px;
      Line-height: 18px;
      padding-right: 50px;
      -webkit-touch-callout: none;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      transition: 0.5s;
      color: rgba(42, 43, 43, 0.73);
      font-weight: 900;
    }

    .notif-content:after {
      content: "";
      Display: block;
      Height: 0;
      Width: 0;
      Border-width: 10px;
      Border-color: transparent transparent #e0e0e0;
      Border-style: solid;
      Position: absolute;
      Top: -20px;
      Right: 92px;
    }

    .notificaciones_fondonegro {
      position: fixed;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      z-index: 998;
      display: none;
      background: transparent;
      transition: 0.5s;
    }

    .notificaciones_fondonegroActivo {
      display: block;
      background: rgba(0, 0, 0, 0.27);
    }

    span.object.tag svg {
      width: 12px;
      height: 12px;
      margin: 4px;
    }

    .trnotificacion {
      transform: translate(350px, 0);
    }
  </style>
</head>

<body>
  <ul id="notif-content" class="notif-content" onclick="setTimeout(function(){ vermenunotificaciones() }, 2000);">

  </ul>
  <script>
    $('.notif').on('click', function() {
      $(this).find('.etc, .next, .button').toggle();
    });
  </script>

  <div class="notificaciones_fondonegro" onclick="vermenunotificaciones()">

  </div>

</body>

</html>