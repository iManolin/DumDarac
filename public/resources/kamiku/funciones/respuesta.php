<html>
	<head>
		<style>
		      /*CONVERSACION*/
			
			.respuestakamiku {
    width: calc(100% - 1rem);
    height: calc(100% - 10rem);
    padding: 0.5rem;
    margin: auto;
    overflow: hidden;
    overflow-y: auto;
    position: relative;
    display: inline-flex;
    align-items: center;
	}

	.respuestakamiku .espacio_mensaje {
		width: 100%;
		height: auto;
		display: inline-block;
		text-align:left;
	}

	.respuestakamiku .grupo_mensajes {
		width: auto;
		max-width: calc(100% - 1rem);
		height: auto;
		padding: 0;
		border-radius: 25px;
		color: white;
		font-size: 1rem;
		display: inline-flex;
		align-items: flex-start;
		margin: 0 0.5rem;
		transition: 0.5s;
	}

	.respuestakamiku .grupo_mensajes:active {
		box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
		background: #ddd;
		align-items:center;
	}

	.respuestakamiku .gm1:active {
		padding-left: 0.5rem;
	}

	.respuestakamiku .gm2:active {
		padding-right: 0.5rem;
	}

	.respuestakamiku .grupo_mensajes .mensajes .mensaje {
		margin: 5px 0rem;
		display: inline-flex;
		width: 100%;
    position: relative;
    -webkit-transition: opacity 0.75s, background-color 0.3s;
    transition: opacity 0.75s, background-color 0.3s;
		align-items:center;
	}
			
			.respuestakamiku .grupo_mensajes .mensajes .mensaje .check {
				display:none;
			}
			
		.respuestakamiku .grupo_mensajes .mensajes .mensaje:active .check {
				    color: #607D8B;
    font-size: 1.2rem;
    z-index: 1;
    background: rgba(255, 255, 255, 0.75);
    border-radius: 100%;
    padding: 0.5rem;
			margin-left:0.5rem;
			transition:0.5s;
			display:block;
			}
			
			.respuestakamiku .grupo_mensajes .mensajes .mensaje:active .doblecheck {
				    color: #2196f3;
			}

	.respuestakamiku .grupo_mensajes .mensajes .mensaje p {
		border-radius: 20px;
		margin: 0 0.5rem;
		padding: 0.5rem 0.7rem;
		min-width: 0.9rem;
		text-align: center;
    align-items: center;
		word-break:break-word;
		font-size:1.3rem;
	}
			
			.respuestakamiku .grupo_mensajes .mensajes .mensaje p .emoji {
		margin-left:1px;
		margin-right:1px;
	}

	.respuestakamiku .grupo_mensajes .avatar {
    min-width: 2rem;
    min-height: 2rem;
    background-color: #ddd;
    border-radius: 100%;
    margin: 0rem;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    background-size: cover;
    background-position: center;
    cursor: pointer;
		margin-top:0.5rem;
	}
			
			.respuestakamiku .grupo_mensajes:active .avatar {
				margin:0;
			}

	.respuestakamiku .grupo_mensajes .avatarm2 {
		background-image: url('http://dumdarac.com/usuario/avatares/<?=$usuario_avatar_id_mismo?>/<?=$avatar_mismo?>');
	}

	.respuestakamiku .gm1 {
		float: left;
	}

	.respuestakamiku .gm2 {
		float: right;
	}

	.respuestakamiku .m1 p {
		background: #693bbb;
	}

	.respuestakamiku .m2 p {
		background:#0aaabd;
	}

	.respuestakamiku .m3 p {
		background: white;
		color: black;
	}
			
			
			
	/*----------------KAMIKU--------------------------*/
			
			.respuestakamiku .grupo_mensajes_kamiku {
				padding: 0 0.5rem;
			}
			
			.respuestakamiku .grupo_mensajes_kamiku .mensajes_kamiku {
				padding-left:0.5rem;
			}
			
			.respuestakamiku .grupo_mensajes_kamiku .mensajes_kamiku .kamiku_mensaje_inline {
				display:inline-flex;
				align-items:center;
			}
			
			.respuestakamiku .grupo_mensajes_kamiku .mensajes_kamiku span {
				font-weight:900;
			}
			
			.respuestakamiku .grupo_mensajes_kamiku .avatar {
				box-shadow:none;
				background-color:transparent;
				background-image:url('http://img.dumdarac.com/kamiku/icono.png');
			}
			
	.respuestakamiku .grupo_mensajes_kamiku .mensajes_kamiku .mensaje_kamiku {
background: rgba(255, 255, 255, 0.75);
    border: 1.1px solid #e0e0e0;
    border-radius: 16.5px;
    color: #212121;
    font-family: Product Sans;
    font-weight: 400;
		cursor:pointer;
	}
			
			.respuestakamiku .grupo_mensajes .mensajes .mensaje_kamiku p {
				color:black;
			}
			
			
			.respuestakamiku .espacio_mensaje_kamiku .respuestas {
	  display: inline-flex;
    align-items: center;
    padding: 0.5rem;
    width: calc(100% - 1rem);
    border-radius: 20px;
    padding-top: 0;
				overflow:hidden;
				overflow-x:auto;
			}
			
			.respuestakamiku .espacio_mensaje_kamiku .respuestas::-webkit-scrollbar {
    width: 10px;
    background-color: #EAEAEA;
    height: 5px;
  
}
      
      .respuestakamiku .espacio_mensaje_kamiku .respuestas::-webkit-scrollbar-thumb {
        background-color: #F90;
}
			
			.respuestakamiku .espacio_mensaje_kamiku .respuestas .icono_kamikuchat {
				padding:0.5rem;
				border-radius:100%;
				background:white;
				margin-right:0.5rem;
				border: 1.1px solid #00695C;
    color: #00695C;
				cursor:pointer;
			}
			
			.respuestakamiku .espacio_mensaje_kamiku .respuestas .icono_kamikuchat:hover {
				box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
			}
			
			.respuestakamiku .espacio_mensaje_kamiku .respuestas .respuesta {
    background: white;
    border-radius: 20px;
    padding: 0.5rem;
    margin: 0 0.2rem;
    border: 1.1px solid #e0e0e0;
    color: #009688;
    cursor: pointer;
    transition: 0.5s;
    display: inline-table;
    width: auto;
    font-size: 1rem;
			}
			
			.respuestakamiku .espacio_mensaje_kamiku .respuestas .respuesta:hover {
				box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
			}
			
		</style>
	</head>
  <body>
    <?php 
  $pregunta = $_GET['pregunta'];
			if($pregunta == ''){
				$pregunta = $_POST['pregunta'];
			}
		$pregunta = strtolower($pregunta);
  include('../../acceso_db.php');
  include('../../datos-usuario.php');
	include("../../resources/idiomas/idiomas.php");

$respuesta_kamiku = mysqli_query($con,"SELECT * FROM kamiku_inteligencia WHERE posibles LIKE '%$pregunta%' OR comando LIKE '%$pregunta%' LIMIT 1");
	while($row_respuesta_kamiku = mysqli_fetch_array($respuesta_kamiku))
  { 
	$nombre_kamiku_resultados_chat = $row_respuesta_kamiku["texto"];
  $id_respuesta_kamiku = $row_respuesta_kamiku["id"]; 
		$id_mensaje_chat = $row_respuesta_kamiku["id"]; 
		$funcion_kamiku_resultados_chat = $row_respuesta_kamiku["funcion"]; 
		$respuestas_kamiku_resultados_chat = $row_respuesta_kamiku["respuestas"]; 
		
		$emoticonos = $nombre_kamiku_resultados_chat;
	      include("../../emoticonos.php");
	     $nombre_kamiku_resultados_chat = $emoticonos;
      
      include('../../kamiku/chat/hora.php');
	?>
	
		
			<div class="espacio_mensaje espacio_mensaje_kamiku" ><div class="grupo_mensajes grupo_mensajes_kamiku" >
		<a href="?p=perfil&id=518" ><div class="avatar avatar_kamiku" ></div></a>
		<div class="mensajes mensajes_kamiku" >
      <div class="kamiku_mensaje_inline" >
  
		<div class="mensaje mensaje_kamiku" onclick="$('#mensajehoraid<?=$id_mensaje_chat?>').toggle();" id="mensajeid<?=$id_mensaje_chat?>" ><p><?=$nombre_kamiku_resultados_chat?></p></div>
      </div>
      
      </div>
		</div>
		
				<?php if($respuestas_kamiku_resultados_chat != ''){?>
  <div class="respuestas" >
     <i class="material-icons icono_kamikuchat">&#xE15E;</i>
    
    <?php $respuestas_lista_kamiku_chat = explode(", ", $respuestas_kamiku_resultados_chat); 
    foreach ($respuestas_lista_kamiku_chat as $respuesta_kamiku_chat_r) {
    ?>
     <div class="respuesta" onclick="respuestakamikuinteligencia('<?=$respuesta_kamiku_chat_r?>')" ><?=$respuesta_kamiku_chat_r?></div>
    <?} ?>
				</div>
				<?}?>
		
		
		</div>
		
		<script>
			function respuestakamikuinteligencia(mensajeint) {
       var inputchatkmkrespuestaint = $("#preguntakamikuinput" );
      inputchatkmkrespuestaint.val( inputchatkmkrespuestaint.val() + mensajeint );
      setTimeout("enviarrespuestainteligenciakamiku()",1000);
    }
			
			function enviarrespuestainteligenciakamiku(){
				$('#formrespuestakamiku').submit();
			}
			
		$('#alertaskamiku').load('./resources/kamiku/funciones/alertas.php?decir=<?=urlencode("$nombre_kamiku_resultados_chat")?>');
		</script>

	<?}?>
  </body>
</html>