<html>

<head>
	<style>
		.loginmodal {
			position: fixed;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
			width: 100%;
			height: 0px;
			overflow: hidden;
			background-color: rgba(var(--PF-color-surface), .95);
			display: flex;
			flex-direction: column;
			align-items: center;
			justify-content: center;
			transition: 0.4s;
			z-index: 999;
		}
		
		.loginmodal .loginmodal-container {
			display: flex;
			flex-direction: column;
			width: 100%;
			height: 0%;
			border-radius: 10px;
			overflow: hidden;
			opacity: 0;
			pointer-events: none;
			transition-duration: 0.3s;
			-webkit-transform: translateY(100px) scale(0.4);
			transform: translateY(100px) scale(0.4);
		}
		
		.loginmodal.is-open {
			height: 100vh;
			overflow: auto;
		}
		
		.loginmodal.is-open .loginmodal-container {
			opacity: 1;
			height: 100%;
			transition-duration: 0.5s;
			pointer-events: auto;
			-webkit-transform: translateY(0) scale(1);
			transform: translateY(0) scale(1);
		}
	</style>
</head>

<body>
	<div class="loginmodal PF shadow">
		<div class="PF PF-icon ripple" closeloginmodal style=" position: absolute; top: .5em; right: .5em; z-index: 1;"><i class="material-icons">close</i></div>
		<div class="loginmodal-container"></div>
	</div>
	<script>
		var isOpenedloginmodal = false;

		function openloginmodal() {
			isOpenedloginmodal = true;
			$.get("./pantallas/login/index.php?modal=true", function(data){
				$('.loginmodal>.loginmodal-container').html(data);
				setTimeout(function(){ 
					$(".loginmodal").addClass("is-open");
					$("body").addClass('overflowhidden');
				}, 500);
			});
		};

		function closeloginmodal() {
			$(".loginmodal").removeClass("is-open");
			$("body").removeClass('overflowhidden');
			setTimeout(function(){ 
				$('.loginmodal>.loginmodal-container').empty();
			}, 500);
		};

		$(window).scroll(function() {
			if (lasturlopendd.includes("?app=") && $(window).scrollTop() > window.innerHeight / 3 && !isOpenedloginmodal) {
				openloginmodal();
			}
		});

		$(document).on("click", "[closeloginmodal]", function() {
			closeloginmodal();
		});

		$(document).on("click", "[opendd-href='?p=login']", function(e) {
			if(lasturlopendd.includes("?app=")){
			openloginmodal();
			e.off();
			e.preventDefault();
			e.stopPropagation();
			return false;
			}
		});
	</script>


</body>

</html>