<html>
  <head>
    <style>
      
      @font-face {
src: url(../../resources/fuentes/DumDarac.ttf);
font-family: DumDarac;
}
      
      body {
            display: inline-flex;
    align-items: center;
    margin: 0;
    height: 100%;
    width: 100%;
    text-align: center;
      }
      
      h1 {
    font-family: DumDarac;
    margin: auto;
    font-size: 15rem;
    background: -webkit-linear-gradient(right bottom, #E91E63, #00BCD4);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    padding: 1.5rem;
      }
    </style>
  </head>
  <body>
    <h1>
      DumDarac
    </h1>
  </body>
</html>