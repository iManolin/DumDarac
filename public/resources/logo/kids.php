<html>
  <head>
    <link href="https://fonts.googleapis.com/css?family=Baumans|Bubblegum+Sans|Caesar+Dressing|Fresca|Grand+Hotel|Kelly+Slab|Leckerli+One|Megrim|Nova+Oval|Reem+Kufi|Skranji|Text+Me+One|Viga" rel="stylesheet">
    <style>
      body {
            display: inline-flex;
    align-items: center;
    margin: 0;
    height: 100%;
    width: 100%;
    text-align: center;
      }
      
      h1 {
    font-family: 'Bubblegum Sans', cursive;
    margin: auto;
    font-size: 9rem;
    background: -webkit-linear-gradient(right bottom, #E91E63, #3F51B5);
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
    padding: 1.5rem;
        font-weight:100;
      }
      
      span {
        font-size:6rem;
        font-weight:900;
      }
    </style>
  </head>
  <body>
    <h1>
      DumDarac <span>Kids</span>
    </h1>
  </body>
</html>