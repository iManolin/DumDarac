<?php

if(!isset($language) and isset($_COOKIE["language"])){
	$language = $_COOKIE["language"];
}
if(isset($_GET['language'])){ $language_get = $_GET['language']; } else { $language_get = null; }
if(isset($_COOKIE['languageoriginal'])){ $language_original = $_COOKIE['languageoriginal']; } else { $language_original = null; }
if(isset($_COOKIE['languageordenador'])){ $language_ordenador = $_COOKIE['languageordenador']; } else { $language_ordenador = null; }

//CHECK IF LANGUAGE EXIST
$language_exist = substr($language_get, 0, 2);
$language_exist=mysqli_query($con,"SELECT * FROM languages WHERE code IN ('$language_exist') AND enabled='1' LIMIT 1");
if(!mysqli_num_rows($language_exist)) {
	$language_get = null;
}

//CHECK IF COUNTRY AND LANGUAGE EXIST
$country_exist = substr($language_get, 3);
$country_exist=mysqli_query($con,"SELECT * FROM languages_countries WHERE LOWER(code) IN (LOWER('$country_exist')) AND enabled='1' LIMIT 1");
if(!mysqli_num_rows($country_exist)) {
	$language_get = null;
}

if($language_get){
	$urlactuallanguageget = str_replace("?language=$language_get&", "?", $server_url);
	$urlactuallanguageget = str_replace("?language=$language_get", "", $urlactuallanguageget);
	$urlactuallanguageget = str_replace("&language=$language_get", "", $urlactuallanguageget);
	header('Location: '.$urlactuallanguageget);
}

//LANGUAGE BY DOMAIN
if(!$language){
$dominio_language = $_SERVER['SERVER_NAME'];
$dominio_language = str_replace("www.","", $dominio_language);
switch($dominio_language) {
  case "dumdarac.us":
    $language_get = "en-US";
    break;
  case "dumdarac.es":
    $language_get = "es-ES";
    break;
  case "dumdarac.cat":
    $language_get = "ca-ES";
    break;
  case "dumdarac.gr":
    $language_get = "el-GR";
    break;
  case "dumdarac.it":
    $language_get = "it-IT";
    break;
  case "dumdarac.fr":
    $language_get = "fr-FR";
    break;
}
}

if($language_get){
	unset($_COOKIE['language']);
	setcookie("language", $language_get);
	$language = $language_get;
}

if(!$language){
	//IF THE LANGUAGE IS EMPTY AND DOESNT GET A NEW ONE THIS WILL BE THE DEFAULT
	if(!$language_get){
		$language_get = "en-US";
	}
	setcookie("language", $language_get);
	setcookie("languageoriginal", $language_get);
	setcookie("languageordenador", $language_get);
	$language = $language_get;
}

if($language_get and !$usuario_mismo_id){
	unset($_COOKIE['languageordenador']);
	setcookie("languageordenador", $language_get);
}

//IF SESSION AND NEW LANGUAGE EXIST UPDATE LANGUAGE OF THE USER
if($usuario_mismo_id and $language_get){
	mysqli_query($con, "UPDATE usuarios SET language='$language_get' WHERE usuario_id='$usuario_mismo_id'");
}

//IF SESSION EXIST SET LANGUAGE OF THE USER
if($usuario_mismo_id and $usuario_mismo_language){
	$language = $usuario_mismo_language;
}

if($language == 'ca-CT'){
	$language = "ca-ES";
}

?>