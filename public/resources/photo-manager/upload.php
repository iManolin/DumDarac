<?php
  
$uploaded_images = array();
$images_uploaded = array();

if(!empty(array_filter($_FILES['images']['name'])) and $usuario_mismo_id){
  echo $_FILES['images']['name'][$key];
  foreach($_FILES['images']['name'] as $key=>$val){
    $errors = null;
    $errors = array();
    $file_name = $_FILES['images']['name'][$key];
    $file_size = $_FILES['images']['size'][$key];
    $file_tmp = $_FILES['images']['tmp_name'][$key];
    $file_type = $_FILES['images']['type'][$key];
    $file_mime = mime_content_type($_FILES['images']['tmp_name'][$key]);
    
    $file_rename = generateImageLargeName();
    
    $extensions= array("image/gif","image/jpg","image/png","image/bmp","image/jpeg");
    if(in_array($file_mime,$extensions)=== false){
      $errors[]="extension not allowed, please choose a JPEG or PNG file.";
    }
    
    if($file_size > 2097152){
      $errors[]='File size must be excately 2 MB';
    }
    
    if(empty($errors)===true){
      //Success
      $mime_upload = str_replace("image/", ".", $file_mime);
      move_uploaded_file($file_tmp, __DIR__."/../../usuario/" . $usuario_mismo_id . "/images/".$file_rename.$mime_upload);
      mysqli_query($con, "INSERT INTO images_manager (original_name, usuario_id, fileid, mime, type, internal, internal_type) VALUES('$file_name', '$usuario_mismo_id', '$file_rename', '$file_mime', '$file_type', '$file_internal', '$file_internal_type')");
      
      $images_uploaded[] = $file_rename;
      
    } else {
      print_r($errors);
      echo false;
    }
  }
  //if(empty($errors)==true){
    //echo implode(",", $images_uploaded);
  //}
}

function generateImageLargeName(){
  $charset = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  $file_rename = null;
  $length = strlen($charset);
  $count = 91;
  while ($count--) {
    $file_rename .= $charset[mt_rand(0, $length-1)];
  }
  return $file_rename;
}

?>