<html lang="<?=$language?>" class="notranslate">

<head>
  <style>
    
    #ddanimation {
      width: 100%;
      height: 100%;
      position: fixed;
      top: 0;
      left:0;
      z-index: 999999;
      pointer-events: none;
    }
    
  </style>
</head>

<body>

<progress class="PF-progress linear loading" id="loading" style="position:fixed; top:0; left:0; z-index:9999;"></progress>
  <div id="ddbg-avatar"></div>

  <?php
  include_once(__DIR__."/resources/header/header.php");
  include_once(__DIR__."/resources/notifications/notifications.php");
  ?>

      <div class="wrapper" id="wrapper">
        <div class="side-menu-overlay"></div>
        <div class="PF PF-drawer left side-menu side-menu-left" id="side-menu-left">
          <?php if($usuario_mismo_id){?>
          <div class="user-holder">
            <div class="avatares">
              <div class="PF-avatar ddbg-avatar header_boton_menu" opendd-href="?p=perfil"></div>
              <?php
              $usuario_people_ids = array_keys($_SESSION["users"]);
              $usuario_people_ids = implode("','", $usuario_people_ids);
              $usuario_people = mysqli_query($con,"SELECT * FROM usuarios WHERE usuario_id IN('$usuario_people_ids') AND usuario_id!='$usuario_mismo_id' ORDER BY lastactivity DESC, usuario_id DESC");
              while($row_usuario_people = mysqli_fetch_array($usuario_people))
              {
                $username_people = $row_usuario_people['usuario_nombre'];
                $name_people = $row_usuario_people['nombre'];
                $lastname_people = $row_usuario_people['apellidos'];
                $color_people = $row_usuario_people['color'];
                $avatar_people = $row_usuario_people['avatar'];
                $user_id_people = $row_usuario_people['usuario_id'];
                if($name_people){$name_show_people = $name_people . " " . $lastname_people;} else {$name_show_people = $username_people;}
                $name_show_people = ucwords($name_show_people);
                if($avatar_people){
                  $avatar_people = "//dumdarac.com/apps/photos/see.php?id=" . $avatar_people;
                } else {
                  $avatar_people = "//usuario.dumdarac.com/usuario.svg";
                }
                ?>
                <a class="PF-avatar header_boton_menu" style=" background-image: url('<?=$avatar_people?>');" href="?account-switch=<?=$user_id_people?>&<?=$server_querystring?>"></a>
              <?}?>
            </div>
            <div class="info">
              <div class="data">
                <h1 class="header_boton_menu" opendd-href="?p=perfil"><?=$usuario_mismo_fullname?></h1>
                <p class="header_boton_menu" opendd-href="?p=perfil" t-dd>See profile</p>
              </div>
              <div class="PF PF-icon ripple" onclick="$('.side-menu-left.PF-drawer .submenu-user').toggleClass('open');"><i class="material-icons">&#xE5CF;</i></div>
            </div>
          </div>
          <?} else {?>
            <div class="PF PF-toolbar onlyborder">
              <div class="PF PF-icon header_boton_menu ripple"><i class="material-icons">&#xE5CD;</i></div>
            </div>
            <?}?>
              <ul>
                <?php if($usuario_mismo_id){?>
                <div class="PF PF-hidedcontent submenu-user">
                  <div class="content">
                    <li class="account-pageelementhide" opendd-href="?p=account"><i class="material-icons">account_circle</i><span t-dd>Account Settings</span></li>
                    <li class="accounts-pageelementhide" opendd-href="?p=accounts"><i class="material-icons">supervised_user_circle</i><span t-dd>Change account</span></li>
                    <li class="activity-pageelementhide" opendd-href="?p=account&section=activity"><i class="material-icons">timelapse</i><span t-dd>Activity</span></li>
                    <li class="logout-pageelementhide" opendd-href="?p=logout<?php if($pagina or $app){?>&continue=<?=$server_url?><?}?>"><i class="material-icons">&#xE879;</i><span t-dd>Log out</span></li>
                  </div>
                </div>
                <?}?>
                    <div id="menuleft"></div>
                    <hr>
                <li class="header_boton_menu opendd languages-pageelementhide" opendd-href="?p=languages" ><i class="material-icons">&#xE894;</i><span t-dd>Languages</span></li>
                    <?php if($usuario_mismo_id){?><li class="header_boton_menu opendd contributors-pageelementhide" opendd-href="?p=contributors"><i class="material-icons">record_voice_over</i><span t-dd>Contributors Platform</span></li><?}?>
                    <li class="header_boton_menu opendd donate-pageelementhide" opendd-href="?p=donate"><i class="material-icons">money</i><span t-dd>Donate</span></li>
                    <li class="header_boton_menu opendd feedback-support-pageelementhide" opendd-href="?p=feedback-support"><i class="material-icons">feedback</i><span t-dd>Feedback & Support</span></li>
                    <li class="header_boton_menu opendd help-pageelementhide" opendd-href="?p=help"><i class="material-icons">help</i><span t-dd>Help center</span></li>
                <li class="header_boton_menu opendd languages-pageelementhide" opendd-href="?p=lastest-updates" ><i class="material-icons">update</i><span t-dd>Lastest Updates</span></li>
                <li onclick="windowdd('./pantallas/settings/index.php', 'fit'); $('.PF-drawer.active.left').removeClass('active');" class="settings-pageelementhide"><i class="material-icons bd">settings</i><span t-dd>Settings</span></li>
                  <!------<li class="darkmodebutton dark header_boton_menu" onclick="$('body').removeClass('PF-light'); $('body').addClass('PF-dark'); document.cookie = 'darkmode=1';"><i class="material-icons">brightness_4</i><span t-dd>Dark mode</span></li>
                    <li class="darkmodebutton light header_boton_menu" onclick="$('body').removeClass('PF-dark'); $('body').addClass('PF-light'); document.cookie = 'darkmode=0';"><i class="material-icons">brightness_4</i><span t-dd>Light mode</span></li>------>
              </ul>
        </div>
        <div class="contenedor" id="contenedor"></div>
      </div>
      
      <script>
        
        $(document).ready(function() {

          $(window).scroll(function() {

      if ($(document).scrollTop() > 10) {
        $("#body").attr("scrolling", "true");
      } else if ($(document).scrollTop() < 10) {
        $("#body").attr("scrolling", "false");
      }
    });

          $("#ddbg-avatar").load('./resources/ddbg-avatar.php');
        
          if(navigator.onLine){
          <?php
          $loadfirstpagedd = "?" . $server_querystring;
          ?>
          openddgo('<?php if($server_querystring){ echo $loadfirstpagedd; }?>');
          }
          
          var timeoutbodyeventos;
          $(document).bind("DOMSubtreeModified", function() {
              clearTimeout(timeoutbodyeventos);
              timeoutbodyeventos = setTimeout(function() {
                autosize($('textarea'));
                
              }, 600);
          });
          
         $.initialize("body .dd_screen .PF-tabs", function() {
            $('body').addClass('surface-enabled');
          });
          
          $.initialize(".PF-tabbar .container ul li span:not([t-dd])", function() {
            $(this).attr('t-dd', '');
          });
          
          $.initialize(".PF-drawer ul li span:not([t-dd])", function() {
            $(this).attr('t-dd', '');
          });

        document.addEventListener('gesturestart', function(e) {
          e.preventDefault();
        });


        });

        $(document).on("click", ".header_boton_menu", function() {
          screenmenu('left');
        });

        $(document).on("click", "[previewperfil-id]", function() {
          previewperfil($(this).attr('previewperfil-id'));
        });

        $('.user_boton_menu').on('click', function() {
          screenmenu('user');
        });

        $('.menurightopen').on('click', function() {
          screenmenu('right');
        });

        $('.side-menu .filter').on('click', function() {
          $(this).toggleClass('active');
        });

        $('.side-menu-overlay').on('click', function() {
          $('.side-menu').removeClass('active');
          $('.side-menu-overlay').removeClass('show');
          $('.side-menu-overlay').removeClass('white');
          $('.bloque_header_1').css('margin-right', '');
          $('.header').removeClass('headercolorvisible');
          $('.header').removeClass('active_app');
          $('#body').removeClass('overflowhidden');
          $('.logo span').show();
          if($('#notifications').has('.open')){
            $('#notifications').removeClass('open');
          }
        });

        function screenmenu(leftright) {
          $('.side-menu-' + leftright).toggleClass('active');
          $('.side-menu-overlay').toggleClass('show');
          if ($('.side-menu').hasClass('active')) {
            $('.header').removeClass('headercolorvisible');
            $('.header').addClass('active_app');
            $('#body').addClass('overflowhidden');
          } else {
            $('.header').removeClass('headercolorvisible');
            $('.header').removeClass('active_app');
            $('#body').removeClass('overflowhidden');
            $('.logo span').show();
          }
        }

        function previewperfil(id) {
          downmenudd("./pantallas/perfil/preview/menu.php?id=" + id);
        }

        /*WINDOWDD*/

        function windowdd(url, height, width) {
          $('#ddwindow').empty();
          $('#loading').show();
          $('#ddwindow .PF-progress.loading').show();
          $('#ddwindow').style = "";
          $('#ddwindow').removeClass('fitheight');
          $('#ddwindow').removeClass('fitwidth');

          if (height == 'fit') {
            $('#ddwindow').addClass('fitheight');
          }

          if (width == 'fit') {
            $('#ddwindow').addClass('fitwidth');
          }

          if(url) {
            $.get(url, function(data, status){
              $('#body').addClass('overflowhidden');
              $('.side-menu-overlay').addClass('closewindowdd').addClass('show');
              $('#loading').hide();
              $('#ddwindow .PF-progress.loading').hide();
              $('#ddwindow').addClass('open');
              $('#ddwindow').html(data);
            });
          }
        }

        $(document).on('click', '.closewindowdd', function() {
          closewindowdd();
        });

        function closewindowdd(){
          $('#body').removeClass('overflowhidden');
          $('.side-menu-overlay').removeClass('show').removeClass('white').removeClass('closewindowdd');
          $('#ddwindow .PF-progress.loading').show();
          $('#ddwindow').removeClass('open');
        }

        /*WINDOWDD*/

        /*SHEETDD*/

        function sheetdd(url, position, height, width) {
          $('#ddsheet').style = "";
          $('#ddsheet').removeClass('fitheight');
          $('#ddsheet').removeClass('fitwidth');

          if (height == 'fit') {
            $('#ddsheet').addClass('fitheight');
          }

          if (width == 'fit') {
            $('#ddsheet').addClass('fitwidth');
          }

          if (url) {
            $('#ddsheet').load(url, function() {
              $('#body').addClass('overflowhidden');
              $('.side-menu-overlay').addClass('closesheetdd').addClass('show');
              $('#ddsheet').addClass('active');
            });
          }
        }

        $(document).on('click', '.closesheetdd', function() {
          $('#body').removeClass('overflowhidden');
          $('.side-menu-overlay').removeClass('show').removeClass('white').removeClass('closesheetdd');
          $('#ddsheet').removeClass('active');
        });

        /*SHEETDD*/
          
            $(document).on('paste', '[contenteditable]', function(e) {
              e.preventDefault();
    var text = '';
    if (e.clipboardData || e.originalEvent.clipboardData) {
      text = (e.originalEvent || e).clipboardData.getData('text/plain');
    } else if (window.clipboardData) {
      text = window.clipboardData.getData('Text');
    }
    if (document.queryCommandSupported('insertText')) {
      document.execCommand('insertText', false, text);
    } else {
      document.execCommand('paste', false, text);
    }
            });
          
      </script>

  <div id="ddanimation"></div>
      <div class="PF PF-sheet" id="ddsheet" style="display:none;"></div>
      <div class="PF PF-window ddwindow" id="ddwindow"></div>
      <div class="PF PF-downmenu downmenudd" id="downmenudd"></div>
  
  
  <?php if($_GET['theming']){ include_once(__DIR__.'/resources/paperflower-components/theming.php'); } ?>
  
</body>

</html>