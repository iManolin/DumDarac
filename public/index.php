<!DOCTYPE html>
<?php
$current_window_id_set = mt_rand(1000, 1000000000);;
include_once(__DIR__."/resources/meta.php");

?>

<html>

<head>
  <?php include_once(__DIR__."/meta-info.php"); ?>
</head>

<body translate="no" id="body" class="bodydd <?php if(isset($_COOKIE['darkmode']) && $_COOKIE['darkmode']){?>PF-dark<?}?>" >

  <script>
    var windowid = "<?=$current_window_id_set?>";
    var xhr = [];
  </script>
  
  <script src="/resources/js/jquery-3.4.1.min.js"></script>
  <script src="/resources/js/jquery-ui.min.js"></script>
  <script src="/resources/js/jquery.initialize.min.js"></script>

  <?php if(!$usuario_mismo_id){ include_once(__DIR__."/resources/login-modal.php"); } ?>

  <script src="/resources/js/paperflower.js?version=<?=$cache_version?>"></script>
  <script src="/resources/dd-functions/PF-alerts.js?version=<?=$cache_version?>"></script>
  <script src="/resources/js/autosize.js"></script>
  <script async src="/resources/js/loadmore.js?version=<?=$cache_version?>"></script>
  <script async src="/resources/js/moment.min.js"></script>
  <script async src="/resources/js/prettydate.js"></script>
  <script async src="/resources/js/jquery.visible.min.js"></script>
  <script async src="/resources/js/jquery.form.min.js"></script>
  <script async src="/resources/js/bodymovin.min.js"></script>
  <script async src="/resources/js/memories.js?version=<?=$cache_version?>"></script>
  <script async src="/resources/js/PFWalk.js?version=<?=$cache_version?>"></script>
  
  <!----DD FUNCTIONS---->
  <script src="/resources/dd-functions/opendd.js?version=<?=$cache_version?>"></script>
  <script src="/resources/dd-functions/no-ios.js?version=<?=$cache_version?>"></script>
  <script src="/resources/dd-functions/language-name.js?version=<?=$cache_version?>"></script>
  
  <script>var language = "<?=$language?>";</script>
  <script src="/resources/dd-functions/t-dd.js?version=<?=$cache_version?>"></script>
  <script src="/resources/dd-functions/rgdd.js?version=<?=$cache_version?>"></script>
  <script async src="/resources/dd-functions/searchdd.js?version=<?=$cache_version?>"></script>
  <script async src="/resources/dd-functions/repeated-br.js?version=<?=$cache_version?>"></script>
  
  <?php
  $ua = strtolower($_SERVER['HTTP_USER_AGENT']);
  if(stripos($ua,'android') !== false) {?>
  <script async src="/resources/dd-functions/android-sound.js?version=<?=$cache_version?>"></script>
  <?}
  
  include_once(__DIR__."/resources/logo-loading.php");
  include_once(__DIR__."/dumdarac.php");
  include_once(__DIR__."/resources/ripple.php");
  if($usuario_mismo_id){
    include_once(__DIR__."/resources/highlight/highlight.php");
    include_once(__DIR__."/resources/dd-functions/follow.php");
    include_once(__DIR__."/resources/more-less/initialize.php");
    include_once(__DIR__."/resources/glance/initialize.php");
    include_once(__DIR__."/resources/reactions/js.php");
  }
  //include_once(__DIR__."/resources/floaty/button.php");
  
  //Feed
  include_once(__DIR__."/resources/feed/initialize.php");

  if($_GET['client'] !== 'app' and $_GET['os'] !== 'android'){
  include_once(__DIR__."/resources/offline.php");
  }
  if($_GET['resetcache'] == true){
?>
<script>

function verificarNovaVersio() {
    var sVersio = localStorage['dd_version'+ location.pathname] || 'v00.0.0000';
    $.ajax({
        url: "./version.txt"
        , dataType: 'text'
        , cache: false
        , contentType: false
        , processData: false
        , type: 'post'
     }).done(function(sVersioFitxer) {
        console.log('App Version: '+ sVersioFitxer +', Cache Version: '+ sVersio);
        if (sVersio < (sVersioFitxer || 'v00.0.0000')) {
            localStorage['dd_version'+ location.pathname] = sVersioFitxer;
            location.reload(true);
        }
    });
}
  
  verificarNovaVersio();

</script>
<?}?>

<?php
if($usuario_mismo_id){?>
<script> $.get("./resources/update-lastactivity.php"); setInterval(function(){ $.get("./resources/update-lastactivity.php"); }, 30000); </script>
<?}?>
<!--------------
<script src="/upup.min.js"></script>
<script>
UpUp.start({
  'content-url': 'offline.html',
  'assets': ['favicon.ico', 'resources/css/paperflower.css'],
  'service-worker-url': '/upup.sw.min.js'
});
</script>
---------->

<script>
/*DARK MODE*/
jQuery(document).ready(function($) {

if (document.cookie.indexOf('darkmode=') === -1) {
    var mql = window.matchMedia('(prefers-color-scheme: dark)')

    mediaqueryresponse(mql) // call listener function explicitly at run time

    mql.addListener(mediaqueryresponse) // attach listener function to listen in on state changes

    function mediaqueryresponse(mql) {
        if (document.cookie.indexOf('darkmode=') === -1) {
            if (mql.matches) {
                $('body').addClass('PF-dark');
            } else {
                $('body').removeClass('PF-dark');
            }
        }
    }
}

});
</script>
</body>

</html>