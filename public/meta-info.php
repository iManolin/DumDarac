
  <meta name="trustpilot-one-time-domain-verification-id" content="8weA4adqc8kGLX6gVdQdbQZdJbI1ebq6QQ306XfY" />
  <meta name="google-site-verification" content="L0T0_6VCNrT3slPROGy2XjzhW8lqVCYTBZ-l5XnEm1U" />
  <link rel="manifest" href="/manifest.json">
  <meta name="application-name" content="DumDarac">

  <title itemprop='name'>DumDarac</title>
  <meta name="description" content="Discover amazing things and connect with passionate people.">
  <meta name="theme-color" content="white">

  <!-- Schema.org markup for Google+ -->
  <meta itemprop="name" content="DumDarac">
  <meta itemprop="description" content="Discover amazing things and connect with passionate people.">
  <meta itemprop="image" content="//img.dumdarac.com/logo/logometa.png">
  <!-- Twitter Card data -->
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="DumDarac">
  <meta name="twitter:title" content="DumDarac">
  <meta name="twitter:description" content="Discover amazing things and connect with passionate people.">
  <meta name="twitter:image:src" content="//img.dumdarac.com/logo/logometa.png">
  <!-- Open Graph data -->
  <meta property="og:title" content="DumDarac" />
  <meta property="og:author" content="DumDarac" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="//dumdarac.com/" />
  <meta property="og:image" content="//img.dumdarac.com/logo/logometa.png" />
  <meta property="og:description" content="Discover amazing things and connect with passionate people.">
  <meta property="og:site_name" content="DumDarac" />


  <meta property="og:type" content="website">
  <link rel="canonical" href="//dumdarac.com/">
  <link rel=”alternate” hreflang=”en” href=”//dumdarac.com/l/en”>
  <link rel=”alternate” hreflang=”es” href=”//dumdarac.com/l/es”>
  <link rel=”alternate” hreflang=”fr” href=”//dumdarac.com/l/fr”>
  <link rel=”alternate” hreflang=”ca” href=”//dumdarac.com/l/ca”>
  <link rel=”alternate” hreflang=”ar” href=”//dumdarac.com/l/ar”>
  <link rel=”alternate” hreflang=”tr” href=”//dumdarac.com/l/tr”>
  <link rel=”alternate” hreflang=”ku” href=”//dumdarac.com/l/ku”>
  <link rel=”alternate” hreflang=”gr” href=”//dumdarac.com/l/gr”>