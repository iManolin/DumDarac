<html>

<head>
  <?php $color_app_dd="white";
  include ("./resources/meta.php");
  ?>
  <style>
    @font-face {
      src: url(./resources/fuentes/DumDaracLogo.ttf);
      font-family: DumDarac Logo;
    }

    * {
      margin: 0;
      padding: 0;
      font-family: DumDarac Logo;
    }

    .bodydiv {
      background: white;
      display: inline-flex;
      align-items: center;
      margin: 0;
      height: 100%;
      width: 100%;
      text-align: center;
      transition: 0.5s;
    }

    @media only screen and (max-width: 500px) {
      .bodydiv {
        background: black;
      }
    }

    .logo {
      margin: auto;
      z-index: 1;
      display: inline-flex;
    }

    .logo.textual {
      font-family: DumDarac Logo, cursive;
      font-size: 10vw;
      color: white;
      padding: 1.5rem;
      font-weight: 900;
      background: -webkit-linear-gradient(top left, #E91E63, #2196F3);
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
    }

    .icono {
      display: none;
      transition: 1s;
      font-size: 20rem;
      padding: 5rem;
      background-image: url(//img.dumdarac.com/logo/logo.png);
      background-size: cover;
      box-shadow: 0px 0px 30px 10px #2196F3;
      border-radius: 100px;
      background-color: #ffffff;
      animation-name: example;
      animation-duration: 2s;
      animation-iteration-count: infinite;
    }

    @keyframes example {
      0% {
        box-shadow: 0px 0px 30px 10px #2196F3;
      }
      50% {
        box-shadow: 0px 0px 30px 10px #E91E63;
      }
      100% {
        box-shadow: 0px 0px 30px 10px #2196F3;
      }
    }

    <?php if($logonover=='') {
      ?>@media only screen and (max-width: 500px) {
        .logo.textual {
          display:none;
        }
        .logo.icono {
          display: block;
        }
      }
      <?
    }

    ?>
  </style>
</head>

<body>
  <script src="./resources/js/jquery-1.11.3.js"></script>
  <script src="./resources/js/jquery.min.js"></script>
  <div class="bodydiv">
    <h1 class="logo textual">DumDarac </h1>
    <div class="logo icono"></div>
  </div>
</body>

</html>