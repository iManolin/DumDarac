<html>

<head>
  <style>
    .configuracionbotonmenu {
      display: none;
    }

    .container_configuracion {
      margin: 0px auto;
      font-size: 14px;
      max-width: 600px;
      width:100%;
    }

    .container_configuracion .groups-container {
      width: 100%;
      display:none;
    }

    .container_configuracion .groups-container .group {
      margin: auto;
      margin-bottom: 0.5rem;
    }
    
    .container_configuracion .groups-container .group:last-child {
      margin-bottom:1rem;
    }

    .container_configuracion .groups-container .group .title {
      margin: auto;
      padding: 0.5rem;
      font-size: 1rem;
      font-weight: 100;
    }

    .container_configuracion .groups-container .group .options {
      width: 100%;
      border-radius: 1em;
      overflow: hidden;
      box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.4);
      margin: auto;
      transition: 0.5s;
    }

    .container_configuracion .groups-container .group .options.open {
      border-radius: 0;
    }
    
    .container_configuracion .groups-container .group .options:empty:parent {
      display:none;
    }

    .container_configuracion .groups-container .group .options .option {
      position: relative;
      background: white;
      position: relative;
      border-bottom: 1px solid rgba(0, 0, 0, 0.05);
      -webkit-transition: all 0.25s;
      transition: all 0.25s;
      margin: auto;
      overflow: hidden;
    }

    .container_configuracion .groups-container .group .options .option .title {
      padding: 0rem;
      margin: 0px;
      width: 100%;
      max-width: 100%;
      -webkit-transition: all 0.25s;
      transition: all 0.25s;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
      display: inline-flex;
      flex-direction: column;
    }

    .container_configuracion .groups-container .group .options .option .title .PF.PF-toolbar {
      background: transparent;
    }
    
    .container_configuracion .groups-container .group .options .option .title .PF.PF-toolbar .iconback {
      display:none;
    }
    
    .container_configuracion .groups-container .group .options .option .settings {
      -webkit-transition: all 0.25s;
      transition: all 0.25s;
      max-height: 0px;
      opacity: 0;
      transition: 0.5s;
      position:relative;
    }

    .container_configuracion .groups-container .group .options .option .settings .setting {
      padding: 0.5rem 0px;
      opacity: 0;
      -webkit-transition: all 0.25s;
      transition: all 0.25s;
    }

    .container_configuracion .groups-container .group .options .option.open {
    margin: 0px auto;
    box-shadow: 0 10px 10px 0 rgba(0, 0, 0, .19), 0 6px 3px 0 rgba(0, 0, 0, .23);
    z-index: 9998;
    height: 100vh;
    display: flex;
    flex-direction: column;
    }

    .container_configuracion .groups-container .group .options .option.open .title {
      background: white;
      color: white;
      box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .16), 0 2px 5px 0 rgba(0, 0, 0, .23);
      cursor: pointer;
      overflow: hidden;
      margin: auto;
      font-size: 1rem;
      min-height: fit-content;
      display: flex;
      flex-direction: column;
      z-index: 2;
    }
    
    .container_configuracion .groups-container .group .options .option.open .title .iconsection {
      display:none;
    }
    
    .container_configuracion .groups-container .group .options .option.open .title .icondown {
      display:none;
    }
    
    .container_configuracion .groups-container .group .options .option.open .title .PF.PF-toolbar .iconback {
      display:block;
    }

    .container_configuracion .groups-container .group .options .option.open .title .imagen {
      background-size: contain;
      background-position: center;
      width: 100%;
      flex: 1 auto;
      background-repeat: no-repeat;
      z-index: 5;
      position: relative;
      background-color: white;
      height: 8rem;
    }

    .container_configuracion .groups-container .group .options .option .settings .setting {
      width: 100%;
    }

    .container_configuracion .groups-container .group .options .option .settings .setting .input-container {
      display: none;
    }

    .container_configuracion .groups-container .group .options .option .settings .setting .titleajuste {
    display: flex;
    flex-direction: column;
    cursor: pointer;
    width: 100%;
      position:relative;
      z-index:1;
    }
    
    .container_configuracion .groups-container .group .options .option .settings .setting .titleajuste .imagen {
    width: 100%;
      min-height:10rem;
      flex:1 auto;
      background-position:center;
      background-size:contain;
      background-color:white;
      z-index:2;
      background-repeat:no-repeat;
    }

    .container_configuracion .groups-container .group .options .option .settings .setting.open .input-container {
      display: block;
    }

    .container_configuracion .groups-container .group .options .option.open .settings .setting {
    opacity: 1;
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);
    overflow: hidden;
    margin: 0;
    padding: 0;
    min-height: fit-content;
    transition: 0.5s;
    max-width: 30rem;
    margin: 0.5rem 0.5rem 0.5rem auto;
    border-radius: 1em;
    border: solid 1px #ddd;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
    background: white;
    }

    .container_configuracion .groups-container .group .options .option.open .settings .setting.open {
    height: auto;
    flex: 1;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
    border: 0px solid transparent;
    }

    .container_configuracion .groups-container .group .options .option.open .settings .setting .contenido {}

    .container_configuracion .groups-container .group .options .option.open .settings .setting.open .contenido {}

    .container_configuracion .groups-container .group .options .option.open:after {
      opacity: 0;
    }

    .container_configuracion .groups-container .group .options .option.open .settings {
    height: 100%;
    max-height: 100%;
    opacity: 9;
    overflow: auto;
    display: flex;
    flex-direction: column;
    padding: 0.5rem;
    overflow-y: scroll;
    }

    .container_configuracion .groups-container .group .options .option {
      width: 100%;
    }

    .container_configuracion .groups-container .group .options .option .input-container {
      position: relative;
      padding: 0.5rem;
      background: white;
      /*margin-top:-1rem;*/
    }
    
    @media (max-width: 45rem) {
      .container_configuracion {
        max-width:100%;
      }
      
      .container_configuracion .groups-container .group .options .option.open .settings {
        padding:0;
      }
      
    }
    
  </style>
</head>

<body>
  
  <div class="container_configuracion PF">

    <div class="PF PF-content center transparent">
      <div class="container">
        <div class="image" style="background-image:url('./pantallas/configuracion/cover.svg');"></div>
        <h1 t-dd>Configuration</h1>
        <p t-dd>One account all DumDarac</p>
      </div>
    </div>

    <div class="groups-container">
      <?php include_once('secciones.php'); ?>
    </div>
    
  </div>
  
  <script>
    $(document).ready(function() {
      $('#header').addClass('noborder');
    });
    
    $('.container_configuracion .groups-container').show();

    $('.group .title').click(function() {
      $(this).parent().children('.settings').load('./pantallas/configuracion/secciones/' + $(this).parent().attr('data-settings') + '/index.php');
    });

    $('.options .option .title').click(function() {
      $(this).parent().children('.settings:not(.loaded)').load('./pantallas/configuracion/seccion.php?id=' + $(this).parent().attr('data-id')).addClass('loaded');
      $(this).parent().toggleClass('open').parent().toggleClass('open');
      $('.side-menu-overlay').toggleClass('show');
      $(this).children('i').toggle();
      $('.options .option .setting.open').removeClass('open');
      scrollToElement($(this).parent());
    });

    $(".side-menu-overlay").on('click', function() {
      $('.options .option .title.open').children('i').toggle();
      $('.options .option.open').removeClass('open').parent().removeClass('open');
      $('.options .option .setting.open').removeClass('open');
    });

    $(document).on("click", ".options .option .setting .titleajuste", function() {
      if($(this).parent().hasClass("open")){
        $(".options .option .setting").removeClass('open');
      } else {
        $(".options .option .setting").removeClass('open');
        $(this).parent().addClass('open');
      }
      var thisparent = $(this).parent();
      scrollToElementinside(thisparent);
    });

    var scrollToElement = function(el) {
      $('#body').toggleClass('overflowhidden').animate({
        scrollTop: $(el).offset().top
      }, 500);
    }

    var scrollToElementinside = function(el) {
      $('html,body .settings').animate({
        scrollTop: $(el).position().top - 64
      }, 500);
    }
  </script>
</body>

</html>