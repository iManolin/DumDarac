<?php
include_once("../../resources/acceso_db.php");
include_once("../../resources/idiomas/$idioma.php");
include_once("../../resources/datos-usuario.php");
$secciones_configuracion = mysqli_query($con,"SELECT * FROM configuracion_secciones WHERE parent='si' ORDER BY titulo ASC");
while($row_secciones_configuracion = mysqli_fetch_array($secciones_configuracion))
{
  $id_seccion_configuracion = $row_secciones_configuracion['id'];
  $parent_seccion_configuracion = $row_secciones_configuracion['parent'];
  $nombre_seccion_configuracion = $row_secciones_configuracion['nombre'];
  $titulo_seccion_configuracion = $row_secciones_configuracion['titulo'];
?>
  <div class="setting open">
    <div class="titleajuste">
      <div class="PF PF-toolbar">
        <h1 class="t-dd translated">Name and surnames</h1>
        <div class="PF PF-icon ripple icondown"><i class="material-icons">&#xE5CF;</i></div>
      </div>
      <div class="imagen" style="background-image:url('./pantallas/configuracion/cover.svg');"></div>
    </div>
    <div class="input-container">
      <form class="PF PF-form">
        <div class="PF PF-twocolumns">
          <div class="PF PF-input"><input class="input-item" type="text" required="" id="input-field1" value="<?=$usuario_nombre_mismo?>"><label class="t-dd">First Name</label><span class="bar"></span></div>
          <div class="PF PF-input"><input class="input-item" type="text" required="" id="input-field" value="<?=$usuario_apellidos_mismo?>"><label class="t-dd">Last Name</label><span class="bar"></span></div>
        </div>
        <div class="PF PF-input"><input class="input-item" type="email" data-error="Invalid email." required="" id="input-field"><label class="t-dd">Email</label><span class="bar"></span></div>
        <div class="PF PF-input"><select class="input-item"><option disabled="" selected=""></option><option value="1">Option 1</option><option value="2">Option 2</option><option value="3">Option 3</option><option value="4">Option 4</option></select><label>Make a selection</label>
          <span class="bar"></span>
        </div>
        <div class="PF PF-input switch"><input type="checkbox" id="id-name--1" name="set-name" class="PF PF-switch-input"><label for="id-name--1" class="PF PF-switch-label">Switch <span class="toggle--on">On</span><span class="toggle--off">Off</span></label></div>
        <div class="PF PF-buttons"><button class="PF PF-button ripple t-dd">Update</button><button class="PF PF-button ripple t-dd" disabled>Send</button><button class="PF PF-button ripple t-dd transparent">Send</button></div>
      </form>
    </div>
  </div>

<div class="setting open">
    <div class="titleajuste">
      <div class="PF PF-toolbar">
        <h1 class="t-dd translated">Name and surnames</h1>
        <div class="PF PF-icon ripple icondown"><i class="material-icons">&#xE5CF;</i></div>
      </div>
    </div>
    <div class="input-container">
      <form class="PF PF-form">
        <div class="PF PF-twocolumns">
          <div class="PF PF-input"><input class="input-item" type="text" required="" id="input-field1" value="<?=$usuario_nombre_mismo?>"><label class="t-dd">First Name</label><span class="bar"></span></div>
          <div class="PF PF-input"><input class="input-item" type="text" required="" id="input-field" value="<?=$usuario_apellidos_mismo?>"><label class="t-dd">Last Name</label><span class="bar"></span></div>
        </div>
        <div class="PF PF-input"><input class="input-item" type="email" data-error="Invalid email." required="" id="input-field"><label class="t-dd">Email</label><span class="bar"></span></div>
        <div class="PF PF-input"><select class="input-item"><option disabled="" selected=""></option><option value="1">Option 1</option><option value="2">Option 2</option><option value="3">Option 3</option><option value="4">Option 4</option></select><label>Make a selection</label>
          <span class="bar"></span>
        </div>
        <div class="PF PF-input switch"><input type="checkbox" id="id-name--1" name="set-name" class="PF PF-switch-input"><label for="id-name--1" class="PF PF-switch-label">Switch <span class="toggle--on">On</span><span class="toggle--off">Off</span></label></div>
        <div class="PF PF-buttons"><button class="PF PF-button ripple t-dd">Update</button><button class="PF PF-button ripple t-dd" disabled>Send</button><button class="PF PF-button ripple t-dd transparent">Send</button></div>
      </form>
    </div>
  </div>

  <?}?>