<html>

<head>
  <style></style>
</head>

<body>

  <?php
$secciones_configuracion = mysqli_query($con,"SELECT * FROM configuracion_secciones WHERE parent='si' ORDER BY titulo ASC");
while($row_secciones_configuracion = mysqli_fetch_array($secciones_configuracion))
{
  $id_seccion_configuracion = $row_secciones_configuracion['id'];
  $parent_seccion_configuracion = $row_secciones_configuracion['parent'];
  $nombre_seccion_configuracion = $row_secciones_configuracion['nombre'];
  $titulo_seccion_configuracion = $row_secciones_configuracion['titulo'];
?>

    <div class="group" data-settings="<?=$nombre_seccion_configuracion?>">
      <div class="title t-dd">
        <?=$titulo_seccion_configuracion?>
      </div>
      <div class="options">

        <?php
$subsecciones_configuracion = mysqli_query($con,"SELECT * FROM configuracion_secciones WHERE parent='$nombre_seccion_configuracion' ORDER BY titulo ASC");
while($row_subsecciones_configuracion = mysqli_fetch_array($subsecciones_configuracion))
{
  $id_subseccion_configuracion = $row_subsecciones_configuracion['id'];
  $parent_subseccion_configuracion = $row_subsecciones_configuracion['parent'];
  $nombre_subseccion_configuracion = $row_subsecciones_configuracion['nombre'];
  $titulo_subseccion_configuracion = $row_subsecciones_configuracion['titulo'];
  $icono_subseccion_configuracion = $row_subsecciones_configuracion['icono'];
?>

          <div class="option" data-id="<?=$id_subseccion_configuracion?>" >
            <div class="title">
              <div class="imagen" style="background-image:url('https://www.gstatic.com/policies/privacy/d1b68e2cd423aba52d74f02573df2d2d.svg');"></div>
              <div class="PF PF-toolbar">
                <div class="PF PF-icon ripple iconback"><i class="material-icons">arrow_back</i></div>
                <?php if($icono_subseccion_configuracion != ''){?>
                <div class="PF PF-icon ripple iconsection"><i class="material-icons bd"><?=$icono_subseccion_configuracion?></i></div>
                <?} else {?>
                  <div class="PF PF-icon ripple iconsection"><i class="material-icons bd">settings</i></div>
                  <?}?>
                    <h1 class="t-dd">
                      <?=$titulo_subseccion_configuracion?>
                    </h1>
                    <div class="PF PF-icon ripple icondown"><i class="material-icons">&#xE5CF;</i></div>
              </div>
            </div>
            <div class="settings">
              <div class="loading_bar_container">
    <div class="loading_bar"></div>
  </div>
            </div>
          </div>

          <?}?>
      
      </div>
    </div>

    <?}?>

</body>

</html>