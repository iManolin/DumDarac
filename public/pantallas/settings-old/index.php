<html>

<head>
  <style>
    .feedback-support-container {
      max-width: 50rem;
      margin: auto;
    }
    
    .feedback-support-container #settingscolorselector .PF-card {
    }
    
    .feedback-support-container #settingscolorselector .PF-card.selected {
      border: solid 3px black;
    }
    
  </style>
</head>

<body>

  <div class="feedback-support-container">
    <div class="PF PF-content full noborder center">
      <div class="container">
        <div class="PF-image PF-avatar ddbg-avatar circle" rgdd onclick="windowdd('./resources/window/cambiar-avatar.php');">
          <div class="PF PF-icon ripple"><i class="material-icons">cloud_upload</i></div>
        </div>
        <h1><span t-dd>Welcome</span>,
          <?=$usuario_mismo_nombre?>
            <?=$usuario_mismo_apellidos?>
        </h1>
        <p t-dd>Manage your info, privacy, and security to make DumDarac better for you</p>
      </div>
    </div>


    <form method="post" action="./resources/actualizar/informacion-basica.php" id="formconfiguracionbasica" class="PF-form">


      <div class="PF PF-card">
        <h3 t-dd>Basic information</h3>

        <div class="PF-twocolumns">
          
          <label class="PF-textfield filled">
            <input placeholder=" " class="formconfiguracionbasica" type="text" name="nombre" required="" id="input-field1" value="<?=$usuario_mismo_nombre?>">
            <span t-dd>First Name</span>
        </label>

          <label class="PF-textfield filled">
            <input placeholder=" " class="formconfiguracionbasica" type="text" name="apellidos" required="" id="input-field1" value="<?=$usuario_mismo_apellidos?>" >
            <span t-dd>Last Name</span>
        </label>

        </div>
        
        <label class="PF-textfield filled">
            <input placeholder=" " class="formconfiguracionbasica" type="date" name="fechadenacimiento" required="" id="input-field1" value="<?=$usuario_mismo_nacimiento?>" >
            <span t-dd>Birthdate</span>
        </label>
        
        
        <label class="PF-textfield filled">
            <textarea placeholder=" " class="input-item formconfiguracionbasica" name="estadodd" type="text" required="" id="input-field" maxlength="150"><?=$usuario_mismo_estado?></textarea>
            <span t-dd>Status</span>
        </label>

        <label class="PF-textfield filled">
            <textarea placeholder=" " class="input-item formconfiguracionbasica" name="biography" type="text" required="" id="input-field"><?=$usuario_mismo_biography?></textarea>
            <span t-dd>Biography</span>
        </label>
        
      </div>

      <div class="PF PF-card">
        <h3 t-dd>Contact</h3>
        
        <label class="PF-textfield filled">
            <input placeholder=" " class="formconfiguracionbasica" type="email" name="email" required="" id="input-field1" value="<?=$usuario_mismo_email?>" >
            <span t-dd>Email</span>
        </label>
        
        <label class="PF-textfield filled">
            <input placeholder=" " class="formconfiguracionbasica" type="url" name="web" required="" id="input-field1" value="<?=$usuario_mismo_web?>" >
            <span t-dd>Website</span>
        </label>

      </div>

      <div class="PF PF-card">
        <h3 t-dd>Home</h3>

        <label class="PF-textfield filled">
            <input placeholder=" " class="formconfiguracionbasica" type="text" name="ciudad" required="" id="input-field1" value="<?=$usuario_mismo_ciudad?>" >
            <span t-dd>City</span>
        </label>
      </div>

      <div class="PF PF-card">
        <h3 t-dd>Work</h3>
        
        <label class="PF-textfield filled">
            <input placeholder=" " class="formconfiguracionbasica" type="text" name="trabajo" required="" id="input-field1" value="<?=$usuario_mismo_trabajo?>" >
            <span t-dd>City</span>
        </label>
        
      </div>

      <div class="PF PF-card">
        <h3 t-dd>Studies</h3>
        
        <label class="PF-textfield filled">
            <input placeholder=" " class="formconfiguracionbasica" type="text" name="escuela" required="" id="input-field1" value="<?=$usuario_mismo_escuela?>" >
            <span t-dd>School</span>
        </label>
        
      </div>

      <div class="PF PF-card">
        <h3 t-dd>Colors</h3>
        <div class="PF PF-grid selector" id="settingscolorselector">
          
          <input class="formconfiguracionbasica" name="color" id="settingscolorselectorinput" style="display:none;" />

          <div class="PF-card ripple PFC-blue" data-color="blue">
            <div class="PF-image" style="background-image:url('//img.dumdarac.com/PF-colors/blue.svg');"></div>
            <div class="info">
              <h1>Blue</h1>
            </div>
          </div>

          <div class="PF-card ripple PFC-red" data-color="red">
            <div class="PF-image" style="background-image:url('//img.dumdarac.com/PF-colors/red.svg');"></div>
            <div class="info">
              <h1>Red</h1>
            </div>
          </div>

          <div class="PF-card ripple PFC-yellow" data-color="yellow">
            <div class="PF-image" style="background-image:url('//img.dumdarac.com/PF-colors/yellow.svg');"></div>
            <div class="info">
              <h1>Yellow</h1>
            </div>
          </div>

          <div class="PF-card ripple PFC-orange" data-color="orange">
            <div class="PF-image" style="background-image:url('//img.dumdarac.com/PF-colors/orange.svg');"></div>
            <div class="info">
              <h1>Orange</h1>
            </div>
          </div>

          <div class="PF-card ripple PFC-green" data-color="green">
            <div class="PF-image" style="background-image:url('//img.dumdarac.com/PF-colors/green.svg');"></div>
            <div class="info">
              <h1>Green</h1>
            </div>
          </div>

          <div class="PF-card ripple PFC-greenwater" data-color="greenwater">
            <div class="PF-image" style="background-image:url('//img.dumdarac.com/PF-colors/greenwater.svg');"></div>
            <div class="info">
              <h1>Green Water</h1>
            </div>
          </div>

          <div class="PF-card ripple PFC-pink" data-color="pink">
            <div class="PF-image" style="background-image:url('//img.dumdarac.com/PF-colors/pink.svg');"></div>
            <div class="info">
              <h1>Pink</h1>
            </div>
          </div>

          <div class="PF-card ripple PFC-purple" data-color="purple">
            <div class="PF-image" style="background-image:url('//img.dumdarac.com/PF-colors/purple.svg');"></div>
            <div class="info">
              <h1>Purple</h1>
            </div>
          </div>

        </div>


      </div>

      <div class="PF PF-card">
        <h3 t-dd>Gender</h3>

        <div class="PF-select is-checked">
          <select id="PF-select" class="formconfiguracionbasica" name="gender" >
      <option selected="selected" value="">-- select an option --</option>
      <option value="BiGender">BiGender</option>
      <option value="Cross-Dresser">Cross-Dresser</option>
      <option value="Drag-King">Drag-King</option>
            <option value="Drag-Queen">Drag-Queen</option>
            <option value="Androgynous">Androgynous</option>
            <option value="Femme">Femme</option>
            <option value="Female to male">Female to male</option>
            <option value="FTM">FTM</option>
            <option value="Gender Bender">Gender Bender</option>
            <option value="Genderqueer">Genderqueer</option>
            <option value="Male to Female">Male to Female</option>
            <option value="MTF">MTF</option>
            <option value="No Op">No Op</option>
            <option value="Hijra">Hijra</option>
            <option value="Pangender">Pangender</option>
            <option value="Transsexual">Transsexual</option>
            <option value="Transperson">Transperson</option>
            <option value="Woman">Woman</option>
            <option value="Male">Male</option>
            <option value="Buch">Buch</option>
            <option value="Two-Spirit">Two-Spirit</option>
            <option value="Trans">Trans</option>
            <option value="Blender Gender">Blender Gender</option>
            <option value="Agender">Agender</option>
            <option value="Third Sex">Third Sex</option>
            <option value="Fluid gender">Fluid gender</option>
            <option value="Non-binary transgender">Non-binary transgender</option>
            <option value="Hermaphrodite">Hermaphrodite</option>
            <option value="Gifted Gender">Gifted Gender</option>
            <option value="Transgender">Transgender</option>
            <option value="Femme Queen">Femme Queen</option>
            <option value="Person with Transgender experience">Person with Transgender experience</option>
    </select>
          <label class="form-label" for="PF-select">Gender</label>
        </div>
      </div>

    </form>

    <div class="PF PF-card">
      <h1>Donate</h1>
      <p>If you like the extension - and we sincerely hope you do - consider donating. It will help the development process and make it easier for us to improve it for everyone.</p>

      <a class="PF PF-button transparent ripple opendd" opendd-href="?p=donate">
        <div class="inside">
          <p t-dd>Donate</p>
        </div>
      </a>
    </div>

  </div>


  <script>
    $(document).ready(function() {
      
      $('#settingscolorselector .PF-card.PFC-<?=$usuario_mismo_color?>').addClass('selected');
      
      var timeoutupdateformusr;
          $(document).on('keyup keydown change paste', '.formconfiguracionbasica', function() {
        clearTimeout(timeoutupdateformusr);
        timeoutupdateformusr = setTimeout(function() {
          $('.header .PF-progress.loading').show();
          $.post('./resources/actualizar/informacion-basica.php', $("#formconfiguracionbasica").serialize(), function(data) {
            $('#resultadoconfiguracionbasica').html(data);
            alertdd.show('Saved');
            $('.header .PF-progress.loading').hide();
          });
        }, 2500);
      });

    });
    
    $(document).on("click", "#settingscolorselector .PF-card", function(e) {
      $('#settingscolorselector .PF-card').removeClass('selected');
    $(this).addClass('selected');
      $('#settingscolorselectorinput').val($(this).attr('data-color')).keyup();
    });
    
  </script>

</body>

</html>