<html>

<head>
	<style>
		.topics-container {
			flex: 1;
		}

		.topics-container .feed {
		  margin-top: 1em;
		}

		.topics-container .PF.follow-button-container {
			margin: auto;
		}
	</style>
</head>

<body>

	<div class="topics-container">
		<?php
		if($topic){
			include(__DIR__.'/pantallas/topic.php');
		} else {
			include(__DIR__.'/pantallas/topics.php');
		} ?>
	</div>

</body>

</html>