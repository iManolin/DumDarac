<?php $section = $_GET['section']; ?>
<html>

<head>
  <style>

<?php if(!$q){?>
  body:not(.PF-dark) .dd_screen {
      background-image: url(//img.dumdarac.com/backgrounds/city_banner_background.svg);
    background-repeat: no-repeat;
    background-position: center bottom;
    background-attachment: fixed;
}
<?}?>

.header .center_hdr {
  max-width: 50em;
  flex: 1 50em;
}
  
    [opendd-href='?p=search'] {
      display: none;
    }
    
    .search_container {
    flex: 1 auto;
    display: flex;
    flex-direction: column;
    margin: auto;
    width: 100%;
    }

    .search_container>.search_title {
      background-color: rgb(var(--PF-color-background));
    }


    .search_container>.search_title>h1 {
          font-size: 1.5em;
    padding: .5em;
    max-width: 50rem;
    margin: auto;
    width: 100%;
    }

    .search_container>.PF-tabs>.PF-tabbar>.container {
      max-width: 50rem;
      width: 100%;
    }
    
    .search_container>.PF-tabs>.tabs>.tab {
      max-width: 50rem;
      margin: .5em auto;
      overflow: hidden;
    }

    .search_container .PF-tabs .tab[data-name="tabssearch-all"] .PF-grid>.PF-card:nth-child(n+5) {
      display: none;
    }

    .search_container .search_empty {
      margin: auto;
      align-items: center;
      display: flex;
      flex-direction: column;
    }

    .search_container .search_empty .trends .content {
      display: flex;
      flex-direction: column;
    }

    .search_container .search_empty .trends .content a {
      padding: .5em;
    }

    
    .search_container .PF-textfield {
      margin: .5em;
      width: calc(100% - 1em);
    }

    .search_container>.PF-tabs .tabs .tab[data-name="tabssearch-all"]>h1 {
      padding-top: .5em;
    max-width: calc(100vw - 1em);
    margin: 0 auto;
    width: 100%;
    }
    .search_container>.PF-tabs .tabs .tab:not([data-name="tabssearch-all"])>h1 {
      display: none;
    }
    
    @media (min-width: 701px) {
      .search_container .PF-textfield {
        display: none;
      }
    }

    .PF-grid:parent {
      background:red!important;
    }
    
  </style>
</head>

<body>




  <div class="search_container">

    <?php if($q){
      mysqli_query($con, "INSERT INTO search_history (query, usuario_id, language, date) VALUES('$q', '$usuario_mismo_id', '$language', '$datetime') ");
    ?>
    <div class="search_title"><h1><span t-dd>Results for</span> "<span><?=$q?></span>"</h1></div>

    <div class="PF PF-tabs" style="flex: 1;">
      <div class="PF PF-tabbar" style="border-bottom: solid 1px rgba(var(--PF-color-on-surface), .1);">
        <div class="container">
          <ul>
            <li class="ripple active" data-type="people" data-for="tabssearch-all" data-taburl="./pantallas/search/pantallas/all.php?limit=8&<?=$server_querystring?>"><span t-dd>All</span></li>
            <li class="ripple" data-type="people" data-for="tabssearch-people" data-taburl="./pantallas/people/pantallas/search.php?<?=$server_querystring?>"><span t-dd>People</span></li>
            <li class="ripple" data-type="posts" data-for="tabssearch-posts" data-taburl="./pantallas/search/pantallas/posts.php?<?=$server_querystring?>"><span t-dd>Posts</span></li>
            <li class="ripple" data-type="videos" data-for="tabssearch-web" data-taburl="./dominio/xyz/resultados.php?<?=$server_querystring?>"><span t-dd>Web</span></li>
            <li class="ripple" data-type="photos" data-for="tabssearch-news" data-taburl="./pantallas/search/pantallas/posts.php?app=news&q=<?=$q?>"><span t-dd>News</span></li>
            <li class="ripple" data-type="topics" data-for="tabssearch-topics" data-taburl="./pantallas/search/pantallas/topics.php?<?=$server_querystring?>" preload><span t-dd>Topics</span></li>
            <li class="ripple" data-type="hashtags" data-for="tabssearch-hashtags" data-taburl="./pantallas/search/pantallas/hashtags.php?<?=$server_querystring?>"><span t-dd>Hashtags</span></li>
            <li class="ripple" data-type="market" data-for="tabssearch-market" data-taburl="./pantallas/search/pantallas/market.php?<?=$server_querystring?>"><span t-dd>Market</span></li>
            <li class="ripple" data-type="help" data-for="tabssearch-help" data-taburl="./pantallas/help/search.php?<?=$server_querystring?>"><span t-dd>Help</span></li>
          </ul>
          <div class="slider"></div>
        </div>
      </div>
      <div class="tabs">
        <div class="tab all" data-name="tabssearch-all"></div>
        <div class="tab" data-name="tabssearch-people"></div>
        <div class="tab" data-name="tabssearch-posts"></div>
        <div class="tab" data-name="tabssearch-web"></div>
        <div class="tab" data-name="tabssearch-news"></div>
        <div class="tab" data-name="tabssearch-topics"></div>
        <div class="tab" data-name="tabssearch-hashtags"></div>
        <div class="tab" data-name="tabssearch-market"></div>
        <div class="tab" data-name="tabssearch-help"></div>
      </div>
    </div>

    <?} else {
    include(__DIR__.'/pantallas/start.php');
    }?>

  </div>

  <script>
  <?php if($section){?> setTimeout(function(){ $("[data-for='tabssearch-<?=$section?>']").click(); }, 600); <?}?>
  
    $('.header .logo span#titlepage').text('Search').removeClass('translated');

  </script>

</body>

</html>