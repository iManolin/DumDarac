<div class="search_empty">
	<div>
		<div class="PF PF-content full noborder center">
			<div class="container">
				<div class="PF PF-image transparent" style="background-image:url('./pantallas/search/search.svg'); background-size: contain; min-height: 6em;"></div>
				<h1 t-dd>Search something</h1>
				<p t-dd>Search a friend, topic, what's trending and much more</p>
			</div>
		</div>
	</div>

	<div class="PF PF-card trends">
		<div class="info">
			<h1 t-dd>Recent searches</h1>
		</div>
		<div class="content">
			<?php
$recent_searches = mysqli_query($con,"SELECT * FROM search_history WHERE usuario_id='$usuario_mismo_id' GROUP BY query ORDER BY date DESC LIMIT 5");
while($row_recent_searches = mysqli_fetch_array($recent_searches))
{
  $title_recent_searches = $row_recent_searches["query"];
?>
<a onclick="searchdd('<?=$title_recent_searches?>');" ><?=$title_recent_searches?></a>
<?}?>
		</div>
	</div>
	
<!--------------
	<div class="PF PF-card trends">
		<div class="info">
			<h1 t-dd>Trending topics search</h1>
		</div>
		<div class="content">
			<?php
$trending_topics = mysqli_query($con,"SELECT * FROM trends WHERE language='$language' AND type='topic' AND action='search' GROUP BY content ORDER BY COUNT(content) DESC, MONTH(date) DESC, YEAR(date) DESC, RAND() LIMIT 5");
while($row_trending_topics = mysqli_fetch_array($trending_topics))
{
  $title_trending_topics = $row_trending_topics["content"];
?>
<a onclick="searchdd('<?=$title_trending_topics?>');" ><?=$title_trending_topics?></a>
<?}?>
		</div>
	</div>-------------->
</div>