<h1 t-dd>Topics</h1>
<div class="PF PF-grid selector" >
<?php
$json_topic_insert = "https://" . substr($language, 0, -3) . ".wikipedia.org/w/api.php?action=opensearch&search=" . urlencode($q) . "&namespace=0&format=json";
$json_topic_insert = file_get_contents($json_topic_insert);
$data_topic_insert = json_decode($json_topic_insert,true);
foreach ($data_topic_insert[1] as &$value_topic_insert) {
	$value_topic_insert = mysqli_real_escape_string($con, $value_topic_insert);
  $color_array_topic_insert = array("orange", "red", "green", "blue", "purple", "pink");
	$color_topic_insert = array_rand($color_array_topic_insert);
	$color_topic_insert = $color_array_topic_insert[$color_topic_insert];
  if($value_topic_insert){
    mysqli_query($con, "INSERT INTO topics (title, demand, language, color) VALUES('$value_topic_insert', '1', '$language', '$color_topic_insert')");
  }
}

$topic_sql = mysqli_query($con,"SELECT * FROM topics WHERE (LOWER(title) LIKE (LOWER('%$q%')) OR LOWER(title) IN(SELECT related FROM topics_related WHERE LOWER(title) IN(LOWER('$q')) AND language IN('$language') ORDER BY id DESC)) ORDER BY LOWER(title)=LOWER('$q') DESC, demand DESC LIMIT 30");
while($row_topic_sql = mysqli_fetch_array($topic_sql))
{
	$topic_id = $row_topic_sql["id"];
	$topic_title = $row_topic_sql["title"];
	$topic_demand = $row_topic_sql["demand"];
	$topic_color = $row_topic_sql["color"];
	$topic_image = $row_topic_sql["image"];

	if(!$topic_insert_norepeat){
  	$topic_insert_norepeat = true;
  	mysqli_query($con, "INSERT INTO trends (content, date, type, action, language, user_id) VALUES('$topic_title', '$datetime', 'topic', 'search', '$language', '$usuario_mismo_id') ");
	}

	if(!$topic_image){
	$json_image_topic = file_get_contents("https://" . substr($language, 0, -3) . ".wikipedia.org/w/api.php?action=query&titles=" . urlencode($topic_title) . "&prop=pageimages&format=json&pithumbsize=300");
	$json_image_topic = json_decode($json_image_topic,true);
	$topic_image = $json_image_topic['query']['pages'][key($json_image_topic['query']['pages'])]['thumbnail']['source'];

	if($topic_image){
		mysqli_query($con, "UPDATE topics SET image='$topic_image' WHERE id='$topic_id' ");
	}
	}
	
?>

<div class="PF PF-card PFC-<?=$topic_color?>" opendd-href="?p=topics&topic=<?=$topic_title?>">
    <div class="PF PF-image ripple <?=$space_avatar_contain_spaces?>" style="background-image:url('<?=$topic_image?>'); background-color: rgba(var(--PF-color-primary), .2);" >
  <div follow-button data-id="<?=$topic_title?>" data-type="topic" data-classes="minimized absolute top right" ></div>
  </div>
    <div class="info">
    <h1><?=$topic_title?></h1>
    </div>
  </div>
	
<?}?>

</div>