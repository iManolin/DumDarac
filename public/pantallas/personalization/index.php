<html>

<head>
	<style>
		.personalization_container {
			margin: auto;
			width: 100%;
			max-width: 50em;
		}
		
		.personalization_container>.PF-card>.PF-grid {
			grid-template-columns: repeat(auto-fill, minmax(120px, 1fr));
		}
	</style>
</head>

<body>

	<div class="personalization_container">

		<?php if($pagina === 'personalization'){?>
		<div class="PF PF-content full noborder center transparent">
			<div class="container">
				<div class="PF-image" style="background-image:url('./pantallas/personalization/personalization.svg'); background-size: contain; min-height: 6em; background-color: transparent;"></div>
				<h1 t-dd>Get better recomendations</h1>
				<p><span t-dd>See more of what you're interested in, less of what you're not.</span> <br/> <span t-dd>Add interests & preferences.</span></p>
			</div>
		</div><?}?>

		<div class="PF PF-card">
			<header>
				<h1 t-dd>Interested</h1>
				<p t-dd>See more of these topics</p>
			</header>
			<div class="PF PF-grid circle center">
				<!----<div class="PF PF-card center">
					<div class="PF-image icon ripple"><i class="material-icons">add</i></div>
					<div class="info">
						<h1 t-dd>Add</h1>
					</div>
				</div>----->

				<?php
				$moreless_more = mysqli_query($con,"SELECT * FROM topics WHERE FIND_IN_SET(LOWER(title), (SELECT LOWER(group_concat(topics)) FROM moreless WHERE usuario_id='$usuario_id_mismo' AND moreless='more' ORDER BY date DESC)) ORDER BY demand DESC");
				while($row_moreless_more = mysqli_fetch_array($moreless_more)){
				$moreless_more_title = $row_moreless_more['title'];
				$moreless_more_image = $row_moreless_more['image'];
					?>
					<div class="PF PF-card center" opendd-href="?p=topics&topic=<?=$moreless_more_title?>">
						<div class="PF-image icon ripple" style="background-image:url('<?=$moreless_more_image?>'); background-color: rgba(var(--PF-color-primary), .2);"></div>
						<div class="info">
							<h1><?=$moreless_more_title?></h1>
						</div>
					</div>
					<?}?>

			</div>
		</div>

		<div class="PF PF-card">
			<header>
				<h1 t-dd>Less Interested</h1>
				<p t-dd>See less of these topics</p>
			</header>
			<div class="PF PF-grid circle center">
				<?php
				$moreless_less = mysqli_query($con,"SELECT * FROM topics WHERE FIND_IN_SET(LOWER(title), (SELECT LOWER(group_concat(topics)) FROM moreless WHERE usuario_id='$usuario_id_mismo' AND moreless='less' ORDER BY date DESC)) ORDER BY demand DESC");
				while($row_moreless_less = mysqli_fetch_array($moreless_less)){
				$moreless_less_title = $row_moreless_less['title'];
				$moreless_less_image = $row_moreless_less['image'];
					?>
					<div class="PF PF-card center" opendd-href="?p=topics&topic=<?=$moreless_less_title?>">
						<div class="PF-image icon ripple" style="background-image:url('<?=$moreless_less_image?>'); background-color: rgba(var(--PF-color-primary), .2);"></div>
						<div class="info">
							<h1><?=$moreless_less_title?></h1>
						</div>
					</div>
					<?}?>
			</div>
		</div>


	</div>

</body>

</html>