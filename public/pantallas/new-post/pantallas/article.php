<div class=" PF PF-form content">
  <div class="textarea-container">
    <div class="PF PF-avatar ddbg-avatar ripple" rgdd></div>
    <div class="textarea" onclick="$('#typed').focus();">
      <div class="writtor">
        <div class="text" contenteditable="true" id="typed"></div>
        <div class="placeholder" t-dd><?php include(__DIR__."/../resources/text-form.php"); ?></div>
      </div>
      <div class="PF PF-chips sugestions" style=" padding: 0; ">
        <div class="PF PF-chip ripple" t-dd>Get ideas</div>
        <div class="PF PF-chip ripple" onclick="$('#filepost').click();" t-dd>Add an image</div>
        <div class="PF PF-chip ripple" t-dd>Write with someone else</div>
      </div>
    </div>
  </div>
  <div class="resources" ></div>
  <div class="PF PF-buttons">
  <div class="PF PF-button transparent ripple preview">
    <div class="inside">
      <p t-dd>Preview</p>
    </div>
  </div>
  <div class="PF PF-button ripple" onclick="$('#nuevapublicacion').submit();">
    <div class="inside">
      <p t-dd>Publish</p>
    </div>
  </div>
</div>
</div>


<div style="display: none;">
  <input type="number" name="post" value=""/>
  <input type="text" name="reply"/>
  <input name="files[]" accept="*;capture=camera" type="file" id="filepost" multiple>
  <input name="title" type="text" />
  <textarea type="text" name="text" id="textareapublicacion" class="textareapublicacion"></textarea>
  <input type="submit" name="submit" id="enviarnewpost" style="display:none;" />
</div>

<script>
  $('#typed').focus();

  <?php if(isset($post)){?>
    $("[name='post']").val('<?=$post?>');
    $('.resources').append("<div post data-id='<?=$post?>'></div>");
  <?}?>

  <?php if(isset($post) && isset($action) && $action === 'reply'){?>
    $("[name='reply']").val(true);
  <?}?>
  
</script>