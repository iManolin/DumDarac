<script>
  var allowsendformnewpost = null;
  $("#nuevapublicacion #typed").on("blur paste", function() {
    setTimeout(function () {
      $(this).text($(this).text());
    }, 100);
});
  
  $("#nuevapublicacion").on("click", function() {
    if(!$(this).attr('data-draft') && !timeoutprocessform){
      allowsendformnewpost = "no";
    $('#nuevapublicacion').attr('data-draft', "notempty");
    $.post( "./pantallas/new-post/resources/create-draft.php", { action: "create" })
      .done(function( data ) {
      $('#nuevapublicacion').attr('data-draft', data);
      alertdd.show('Draft created');
      allowsendformnewpost = null;
    });
    }
  });
  
  var timeoutprocessform = null;
  $("#nuevapublicacion, #nuevapublicacion input, #nuevapublicacion select, #nuevapublicacion textarea").on("keyup keydown change paste", function() {
    if(!allowsendformnewpost){
      allowsendformnewpost = "no";
    $('#nuevapublicacion #textareapublicacion').val(document.getElementById("typed").innerText);
    $('.nuevapublicacion .PF-progress.loading').show();
    clearTimeout(timeoutprocessform);
    timeoutprocessform = setTimeout(function() {
      allowsendformnewpost = "no";
      var draftid = $('#nuevapublicacion').attr('data-draft');
      var datadraft = new FormData($('#nuevapublicacion')[0]);
      datadraft.append('id', draftid);
      datadraft.append('action', 'update');
      
      $.ajax({
        url: './pantallas/new-post/resources/process-text.php',
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        data: datadraft,
        success: function(data) {
          eval(data);
          $('.nuevapublicacion .PF-progress.loading').hide();
          /*alertdd.show('Draft processed');*/
        }
      });
      
      
      
      $('.nuevapublicacion .PF-progress.loading').show();
      $.ajax({
        url: './pantallas/new-post/resources/create-draft.php',
        data: datadraft,
        type: "POST",
        enctype: 'multipart/form-data',
        processData: false,  // Important!
        contentType: false,
        cache: false,
        success: function(data) {
          $('.nuevapublicacion .PF-progress.loading').hide();
          alertdd.show('Draft saved' + data);
          allowsendformnewpost = null;
        }
      });
        
    }, 1000);
    }
  });
  
  
  
  var timeoutpublishpost = null;
  $("#nuevapublicacion").submit(function(e) {
    if(!allowsendformnewpost){
    $('.nuevapublicacion .PF-progress.loading').show();
    clearTimeout(timeoutpublishpost);
    timeoutpublishpost = setTimeout(function() {
      var draftid = $('#nuevapublicacion').attr('data-draft');
      var datadraft = $('#nuevapublicacion').serializeArray();
      datadraft.push({name: 'id', value: draftid});
      datadraft.push({name: 'action', value: 'publish'});
      $.ajax({
        url: './pantallas/new-post/resources/create-draft.php',
        type: 'POST',
        data: datadraft,
        success: function(data) {
          $('.nuevapublicacion .PF-progress.loading').hide();
          $('.side-menu-overlay').removeClass('show');
          alertdd.show('Post created correctly');
          $('#nuevapublicacion').attr('data-draft', '');
          openddgo('?p=publication&post=' + draftid);
        }
    });
    }, 1000);
    } else {
      alertdd.show('The post is being saved');
    }
    e.preventDefault();
});

var timeoutpostpreview = null;
$(document).on("click", "#nuevapublicacion .preview", function() {
  if(!allowsendformnewpost){
  clearTimeout(timeoutpostpreview);
    timeoutpostpreview = setTimeout(function() {
      windowdd('./pantallas/publication/index.php?post=' + $('#nuevapublicacion').attr('data-draft') + '&<?=$_SERVER["QUERY_STRING"];?>', 'fit');
    }, 1000);
  } else {
    alertdd.show('The post is being saved');
  }
});
  
  
  
  
  
var settimeoutcleartyped
$("#typed").on('input', function() {
  var texttyped = $('#typed').text();
  var leghttyped = texttyped.length;
  
  $(".nuevapublicacion").removeClass('big');
  $(".nuevapublicacion").removeClass('large');
  if (leghttyped <= 20 && leghttyped >= 1 && $(".nuevapublicacion.big.large").length === 0) {
    $(".nuevapublicacion").addClass('big').addClass('large');
  } else if (leghttyped <= 250 && leghttyped >= 1 && $(".nuevapublicacion.big").length === 0) {
    $(".nuevapublicacion").addClass('big');
  }

});


function successnuevapublicacionform(postid) {
  $.get("./resources/feed/feeds/posts/feed.php?post=" + postid, function(data) {
    if (data) {
      if ($('#feed .block').last().attr('data-feed') == 'posts') {
        $("#feed .block").last().children('.PF-toolbar').after(data);
      } else {
        $("#feed").last().prepend("<div class='block'>" + data + "</div>");
      }
    }
  });
  openddgo('?p=publication&post=' + postid );
  alertdd.show('Publication created correctly');
  $('.side-menu-overlay').removeClass('show').removeClass('white');
  $(".nuevapublicacion").removeClass('open');
  $('.nuevapublicacion .PF-progress.loading').hide();
  $('.nuevapublicacion .bottom .container .downbar').hide();
  $("#typed").text('');
  document.getElementById("textareapublicacion").value = "";
  $('.nuevapublicacion .content-to-share').empty().hide();
  $("#nuevapublicacion")[0].reset();
  $('.nuevapublicacion .bottom .container .downbar .delete').hide();
  $('.nuevapublicacion .bottom .container .downbar .post').attr('disabled', true);
}

  
  
  

  
</script>