<?php
include_once(__DIR__."/../../../../resources/url-info.php");
$value_link = $_GET['url'];
if($value_link){
  $url_dominio = parse_url($value_link)['host'];
  $titulo_link = urlinfo($value_link, "title");
  $dominio_link = urlinfo($value_link, "domain");
  $image_link = urlinfo($value_link, "image");
?>

<a href="<?=$value_link?>" target="_blank">
  <div class="PF PF-card horizontal">
    <div class="imagen_link" style="background-image:url('//img.dumdarac.com/logo/logometa.png');"></div>
    <div class="info_link">
      <p><?=$url_dominio?></p>
      <h1><?=$titulo_link?></h1>
    </div>
  </div>
</a>

<?}?>