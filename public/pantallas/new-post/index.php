<html>

<head>
  <style>

  html {
    height: 100%;
  }

  body {
    overflow: hidden;
  }

  #wrapper {
    overflow: auto;
  }
    
    .newpost_container .textarea-container {
    display: inline-flex;
    width: 100%;
    transition: 0.5s;
    flex: 1 auto;
      position: relative;
    }

    .newpost_container .textarea-container .PF-avatar {
    min-width: 3rem;
    min-height: 3rem;
    max-width: 3rem;
    max-height: 3rem;
    background-color: rgb(var(--PF-surface-rgb));
    border-radius: 100px;
    margin: 1em;
      margin-right: .5em;
    background-size: cover;
    background-position: center;
    border: solid 1px #ddd;
    }
    
    .newpost_container .textarea-container .textarea {
    flex: 1 auto;
    padding: 1em;
    padding-left: .5em;
      cursor: text;
    }
    
    .newpost_container .textarea-container .textarea .resources {
      flex: 1 auto;
      display:flex;
      flex-direction: column;
    }
    
    .newpost_container .textarea-container .textarea .resources .PF-card {
      margin: 0;
      margin-bottom: 1em;
      width: 100%;
      cursor: pointer;
    }
    
    .newpost_container .textarea-container .textarea .writtor {
      transition: .5s;
      min-height: 0em;
      padding-bottom: .5em;
    }

    .newpost_container .textarea-container .textarea .writtor>.text {
      word-break: break-word;
      opacity: 0;
      white-space: pre-wrap;
    }

    .newpost_container .textarea-container .textarea .writtor>.text:empty {
      position: absolute;
    }

    .newpost_container .textarea-container .textarea .writtor>.text:not(:empty) {
      opacity: 1;
    }

    .newpost_container .textarea-container .textarea .writtor>.placeholder {
      transition: .5s;
      min-height: 0em;
      display: inline-block;
    }
    
    .newpost_container .textarea-container .textarea .writtor>.text:empty:not(:focus) + .placeholder {
      min-height: 3em;
    }

    .newpost_container .textarea-container .textarea .writtor>.text:not(:empty) + .placeholder {
      opacity: 0;
      height: 0;
      overflow: hidden;
    }

    .newpost_container.empty .textarea-container {
      align-items: center;
    }
    
    .newpost_container .textarea-container .textarea .writtor>.placeholder:before {
      content: ' ';
    color: #555;
    padding-right: 0.2rem;
    font-size: 1rem;
    transition: 0s;
    height: fit-content;
    }

    .newpost_container .textarea-container .textarea .writtor>.text:focus + .placeholder:before {
      animation: escribiendoform 1.2s infinite;
      border-right: 1px solid rgb(var(--PF-color-on-surface));
    }

    .newpost_container .textarea-container .textarea .writtor>.placeholder:after {
    content: ' ';
    width: 1.5em;
    height: 1.5em;
    display: inline-block;
    margin: -.4em;
    background-image: url(//img.dumdarac.com/iconos/kamiku.svg);
    background-size: contain;
    background-repeat: no-repeat;
    background-position: center;
    margin-left: 0.2em;
    }

    .newpost_container .textarea-container .textarea .writtor>.placeholder:before {
      color: #555;
      padding-right: 0.2rem;
      font-size: 1rem;
    }

    @keyframes floatingkmkform {
      0% {
        transform: translatey(1.5px);
      }
      50% {
        transform: translatey(-3.5px);
      }
      100% {
        transform: translatey(1.5px);
      }
    }

    @keyframes escribiendoform {
      40% {
        border-right: 1px solid rgb(var(--PF-color-on-surface));
      }
      50% {
        border-right: 1px solid transparent;
      }
      100% {
        border-right: 1px solid transparent;
      }
    }
    
    .newpost_container .resources {
    display: inline-flex;
    width: 100%;
      position: relative;
      border-radius: 1em;
    }
    
    .newpost_container .resources:empty {
      display: none;
    }
    
    
    .newpost_container {
      display: flex;
      align-items: center;
      flex: 1 auto;
      flex-direction: column;
    }

    .newpost_container .content {
    max-width: 50rem;
    margin: auto;
    width: 100%;
    margin: .5em auto;
    display: flex;
    flex-direction: column;
    flex: 1 auto;
    }

    .newpost_container .PF-buttons {
      width: 100%;
    }
    
    .newpost_container .photos {
      padding: 4px;
      max-width: 40rem;
      margin: .5em auto;
      width: 100%;
    }
  </style>
</head>

<body>

  <form class="newpost_container" id="nuevapublicacion" method="post" enctype="multipart/form-data">
    <div class="PF-tabs" style="width: 100%;">
      <div class="PF PF-tabbar" style="box-shadow:0 2px 2px 0 rgba(0, 0, 0, 0.1);">
        <div class="container">
          <ul>
            <li class="ripple active" data-for="tabsnewpost-1" data-taburl="./pantallas/new-post/pantallas/article.php?<?=$server_querystring;?>"><span t-dd>Article</span></li>
            <?php if($pagina == 'new-post'){?>
            <li class="ripple" data-for="tabsnewpost-2" data-taburl="./pantallas/new-post/pantallas/media.php?<?=$server_querystring;?>"><span t-dd>Media</span></li>
            <li class="ripple" data-for="tabsnewpost-4" data-taburl="./pantallas/new-post/pantallas/live.php?<?=$server_querystring;?>"><span t-dd>Live</span></li>
            <?}?>
            <?php if($pagina != 'new-post'){?>
            <li class="ripple" data-for="tabsnewpost-editor" opendd-href="?p=new-post"><span t-dd>Go fullscreen</span></li>
            <?}?>
          </ul>
          <div class="slider"></div>
        </div>
      </div>
      <div class="tabs">
        <div class="tab" data-name="tabsnewpost-1"></div>
        <?php if($pagina == 'new-post'){?>
        <div class="tab" data-name="tabsnewpost-2"></div>
        <div class="tab" data-name="tabsnewpost-3"></div>
        <div class="tab" data-name="tabsnewpost-4"></div>
        <?}?>
        <div class="tab" data-name="tabsnewpost-editor"><h1 style="margin: 1em;" t-dd >Loading</h1></div>
      </div>
    </div>
  </form>

  <script>
    <?php if($usuario_color != null){?>$('body').addClass('PFC-<?=$usuario_color?>');<?}?>
  </script>
  
  <?php include(__DIR__."/resources/js.php");?>

</body>

</html>