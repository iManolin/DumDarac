<html>

<head>
  <style>
    .people_container {
      flex: 1 auto;
      display: flex;
      flex-direction: column;
    }

    .people_container>.PF-tabs .tabs .tab.all>h1 {
padding-top: .5em;
    max-width: calc(100vw - 1em);
    margin: auto;
    width: 100%;
    }
    .people_container>.PF-tabs .tabs .tab:not(.all)>h1 {
      display: none;
    }
  </style>
</head>

<body>

  <div class="people_container">
    <div class="PF-tabs">
      <div class="PF PF-tabbar shadow">
        <div class="container">
          <ul>
            <li class="ripple" data-for="tabs-1" data-taburl="./pantallas/people/pantallas/newest.php?<?=$server_querystring?>"><span t-dd>Newest</span></li>
            <li class="ripple" data-for="tabs-2" data-taburl="./pantallas/people/pantallas/people.php?<?=$server_querystring?>"><span t-dd>Find people</span></li>
            <li class="ripple active" data-for="tabs-3" data-taburl="./pantallas/people/pantallas/explore.php?<?=$server_querystring?>"><span t-dd>Explore</span></li>
            <li class="ripple" data-for="tabs-4" data-taburl="./pantallas/perfil/pantallas/following.php?<?=$server_querystring?>"><span t-dd>Following</span></li>
            <li class="ripple" data-for="tabs-5" data-taburl="./pantallas/perfil/pantallas/followers.php?<?=$server_querystring?>"><span t-dd>Followers</span></li>
          </ul>
          <div class="slider"></div>
        </div>
      </div>
      <div class="tabs" style="max-width:50rem; margin:auto;">
        <div class="tab" data-name="tabs-1"></div>
        <div class="tab" data-name="tabs-2"></div>
        <div class="tab" data-name="tabs-3"></div>
        <div class="tab" data-name="tabs-4"></div>
        <div class="tab" data-name="tabs-5"></div>
      </div>
    </div>
  </div>

</body>

</html>