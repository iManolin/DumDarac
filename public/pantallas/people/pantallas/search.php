<h1 t-dd>People</h1>
<div class="people PF-grid circle" >
<?php
  
  include(__DIR__.'/../search-algorithms/username.php');
  include(__DIR__.'/../search-algorithms/name.php');
  include(__DIR__.'/../search-algorithms/lastname.php');
  
  include(__DIR__.'/../search-algorithms/state.php');
  include(__DIR__.'/../search-algorithms/city.php');
  include(__DIR__.'/../search-algorithms/language.php');
  
  
  $people_algorithm[] = implode(" AND ",$people_algorithm_filter);
  $people_algorithm[] = implode(" OR ",$people_algorithm_search);
  
  $people_algorithm = implode("",$people_algorithm);
  
$randomnumber_limit = $randomnumber_limit[rand(0, count($randomnumber_limit) - 1)];
$usuario = mysqli_query($con,"SELECT * FROM usuarios WHERE $people_algorithm ORDER BY FIND_IN_SET(LOWER('$q'), LOWER(usuario_nombre)) DESC, FIND_IN_SET(LOWER('$q'), LOWER(nombre)) DESC, FIND_IN_SET(LOWER('$q'), LOWER(apellidos)) DESC, lastactivity DESC, usuario_id DESC LIMIT 30");
while($row_usuario_people = mysqli_fetch_array($usuario))
  { 
$usuario_nombre_people = $row_usuario_people['usuario_nombre'];
$usuario_nombre_nombre_people = $row_usuario_people['nombre'];
$usuario_apellidos_people = $row_usuario_people['apellidos'];
$usuario_ciudad_people = $row_usuario_people['ciudad'];
$usuario_trabajo_people = $row_usuario_people['trabajo'];
$email_people = $row_usuario_people['usuario_email'];
$descripcion_people = $row_usuario_people['descripcion'];
$estado_people = $row_usuario_people['estado'];
$color_people = $row_usuario_people['color_perfil'];
$usuario_avatar_people = $row_usuario_people['avatar'];
$usuario_id_people = $row_usuario_people['usuario_id'];
$usuario_id_people_id = $row_usuario_people['usuario_id'];
$usuario_id_people_portada = $row_usuario_people['usuario_id'];
if ($usuario_avatar_people == null) {
  $usuario_avatar_people = "usuario.svg";
  $usuario_id_people_id = '0';
}
$b_username_people = $q;
$final_username = str_ireplace($q, $b_username_people, $usuario_nombre_people);
$final_username = ucwords($final_username);
if($usuario_nombre_nombre_people != ''){$usuario_nombre_mostrar_people = $usuario_nombre_nombre_people;} else {$usuario_nombre_mostrar_people = $usuario_nombre_people;}
if($descripcion_people != ''){$descripcion_y_ciudad = $descripcion_people;} else {$descripcion_y_ciudad = $usuario_ciudad_people;}
?>

<div class="PF PF-card opendd circle center person" data-usuarioid="<?=$usuario_id_people?>" opendd-href="?p=perfil&u=<?=$usuario_nombre_people?>">
    <div class="PF-image ripple" style="background-image:url('//dumdarac.com/apps/photos/see.php?id=<?=$usuario_avatar_people?>');" rgdd ></div>
  <div follow-button data-id="<?=$usuario_id_people?>" data-type="usuario" data-classes="minimized absolute top right" ></div>
    <div class="info">
      <?php if($usuario_nombre_mostrar_people != ''){?><h1><?=$usuario_nombre_mostrar_people?> <?=$usuario_apellidos_people?></h1><?}?>
      <?php if($descripcion_y_ciudad != ''){?><p><?=$descripcion_y_ciudad?></p><?}?>
    </div>
  </div>

<?}?>
  
  </div>