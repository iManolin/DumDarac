<h1 t-dd>People</h1>
<div class="people PF-grid circle" >
<?php
$usuario_people_explore_select[] = " usuario_id!='$usuario_mismo_id' ";
if($qlanguage){
  $usuario_people_explore_select[] = " language='$qlanguage' ";
}
$usuario_people_explore_select = implode(" AND ", $usuario_people_explore_select);
$usuario_people = mysqli_query($con,"SELECT * FROM usuarios WHERE $usuario_people_explore_select ORDER BY ISNULL(avatar), lastactivity DESC, signup_date DESC, RAND() LIMIT 25");
while($row_usuario_people = mysqli_fetch_array($usuario_people))
  {
    $username_people = $row_usuario_people['usuario_nombre'];
    $name_people = $row_usuario_people['nombre'];
    $lastname_people = $row_usuario_people['apellidos'];
    $color_people = $row_usuario_people['color'];
    $avatar_people = $row_usuario_people['avatar'];
    $user_id_people = $row_usuario_people['usuario_id'];
    if($name_people){$name_show_people = $name_people . " " . $lastname_people;} else {$name_show_people = $username_people;}
    $name_show_people = ucwords($name_show_people);
    if($avatar_people){
      $avatar_people = "//dumdarac.com/apps/photos/see.php?id=" . $avatar_people;
    } else {
      $avatar_people = "//usuario.dumdarac.com/usuario.svg";
    }
    ?>

    <div class="PF PF-card circle center <?php if($color_people){?>PFC-<?=$color_people?><?}?>" data-usuarioid="<?=$user_id_people?>" opendd-href="?p=perfil&u=<?=$username_people?>">
      <div class="PF PF-image ripple" style="background-image:url('<?=$avatar_people?>');" rgdd ></div>
      <div follow-button data-id="<?=$user_id_people?>" data-type="usuario" data-classes="minimized absolute top right" ></div>
      <div class="info">
        <?php if($name_show_people){?><h1><?=$name_show_people?></h1><?}?>
      </div>
    </div>
  <?}?>
</div>
