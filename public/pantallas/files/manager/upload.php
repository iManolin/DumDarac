<?php
  
$uploaded_files = array();
$files_uploaded = array();

if(!empty(array_filter($_FILES['files']['name'])) and $usuario_mismo_id){
  //echo $_FILES['files']['name'][$key];
  foreach($_FILES['files']['name'] as $key=>$val){
    $errors = null;
    $errors = array();
    $file_name = $_FILES['files']['name'][$key];
    $file_size = $_FILES['files']['size'][$key];
    $file_tmp = $_FILES['files']['tmp_name'][$key];
    $file_type = $_FILES['files']['type'][$key];
    $file_mime = mime_content_type($_FILES['files']['tmp_name'][$key]);
    
    $file_rename = generateImageLargeName();
    
    $extensions= array("image/gif","image/jpg","image/png","image/bmp","image/jpeg");
    if(in_array($file_mime,$extensions)=== false){
      $errors[]="extension not allowed, please choose a JPEG or PNG file.";
    }
    
    if($file_size > 2097152){
      $errors[]='File size have to be less than 2 MB';
    }
    
    if(empty($errors)===true){
      $mime_upload = str_replace("image/", ".", $file_mime);
      move_uploaded_file($file_tmp, __DIR__."/../../../usuario/" . $usuario_mismo_id . "/images/".$file_rename.$mime_upload);
      mysqli_query($con, "INSERT INTO files (original_name, usuario_id, fileid, mime, type) VALUES('$file_name', '$usuario_mismo_id', '$file_rename', '$file_mime', '$file_type')") or die();
      $last_id_uploaded_file = mysqli_insert_id($con);

      if(empty($errors)===false){
        $errors[]= "File not inserted in database";
      } else {
        $files_uploaded[] = $file_rename;
      }
      
    }
    
    if($errors){
      $errors_debug = implode(",", $errors);
      mysqli_query($con, "INSERT INTO errors (usuario_id, text) VALUES('$usuario_mismo_id', '$errors_debug')") or die();
      echo false;
    } else {
      //echo implode(",", $files_uploaded);
    }
  }
  
}

function generateImageLargeName(){
  $charset = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
  $file_rename = null;
  $length = strlen($charset);
  $count = 91;
  while ($count--) {
    $file_rename .= $charset[mt_rand(0, $length-1)];
  }
  return $file_rename;
}

?>