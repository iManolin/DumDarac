<html>

<head>
  <style>
    
    [opendd-href='?p=files'] {
      display:none;
    }
    
    .files_container .PF-tabs .tabs {
      max-width: 50rem;
    }
    
    .files_container .PF-textfield {
      margin: .5em;
    width: calc(100% - 1em);
    }
    
    @media (min-width: 701px) {
      .files_container .PF-textfield {
      display:none;
      }
    }
    
  </style>
</head>

<body>




  <div class="files_container" >
    
    <div class="PF PF-tabs">
    <div class="PF PF-tabbar shadow circle">
      <div class="container">
        <ul>
          <li class="ripple active" data-for="tabsfiles-photos" data-taburl="./apps/photos/resources/photos.php?<?=$_SERVER["QUERY_STRING"];?>"><span t-dd>Photos</span></li>
          <li class="ripple" data-for="tabsfiles-videos" data-taburl="./pantallas/files/pantallas/files.php?<?=$_SERVER["QUERY_STRING"];?>" ><span t-dd>Videos</span></li>
          <li class="ripple" data-for="tabsfiles-documents" data-taburl="./pantallas/files/pantallas/files.php?<?=$_SERVER["QUERY_STRING"];?>" ><span t-dd>Documents</span></li>
          <li class="ripple" data-for="tabsfiles-audio" data-taburl="./pantallas/files/pantallas/files.php?<?=$_SERVER["QUERY_STRING"];?>" ><span t-dd>Audio</span></li>
        </ul>
        <div class="slider"></div>
      </div>
    </div>
    <div class="tabs">
      <div class="tab" data-name="tabsfiles-photos"></div>
      <div class="tab" data-name="tabsfiles-videos"></div>
      <div class="tab" data-name="tabsfiles-documents"></div>
      <div class="tab" data-name="tabsfiles-audio"></div>
    </div>
  </div>

  </div>
  
  <script>
    
    $('.header .logo span#titlepage').text('Files').removeClass('translated');
  
    $("#buscadoronpage").on("keyup paste", function(event) {
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
        buscardd($(this).val());
      }
    });
  
  </script>
  
</body>

</html>