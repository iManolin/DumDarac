<html>

<head>
  <style>

    .contenedor-publicaciones {
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      max-width: 78rem;
      margin: 0 auto 0;
      width: 100%;
      padding: 0.5rem;
    }
    
    .contenedor-publicaciones .publicaciones {
     flex:1 auto;
      padding:0.5rem;
      max-width:calc(500px + 1rem);
      margin: 0 auto;
    }

    .contenedor-publicaciones .widget {
      width: 100%;
      height: auto;
      padding: 0.5rem;
      max-width: 22rem;
      position: sticky;
      top: 4rem;
      display: inline-table;
    }

    .contenedor-publicaciones .widget.sticked {
      padding-top: 5rem;
    }

    .contenedor-publicaciones .widget .card_widget {
      width: 100%;
      padding: 0.5rem;
      /*background-color: #f7f7f7;*/
      border-radius: 1em;
    }

    .contenedor-publicaciones .card_widget .titulo {
    font-size: 1.2em;
    width: 100%;
    padding-left: 0.5rem;
    padding-right: 0.5rem;
    padding-bottom: .2em;
    }

    .contenedor-publicaciones .card_widget.tendencias a {
      width: 100%;
      display: flex;
      padding: 0.5rem 0.5rem;
      cursor: default;
    }

    .contenedor-publicaciones .card_widget.tendencias a p {
      width: fit-content;
    }

    .contenedor-publicaciones .card_widget.tendencias p:not(.menciones) {
      font-weight: 900;
    }

    .contenedor-publicaciones .card_widget.tendencias .menciones {
      text-align: right;
    }

    .contenedor-publicaciones .card_widget.tendencias i {
      margin-left: 0.5rem;
    }

    /*INFO USER*/

    .contenedor-publicaciones .widget.widget-1 .card_widget.info-user {
      display: inline-flex;
      flex-direction: column;
      text-align:center;
      cursor:default;
    }

    .contenedor-publicaciones .widget.widget-1 .card_widget.info-user .avatar {
      min-width: 5rem;
      height: 5rem;
      border-radius: 10em;
      margin: 0.5rem auto;
      background-size: cover;
      background-position: center;
    }
    
    .contenedor-publicaciones .widget.widget-1 .card_widget.info-user .info {
      padding-bottom:0.5rem;
    }
    
    .contenedor-publicaciones .widget.widget-1 .card_widget.info-user .info h1 {
    padding: 0.5rem 0;
    padding-bottom: 0;
    font-size: 2em;
    font-weight: 600;
    }
    
    .contenedor-publicaciones .widget.widget-1 .card_widget.info-user .info p {
    font-size: 15px;
    font-weight: 500;
    }

    .contenedor-publicaciones .widget.widget-1 .card_widget.info-user .info {
      display: inline-flex;
      flex-direction: column;
      width: 100%;
    }
    
    
    
    
    .contenedor-publicaciones .statistics {
      padding: .5em;
      margin: auto;
      width: 100%;
      align-items: center;
      display: grid;
      grid-template-columns: 1fr 1fr;
      column-gap: .5em;
      row-gap: .5em;
      grid-auto-rows: 1fr;
    }

    .contenedor-publicaciones .statistics>.stat {
      text-align: center;
      border: solid 1px;
      border-color: rgba(var(--PF-color-on-surface), .1);
      border-radius: 1em;
      padding: 0.5em;
      height: 100%;
      align-items: center;
      vertical-align: middle;
      display: flex;
      flex-direction: column;
      cursor: pointer;
      background-color: rgb(var(--PF-color-surface));
    }

    :root .PF-dark .contenedor-publicaciones .statistics>.stat {
      border-color: rgb(var(--PF-color-surface));
      box-shadow: 0px 1px 3px 1px rgba(0,0,0, 0.1);
    }

    .contenedor-publicaciones .statistics>.stat>div>h1 {
      font-size: 2em;
      font-weight: 600;
    }

    .contenedor-publicaciones .statistics>.stat>div>p {
      font-size: 1em;
      font-weight: 500;
    }

    @media (max-width: 45em) {
      .contenedor-publicaciones .statistics>.stat>div>h1 {
        font-size: 1.8em;
        font-weight: 500;
      }
      .contenedor-publicaciones .statistics>.stat>div>p {
        font-size: 0.8em;
        font-weight: 500;
      }
    }
    
    
    
    

    @media (max-width: 78rem) {
      .header {
        box-shadow: none;
      }
      .contenedor-publicaciones .widget.widget-1 {
        display: none;
      }
    }

    @media (max-width: 50rem) {
      .contenedor-publicaciones .widget.widget-2 {
        display: none;
      }
    }

    @media (max-width: 30rem) {
      
      .glance .container h1 {
        padding-top:0;
      }
      
      .contenedor-publicaciones .publicaciones {
        margin: 0 auto;
        padding: 0;
        width: 100%;
        order:2;
      }
      .contenedor-publicaciones {
        padding: 0;
        margin: 0;
        width: 100%;
      }
      .contenedor-publicaciones {
        max-width: 52rem;
        padding: 0;
        margin: 0;
        width: 100%;
        display: flex;
        flex-direction: column;
      }
      .contenedor-publicaciones .widget.widget-2 {
        max-width: 100%;
        padding: 0;
        width: 100%;
        display: block;
        position: relative;
        top: 0;
        order:1;
        display: none;
      }
      .contenedor-publicaciones .widget.widget-2 .card_widget:not(.storieswidget) {
        display: none;
      }
      .contenedor-publicaciones .widget.widget-2 .storieswidget {
        border: none;
        border-radius: initial;
        padding: 0;
      }
      .contenedor-publicaciones .widget.widget-2 .storieswidget .recent-users {
        white-space: nowrap;
        overflow: auto;
      }

      .contenedor-publicaciones .widget.widget-2 .storieswidget .recent-users::-webkit-scrollbar {
        display: none;
      }
      
      .contenedor-publicaciones .card_widget.storieswidget .titulo {
    padding: 1rem;
    padding-bottom: 0;
      }
    }
  </style>
</head>

<body>
  <div class="contenedor-publicaciones">
    <div class="widget widget-1">
      <?php include_once(__DIR__."/resources/info-user.php"); ?>
    </div>
    <div class="publicaciones" >
      <div glance data-currenturl="?<?=$server_querystring?>"></div>
      <div class="posts_container" id="posts_container">
      
        <div feed data-type="all" data-classes="shadow" data-currenturl="?<?=$server_querystring?>" ></div>
      
      </div>
    </div>
    <div class="widget widget-2">
      <div class="PF card_widget storieswidget">
        <h1 class="titulo" t-dd>Recent Users</h1>
        <div id="ddstoriesload">
          <?php include_once(__DIR__."/resources/recent-users.php"); ?>
        </div>
      </div>
      <div class="card_widget tendencias">
        <h1 class="titulo" t-dd>Trendings</h1>
        <ul>
          <?php include_once(__DIR__."/resources/tendencias.php"); ?>
        </ul>
      </div>
    </div>
  </div>
  <?php if($_GET['welcome']){?>
  <script>
    $.PFWalk([
         {
             target: '#posts_container',
             content: 'Amazing things are all around us.',
             color: 'blue',
             acceptText: 'See more'
         }, {
             target: '#glance',
             content: 'At a glance will show you info in real time.',
             color: 'red',
             acceptText: 'See more'
         }, {
             target: '#buscador',
             content: 'Discover amazing things and connect with passionate people.',
             color: 'green',
             acceptText: 'See more'
         }, {
             target: '.PF-avatar',
             content: 'Here you can go to your profile.',
           color: 'pink',
             acceptText: 'Got It'
         }, {
             target: '#recent-users',
             content: 'Here you can see the recent users.',
           color: 'pink',
             acceptText: 'Got It'
         }
 ]);
  </script>
  <?}?>
  
  <script>
    <?php if($usuario_color != null){?>$('body').addClass('PFC-<?=$usuario_color?>');<?}?>
  </script>
  
</body>

</html>