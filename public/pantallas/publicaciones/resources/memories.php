<html>

<head>
  <style>
  
  .memories_window {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.9);
    z-index: 999;
    align-items: center;
    overflow: hidden;
    display: none;
    opacity: 0;
    transition: .5s;
  }
  
  .memories_window.open {
    display: flex;
    opacity: 1;
  }
  
  .memories_window .pages {
    margin: auto;
  }
  
  .memories_window .pages .page {
    transition: .5s;
        -webkit-transform: scale(0.9) translateX(calc(-350% - 192px)) translateY(0%);
    transform: scale(0.9) translateX(calc(-350% - 192px)) translateY(0%);
  }
  
  .memories_window .pages .page:first-child {
    transform: inherit;
  }
  
  .memories_window .pages .page .info {
    text-align: center;
    color: white;
  }
  
  .memories_window .pages .page .info .data {
    padding: .5em;
    padding-top: 0;
  }
  
  .memories_window .pages .page .info .data h1 {
    font-size: 1.2em;
    font-weight: 500;
  }
  
  .memories_window .pages .page .group {
    display: flex;
    margin: auto;
  }
  
  .memories_window .pages .page .group,
  .memories_window .pages .page .group .screen {
    max-height: 75vh;
    max-width: 45vh;
    min-width: 320px;
    min-height: 533px;
  }
  
  .memories_window .pages .page .group .screen {
    position: absolute;
    box-shadow: 0 50px 200px rgba(0,0,0,0.7);
    margin: auto;
    background: black;
    border-radius: 1em;
    transition: .5s;
    -webkit-transform: scale(0.9) translateX(calc(-350% - 192px)) translateY(0%);
    transform: scale(0.9) translateX(calc(-350% - 192px)) translateY(0%);
    background-size: cover;
    background-position: center;
  }
  
  .memories_window .pages .page .group .screen:first-child {
    transform: inherit;
  }
  
  .memories_window .pages .page .progress-container {
    margin: .5em;
  display: flex;
  flex-direction: row;
  flex: 1 auto;
  padding: 10px 0;
  cursor: pointer;
}

.memories_window .pages .page .progress-container .PF-progress {
    flex-grow: 1;
    border-radius: 1em;
    margin: 0 2px;
    display: flex;
}

.memories_window .pages .page .progress-container .progress.active {
    animation-name: Loader;
}

.memories_window .pages .page .progress-container .progress.passed {
    background-position: 0 0; 
}

  </style>
</head>

<body>
  <ul class="memories" id="memories">
    <?php
$usuario_people = mysqli_query($con,"SELECT * FROM usuarios WHERE usuario_id!='$usuario_mismo_id' ORDER BY ISNULL(avatar), lastactivity DESC, signup_date DESC, RAND() LIMIT 8");
while($row_usuario_people = mysqli_fetch_array($usuario_people))
  {
    $username_people = $row_usuario_people['usuario_nombre'];
    $name_people = $row_usuario_people['nombre'];
    $lastname_people = $row_usuario_people['apellidos'];
    $color_people = $row_usuario_people['color'];
    $avatar_people = $row_usuario_people['avatar'];
    $user_id_people = $row_usuario_people['usuario_id'];
    if($name_people){$name_show_people = $name_people . " " . $lastname_people;} else {$name_show_people = $username_people;}
    $name_show_people = ucwords($name_show_people);
    ?>
    <li class="memories--item loading <!----seen loading------>" opendd-href="?p=perfil&id=<?=$user_id_people?>">
      <div class="memories--item_image">
        <div class="memo ripple" style="background-image:url('//dumdarac.com/apps/photos/see.php?id=<?=$avatar_people?>');" rgdd alt="Rafael Fragoso"></div>
      </div>
      <span class="memories--item_text"><?=$name_show_people?></span>
    </li>
    <?}?>
  </ul>
  
</body>

</html>