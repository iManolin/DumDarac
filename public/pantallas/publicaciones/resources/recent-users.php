<html>

<head>
  <style>
.recent-users {
    list-style: none;
    padding-top: 3px;
    margin-top: 0.5rem;
    width: fit-content;
    max-width: 100%;
}

.recent-users .recent-users--item {
  display: inline-block;
  width: 70px;
  height: auto;
  margin: 0 0 0 5px;
  position: relative;
  text-align: center;
  white-space: nowrap;
}

.recent-users .recent-users--item .recent-users--item_image {
  display: block;
  cursor: pointer;
  width: 60px;
  height: 60px;
  position: relative;
  margin: auto;
}
.recent-users .recent-users--item.seen .recent-users--item_text {
  color: #b3b3b3;
}


.recent-users .recent-users--item .recent-users--item_image .rece {
  border-radius: 100%;
  width: 100%;
  height: 100%;
  position: relative;
  border: 1px solid rgba(var(--PF-color-primary), .2);
  z-index: 2;
  transition: all 0.1s linear;
  margin: auto;
  background-size: cover;
  background-position: center;
}

.recent-users .recent-users--item .recent-users--item_text {
    font-size: 13px;
    margin: 0;
    padding: 5px;
    display: block;
    font-weight: 500;
    text-overflow: ellipsis;
    overflow: hidden;
}
  </style>
</head>

<body>
  <ul class="recent-users" id="recent-users">
    <?php
$usuario_people = mysqli_query($con,"SELECT * FROM usuarios WHERE usuario_id!='$usuario_mismo_id' ORDER BY ISNULL(avatar), lastactivity DESC, signup_date DESC, RAND() LIMIT 8");
while($row_usuario_people = mysqli_fetch_array($usuario_people))
  {
    $username_people = $row_usuario_people['usuario_nombre'];
    $name_people = $row_usuario_people['nombre'];
    $lastname_people = $row_usuario_people['apellidos'];
    $color_people = $row_usuario_people['color'];
    $avatar_people = $row_usuario_people['avatar'];
    $user_id_people = $row_usuario_people['usuario_id'];
    if($name_people){$name_show_people = $name_people . " " . $lastname_people;} else {$name_show_people = $username_people;}
    $name_show_people = ucwords($name_show_people);
    ?>
    <li class="recent-users--item PFC-<?=$color_people?>" opendd-href="?p=perfil&id=<?=$user_id_people?>">
      <div class="recent-users--item_image">
        <div class="rece ripple" style="background-image:url('//dumdarac.com/apps/photos/see.php?id=<?=$avatar_people?>');" rgdd alt="Rafael Fragoso"></div>
      </div>
      <span class="recent-users--item_text"><?=$name_show_people?></span>
    </li>
    <?}?>
  </ul>
  
</body>

</html>