<div class="PF PF-card center transparent card_widget info-user">
  <div class="PF-image circle ripple ddbg-avatar" style="height: 10rem;
    padding: 0;
    width: 10rem;
    margin: auto;
    margin-bottom: 0.5rem;" onclick="windowdd('./resources/window/cambiar-avatar.php');" rgdd>
    <div class="PF PF-icon ripple"><i class="material-icons">cloud_upload</i></div>
  </div>
  <div class="info">
    <h1><?=$usuario_mismo_nombre?> <?=$usuario_mismo_apellidos?></h1>
    <?php if($usuario_estado !='') {?><p><?=$usuario_estado?></p><?}?>
  </div>
  <div class="statistics">
    <script> $('.contenedor-publicaciones .statistics').load('./pantallas/perfil/resources/statistics.php?<?=$_SERVER["QUERY_STRING"];?>'); </script>
  </div>
</div>