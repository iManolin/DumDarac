<html>

<head>
  <style>
    .comunicado_container {
      padding-top: 20px;
      font-size: 20px;
      line-height: 36px;
      color: #262626;
      width: calc(100% - 1rem);
      margin: 0.5rem;
      max-width: 60rem;
      padding: 1em;
      margin: auto;
    }

    .comunicado_container * {
      display: inline-block;
    }

    .comunicado_container p {
      margin: 0.5rem;
      padding: 0px;
      text-align: justify;
      color: var(--PF-color-text-first-default);
    }

    .comunicado_container h1 {
      font-size: 45px;
      color: #ffffff;
      background-color: #434cff;
      display: inline-block;
      padding: 0.5em;
      margin: 0.5rem;
    }

    .comunicado_container span {
      display: block;
      margin: 0.5rem;
      margin-top: 20px;
      font-weight: 600;
      font-size: 20px;
      color: var(--PF-color-text-first-default);
    }

    .comunicado_container blockquote {
      font-size: 21px;
      background-color: #fdc689;
      padding: 1em;
      margin: 0.5rem;
    }

    /*PAPERFLOWER DARK*/

    .PF-dark .comunicado_container h1 {
      background-color: black;
      color: white;
    }

    .PF-dark .comunicado_container blockquote {
      background-color: rgba(0, 0, 0, 0.2);
      color: white;
    }
  </style>
</head>

<body>
  <div class="comunicado_container">
    <span>September 5, 2018</span>
    <h1>An interesting title</h1>
    <p>Among the numerous faults of those who pass their lives recklessly and without due reflexion, my good friend Liberalis, I should say that there is hardly any one so hurtful to society as this, that we neither know how to bestow or how to receive a
      benefit. It follows from this that benefits are badly invested, and become bad debts: in these cases it is too late to complain of their not being returned, for they were thrown away when we bestowed them. Nor need we wonder that while the greatest
      vices are common, none is more common than ingratitude: for this I see is brought about by various causes. </p>
    <blockquote>"None is more common than ingratitude: for this I see is brought about by various causes."</blockquote>
  </div>


</body>

</html>