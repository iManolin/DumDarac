<html>

<head>
  <style>

.notifications_container {
  margin: auto;
}
  
    .PF-notifications {
    max-width: 22rem;
    width: 100%;
    position: relative;
    margin: auto;
    border-radius: 1.5em;
    overflow: hidden;
    }

    .PF-notifications .PF-notification {
      overflow: hidden;
      user-select: none;
      width: 100%;
      display: flex;
      flex-direction: column;
      position: relative;
      background: white;
    }

    .PF-notifications .PF-notification:first-child {
      border-radius: 1em 1em 0 0;
    }

    .PF-notifications .PF-notification:last-child {
      border-radius: 0 0 1em 1em;
    }

    .PF-notifications .PF-notification+.PF-notification {
      border-top: solid 1px rgba(0, 0, 0, 0.1);
    }

    .PF-notification #info,
    .PF-notification #image {
      position: relative;
    }

    .PF-notification #info {
      background-color: white;
      color: black;
      width: 100%;
      z-index: 2;
      padding: 0.5rem;
      border: none;
      display: flex;
      flex-direction: column;
    }

    .PF-notification.music #info {
      background-color: #F44336;
      color: white;
    }

    .PF-notification #image:after {
      background: linear-gradient(to right, #F44336, transparent);
    content: '';
    height: 100%;
    position: absolute;
    top: 0px;
    left: 0px;
    width: 100%;
    }

    .PF-notification.settings #info #controls,
    .PF-notification.settings #info .specifics {
      display: none;
    }

    .PF-notification .content {
    width: 100%;
    display: inline-flex;
    flex: 1 auto;
    }

    .PF-notification #info .specifics {
      padding: 0.5rem;
      width: 100%;
      color: inherit;
      font-size: 16px;
    }

    .PF-notification #info .specifics h1 {
      font-size: inherit;
      font-weight: 500;
      margin: 0px;
    }

    .PF-notification #info .specifics h1.song {
      color: inherit;
    }

    .PF-notification #info .specifics h1.artist {
      color: inherit;
      opacity: 0.7;
      font-size: 0.9em;
    }

    .PF-notification #info #controls {
    padding: 0.5rem;
    width: 100%;
    display: flex;
    align-items: center;
    }

    .PF-notification #info #controls i {
      color: white;
      display: inline-block;
      font-size: 20px;
      margin: 0px 10px;
    }

    .PF-notification #info #controls i:first-child {
      margin-left: 0px;
    }

    .PF-notification #image {
      background-image: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/1468070/odesza-in-return.jpg");
      background-position: center;
      background-size: cover;
      right: 0px;
      content: 0px;
      min-width: 90px;
    }

    .PF-notification #settings {
      background-color: white;
      height: fit-content;
      display: none;
      position: relative;
      transition: all 0.5s, border-radius .75s;
      width: 100%;
      z-index: 11;
    }

    .PF-notification.settings #settings {
      display: block;
    }

    .PF-notification #settings #settings-contents {
      height: fit-content;
      position: relative;
      transition: all 0.25s;
      padding: 0.5rem;
    }

    .PF-notification #settings #settings-contents .service i,
    .PF-notification #settings #settings-contents .service h1 {
      color: #282828;
    }

    .PF-notification #settings #settings-contents #details {
      padding: 0.5rem;
    }

    .PF-notification #settings #settings-contents #details h1 {
      font-weight: 500;
      margin: 0px;
    }

    .PF-notification #settings #settings-contents #details .main {
      color: #282828;
      font-size: 16px;
    }

    .PF-notification #settings #settings-contents #details .sub {
      color: #787878;
      font-size: 12px;
      margin-top: 2px;
    }

    .PF-notification .service {
    display: inline-flex;
    padding: 0.5rem;
    width: 100%;
    font-size: 12px;
    color: inherit;
    }

    .PF-notification .service i,
    .PF-notification .service h1 {
      font-size: inherit;
      margin: 0px;
      display: inline;
      vertical-align: middle;
    }

    .PF-notification .service i {
      margin-right: 0.5rem;
      font-size: 16px;
    }

    .PF-notification .service h1 {
    font-weight: 500;
    font-size: 13px;
    white-space: nowrap;
    }

    .PF-notification .service h1.name {
    }

    .PF-notification .service h1.details {
      color: inherit;
      opacity: 0.7;
    }
  </style>
</head>

<body>

  <div class="notifications_container">
    <div class="PF PF-toolbar center transparent">
      <div class="PF PF-icon ripple"><i class="material-icons">&#xE643;</i></div>
      <h1 class="t-dd">Notifications</h1>
      <div class="PF PF-icon ripple"><i class="material-icons">&#xE0B8;</i></div>
      <div class="PF PF-icon ripple" style="display:none;"><i class="material-icons">&#xE923;</i></div>
    </div>
    <div class="PF PF-notifications shadow">
    <div class="PF-notification music" id="PF-notification" onclick="$(this).toggleClass('settings');">
      <div class="content">
        <div id="info">
          <div class="service">
            <i class="material-icons">music_note</i>
            <h1 class="name">DumDarac Play Music</h1>
            <h1 class="details">• In Return</h1>
            <i class="material-icons downup">&#xE313;</i>
          </div>
          <div class="specifics">
            <h1 class="song">Always this day</h1>
            <h1 class="artist">Aloa aloe</h1>
          </div>
          <div id="controls"><i class="material-icons">thumb_up</i><i class="material-icons">fast_rewind</i><i class="material-icons">play_arrow</i><i class="material-icons">fast_forward</i><i class="material-icons">thumb_down</i></div>
        </div>
        <div id="image"></div>
      </div>
      <div id="settings">
        <div id="settings-contents">
          <div id="details">
            <h1 class="main">Playback</h1>
            <h1 class="sub">1 out of 5 PF-notification categories from this app</h1>
          </div>
        </div>
      </div>
    </div>
    <div class="PF-notification" id="PF-notification">
      <div id="info">
        <div class="service">
          <i class="material-icons">music_note</i>
          <h1 class="name">DumDarac Play Music</h1>
          <h1 class="details">• In Return</h1>
          <i class="material-icons downup">&#xE313;</i>
        </div>
        <div class="specifics">
          <h1 class="song">Always this day</h1>
          <h1 class="artist">Aloa aloe</h1>
        </div>
      </div>
      <div id="settings">
        <div id="settings-contents">
          <div id="details">
            <h1 class="main">Playback</h1>
            <h1 class="sub">1 out of 5 PF-notification categories from this app</h1>
          </div>
        </div>
      </div>
    </div>
    <div class="PF-notification" id="PF-notification">
      <div id="info">
        <div class="service">
          <i class="material-icons">music_note</i>
          <h1 class="name">DumDarac Play Music</h1>
          <h1 class="details">• In Return</h1>
          <i class="material-icons downup">&#xE313;</i>
        </div>
        <div class="specifics">
          <h1 class="song">Always this day</h1>
          <h1 class="artist">Aloa aloe</h1>
        </div>
      </div>
      <div id="settings">
        <div id="settings-contents">
          <div id="details">
            <h1 class="main">Playback</h1>
            <h1 class="sub">1 out of 5 PF-notification categories from this app</h1>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
  
</body>

</html>