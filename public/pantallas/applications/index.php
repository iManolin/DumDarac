<html>

<head>
  <style>

body:not(.PF-dark) .dd_screen {
      background-image: url(//img.dumdarac.com/backgrounds/city_banner_background.svg);
    background-repeat: no-repeat;
    background-position: center bottom;
    background-attachment: fixed;
}
  
    .applications_container {
      text-align:center;
      max-width: 55rem;
      margin: auto;
      width:100%;
      
    }
    
    .applications_container .PF-grid .PF-card {
      flex: 1 1 20%;
    }
    
    @media (max-width: 75em) {
      .cameraapp {
        display:none!important;
      }
    }
    
  </style>
</head>

<body>

  <div id="applications_container" class="applications_container">
    <div class="head">
      <h1 t-dd>Applications</h1>
    </div>
    <div class="PF PF-grid circle center" opendd-href="?app=news">
      
      <div class="PF PF-card center">
        <div class="PF-image ripple rgdd" style=" background-image: url('//img.dumdarac.com/iconos/news.png');"></div>
        <div class="info">
          <h1 t-dd>News</h1>
        </div>
      </div>
      <div class="PF PF-card center" opendd-href="?app=shopping">
        <div class="PF-image ripple" style=" background-image: url('//img.dumdarac.com/iconos/shopping.png');" rgdd></div>
        <div class="info">
          <h1 t-dd>Shopping</h1>
        </div>
      </div>
      <div class="PF PF-card center" opendd-href="?app=weather">
        <div class="PF-image ripple" style=" background-image: url('//img.dumdarac.com/iconos/weather.png');" rgdd></div>
        <div class="info">
          <h1 t-dd>Weather</h1>
        </div>
      </div>
      <?php if($usuario_mismo_id != null){?>
      <!-----<div class="PF PF-card center" opendd-href="?p=hey">
        <div class="PF-image ripple rgdd" style=" background-image: url('//img.dumdarac.com/iconos/hey.png');"></div>
        <div class="info">
          <h1>Hey</h1>
        </div>
      </div>
      <div class="PF PF-card center" opendd-href="?app=photos">
        <div class="PF-image ripple rgdd" style=" background-image: url('//img.dumdarac.com/iconos/photos.png');"></div>
        <div class="info">
          <h1 t-dd>Photos</h1>
        </div>
      </div>------>
      <div class="PF PF-card center" opendd-href="?app=pay">
        <div class="PF-image ripple rgdd" style="background-image: url('//img.dumdarac.com/iconos/pay.png');background-size: 112% auto;"></div>
        <div class="info">
          <h1>Pay</h1>
        </div>
      </div>
      <?}?>
      <div class="PF PF-card center" opendd-href="?app=breathing">
        <div class="PF-image ripple rgdd" style=" background-image: url('//img.dumdarac.com/iconos/breath.png'); background-size: 84%;"></div>
        <div class="info">
          <h1 t-dd>Breathing</h1>
        </div>
      </div>
      <!-----<div class="PF PF-card center" opendd-href="?app=music">
        <div class="PF-image ripple rgdd" style=" background-image: url('//img.dumdarac.com/iconos/music.png');"></div>
        <div class="info">
          <h1 t-dd>Music</h1>
        </div>
      </div>
      <div class="PF PF-card center" opendd-href="?app=clock">
        <div class="PF-image ripple rgdd" style=" background-image: url('//img.dumdarac.com/iconos/clock.png');"></div>
        <div class="info">
          <h1 t-dd>Clock</h1>
        </div>
      </div>
      <div class="PF PF-card center" opendd-href="?app=movies">
        <div class="PF-image ripple rgdd" style=" background-image: url('//img.dumdarac.com/iconos/movies.png');"></div>
        <div class="info">
          <h1 t-dd>Movies</h1>
        </div>
      </div>------->
      <div class="PF PF-card center" opendd-href="?app=translate">
        <div class="PF-image ripple rgdd" style=" background-image: url('//img.dumdarac.com/iconos/translate.png');"></div>
        <div class="info">
          <h1 t-dd>Translate</h1>
        </div>
      </div>
      <!-----<div class="PF PF-card center cameraapp" opendd-href="?app=camera">
        <div class="PF-image ripple rgdd" style=" background-image: url('//img.dumdarac.com/iconos/camera.png');"></div>
        <div class="info">
          <h1 t-dd>Camera</h1>
        </div>
      </div>------->
      <div class="PF PF-card center" opendd-href="?p=genius">
        <div class="PF-image ripple rgdd" style=" background-image: url('//img.dumdarac.com/iconos/kamiku.svg'); background-size: 8em;"></div>
        <div class="info">
          <h1>Kamiku</h1>
        </div>
      </div>
      <div class="PF PF-card center" opendd-href="?app=famous">
        <div class="PF-image ripple rgdd" style=" background-image: url('//img.dumdarac.com/iconos/famous.png');"></div>
        <div class="info">
          <h1 t-dd>Known people</h1>
        </div>
      </div>
    </div>

  </div>


</body>

</html>