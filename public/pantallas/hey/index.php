<html>

<head>
	<style>
		.hey-wrapper {
			position: relative;
			display: flex;
			flex-direction: column;
			justify-content: center;
			align-items: center;
			padding: 20px;
			flex: 1;
			height: calc(90vh - 2em);
			overflow: hidden;
		}
		
		.hey-wrapper>.app-wrapper {
			width: 100%;
			max-width: 90%;
			flex: 1;
			background: rgb(var(--PF-color-surface));
			box-shadow: 0px 2px 5px rgba(0, 0, 0, 0.25);
			display: flex;
			border-radius: 1em;
			overflow: hidden;
		}
		
		.hey-wrapper>.app-wrapper:after {
			position: fixed;
			z-index: -1;
			background-color: rgba(var(--PF-color-primary), .1);
			width: 100%;
			height: 40%;
			content: '';
			bottom: 0;
			left: 0;
			opacity: 0.8;
		}
		
		.hey-wrapper>.app-wrapper .sidebar {
			flex: 1;
			max-width: 350px;
			background: rgb(var(--PF-color-surface));
			border-right: 1px solid rgba(var(--PF-color-on-surface), .1);
		}
		
		.hey-wrapper>.app-wrapper .main {
			display: flex;
			flex-direction: column;
			flex: 1;
			background: rgb(var(--PF-color-surface));
		}
		
		.hey-wrapper>.app-wrapper .PF-toolbar {
			background: rgb(var(--PF-color-background));
			border-bottom: 1px solid rgba(var(--PF-color-on-surface), .1);
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content {
			overflow: auto;
			flex: 1;
			display: flex;
			flex-direction: column;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block {
			display: flex;
			flex-direction: column;
			width: 100%;
			max-width: 55em;
			margin: auto;
			margin-bottom: 0;
			position: relative;
			min-height: fit-content;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages {
			display: flex;
			flex-direction: column;
			padding: .5em;
			min-height: fit-content;
			position: relative;
			flex: 1;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.datastamp {
			text-align: center;
			padding: .5em;
			margin: .5em auto;
			color: rgba(var(--PF-color-on-surface), .6);
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group {
			flex: 1;
			display: flex;
			flex-direction: row;
			align-items: flex-end;
			margin-bottom: calc(.5em - 2px);
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group.me {
			flex-direction: row-reverse;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group.me * {
			align-items: flex-end;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group>.PF-avatar {
			width: 39px;
			height: 39px;
			margin: .5em;
			margin-top: auto;
			margin-bottom: 2px;
			position: sticky;
			bottom: 4em;
			left: 0;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group>.messages-group {
			flex: 1;
			display: flex;
			flex-direction: column;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group>.messages-group>.message-container {
			flex: 1;
			display: flex;
			flex-direction: column;
			opacity: 0;
			animation-name: message-up;
			animation-duration: .8s;
			animation-fill-mode: forwards;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group>.messages-group>.message-container>.message {
			flex: 1;
			display: flex;
			flex-direction: column;
			max-width: 520px;
			margin-right: calc(39px + .5em);
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group.me>.messages-group>.message-container>.message {
			margin-left: calc(39px + .5em);
			margin-right: 0;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group>.messages-group>.message-container>.message>.content {
			flex: 1;
			display: flex;
			flex-direction: column;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group>.messages-group>.message-container>.message.bg,
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group>.messages-group>.message-container>.message:not(.bg)>.content>p {
			background-color: rgba(var(--PF-color-primary), 0.1);
			min-height: 20px;
			outline: none;
			overflow: hidden;
			position: relative;
			white-space: pre-wrap;
			width: fit-content;
			word-wrap: break-word;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group.me>.messages-group>.message-container>.message.bg,
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group.me>.messages-group>.message-container>.message:not(.bg)>.content>p {
			background-color: rgba(var(--PF-color-on-surface), .2);
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group>.messages-group>.message-container>.message:not(.bg)>.content>* {
			margin: 1px 0;
			border-radius: .25em 1.5em 1.5em .25em;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group.me>.messages-group>.message-container>.message:not(.bg)>.content>* {
			border-radius: 1.5em .25em .25em 1.5em;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group>.messages-group>.message-container>.message>.content>p {
			padding: 10px 12px;
		}
		/*FIRST*/
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group:not(.me)>.messages-group>.message-container:first-child>.message.bg,
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group:not(.me)>.messages-group>.message-container:first-child>.message:not(.bg)>.content>:first-child {
			border-radius: 1.5em 1.5em 1.5em .25em;
		}
		/*LAST MESSAGE*/
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group:not(.me)>.messages-group>.message-container:last-child>.message.bg,
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group:not(.me)>.messages-group>.message-container:last-child>.message:not(.bg)>.content>:last-child {
			border-radius: .25em 1.5em 1.5em 1.5em;
		}
		/*MIDDLE MESSAGE*/
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group:not(.me)>.messages-group>.message-container:only-child>.message.bg,
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group:not(.me)>.messages-group>.message-container:only-child>.message:not(.bg)>.content>:only-child {
			border-radius: 1.5em;
		}
		/*FIRST*/
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group.me>.messages-group>.message-container:first-child>.message.bg,
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group.me>.messages-group>.message-container:first-child>.message:not(.bg)>.content>:first-child {
			border-radius: 1.5em 1.5em .25em 1.5em;
		}
		/*LAST MESSAGE*/
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group.me>.messages-group>.message-container:last-child>.message.bg,
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group.me>.messages-group>.message-container:last-child>.message:not(.bg)>.content>:last-child {
			border-radius: 1.5em .25em 1.5em 1.5em;
		}
		/*MIDDLE MESSAGE*/
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group.me>.messages-group>.message-container:only-child>.message.bg,
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group.me>.messages-group>.message-container:only-child>.message:not(.bg)>.content>:only-child {
			border-radius: 1.5em;
		}
		/*IMAGE*/
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group>.messages-group>.message-container>.message>.content>.PF-image {
			max-width: 15em;
			border: solid 1px #ddd;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group>.messages-group>.message-container>.message.bg>.content>.PF-image {
			border-radius: 0 0 1.5em 1.5em;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group>.messages-group>.message-container>.message>.info {
			color: #5f6368;
			font-size: 14px;
			font-weight: 400;
			margin: .5em;
			margin-top: 2px;
			max-height: 500;
			transition: .5s;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group>.messages-group>.message-container:not(.show-info)>.message>.info {
			max-height: 0;
			overflow: hidden;
			margin-top: 0;
			margin-bottom: 0;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.compose {
			width: calc(100% - 1em);
			display: flex;
			align-items: flex-end;
			z-index: 1;
			padding: .5em;
			max-width: 55em;
			position: sticky;
			bottom: 0;
			left: 0;
			right: 0;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.compose>.container {
			flex: 1 auto;
			border-radius: 1.5em;
			display: flex;
			flex-direction: column;
			overflow: hidden;
			background-color: rgb(var(--PF-color-surface));
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.compose>.container>.box {
			flex: 1 auto;
			display: flex;
			align-items: center;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.compose>.container>.box>.PF-icon {
			margin: .2em;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.compose>.container>.box>.textarea {
			flex: 1 auto;
			max-height: 20em;
			overflow: hidden;
			overflow-y: auto;
		}
		
		.hey-wrapper>.app-wrapper>.main>.main-content>.block>.compose>.send {
			margin: .5em;
			background-color: rgb(var(--PF-color-surface));
		}
		
		#formCompose {
			display: none;
		}
		/*PHONE*/
		
		@media (max-width: 75em) {
			.hey-wrapper {
				padding: 0;
			}
			.hey-wrapper>.app-wrapper {
				max-width: 100%;
				border-radius: 0;
			}
		}
		
		@media (max-width: 50em) {
			.hey-wrapper>.app-wrapper .page {
				display: none;
			}
			.hey-wrapper>.app-wrapper .page.active {
				max-width: initial;
				border: none;
				display: flex;
			}
			.hey-wrapper>.app-wrapper>.main>.main-content>.block>.messages>.group.me>.PF-avatar {
				display: none;
			}
		}
		
		@keyframes message-up {
			0% {
				opacity: 0;
				transform: translateY(10px);
			}
			80% {
				opacity: 1;
				transform: translateY(0px);
			}
			100% {
				opacity: 1;
				transform: translateY(0px);
			}
		}
	</style>
</head>

<body>
	<div class="hey-wrapper">
		<div class="app-wrapper">
			<aside class="page sidebar">
				<div class="PF PF-toolbar">
					<div class="space"></div>
					<div class="PF PF-icon ripple" onclick="$('.header').toggleClass('hidden');"><i class="material-icons">fullscreen</i></div>
					<div class="PF PF-icon ripple"><i class="material-icons">&#xE5D4;</i></div>
				</div>
				<div class="sidebar-content">

					<ul class="PF PF-list" id="conversations">
						<li class="ripple">
							<div class="PF-avatar"></div>
							<div class="data">
								<h1>Albert Isern Alvarez</h1>
								<p>albert@dumdarac.com</p>
							</div>
						</li>
					</ul>

				</div>
			</aside>
			<main class="page main active">
				<div class="PF PF-toolbar">
					<h1>Titulo</h1>
					<div class="PF PF-icon ripple" onclick="$('.header').toggleClass('hidden');"><i class="material-icons">fullscreen</i></div>
					<div class="PF PF-icon ripple"><i class="material-icons">&#xE5D4;</i></div>
				</div>
				<div class="main-content flex1">
					<div class="block">
						<div class="messages" id="messages">

							<!--------
							<div class="group">
								<div class="PF-avatar ripple ddbg-avatar" rgdd></div>
								<div class="messages-group">
									<div class="message-container">
										<div class="message">
											<div class="content">
												<div class="PF PF-image">
													<img src="https://www.hypnotherapists.org.uk/cms/wp-content/uploads/2017/03/happy-people-friends.jpg">
												</div>
												<p>Guys, buenos dias!</p>
											</div>
										</div>
									</div>
									<div class="message-container">
										<div class="message">
											<div class="content">
												<p>Please come by tomorrow, Thursday, because I will work out of the office today.</p>
											</div>
											<div class="info">
												<p>26 abr., 6:28</p>
											</div>
										</div>
									</div>
									<div class="message-container">
										<div class="message">
											<div class="content">
												<p>Have a nice day!</p>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="datastamp">
								<div class="data">
									<p>Friday, 26 apr. · 6:27</p>
								</div>
							</div>
							
							<div class="group me" data-userid="108">
								<div class="PF-avatar ripple ddbg-avatar" rgdd></div>
								<div class="messages-group">
									<div class="message-container">
										<div class="message">
											<div class="content">
												<p>Guys, buenos dias!</p>
											</div>
										</div>
									</div>
									<div class="message-container">
										<div class="message">
											<div class="content">
												<p>Please come by tomorrow, Thursday, because I will work out of the office today.</p>
											</div>
										</div>
									</div>
									<div class="message-container">
										<div class="message">
											<div class="content">
												<p>Have a nice day!</p>
											</div>
										</div>
									</div>
								</div>
							</div>
							-------->

						</div>


						<div class="compose" id="compose">
							<div class="container PF shadow">
								<div class="box">
									<div class="PF PF-icon ripple"><i class="material-icons">insert_emoticon</i></div>
									<div class="textarea" id="inputMessageText" contenteditable="true"></div>
								</div>
							</div>
							<div class="PF PF-icon send shadow" onclick="sendmessage();"><i class="material-icons">send</i></div>
						</div>

						<form id="formCompose">
							<textarea id="formInputMessageText" name="text"></textarea>
						</form>

					</div>

				</div>
			</main>
		</div>
	</div>

	<script>
		var loadedmessages = [];
		var conversation = "1";
		var allowloadmessages = null;

		function loadmessages() {
			if (!allowloadmessages) {
				allowloadmessages = "no";
				$.ajax({
					type: 'POST',
					url: './pantallas/hey/resources/messages/json.php',
					data: {
						conversation: conversation,
						loadedmessages: JSON.stringify(loadedmessages)
					},
					success: function(data) {
						var lastgroupuser;
						var lastdatelimit;

						var dateseparator = $('<div></div>');
						var group = $('<div></div>');
						var messagesGroup = $('<div></div>');

						var timeoutscrollmessages;

						data.forEach(function(element) {

							loadedmessages.push(element.id);

							var lastgroup = $('#messages .group[data-userid]').last();

							var messageContainer = $('<div></div>');
							messageContainer.addClass('message-container');
							messageContainer.attr('data-userid', element.user.id);
							messageContainer.attr('data-id', element.id);

							var message = $('<div></div>');
							message.addClass('message');

							var messageContent = $('<div></div>');
							messageContent.addClass('content');

							var messageContentText = $('<p></p>');
							messageContentText.text(element.text);
							messageContent.append(messageContentText);


							message.append(messageContent);
							messageContainer.append(message);

							if (lastgroup.attr('data-userid') == element.user.id) {
								lastgroup.children('.messages-group').append(messageContainer);
							} else if (lastgroupuser != element.user.id) {
								group = $('<div></div>');
								group.addClass('group');
								group.attr('data-userid', element.user.id);


								avatar = $('<div></div>');
								avatar.addClass('PF-avatar ripple');

								if (element.user.id === '<?=$usuario_mismo_id?>') {
									avatar.addClass('ddbg-avatar');
									group.addClass('me');
								} else {
									avatar.css('background-image', 'url(' + element.user.avatar + ')');
								}

								group.append(avatar);

								messagesGroup = $('<div></div>');
								messagesGroup.addClass('messages-group');

								messagesGroup.append(messageContainer);
								group.append(messagesGroup);
								$('#messages').append(group);

							}

							lastgroupuser = element.user.id;
							lastdatelimit = element.datelimit;

						});
						allowloadmessages = null;
						$(".main-content").animate({
							scrollTop: $('.main-content').prop("scrollHeight")
							}, 500);
					},
					error: function(xhr, status, error) {
						console.log(error);
						allowloadmessages = null;
					}
				});
			}
		}

		function checkmessages() {
			$.ajax({
				type: 'POST',
				url: './pantallas/hey/resources/messages/check.php',
				data: {
					conversation: conversation,
					loadedmessages: JSON.stringify(loadedmessages)
				},
				success: function(data) {
					if (data) {
						loadmessages();
					}
				},
				error: function(xhr, status, error) {
					console.log(error);
				}
			});

		}

		function updateconversations() {
			$.ajax({
				type: 'POST',
				url: './pantallas/hey/resources/conversations/json.php',
				success: function(data) {
					if (data) {
						var conversations = [];
						data.forEach(function(element) {
							var conversation = $('<li class="ripple" conversation> <div class="PF-avatar"></div> <div class="data"> <h1>Albert Isern Alvarez</h1> <p>albert@dumdarac.com</p> </div> </li>');
							conversation.attr('data-id', element.id);
							
							conversations.push(conversation);
						});
						$('#conversations').html(conversations);
					}
				},
				error: function(xhr, status, error) {
					console.log(error);
				}
			});

		}

		loadmessages();
		setInterval(function() {
			updateconversations();
			checkmessages();
		}, 500);

		var sendmessagetimeout;

		function sendmessage() {
			$('#loading').show();
			$('#formCompose #formInputMessageText').val($("#compose #inputMessageText").text());
			$("#compose #inputMessageText").empty();
			$.ajax({
				url: './pantallas/hey/resources/messages/send.php',
				data: $("#formCompose").serialize() + "&conversation=" + conversation,
				type: "POST",
				success: function(data) {
					checkmessages();
					$('#loading').hide();
				}
			});
			return false;
		}


		$("#compose #inputMessageText").keydown(function(event) {
			if (event.which == 13) {
				event.preventDefault();
				event.stopImmediatePropagation();
				sendmessage();
				return false;
			}
		});

		$(document).on("click", ".hey-wrapper [conversation]", function() {
			if($(this).attr('data-id')){
				conversation = $(this).attr('data-id');
				loadedmessages = [];
				$('#messages').empty();
			}
		});
	</script>

</body>

</html>