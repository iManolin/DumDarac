<html>
  <head>
    <style>
      
      .contenedorcomunidad {
        width:100%;
        display:inline-flex;
        align-items:top;
      }
      
      .contenedorcomunidad .info {
        width:100%;
        max-width:20rem;
        height:calc(100% - 64px);
        background:white;
        box-shadow: 0 2px 5px 0 rgba(0,0,0,.16), 0 2px 5px 0 rgba(0,0,0,.23);
        margin-right:0.5rem;
      }
      
      
      .contenedorcomunidad .info .icono {
        background-image:url('https://cdn.dribbble.com/users/1044993/screenshots/3635074/koala-sleeping_dribbble.png');
        background-size:cover;
        background-position:center;
        width:15rem;
        height:15rem;
        border-radius:100%;
        margin:auto;
        margin-top:2.5rem;
      }
      
      .contenedorcomunidad .info h1 {
        font-size:1.5rem;
        color:black;
        padding:1rem;
        width:calc(100% - 2rem);
        font-weight:900;
      }
      
      .contenedorcomunidad .info p {
        font-size:1rem;
        color:black;
        padding:1rem;
        padding-top:0;
        width:calc(100% - 2rem);
        font-weight:900;
      }
      
      .contenedorcomunidad .info .buttonunirme {
    padding: 1rem;
    font-size: 1rem;
    background: #009688;
    color: white;
    border-radius: 2px;
    margin: 0.5rem;
    display: block;
    text-align: center;
    box-shadow: 0 1px 1.5px 0 rgba(0,0,0,.12), 0 1px 1px 0 rgba(0,0,0,.24);
      }
      
      .contenedorcomunidad .contentcomunidad {
        width:100%;
        height:auto;
      }
      
    </style>
  </head>
  <body>
    
    <div class="contenedorcomunidad" >
      
      <div class="info" >
        <div class="icono" ></div>
        <h1>
          El Koala dormido
        </h1>
        <p>
          Esto es una falsa descripcion para describir.
        </p>
        <a class="buttonunirme" >Unirme</a>
        
        <div class="people" >
          
        </div>
        
      </div>
      
      <div class="contentcomunidad" >
      </div>
      
    </div>
    
  </body>
</html>