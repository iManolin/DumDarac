<html>

<head>
</head>

<body>
  <div class="internal_container PF shadow">
    <progress class="PF-progress linear loading" style="position: absolute; top: 0px; left: 0px; display: none;"></progress>
    <div class="box_container">
      <div class="box">
        <form id="logoutform" method="post" action="">
          
          <?php
          if($usuario_id_mismo != ''){
            $fecha_actividad = date("Y-m-d H:i:s");
            $id_actividad = $_GET['id'];
            $url_actual_actividad = "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            mysqli_query($con, "INSERT INTO actividad (usuario_id, tipo, url, id_actual, fecha, funcion) VALUES('$usuario_id_mismo', 'pagina', '?p=logout', '$id_actividad', '$fecha_actividad', 'logout' ) ") or die(mysqli_error());
            session_start();
            session_unset();
            session_destroy();
            $title_logout = "Session closed correctly";
            $description_logout = "We hope to see you again soon 😃";
          } else {
            $title_logout = "There is no open session";
            $description_logout = "If you think it is due to an error, you can contact us at the help button. 💁‍♀️";
          }
          ?>
          
          <div class= "content step auto">
            <div class="logo_container">
              <div class="logo" title="DumDarac"></div>
            </div>
            <div class="presentation">
              <h1><span t-dd >See you soon</span><?php if($usuario_mismo_nombre){?>, <?=$usuario_mismo_nombre?><?}?></h1>
              <p t-dd >Leaving the session... In a few moments you will be redirected.</pt>
            </div>
          </div>
          
          <div class= "content step auto">
            <div class="logo_container">
              <div class="logo" title="DumDarac"></div>
            </div>
            <div class="presentation">
              <h1 t-dd><?=$title_logout?></h1>
              <p t-dd><?=$description_logout?></p>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>

  <?php include(__DIR__."/js.php"); ?>

</body>

</html>