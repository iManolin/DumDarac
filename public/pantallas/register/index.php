<html>

<head>
</head>

<body>
  <div class="internal_container PF shadow">
    <progress class="PF-progress linear loading" style="position: absolute; top: 0px; left: 0px; display: none;"></progress>
    <div class="box_container">
      <div class="box">
        <form id="registerform" method="post" action="./pantallas/register/create-account.php" autocomplete="off" class="PF PF-form">
          <div class="overlay"></div>
          <div class="processSteps"></div>
          <div class="steps">
            <div class="content step">
              <div class="logo_container">
                <div class="logo" title="DumDarac" alt="DumDarac"></div>
              </div>
              <div class="presentation">
                <h1 t-dd>Create your DumDarac Account</h1>
              </div>
              <div class="block">

                <div class="PF PF-twocolumns">

                  <label class="PF-textfield outlined">
                    <input placeholder=" " type="text" name="name">
                    <span t-dd>First Name</span>
                  </label>

                  <label class="PF-textfield outlined">
                    <input placeholder=" " type="text" name="lastname">
                    <span t-dd>Last Name</span>
                  </label>

                </div>

                <label class="PF-textfield outlined">
                  <input placeholder=" " type="text" name="username" required="">
                  <span t-dd>Username</span>
                </label>

                <label class="PF-textfield outlined">
                  <input placeholder=" " type="email" name="email" required="">
                  <span t-dd>Email</span>
                </label>


              </div>
              <!-----<div class="block">
                <p><span t-dd>Not your computer? Use Anonymous mode to sign in privately.</span> <a onclick="openddgo('?p=anonymous'); $('body').addClass('PF-dark');" t-dd>Learn more</a></p>
              </div>----->
              <div class="block">
                <div class="PF PF-buttons flex full">
                  <div class="PF-button text" opendd-href="?p=login" t-dd>Sign in instead</div>
                  <div class="space"></div>
                  <button type="button" class="PF-button next" t-dd>Next</button>
                </div>
              </div>
            </div>

            <div class="content step">
              <div class="logo_container">
                <div class="logo" title="DumDarac" alt="DumDarac"></div>
              </div>
              <div class="presentation">
                <h1 t-dd>Create a strong password</h1>
                <p>Create a strong password with a mic of letters, numbers and symbols</p>
              </div>
              <div class="block">

                <div class="PF PF-twocolumns">
                  <label class="PF-textfield outlined">
                <input placeholder=" " type="password" name="usuario_clave" pattern=".{8,60}" required>
                <span t-dd >Password</span>
              </label>

                  <label class="PF-textfield outlined">
                <input placeholder=" " type="password" name="confirm" pattern=".{8,60}" required>
                <span t-dd >Confirm</span>
              </label>
                </div>


                <p t-dd>In order to protect your account, make sure your password:</p>
                <ul style="list-style:circle">
                  <li t-dd>Is longer than 7 characters.</li>
                  <li t-dd>Does not match or significantly contain your username, e.g. do not use 'username123'.</li>
                  <li><span t-dd>Is not a member of this</span> <a target="_blank" href="?p=common-passwords">list of common passwords</a><span>.</span></li>
                  <li t-dd>Is not a member of the Have <a target="_blank" href="https://haveibeenpwned.com/">I Been Pwned breach</a> database.</li>
                </ul>

              </div>
              <div class="block">
                <div class="PF PF-buttons flex full">
                  <button type="button" class="PF-button back transparent" t-dd>Back</button>
                  <div class="space"></div>
                  <button type="button" class="PF-button next" t-dd>Next</button>
                </div>
              </div>
            </div>

            <!----------
            <div class="content step">
              <div class="logo_container">
                <div class="logo" title="DumDarac"></div>
              </div>
              <div class="presentation">
                <h1><span data-value="user_name"></span>, <span t-dd>tell us a bit about yourself</span></h1>
                <p t-dd>with your DumDarac Account</p>
              </div>
              <div class="block">
                <div class="PF PF-grid">
                  <div class="PF PF-card"></div>
                  <div class="PF PF-card"></div>
                  <div class="PF PF-card"></div>
                  <div class="PF PF-card"></div>
                  <div class="PF PF-card"></div>
                  <div class="PF PF-card"></div>
                  <div class="PF PF-card"></div>
                  <div class="PF PF-card"></div>
                  <div class="PF PF-card"></div>
                </div>
              </div>
              <div class="block">
                <div class="PF PF-buttons flex full">
                  <div class="PF-button text next" t-dd>Skip</div>
                  <div class="space"></div>
                  <button type="button" class="PF-button next" t-dd>Next</button>
                </div>
              </div>
            </div>
--------------->

            <div class="content step auto">
              <div class="logo_container">
                <div class="logo" title="DumDarac"></div>
              </div>
              <div class="presentation">
                <h1><span t-dd>Welcome</span> <span data-value="user_name"></span></h1>
                <p t-dd>In a few moments you will be redirected.</p>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <?php include(__DIR__."/js.php"); ?>

</body>

</html>