<html>

<head>
	<style>

body:not(.PF-dark) .dd_screen {
      background-image: url(//img.dumdarac.com/backgrounds/city_banner_background.svg);
    background-repeat: no-repeat;
    background-position: center bottom;
    background-attachment: fixed;
}
	
		html {
			height: 100%;
		}
		
		body {
			overflow: hidden;
		}
		
		#wrapper {
			overflow: auto;
		}
		
		div#animation_logo svg * {
			fill: rgb(var(--PF-color-primary));
			stroke: none;
		}
		
		.genius-container {
			flex: 1;
		}
		
		.genius-container>.PF-content>.container>h1 {
			font-size: 34px;
			font-weight: 900;
		}
		
		.genius-container #settingscolorselector .PF-card {}
		
		.genius-container #settingscolorselector .PF-card.selected {
			border: solid 3px black;
		}
	</style>
</head>

<body>

	<div class="genius-container">

		<div class="PF PF-content full noborder center between">
			<div class="container">
				<div id="animation_logo" class="animation_logo PF-image circle"></div>
				<h1 t-dd>Hi, I'm Kamiku.</h1>
			</div>
		</div>

		<div class="PF-tabs">
			<div class="PF PF-tabbar shadow">
				<div class="container">
					<ul>
						<li class="ripple active" data-for="home-geniustab" data-taburl="./pantallas/genius/pantallas/home.php?<?=$server_querystring?>"><span t-dd>Glance</span></li>
						<li class="ripple" data-for="personalinfo-geniustab" data-taburl="./pantallas/genius/pantallas/personal-info.php?<?=$server_querystring?>"><span t-dd>Personal Info</span></li>
						<li class="ripple" data-for="datapersonalization-geniustab"><span t-dd>Settings</span></li>
					</ul>
					<div class="slider"></div>
				</div>
			</div>
			<div class="tabs">
				<div class="tab" data-name="home-geniustab"></div>
				<div class="tab" data-name="personalinfo-geniustab"></div>
				<div class="tab" data-name="datapersonalization-geniustab"></div>
				<div class="tab" data-name="security-geniustab"></div>
			</div>
		</div>

	</div>

	<script src="//dumdarac.com/resources/js/bodymovin.min.js"></script>
	<script type="text/javascript">

$('body').addClass('PFC-meat');
	
		bodymovin.loadAnimation({
			container: document.getElementById("animation_logo"),
			renderer: "svg",
			loop: !0,
			autoplay: !0,
			path: "./pantallas/genius/static/listening.json"
		});
	</script>

</body>

</html>