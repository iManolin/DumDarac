<?php

$feed['blocks']['2'][] = array(
  		'title' => 'posts',
  		'fullcoverage' => array(),
  		'cards' => array());

include(__DIR__.'/algorithm.php');
include(__DIR__.'/process-text.php');
  		
$posts = mysqli_query($con,"SELECT * FROM posts WHERE id IN($ids_select_post) LIMIT 10");
while($row_posts = mysqli_fetch_array($posts))
{

	$ids_feedsloaded[] =  $row_posts['id'];

	$usuario_id_posts = $row_posts['usuario_id'];

	//DATOS USUARIO
  $posts_usuario_post = mysqli_query($con,"SELECT * FROM usuarios WHERE usuario_id='$usuario_id_posts'");
  while($row_posts_usuario_post = mysqli_fetch_array($posts_usuario_post)){
    $usuario_id_post = $row_posts_usuario_post["usuario_id"];
    $user_username_post = $row_posts_usuario_post["usuario_nombre"];
    $user_name_post = $row_posts_usuario_post["nombre"];
    $user_lastname_post = $row_posts_usuario_post["apellidos"];
    $user_avatar_post = $row_posts_usuario_post["avatar"];
    if($usuario_avatar_post == null) {
      $usuario_avatar_post = "usuario.svg";
      $usuario_avatar_id_post = '0';
    }
   }
 

	$post_image = null;
	if($row_posts['type'] == 'photo'){
		$post_image = "//dumdarac.com/apps/photos/see.php?id=" . $row_posts['contentid'];
	}
	
  
  $feed['blocks']['2'][0]['cards'][] = array(
  	'id' => $row_posts['id'],
  	'topic' => null,
  	'category' => null,
  	'title' => $row_posts['title'],
  	'description' => null,
  	'date' => $row_posts['date'],
  	'source' => array(
  		'name' => null,
  		'logo' => null,
  		'link' => null),
  	'author' => array(
  	  'username' => $user_username_post,
  		'name' => $user_name_post,
  		'lastname' => $user_lastname_post,
  		'image' => null),
  	'content' => array(
  		'text' => processtext_post($row_posts['text']),
  		'images' => array($post_image),
  		'links' => array()
  		),
  		'tags' => $row_posts["etiquetas"]
  	);

}

updatefeedsloaded($con, $usuario_id_mismo, $ids_feedsloaded, "posts");

?>