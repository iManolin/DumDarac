<?php

function processtext_post($publicacion_texto_p){

	$publicacion_texto_p = strip_tags($publicacion_texto_p);
  
$url_srch = '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i';
if(stripos($publicacion_texto_p, 'dumdarac.com') !== false and stripos($publicacion_texto_p, 'app') !== false){
  $publicacion_texto_p = preg_replace($url_srch, '<a href="$0" title="$0">$0</a>', $publicacion_texto_p);
} else {
  $publicacion_texto_p = preg_replace($url_srch, '<a href="$0" target="_blank" title="$0">$0</a>', $publicacion_texto_p);
}


// #HASHTAG @USERNAME +PAGE
//@USERNAME
$publicacion_texto_p = preg_replace("/@([^\s[:punct:]]+)/i", '<a href="/u/$1">$0</a>', $publicacion_texto_p);
  
  //BOLD
  $publicacion_texto_p = preg_replace("/\*\*(.*?)\*\*/", "<b>$1</b>", $publicacion_texto_p);


//Añadimos para que haga el enter
$chars = array("\r\n", '\\n', '\\r', "\n", "\r", "\t", "\0", "\x0B");
$publicacion_texto_p = str_replace($chars,"<br>",$publicacion_texto_p);

$publicacion_texto_p = preg_replace('#\*{2}(.*?)\*{2}#', '<b>$1</b>', $publicacion_texto_p);

$publicacion_texto_p = preg_replace('~(^<br />\s*)|((?<=<br />)\s*<br />)|(<br />\s*$)~', '', $publicacion_texto_p);

$publicacion_texto_p = preg_replace('/^\s*(?:<br\s*\/?>\s*)*/i', '', $publicacion_texto_p);
  
  $publicacion_texto_p = utf8_encode($publicacion_texto_p);
  
  $publicacion_texto_p = utf8_decode($publicacion_texto_p);
  
  $publicacion_texto_p = iconv("UTF-8","UTF-8//IGNORE",$publicacion_texto_p);
  
  $publicacion_texto_p_encoding = mb_detect_encoding($publicacion_texto_p);
  
  return $publicacion_texto_p;
}

?>