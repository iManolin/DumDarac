<html>

<head>
  <style>
    .feedback-support-container {
      max-width: 50rem;
      margin: auto;
    }
  </style>
</head>

<body>

  <?php if($_GET['p'] == ''){?>
    <div class="PF PF-toolbar">
      <div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
    </div>
    <?}?>

      <div class="feedback-support-container">
        <div class="PF PF-content full noborder center">
          <div class="container">
            <div class="image" style="background-image:url('./pantallas/donate/donate.svg');"></div>
            <h1 t-dd>Donate</h1>
            <p t-dd>Make us a donation, so we can keep improving DumDarac.</p>
          </div>
        </div>

        <div class="PF-page">
          <header>
            <div class="info">
              <h1 t-dd>Mission Statement</h1>
            </div>
          </header>
          <div class="content">

            <p t-dd>DumDarac adheres to the values of the open source movement, and so its code is published and it does not store user data. It aims to provide a high quality and engaging interface as an open source social network, rivalling the Establishment
              social networks, but fundamentally differing from them in essence. In this way DumDarac hopes to fulfill the desires of those users wishing to leave Facebook, Twitter, etc. due to legitimate privacy concerns as well as offering a positive
              example of establishing concrete, popular alternatives to dystopian social networks. At the present it functions as its own social network (with potentiality to add many new features) and, in the interests of a realistic transition, it also
              functions as an overall internet/social network aggregator. This means that users will still be able to follow others on current widely used social networks without compromising their own personal privacy, all while benefiting from similar
              services on DumDarac, but in a functional, open source and alternative way.</p>

            <a class="PF PF-button transparent ripple" target="_blank" href="https://gogetfunding.com/free-social-platform-dumdarac/">
              <div class="inside">
                <p t-dd>Open GoGetFunding</p>
              </div>
            </a>
            <a class="PF PF-button transparent ripple" target="_blank" href="https://medium.com/dumdarac/what-is-3649a9f2a850">
              <div class="inside">
                <p t-dd>Medium article</p>
              </div>
            </a>

          </div>

        </div>

        <div class="PF PF-page">
          <header>
            <div class="info">
            <h1 t-dd>Donate now</h1>
            <p t-dd>You can use any of these methods to make your contribution.</p>
          </div>
          </header>
          <div class="PF PF-grid selector">
            
            <div class="PF-card" onclick="windowdd('./pantallas/donate/windows/bank-transfer.php', 'fit');">
              <div class="PFC-green PF-image ripple" style="background-image:url('//img.dumdarac.com/donaciones/euro.jpg');"></div>
              <div class="info">
                <h1>Euros</h1>
              </div>
            </div>
            
            <div class="PF-card" onclick="windowdd('./pantallas/donate/windows/bank-transfer.php', 'fit');">
              <div class="PFC-green PF-image ripple" style="background-image:url('//img.dumdarac.com/donaciones/dollar.jpg');"></div>
              <div class="info">
                <h1>Dollars</h1>
              </div>
            </div>


            <div class="PF-card" target="_blank" onclick="windowdd('./pantallas/donate/windows/bitcoin.php', 'fit');">
              <div class="PFC-orange PF-image ripple" style="background-image:url('//img.dumdarac.com/donaciones/bitcoin.jpg');"></div>
              <div class="info">
                <h1>Bitcoin</h1>
              </div>
            </div>

            <div class="PF-card" target="_blank" onclick="windowdd('./pantallas/donate/windows/ethereum.php', 'fit');">
              <div class="PFC-orange PF-image ripple" style="background-image:url('//img.dumdarac.com/donaciones/ethereum.jpg');"></div>
              <div class="info">
                <h1>Ethereum</h1>
              </div>
            </div>

            <div class="PF-card" target="_blank" onclick="windowdd('./pantallas/donate/windows/litecoin.php', 'fit');">
              <div class="PFC-orange PF-image ripple" style="background-image:url('//img.dumdarac.com/donaciones/litecoin.jpg');"></div>
              <div class="info">
                <h1>Litecoin</h1>
              </div>
            </div>

            <div class="PF-card" target="_blank" onclick="windowdd('./pantallas/donate/windows/ripple.php', 'fit');">
              <div class="PFC-orange PF-image ripple" style="background-image:url('//img.dumdarac.com/donaciones/ripple.jpg');"></div>
              <div class="info">
                <h1>Ripple</h1>
              </div>
            </div>

            <div class="PF-card" target="_blank" onclick="windowdd('./pantallas/donate/windows/faircoin.php', 'fit');">
              <div class="PFC-yellow PF-image ripple" style="background-image:url('//img.dumdarac.com/donaciones/faircoin.jpg');"></div>
              <div class="info">
                <h1>Faircoin</h1>
              </div>
            </div>

            <div class="PF-card" onclick="windowdd('./pantallas/donate/windows/bank-transfer.php', 'fit');">
              <div class="PFC-green PF-image ripple" style="background-image:url('//img.dumdarac.com/donaciones/bank.jpg');"></div>
              <div class="info">
                <h1>Bank transfer</h1>
              </div>
            </div>
            
            <?php if($usuario_mismo_id){?>
            <div class="PF-card" target="_blank" opendd-href="?p=contributors">
              <div class="PFC-yellow PF-image ripple contain" style="background-image:url('//img.dumdarac.com/donaciones/contributors.svg');"></div>
              <div class="info">
                <h1 t-dd>Contribute</h1>
              </div>
            </div>
            <?}?>

          </div>
        </div>

      </div>

</body>

</html>