<div class="PF PF-toolbar">
  <div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
  <h1 t-dd>Bank transfer</h1>
</div>

<div class="PF PF-card" >
  <div class="info">
    <p><b>IBAN</b>: GB13 REVO 0099 7007 5572 09</br></br>
<b>BIC</b>: REVOGB21</br></br>
<b>Beneficiary</b>: Albert Isern Alvarez</br></br>
<b>Beneficiary address</b>: Themistokleus 42, 10678, Greece, Athens, GR</p>
  </div>
</div>