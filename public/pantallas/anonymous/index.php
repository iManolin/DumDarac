<html>

<head>
  <link rel="stylesheet" type="text/css" href="//dumdarac.com/resources/css/dumdarac-internal.css?version=<?=$cache_version?>">
</head>

<body>
  <div class="internal_container PF shadow">
    <progress class="PF-progress linear loading" style="position: absolute; top: 0px; left: 0px; display: none;"></progress>
    <div class="box_container">
      <div class="box">
        <form id="testform" method="post">
          <div class= "content step">
            <div class="logo_container">
              <div class="logo" title="DumDarac" alt="DumDarac"></div>
            </div>
            <div class="presentation">
              <h1 t-dd >Anonymous</h1>
              <p t-dd >Without DumDarac Account, easy and fast</p>
            </div>
            <div class="block">
              <label class="PF-textfield filled">
                <input placeholder=" " type="text" name="">
                <span t-dd >Temporary DumDarac ID</span>
              </label>
              <a opendd-href="?p=recover&type=email" >Want to create an account?</a>
            </div>
            <div class="block">
              <p>Don't want to be anonymous? <a onclick="openddgo('?p=login'); $('body').removeClass('PF-dark');" >Sign in normally</a></p>
            </div>
            <div class="block">
              <div class="PF PF-buttons flex full">
                <div class="space"></div>
                <button type="button" class="PF-button next" t-dd>Next</button>
              </div>
            </div>
          </div>

          <div class= "content step">
            <div class="logo_container">
              <div class="logo" title="DumDarac"></div>
            </div>
            <div class="presentation">
              <h1>Hi Anonymous</h1>
              <p>Please confirm that you're not a robot.</p>
            </div>
            <div class="block">

              <label class="PF-textfield filled">
                <input placeholder=" " type="password">
                <span>Password</span>
              </label>
            </div>
            <div class="block">
              <div class="PF PF-buttons flex full">
                <div class="space" ></div>
                <button type="button" class="PF-button next" t-dd>Next</button>
              </div>
            </div>
          </div>
          
          <div class= "content step auto">
            <div class="logo_container">
              <div class="logo" title="DumDarac"></div>
            </div>
            <div class="presentation">
              <h1>Accessing anonymously</h1>
              <p>In a few moments you will be redirected.</p>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>

  <?php include(__DIR__."/js.php"); ?>

</body>

</html>