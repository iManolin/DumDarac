<?php if($pagina == 'perfil' or !$pagina){?>
<div class="PF PF-toolbar">
  <div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
  <h1 t-dd>Following</h1>
</div>
<?}?>

<ul class="PF PF-list loadfollowers">
  
  <?php
    $id_amigos_publicaciones_p = array();
    $amigos_publicaciones_p = mysqli_query($con,"SELECT * FROM user_follows WHERE type='usuario' AND usuario_id='$usuario_id'");
  while($row_amigos_publicaciones_p = mysqli_fetch_array($amigos_publicaciones_p))
  {
    $id_amigos_publicaciones_p[] = $row_amigos_publicaciones_p["id_seguir"];
  }
  $id_amigos_publicaciones_p = implode(',', $id_amigos_publicaciones_p);
  $usuario = mysqli_query($con,"SELECT * FROM usuarios WHERE usuario_id IN ($id_amigos_publicaciones_p) ORDER BY lastactivity DESC, RAND()");
  while($row_usuario_followers = mysqli_fetch_array($usuario))
  {
    $usuario_id_followers = $row_usuario_followers['usuario_id'];
$usuario_nombre_followers = $row_usuario_followers['usuario_nombre'];
$usuario_nombre_nombre_followers = $row_usuario_followers['nombre'];
$usuario_apellidos_followers = $row_usuario_followers['apellidos'];
$usuario_ciudad_followers = $row_usuario_followers['ciudad'];
$usuario_trabajo_followers = $row_usuario_followers['trabajo'];
$email_followers = $row_usuario_followers['usuario_email'];
$descripcion_followers = $row_usuario_followers['descripcion'];
$estado_followers = $row_usuario_followers['estado'];
$color_followers = $row_usuario_followers['color'];
$usuario_avatar_followers = $row_usuario_followers['avatar'];
$usuario_id_followers = $row_usuario_followers['usuario_id'];
$usuario_id_followers_id = $row_usuario_followers['usuario_id'];
if($usuario_nombre_nombre_followers != ''){$usuario_nombre_mostrar_followers = $usuario_nombre_nombre_followers;} else {$usuario_nombre_mostrar_followers = $usuario_nombre_followers;}
if($usuario_avatar_followers == null) {
  $usuario_id_followers_id = '0';
  $usuario_avatar_followers = "usuario.svg";
}
  ?>
<li class="ripple opendd closewindowdd follower <?php if($color_followers != null){?>PFC-<?=$color_followers?><?}?>" data-usuarioid="<?=$usuario_id_followers?>" opendd-href="?p=perfil&id=<?=$usuario_id_followers?>" >
    <div class="PF PF-avatar" style="background-image:url('//dumdarac.com/apps/photos/see.php?id=<?=$usuario_avatar_followers?>');" rgdd ></div>
    <div class="data">
      <?php if($usuario_nombre_mostrar_followers != ''){?><h1><?=$usuario_nombre_mostrar_followers?> <?=$usuario_apellidos_followers?></h1><?}?>
      <?php if($descripcion_y_ciudad != ''){?><p><?=$descripcion_y_ciudad?></p><?}?>
    </div>
  <div follow-button data-id="<?=$usuario_id_followers?>" data-type="usuario" data-classes="minimized" ></div>
  </li>
<?}?>
    
    </ul>

<?php if($usuario_id_followers == null){?>
  <div class="PF PF-emptypage">
    <div class="container">
      <div class="image">
        <i class="material-icons">supervisor_account</i>
      </div>
      <h1 t-dd>Not following anyone yet</h1>
      <p t-dd>Here will be shown the users that follow.</p>
    </div>
  </div>
<?}?>

  <script>
        $(".header .loading_bar_container").hide();
  </script>