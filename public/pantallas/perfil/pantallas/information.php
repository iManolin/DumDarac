<div <?php if($usuario_color){?>class="PFC-<?=$usuario_color?>"<?}?> >

<div>
  <div class="PF PF-toolbar">
  <div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
  <h1 t-dd>Information</h1>
</div>

<div class="PF PF-content full noborder center" style="background-image: url(https://img.dumdarac.com/backgrounds/information.png); background-size: cover; background-position: center;">
      <div class="container">
        <div class="PF-image PF-avatar circle ripple" style="background-image: url('<?=$usuario_avatar?>');" rgdd></div>
        <h1><?=$usuario_nombre?> <?=$usuario_apellidos?></h1>
        <?php if($usuario_mismo_id === $usuario_id){?>
        <button class="PF PF-button ripple" style="margin: auto;" opendd-href="?p=account" t-dd >Edit information</button>
        <?}?>
      </div>
  <div follow-button data-id="<?=$usuario_id?>" data-type="usuario" data-size="" ></div>
    </div>
  
</div>

<div class="PF-card" >
  <div class="info">
    <h1 t-dd >Basic</h1>
  <?php if($usuario_nombre != null){?><p><b t-dd >Name:</b> <?=$usuario_nombre?></p><?}?>
  <?php if($usuario_apellidos != null){?><p><b t-dd >Surnames:</b> <?=$usuario_apellidos?></p><?}?>
  <?php if($usuario_nacimiento != null){?><p><b t-dd >Birthdate:</b> <?=$usuario_nacimiento?></p><?}?>
  <?php if($usuario_ciudad != null){?><p><b t-dd >City:</b> <?=$usuario_ciudad?></p><?}?>
  <?php if($usuario_language != null){?><p><b t-dd >Speak:</b> <?=$usuario_language?></p><?}?>
  <?php if($usuario_karma != null){?><p><b t-dd >Karma:</b> <?=$usuario_karma?></p><?}?>
  <?php if($usuario_gender){?><p><b t-dd >Gender:</b> <span t-dd><?=$usuario_gender?></span></p><?}?>
  <?php if($usuario_estado){?><p><b t-dd >Status:</b> <?=replaceemojis($usuario_estado)?></p><?}?>
  </div>
</div>

<?php if($usuario_biography != null){?>
<div class="PF-card" >
  <div class="info">
    <h1 t-dd >Biography</h1>
    <p><?=$usuario_biography?></p>
  </div>
</div>
<?}?>

<?php if($usuario_trabajo != null){?>
<div class="PF-card" >
  <div class="info">
    <h1 t-dd >Work</h1>
    <p><?=$usuario_trabajo?></p>
  </div>
</div>
<?}?>

<?php if($usuario_escuela != null){?>
<div class="PF-card" >
  <div class="info">
    <h1 t-dd >Education</h1>
    <p><b t-dd >School:</b> <?=$usuario_escuela?></p>
  </div>
</div>
<?}?>

<?php if($usuario_mismo_id != $usuario_id && $usuario_nombre != null){

$followbutton_me = mysqli_query($con,"SELECT * FROM user_follows WHERE type='usuario' AND id_seguir='$usuario_id' AND usuario_id='$usuario_mismo_id' LIMIT 1");
while($followbutton_me_row = mysqli_fetch_array($followbutton_me))
{ 
  $id_followbutton_me = $followbutton_me_row['id'];
  $date_followbutton_me = $followbutton_me_row['date'];
}
  
$followbutton_he = mysqli_query($con,"SELECT * FROM user_follows WHERE type='usuario' AND id_seguir='$usuario_mismo_id' AND usuario_id='$usuario_id' LIMIT 1");
while($followbutton_he_row = mysqli_fetch_array($followbutton_he))
{ 
  $id_followbutton_he = $followbutton_he_row['id'];
  $date_followbutton_he = $followbutton_he_row['date'];
}

?>
<div class="PF-card" >
  <div class="info">
    <h1><?=$usuario_nombre?> <span t-dd>& You</span></h1>
  <p>
  <?php if($id_followbutton_me){?><span t-dd >You followed</span> <?=$usuario_nombre?> <span prettydate ><?=$date_followbutton_me?></span> <?} else {?> <span t-dd >You do not follow</span> <?=$usuario_nombre?> <?}?> and <?php if($id_followbutton_he){?>  <?=$usuario_nombre?> <span t-dd >followed you</span> <span prettydate ><?=$date_followbutton_he?></span> <?} else {?> <?=$usuario_nombre?> <span t-dd >do not follow you</span><?}?>.</p>
  
  <!-------<p><span t-dd>Affinity</span> 0%</p>------>
  </div>
</div>
<?}?>

<?php if($usuario_signup_date != null){?>
<div class="PF-card" >
  <div class="info">
    <h1 t-dd >Other information</h1>
  <p><span t-dd >Joined DumDarac</span> <span prettydate ><?=$usuario_signup_date?></span> 😃</p>
  </div>
</div>
<?}?>
    
</div>