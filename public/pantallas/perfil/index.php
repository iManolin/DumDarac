<html>

<head>
  <style>
    .perfil_container {
      max-width: 50rem;
      width: 100%;
      margin: 0.5rem auto;
      display: flex;
      display: -webkit-box;
      display: -moz-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      flex-direction: column;
    }

    .perfil_container>.top {
      display: flex;
      display: -webkit-box;
      display: -moz-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      flex-direction: column;
      max-width: calc(500px + 3em);
      margin: auto;
      width: 100%;
      padding: .5em;
    }

    .perfil_container>.top>.bar {
      display: flex;
      flex-direction: row;
    }

    .perfil_container>.top>.bar>.PF-avatar {
      width: 5em;
      height: 5em;
      margin-right: auto;
    }

    .perfil_container>.top>.data {
      display: flex;
      display: -webkit-box;
      display: -moz-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      flex-direction: column;
      padding: 0.5rem;
    }

    .perfil_container>.top>.data>h1 {
      font-size: 2.5em;
      font-weight: 600;
    }

    .perfil_container>.top>.data>p {
      font-size: 1em;
      font-weight: 500;
      margin-top: .5em;
    }

    .perfil_container>.top>.statistics {
      padding: .5em;
      margin: auto;
      width: 100%;
      align-items: center;
      display: grid;
      grid-template-columns: 1fr 1fr 1fr 1fr;
      column-gap: .5em;
      row-gap: .5em;
      grid-auto-rows: 1fr;
    }

    .perfil_container>.top>.statistics>.stat {
      text-align: center;
      border: solid 1px;
      border-color: rgba(var(--PF-color-on-surface), .1);
      border-radius: 1em;
      padding: 0.5em;
      height: 100%;
      align-items: center;
      vertical-align: middle;
      display: flex;
      flex-direction: column;
      cursor: pointer;
      background-color: rgb(var(--PF-color-surface));
    }

    :root .PF-dark .perfil_container>.top>.statistics>.stat {
      border-color: rgb(var(--PF-color-surface));
      box-shadow: 0px 1px 3px 1px rgba(0,0,0, 0.1);
    }

    .perfil_container>.top>.statistics>.stat>div>h1 {
      font-size: 2em;
      font-weight: 600;
    }

    .perfil_container>.top>.statistics>.stat>div>p {
      font-size: 1em;
      font-weight: 500;
    }

    @media (max-width: 45em) {
      .perfil_container>.top>.statistics>.stat>div>h1 {
        font-size: 1.8em;
        font-weight: 500;
      }
      .perfil_container>.top>.statistics>.stat>div>p {
        font-size: 0.8em;
        font-weight: 500;
      }
    }
  </style>
</head>

<body>

  <div class="perfil_container">

    <div class="top">

      <div class="bar">
        <div class="PF PF-avatar ripple <?php if($usuario_id === $usuario_id_mismo) {?>ddbg-avatar<?}?>" <?php if($usuario_id === $usuario_id_mismo) {?>opendd-href="?p=davatar"<?}?> <?php if($usuario_id != $usuario_id_mismo) {?>style="background-image: url('<?=$usuario_avatar?>');"<?}?> rgdd></div>
        <div class="PF PF-icon ripple" onclick="downmenudd('./pantallas/perfil/preview/menu.php?<?=$server_querystring;?>');"><i class="material-icons">more_horiz</i></div>
        <div follow-button data-id="<?=$usuario_id?>" data-type="usuario" data-size=""></div>
      </div>

      <div class="data">
        <h1><?=$usuario_nombre?> <?=$usuario_apellidos?><?=$current_window_id?></h1>
        <?php if($usuario_estado !='') {?>
          <p><?=replaceemojis($usuario_estado)?></p>
        <?}?>
      </div>

      <div class="statistics"></div>

    </div>
    <div class="content">
      <?php if($usuario_status == 'private' and $usuario_id != $usuario_id_mismo){?>
      <div class="PF PF-emptypage">
        <div class="container">
          <div class="image"><i class="material-icons">security</i></div>
          <h1 t-dd>This account is private</h1>
          <p t-dd>You can submit a follow request and if it accepts you will be able to view his profile.</p>
        </div>
      </div>
      <?} else {?>
        <div feed data-type="all" data-classes="shadow" data-currenturl="?<?=$server_querystring;?>"></div>
      <?}?>
    </div>

  </div>

  <script>
    
    <?php if($usuario_color != null){?>$('body').addClass('PFC-<?=$usuario_color?>');<?}?>
    $('.perfil_container .top .statistics').load('./pantallas/perfil/resources/statistics.php?<?=$server_querystring;?>');

    $(document).on("click", ".stat.posts", function() {
      $('html, body').animate({
        scrollTop: $(".feed").offset().top
      }, 1000);
    });
  </script>

</body>

</html>