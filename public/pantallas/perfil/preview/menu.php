<?php if($usuario_id != $usuario_mismo_id) {?>
<div class="PF PF-toolbar">
  <div class="PF PF-avatar ripple" style="background-image: url('<?=$usuario_avatar?>');" rgdd></div>
  <div class="dbl" >
    <h1><?=$usuario_nombre?> <?=$usuario_apellidos?></h1>
    <?php if($usuario_estado !='') {?><p><?=$usuario_estado?></p><?}?>
  </div>
  <div follow-button data-id="<?=$usuario_id?>" data-type="usuario" data-size=""></div>
</div>
<?}?>

  <ul>
    <?php if($pagina != 'perfil'){?>
    <a opendd-href="?p=perfil&id=<?=$usuario_id?>" ><li><i class="material-icons">person_outline</i><span t-dd >See profile</span></li></a>
    <?}
    if(in_array($usuario_id, array_keys($_SESSION["users"])) and $usuario_id !== $usuario_mismo_id or (in_array($account_switch, $public_accounts_switch))){?>
    <a href="?account-switch=<?=$usuario_id?>&<?=$server_querystring?>" ><li><i class="material-icons">supervised_user_circle</i><span t-dd >Switch user</span></li></a>
    <?}?>
    <li onclick="windowdd('./pantallas/perfil/pantallas/information.php?id=<?=$usuario_id?>&<?=$_SERVER["QUERY_STRING"];?>', 'fit');" ><i class="material-icons">info</i><span t-dd >Information</span></li>
    <?php if($usuario_id == $usuario_mismo_id){?>
    <li opendd-href="?p=account" ><i class="material-icons">settings</i><span t-dd >Edit profile</span></li>
    <?} else {?>
    <hr>
    <li opendd-href="?app=phone&id=<?=$usuario_id?>" ><i class="material-icons">call</i><span t-dd >Call</span></li>
    <li onclick="alertdd.show('The video call could not be made');" ><i class="material-icons">video_call</i><span t-dd >Video Call</span></li>
    <li opendd-href="?p=hey&id=<?=$usuario_id?>" ><i class="material-icons">chat</i><span t-dd >Send a message</span></li>
    <li onclick="windowdd('./apps/pay/resources/transfer.php?app=pay&id=<?=$usuario_id?>', 'fit');" ><i class="material-icons">money</i><span t-dd >Send money</span></li>
    <hr>
    <?}?>
    <li onclick="alertdd.show('The profile could not be shared');" ><i class="material-icons">share</i><span class="t-dd" >Share this profile</span></li>
  </ul>