<html>

<head>
  <style>
  
    .header {
      position: fixed;
      margin-top: -10em;
      opacity: 0;
      pointer-events: none;
    }
    
    main + .PF-footer {
          position: sticky;
    bottom: 0;
    left: 0;
    }
  
  </style>
  <link rel="stylesheet" href="//dumdarac.com/pantallas/about/static/css/home.min.css">
  <link href="//dumdarac.com/resources/css/aos.css" rel="stylesheet">
</head>

<body>

  <main class="about_page" >
    <section id="hero" class="hero" data-c-hero="">
      <div class="hero__container container scroll-aware-enter">
        <img class="hero__logo" data-src="//img.dumdarac.com/logo/logo.svg" src="//img.dumdarac.com/logo/logo.svg" alt="DumDarac logo">

        <h1 class="hero__heading">The first free integrated Social Platform with many services and applications, developed with collaborative feedback.</h1>


        <a href="/" target="_blank" rel="noopener" class="PF PF-button hero__cta button--desktop">Start</a>



        <a href="https://play.DumDarac.com/store/apps/details?id=com.DumDarac.android.apps.ness" target="_blank" rel="noopener" class="hero__cta hero__cta--mobile button--play button--hidden" data-g-event="cta" data-g-action="clicked" data-g-label="get_the_app|android|hero">
            <img data-c-lazy-image="" data-src="https://lh3.googleusercontent.com/IjGxXKVqOL94AFXcj3bgPDAqdm6Yo7ZmNitPoJWKadDn5Tdz4Fr5FQZN4-WmI6wqXpjQHWQ3CJyD_C_x5pTUyQ=w320" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAYEBAQFBAYFBQYJBgUGCQsIBgYICwwKCgsKCgwQDAwMDAwMEAwODxAPDgwTExQUExMcGxsbHB8fHx8fHx8fHx8BBwcHDQwNGBAQGBoVERUaHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fH//AABEIAAEAAQMBEQACEQEDEQH/xABKAAEAAAAAAAAAAAAAAAAAAAAIEAEAAAAAAAAAAAAAAAAAAAAAAQEAAAAAAAAAAAAAAAAAAAAAEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwBUg//Z"
              alt="DumDarac play logo">

        </a>



        <a href="https://play.DumDarac.com/store/apps/details?id=com.DumDarac.android.wearable.app" target="_blank" rel="noopener" class="hero__cta hero__cta--mobile button--ios button--hidden" data-g-event="cta" data-g-action="clicked" data-g-label="get_the_app|ios|hero">

            <img data-c-lazy-image="" data-src="https://lh3.googleusercontent.com/GQqvA15HscjHO9iFcWRCxLWmhr9c9IqD8VX8Ov_ClwISP-JCZO-ZwA8AID_95nQ1ollBqjW-cMa-OGGRJpm-=w320" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAYEBAQFBAYFBQYJBgUGCQsIBgYICwwKCgsKCgwQDAwMDAwMEAwODxAPDgwTExQUExMcGxsbHB8fHx8fHx8fHx8BBwcHDQwNGBAQGBoVERUaHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fH//AABEIAAEAAQMBEQACEQEDEQH/xABKAAEAAAAAAAAAAAAAAAAAAAAIEAEAAAAAAAAAAAAAAAAAAAAAAQEAAAAAAAAAAAAAAAAAAAAAEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwBUg//Z"
              alt="Apple i0S logo">
        </a>

        <button class="button button--scroll" id="scrolltoactivity">
        <svg class="icon--svg" aria-hidden="true">
    <use xlink:href="#material-arrow-scroll"></use>
  </svg>


</button>

      </div>
    </section>
    <section id="activity" class="activity scroll-aware-enter" data-aos="fade-up">
      <div class="PF shadow activity__container container">
        <h2 class="activity__title scroll-aware-enter" data-aos="fade-up" >The platform has been in beta mode for a while but now we would like to be able to expand the team and hire programmers to get the platform fully up and running in less than a year.</h2>
        <div class="activity__poster" data-aos="fade-left">
          <img class="features__feature-image-watch-video" src="//dumdarac.com/pantallas/about/static/imgs/iphone_x_matte_ddmockup.png" />
        </div>
        <div class="activity__tiles" data-aos="fade-up">
          <div class="activity__tiles__item">
            <h3 class="activity__tiles__item__heading">Mission Statement</h3>
            <div class="activity__tiles__item__description">
              <p>DumDarac adheres to the values of the open source movement, and so its code is published and it does not store user data. It aims to provide a high quality and engaging interface as an open source social network, rivalling the Establishment social networks, but fundamentally differing from them in essence. In this way DumDarac hopes to fulfill the desires of those users wishing to leave Facebook, Twitter, etc. due to legitimate privacy concerns as well as offering a positive example of establishing concrete, popular alternatives to dystopian social networks.</p>
            <p>At the present it functions as its own social network (with potentiality to add many new features) and, in the interests of a realistic transition, it also functions as an overall internet/social network aggregator. This means that users will still be able to follow others on current widely used social networks without compromising their own personal privacy, all while benefiting from similar services on DumDarac, but in a functional, open source and alternative way.</p>
            </div>
          </div>
          <div class="activity__tiles__item">
            <div class="activity__tiles__item__description">
              <p>The platform has been in beta mode for a while but now we would like to be able to expand the team and hire programmers to get the platform fully up and running in less than a year.

The current team needs to find programmers, translators, vision contributors, investors, internet security & cryptocurrency-ico experts. A non-profit platform whose funding is based on donations.

All the contributions of will be destined to it.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="features" class="features container" data-c-features="">
      <div class="features__feature" >
        <div class="features__feature-text" data-aos="fade-left">
          <h1>Tell your story, your way</h1>
          <p class="subhead">With creative tools like filters, themes, and effects, it's easy to express yourself in ways that delight your followers and make it even more fun.</p>
        </div>
        <div class="features__feature-images" data-aos="fade-up">
          <div class="features__feature-image-watch">

            <img class="features__feature-image-watch-img" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAYEBAQFBAYFBQYJBgUGCQsIBgYICwwKCgsKCgwQDAwMDAwMEAwODxAPDgwTExQUExMcGxsbHB8fHx8fHx8fHx8BBwcHDQwNGBAQGBoVERUaHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fH//AABEIAAEAAQMBEQACEQEDEQH/xABKAAEAAAAAAAAAAAAAAAAAAAAIEAEAAAAAAAAAAAAAAAAAAAAAAQEAAAAAAAAAAAAAAAAAAAAAEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwBUg//Z"
              alt="Watch video">

          </div>
          <div class="features__feature-image-phone"> <video aria-hidden="true" muted="" playsinline="" class="features__feature-image-phone-video" data-c-video="" preload="true">
  </video>

            <img class="features__feature-image-phone-img" src="https://lh3.googleusercontent.com/fcRxYMn5GleUQ19dTcT1fHfREE5wzQOw0xUNcJgDxzieU7y6N12rfhf9gl46uGyPhjstH5O29f3MYCWpYRrAFQ=w320" alt="">

          </div>
        </div>
      </div>
      <div class="features__feature">
        <div class="features__feature-text" data-aos="fade-right">
          <h1>Sign in easily from any device</h1>
          <p class="subhead">From swimming to strolling, any activity that gets you moving makes an impact on your health. That's why DumDarac works with many of your favourite apps and health devices<sup>1</sup> to give you credit for all your moves and provide a holistic
            view of your health.</p>
        </div>
        <div class="features__feature-images" data-aos="fade-up">
          <div class="features__feature-image-phone">
            <img class="features__feature-image-phone-img" src="https://dumdarac.com/pantallas/about/static/imgs/iphone_x_matte_ddmockup.png" alt="">
          </div>
        </div>
      </div>
      <div class="features__feature">
        <div class="features__feature-text" data-aos="fade-left">
          <h1>Express yourself and react</h1>
          <p class="subhead">Each person’s News Feed is personalized based on their interests and the sharing activity of their friends.</p>
        </div>
        <div class="features__feature-images" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
          <div class="features__feature-image-watch">

            <img class="features__feature-image-watch-img" data-c-lazy-image="" data-src="https://lh3.googleusercontent.com/oX_t-WCdTm5ZwQ_eEZ-w8jBEUTqh6L6Hziq2VtrHhFIqduete6g_HPPzTj2zAbmkflab7_b-uuLohxWGYDL3=w320" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAYEBAQFBAYFBQYJBgUGCQsIBgYICwwKCgsKCgwQDAwMDAwMEAwODxAPDgwTExQUExMcGxsbHB8fHx8fHx8fHx8BBwcHDQwNGBAQGBoVERUaHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fH//AABEIAAEAAQMBEQACEQEDEQH/xABKAAEAAAAAAAAAAAAAAAAAAAAIEAEAAAAAAAAAAAAAAAAAAAAAAQEAAAAAAAAAAAAAAAAAAAAAEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwBUg//Z"
              alt="Watch video">

          </div>
          <div class="features__feature-image-phone">
            <img class="features__feature-image-phone-img" src="//dumdarac.com/pantallas/about/static/imgs/react.png" alt="" />
          </div>
        </div>
      </div>
    </section>
    <section id="apps" class="apps container">
      <h4 class="apps__heading" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
        <a href="/?p=discover" target="_blank" rel="noopener">Discover more on DumDarac</a>
      </h4>
    </section>
    <section id="download" class="download PFC-yellow" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
      <div class="download__container container">
        <div class="download__content" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
          <h2 class="download__title">Get DumDarac on Android</h2>
          
          <a href="https://play.google.com/store/apps/details?id=com.dumdarac.application" target="_blank" rel="noopener" class="PF PF-button download__cta button--desktop">Get the app</a>
          
          <a href="https://play.DumDarac.com/store/apps/details?id=com.DumDarac.android.apps.ness" target="_blank" rel="noopener" class="download__cta button--play button--hidden">
            <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAYEBAQFBAYFBQYJBgUGCQsIBgYICwwKCgsKCgwQDAwMDAwMEAwODxAPDgwTExQUExMcGxsbHB8fHx8fHx8fHx8BBwcHDQwNGBAQGBoVERUaHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fH//AABEIAAEAAQMBEQACEQEDEQH/xABKAAEAAAAAAAAAAAAAAAAAAAAIEAEAAAAAAAAAAAAAAAAAAAAAAQEAAAAAAAAAAAAAAAAAAAAAEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwBUg//Z" alt="DumDarac play logo">
            <span class="visually-hidden">Get it on DumDarac Play</span>
          </a>



          <a href="https://play.DumDarac.com/store/apps/details?id=com.DumDarac.android.wearable.app" target="_blank" rel="noopener" class="download__cta button--ios button--hidden" data-g-event="cta" data-g-action="clicked" data-g-label="get_the_app|ios|footer">
            <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAYEBAQFBAYFBQYJBgUGCQsIBgYICwwKCgsKCgwQDAwMDAwMEAwODxAPDgwTExQUExMcGxsbHB8fHx8fHx8fHx8BBwcHDQwNGBAQGBoVERUaHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fH//AABEIAAEAAQMBEQACEQEDEQH/xABKAAEAAAAAAAAAAAAAAAAAAAAIEAEAAAAAAAAAAAAAAAAAAAAAAQEAAAAAAAAAAAAAAAAAAAAAEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwBUg//Z" alt="Apple iOS logo">
            <span class="visually-hidden">Download on the App Store</span>
          </a>

        </div>
        <div class="download__image-container" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
          <img class="download__image" src="https://www.chetu.com/img/case-study/content-image/instagram-api-python-woman-using-phone.png" alt="DumDarac logo">
        </div>
      </div>
    </section>
  </main>

  <footer class="PF PF-footer">
    <div class="block middle">

      <div class="section">
        <h1>Features</h1>
        <ul>
          <li><a href="/?p=help&q=terms">Terms</a></li>
          <li><a href="#">Partners</a></li>
          <li><a href="#">Updates</a></li>
        </ul>
      </div>

      <div class="section">
        <h1>Details</h1>
        <ul>
          <li><a href="#">Specs</a></li>
          <li><a href="#">Tools</a></li>
          <li><a href="#">Resources</a></li>
        </ul>
      </div>

      <div class="section">
        <h1>Technology</h1>
        <ul>
          <li><a href="#">How it works</a></li>
          <li><a href="#">Patterns</a></li>
          <li><a href="#">Usage</a></li>
          <li><a href="#">Products</a></li>
          <li><a href="#">Contracts</a></li>
        </ul>
      </div>

      <div class="section">
        <h1>FAQ</h1>
        <ul>
          <li><a href="#">Questions</a></li>
          <li><a href="#">Answers</a></li>
          <li><a href="#">Contact us</a></li>
        </ul>
      </div>

    </div>

    <div class="block bottom">
      <h1>Title</h1>
      <ul>
        <li><a href="#">Help</a></li>
        <li><a href="#">Privacy & Terms</a></li>
      </ul>
    </div>

  </footer>
  <svg class="svg-icons" xmlns="http://www.w3.org/2000/svg">
      <symbol id="material-arrow" viewBox="0 0 24 24">
  <polyline fill="none" stroke="#000" stroke-width="2" points="7 22 17.001 12 7 2" transform="matrix(0 1 1 0 0 0)"></polyline>
</symbol>
      <symbol id="material-arrow-scroll" viewBox="0 0 24 24">
  <polyline fill="none" stroke-width="4" points="7 22 17.001 12 7 2" transform="matrix(0 1 1 0 0 0)"></polyline>
</symbol>
</svg>
  
  
  
  <script src="//dumdarac.com/resources/js/aos.js"></script>
<script type="text/javascript"> 
  setTimeout(function(){ AOS.init(); }, 1000);
  
  $("#scrolltoactivity").click(function() {
    $('html, body').animate({
        scrollTop: $("#activity").offset().top + -100
    }, 2000);
});
  
  </script>
  
  
</body>

</html>