<html>

<head>
  <style>
@media (min-width: 50em) {
body:not([scrolling='true']) .header .flex-container .flex-center,
body:not([scrolling='true']) .header .flex-container .flex-right .icons{
    opacity: 0;
    pointer-events: none;
    }
}
    
    main+.PF-footer {
      position: sticky;
      bottom: 0;
      left: 0;
    }

    .about_page * {
      transform: translateY(0);
    }
  </style>
  <link rel="stylesheet" href="//dumdarac.com/pantallas/about/static/css/home.min.css">
  <link href="//dumdarac.com/resources/css/aos.css" rel="stylesheet">
</head>

<body>

  <main class="PF about_page">
    <section id="hero" class="hero" data-c-hero="">
      <div class="hero__container container scroll-aware-enter">
        <img class="hero__logo" data-src="//img.dumdarac.com/logo/logo.svg" src="//img.dumdarac.com/logo/logo.svg" alt="DumDarac logo">

        <h1 class="hero__heading" t-dd>The first free integrated Social Platform with many services and applications, developed with collaborative feedback.</h1>

        <!-----<a href="/" target="_blank" rel="noopener" class="PF PF-button hero__cta button--desktop">Start</a>----->
        <button class="button button--scroll" id="scrolltofeatures">
        <svg class="icon--svg" aria-hidden="true">
    <use xlink:href="#material-arrow-scroll"></use>
  </svg>
</button>

      </div>
    </section>
    <section id="features" class="features container" data-c-features="">
      <div class="features__feature">
        <div class="features__feature-text" data-aos="fade-left">
          <h1><?php if($usuario_mismo_nombre){?> <?=$usuario_mismo_nombre?>, <span t-dd>t<?} else {?><span t-dd>T<?}?>ell your story, your way</span></h1>
          <p class="subhead" t-dd>With creative tools like filters, themes, and effects, it's easy to express yourself in ways that delight your followers and make it even more fun.</p>
        </div>
        <div class="features__feature-images" data-aos="fade-up">
          <div class="features__feature-image-phone">
            <img class="features__feature-image-phone-img" src="//dumdarac.com/pantallas/about/static/imgs/profile.png?not" alt="">
          </div>
        </div>
      </div>
      <div class="features__feature">
        <div class="features__feature-text" data-aos="fade-right">
          <h1 t-dd>Sign in easily from any device</h1>
          <p class="subhead" t-dd>You can use your DumDarac Account to sign in to third-party sites and apps. You won't have to remember individual usernames and passwords for each account.</p>
        </div>
        <div class="features__feature-images" data-aos="fade-up">
          <div class="features__feature-image-phone">
            <img class="features__feature-image-phone-img" src="//dumdarac.com/pantallas/about/static/imgs/signin.png?not" alt="">
          </div>
        </div>
      </div>
      <div class="features__feature">
        <div class="features__feature-text" data-aos="fade-left">
          <h1 t-dd>Express yourself and react</h1>
          <p class="subhead" t-dd>Each person’s News Feed is personalized based on their interests and the sharing activity of their friends.</p>
        </div>
        <div class="features__feature-images" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
          <div class="features__feature-image-phone">
            <img class="features__feature-image-phone-img" src="//dumdarac.com/pantallas/about/static/imgs/post.png?not" alt="" />
          </div>
        </div>
      </div>
      <div class="features__feature">
        <div class="features__feature-text" data-aos="fade-right">
          <h1 t-dd>Say hello to "Spaces"</h1>
          <p class="subhead" t-dd>A space can literally be anything, a project, a group or just a simple topic. For every space you can invite multiple users.</p>
        </div>
        <div class="features__feature-images" data-aos="fade-up">
          <div class="features__feature-image-phone">
            <img class="features__feature-image-phone-img" src="//dumdarac.com/pantallas/about/static/imgs/spaces.png?not" alt="">
          </div>
        </div>
      </div>
      <div class="features__feature">
        <div class="features__feature-text" data-aos="fade-left">
          <h1 t-dd>DumDarac Hey</h1>
          <p class="subhead" t-dd>Hey is the DumDarac messaging application, message anyone from anywhere with the reliability of texting and the richness of chat. Stay in touch with friends and family, send group texts, and share your favorite pictures, GIFs, emoji, stickers, videos and audio messages.</p>
        </div>
        <div class="features__feature-images" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
          <div class="features__feature-image-phone">
            <img class="features__feature-image-phone-img" src="//dumdarac.com/pantallas/about/static/imgs/hey.png?not" alt="" />
          </div>
        </div>
      </div>
    </section>
    <section id="activity" class="activity scroll-aware-enter" data-aos="fade-up">
      <div class="PF shadow activity__container container">
        <h2 class="activity__title scroll-aware-enter" data-aos="fade-up" t-dd>The platform has been in beta mode for a while but now we would like to be able to expand the team and hire programmers to get the platform fully up and running in less than a year.</h2>
        <div class="activity__poster" data-aos="fade-left">
          <img class="features__feature-image-watch-video" src="//dumdarac.com/pantallas/about/static/imgs/iphone_x_matte_ddmockup.png?not" />
        </div>
        <div class="PF activity__tiles shadow" data-aos="fade-up">
          <div class="activity__tiles__item">
            <h3 class="activity__tiles__item__heading" t-dd>Mission Statement</h3>
            <div class="activity__tiles__item__description">
              <p t-dd>DumDarac adheres to the values of the open source movement, and so its code is published and it does not store user data. It aims to provide a high quality and engaging interface as an open source social network, rivalling the Establishment
                social networks, but fundamentally differing from them in essence. In this way DumDarac hopes to fulfill the desires of those users wishing to leave Facebook, Twitter, etc. due to legitimate privacy concerns as well as offering a positive
                example of establishing concrete, popular alternatives to dystopian social networks.</p>
              <p t-dd>At the present it functions as its own social network (with potentiality to add many new features) and, in the interests of a realistic transition, it also functions as an overall internet/social network aggregator. This means that users
                will still be able to follow others on current widely used social networks without compromising their own personal privacy, all while benefiting from similar services on DumDarac, but in a functional, open source and alternative way.</p>
            </div>
          </div>
          <div class="activity__tiles__item">
            <div class="activity__tiles__item__description">
              <p t-dd>The platform has been in beta mode for a while but now we would like to be able to expand the team and hire programmers to get the platform fully up and running in less than a year. The current team needs to find programmers, translators,
                vision contributors, investors, internet security & cryptocurrency-ico experts. A non-profit platform whose funding is based on donations. All the contributions of will be destined to it.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="apps" class="apps container">
      <h4 class="apps__heading">
        <a opendd-href="/" class="PF PF-button" t-dd>Discover more on DumDarac</a>
        <a opendd-href="/?p=donate" class="PF PF-button PFC-red" t-dd>Make us a donation</a>
      </h4>
    </section>
    <section id="download" class="download PFC-yellow" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
      <div class="download__container container">
        <div class="download__image-container" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
          <img class="download__image" src="//dumdarac.com/pantallas/about/static/imgs/start-using.png?not?not" alt="DumDarac logo">
        </div>
        <div class="download__content" data-aos="fade-up" data-aos-anchor-placement="bottom-bottom">
          <h2 class="download__title" t-dd>Let’s build the future, together</h2>
          <a href="https://play.google.com/store/apps/details?id=com.dumdarac.application" target="_blank" rel="noopener" class="PF download__cta button--desktop" t-dd><img style="width:10em;" src="https://play.google.com/intl/en_us/badges/images/generic/<?=substr($language, 0, 2)?>_badge_web_generic.png?not"></a>
          <a opendd-href="?p=login" rel="noopener" class="PF PF-button download__cta button--desktop" t-dd>Open on the web</a>
        </div>
      </div>
    </section>
  </main>

  <footer class="PF PF-footer">
    <div class="block middle">

      <div class="section">
        <h1 t-dd>Features</h1>
        <ul>
          <li><a opendd-href="/?p=help&q=terms" t-dd>Terms</a></li>
          <li><a opendd-href="?p=applications" t-dd>Applications</a></li>
          <li><a opendd-href="?p=lastest-updates" t-dd>Lastest Updates</a></li>
        </ul>
      </div>

      <div class="section">
        <h1 t-dd>Details</h1>
        <ul>
          <li><a opendd-href="?p=team" t-dd>Team</a></li>
          <li><a opendd-href="?p=team&action=contact" t-dd>Contact</a></li>
        </ul>
      </div>

      <div class="section">
        <h1 t-dd>Technology</h1>
        <ul>
          <li><a href="#" t-dd>How it works</a></li>
          <li><a opendd-href="?p=about&info=algorithms" t-dd>Algorithms</a></li>
          <li><a opendd-href="?p=transparency" t-dd>Transparency</a></li>
        </ul>
      </div>

      <div class="section">
        <h1 t-dd>FAQ</h1>
        <ul>
          <li><a opendd-href="?p=help" t-dd>Help Center</a></li>
          <li><a opendd-href="?p=help&section=forum&action=questions" t-dd>Questions</a></li>
          <li><a opendd-href="?p=help&section=forum&action=answers" t-dd>Answers</a></li>
        </ul>
      </div>

    </div>

    <div class="block bottom">
      <h1 style="font-family: DumDarac Logo; font-size: 1em; font-weight: 900;">DumDarac</h1>
      <ul>
        <li><a href="/?p=help" t-dd>Help</a></li>
        <li><a opendd-href="?p=terms" t-dd>Privacy & Terms</a></li>
      </ul>
    </div>

  </footer>

<p style="padding: 1em;">Made with ❤️ in the earth, from the <a opendd-href="?p=team">DumDarac Team</a>.</p>
  
  <svg class="svg-icons" xmlns="http://www.w3.org/2000/svg">
      <symbol id="material-arrow" viewBox="0 0 24 24">
  <polyline fill="none" stroke="#000" stroke-width="2" points="7 22 17.001 12 7 2" transform="matrix(0 1 1 0 0 0)"></polyline>
</symbol>
      <symbol id="material-arrow-scroll" viewBox="0 0 24 24">
  <polyline fill="none" stroke-width="4" points="7 22 17.001 12 7 2" transform="matrix(0 1 1 0 0 0)"></polyline>
</symbol>
</svg>



  <script src="//dumdarac.com/resources/js/aos.js"></script>
  <script type="text/javascript">
    setTimeout(function() {
      AOS.init();
    }, 1000);

    $("#scrolltofeatures").click(function() {
      $('html, body').animate({
        scrollTop: $("#features").offset().top - 200
      }, 2000);
    });

    var scrollingcity;
    var scrollingcitybg;
    $(window).scroll(function(){
      $("#hero").css("opacity", 1 - $(window).scrollTop() / 500);
    $("#hero").css("background-position", "center bottom -" + $(window).scrollTop() / 5 + "px");
  });
  
  
  </script>


</body>

</html>