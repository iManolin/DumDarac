<html>

<head>
  <style>
    .feedback-support-container {
      max-width: 50rem;
      margin:auto;
    }
  </style>
</head>

<body>

  <div class="feedback-support-container" >
    <div class="PF PF-content full noborder center transparent">
    <div class="container">
      <div class="image" style="background-image:url('./pantallas/feedback-support/feedback-support.svg');"></div>
      <h1 t-dd>Feedback & Support</h1>
      <p t-dd>Submit feedback about DumDarac</p>
    </div>
  </div>
    
    <div class="PF-card">
      <div class="info">
        <h1 t-dd >Feedback and support</h1>
    <p t-dd >
      If you have any questions, feature requests or bug reports, let us know.
      We can't guarantee all messages get an answer due to the vast amount of messages we get.
    </p>
      </div>
      
        <a class="PF PF-button transparent ripple" opendd-href="?p=help">
        <div class="inside">
          <p t-dd>Ask a question</p>
        </div>
      </a>
      <a class="PF PF-button transparent ripple PFC-red opendd" opendd-href="?p=bugs-report">
        <div class="inside">
          <p t-dd>Report a bug</p>
        </div>
      </a>
      <a class="PF PF-button transparent ripple PFC-green" target="_blank" href="mailto:requestfeature@dumdarac.com">
        <div class="inside">
          <p t-dd>Request a feature</p>
        </div>
      </a>
      
    </div>
    
    <div class="PF PF-card" >
      <div class="info">
        <h1>Translations</h1>
      <p>We are in desperate need for community translators. We would highly appreciate it if you could help us out.</p>
      </div>
      <div class="footer">
      <a class="PF PF-button transparent ripple opendd" opendd-href="?p=contributors">
        <div class="inside">
          <p t-dd>Help with translations</p>
        </div>
      </a>
      </div>
    </div>
    
    <div class="PF PF-card" >
      <div class="info">
        <h1>Donate</h1>
      <p>If you like the extension - and we sincerely hope you do - consider donating. It will help the development process and make it easier for us to improve it for everyone.</p>
      </div>
      <div class="footer">
      <a class="PF PF-button transparent ripple opendd" opendd-href="?p=donate">
        <div class="inside">
          <p t-dd>Donate</p>
        </div>
      </a>
      </div>
    </div>

  </div>
  
</body>

</html>