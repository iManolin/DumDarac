<html>

<head>
  <style>
    .team_container {
      max-width: 55rem;
      margin: auto;
      border: none;
    }
    
    .team_container>header>.info>h1 {
      font-size: 2.5em;
      position: relative;
      width: fit-content;
    }
    
    .team_container>header>.info>h1:before {
      content: "";
      position: absolute;
      left: 0;
      right: 0;
      top: 0.75em;
      bottom: 0.3em;
      -webkit-transform: rotateZ(-2deg);
      -ms-transform: rotate(-2deg);
      transform: rotateZ(-2deg);
      background: rgba(var(--PF-color-primary), .4);
      z-index: -1;
    }
    
    .team_container>header>.info>p {
      font-size: 1.5em;
    }
    
    .team_container .PF-grid {
      grid-template-columns: repeat(auto-fill, minmax(14em, 1fr));
    }

    .team_container .PF-grid .PF-card .info p {
      -webkit-line-clamp: initial;
    }
    
    .team_container .PF-grid .PF-card {
      flex: 1 1 20%;
    }

    .team_container .PF-grid.circle>.PF-card>.PF-image {
      background-color: rgba(var(--PF-color-primary), 0.1);
      box-shadow: none;
    }
    
    .team_container .PF-grid .PF-card .info h1 {
      font-size: 1.5em;
      position: relative;
    }
    
    .team_container .PF-grid .PF-card .info h1:before {
      content: "";
      position: absolute;
      left: 0;
      right: 0;
      top: 0.65em;
      bottom: 0.3em;
      -webkit-transform: rotateZ(-2deg);
      -ms-transform: rotate(-2deg);
      transform: rotateZ(-2deg);
      background: rgba(var(--PF-color-primary), .4);
      z-index: -1;
    }

    @media (min-width: 28em) {
      .team_container .founder-grid {
        width:fit-content;
        margin: auto;
      }
    }

    @media (max-width: 28em) {
      .team_container .PF-grid.other-team {
        grid-template-columns:repeat(auto-fill, minmax(10em, 1fr));
      }
    }
    
  </style>
</head>

<body>

  <div id="team_container" class="team_container PF-page">
    <header>
      <div class="info">
        <h1 t-dd>Team</h1>
        <p t-dd>We are a small team united by a common passion, to make a better world. Most of us come from long adventures and experiences that have made us think in such a way that we want to do our bit, and we are all very passionate about solving real problems
          of users in this space. As a team, we also love electric cars and dogs, but otherwise we have a very diverse set of passions outside of work.</p>
      </div>
    </header>

    <div class="content">
      <div class="PF PF-grid circle center founder-grid">

        <div class="PF PF-card PFC-red" opendd-href="?p=perfil&id=108">
          <div class="PF PF-image ripple" style=" background-image: url('./pantallas/team/photos/108.png');" rgdd></div>
          <div class="info">
            <h1>Albert Isern Alvarez</h1>
            <p t-dd>CEO, Founder & Developer.</p>
          </div>
        </div>
        
      </div>
      <div class="PF PF-grid circle center other-team">

        <div class="PF PF-card PFC-green" opendd-href="?p=perfil&id=49">
          <div class="PF PF-image ripple" style=" background-image: url('./pantallas/team/photos/49.png');" rgdd></div>
          <div class="info">
            <h1>Pablo Gomez Prieto</h1>
            <p t-dd>Developer, User experience & DumDarac Ambassador in Kuristan.</p>
          </div>
        </div>

        <div class="PF PF-card PFC-purple" opendd-href="?p=perfil&id=683">
          <div class="PF PF-image ripple" style=" background-image: url('./pantallas/team/photos/683.png');" rgdd></div>
          <div class="info">
            <h1>Nikolaos Branidis</h1>
            <p t-dd>Nikolaos focuses on strategy, investor relations, and sending DumDarac love around the globe.</p>
          </div>
        </div>

        <div class="PF PF-card PFC-blue" opendd-href="?p=perfil&id=391">
          <div class="PF PF-image ripple" style=" background-image: url('./pantallas/team/photos/391.png');" rgdd></div>
          <div class="info">
            <h1>Abigail Castro</h1>
            <p t-dd>Collaborator & Support.</p>
          </div>
        </div>

        <div class="PF PF-card PFC-orange" opendd-href="?p=perfil&id=422">
          <div class="PF PF-image ripple" style=" background-image: url('./pantallas/team/photos/422.png');" rgdd></div>
          <div class="info">
            <h1>Alex Gomez</h1>
            <p t-dd>Developer.</p>
          </div>
        </div>

      </div>
    </div>

  </div>


</body>

</html>