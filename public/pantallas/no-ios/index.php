<html>

<head>
	<style>
		.no-ios {
			max-width: calc(500px + 2rem);
			margin: auto;
		}
		
		.no-ios .feed .post .top {
			display: none;
		}
	</style>
</head>

<body>

	<div class="no-ios">
		<div class="PF PF-content full noborder center">
			<div class="container">
				<div class="PF PF-image transparent" style="background-image:url('./pantallas/no-ios/no-ios.png'); background-size: contain; min-height: 6em;" rgdd></div>
				<h1 t-dd>Sorry, but DumDarac is not yet available for iOS.</h1>
			</div>
		</div>

		<div class="PF PF-card">
			<div class="info">
				<h1 t-dd>The last DumDarac version is: <?php echo file_get_contents(__DIR__.'/../../version.txt'); ?></h1>
			</div>
		</div>

		<div class="PF PF-card">
			<div class="info">
				<h1 t-dd>Donate</h1>
			</div>

			<a class="PF PF-button transparent ripple" opendd-href="?p=donate">
				<div class="inside">
					<p t-dd>Donate</p>
				</div>
			</a>
		</div>

	</div>

	<script> $('#header').addClass('hidden'); </script>

</body>

</html>