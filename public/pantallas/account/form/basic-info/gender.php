<div class="PF PF-toolbar">
	<div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
	<h1 t-dd>Gender</h1>
</div>
<form class="PF PF-form" id="formaccount-update-gender" action="./pantallas/account/update/basic-info/gender.php" method="post" enctype="multipart/form-data">
	<div class="PF-select">
		<select id="PF-select" name="gender">
			<?php if($usuario_mismo_gender){?>
			<option value=""></option>
			<option selected="selected" value="<?=$usuario_mismo_gender?>"><?=$usuario_mismo_gender?></option>
			<?} else {?>
			<option selected="selected" value=""></option>
			<?}?>
			<option value="BiGender">BiGender</option>
			<option value="Cross-Dresser">Cross-Dresser</option>
			<option value="Drag-King">Drag-King</option>
			<option value="Drag-Queen">Drag-Queen</option>
			<option value="Androgynous">Androgynous</option>
			<option value="Femme">Femme</option>
			<option value="Female to male">Female to male</option>
			<option value="FTM">FTM</option>
			<option value="Gender Bender">Gender Bender</option>
			<option value="Genderqueer">Genderqueer</option>
			<option value="Male to Female">Male to Female</option>
			<option value="MTF">MTF</option>
			<option value="No Op">No Op</option>
			<option value="Hijra">Hijra</option>
			<option value="Pangender">Pangender</option>
			<option value="Transsexual">Transsexual</option>
			<option value="Transperson">Transperson</option>
			<option value="Woman">Woman</option>
			<option value="Male">Male</option>
			<option value="Buch">Buch</option>
			<option value="Two-Spirit">Two-Spirit</option>
			<option value="Trans">Trans</option>
			<option value="Blender Gender">Blender Gender</option>
			<option value="Agender">Agender</option>
			<option value="Third Sex">Third Sex</option>
			<option value="Fluid gender">Fluid gender</option>
			<option value="Non-binary transgender">Non-binary transgender</option>
			<option value="Hermaphrodite">Hermaphrodite</option>
			<option value="Gifted Gender">Gifted Gender</option>
			<option value="Transgender">Transgender</option>
			<option value="Femme Queen">Femme Queen</option>
			<option value="Person with Transgender experience">Person with Transgender experience</option>
		</select>
		<label class="form-label" for="PF-select">Gender</label>
	</div>
	<button class="PF-button" style="margin-left: auto;" t-dd>Save</button>
</form>

<script>
	$("#formaccount-update-gender").ajaxForm({
		beforeSubmit: function(arr, $form, options) {
			$('.header .PF-progress.loading').show();
			alertdd.show('Saving gender');
		},
		success: function(data) {
			$('.header .PF-progress.loading').hide();
			eval(data);
		}
	});
</script>