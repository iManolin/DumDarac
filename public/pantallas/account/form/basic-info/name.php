<div class="PF PF-toolbar">
	<div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
	<h1 t-dd>Name</h1>
</div>
<form class="PF PF-form" id="formaccount-update-name" action="./pantallas/account/update/basic-info/name.php?<?=$server_querystring?>" method="post" enctype="multipart/form-data">
	<div class="PF-twocolumns">
		<label class="PF-textfield filled">
			<input type="text" placeholder=" " name="name" value="<?=$usuario_mismo_nombre?>">
			<span t-dd>Name</span>
		</label>
		<label class="PF-textfield filled">
			<input type="text" placeholder=" " name="lastname" value="<?=$usuario_mismo_apellidos?>">
			<span t-dd>Last Name</span>
		</label>
	</div>
	<button class="PF-button" style="margin-left: auto;" t-dd>Save</button>
</form>

<script>

  $("#formaccount-update-name").ajaxForm({
    beforeSubmit: function (arr, $form, options) {
      $('.header .PF-progress.loading').show();
      alertdd.show('Saving name');
    },
    success: function (data) {
      $('.header .PF-progress.loading').hide();
        eval(data);
    }
  });
  
</script>