<div class="PF PF-toolbar">
	<div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
	<h1 t-dd>Birth</h1>
</div>
<form class="PF PF-form">
	<div class="PF-twocolumns">
		<label class="PF-textfield filled">
			<input placeholder="" value="<?=$usuario_mismo_birth_city?>">
			<span t-dd>City of birth</span>
		</label>
		<label class="PF-textfield filled">
			<input placeholder="" value="<?=$usuario_mismo_birth_day?>">
			<span t-dd>Date of birth</span>
		</label>
	</div>
</form>