<div class="PF PF-page">
	<header>
		<div class="info">
			<h1 t-dd>Profile</h1>
			<p><span t-dd>Some info may be visible to other people using DumDarac services.</span></p>
		</div>
	</header>
	<div class="grid-info">

		<div class="item ripple" onclick="windowdd('./resources/window/cambiar-avatar.php');">
			<div class="left">
				<span t-dd>Avatar</span>
			</div>
			<div class="right">
				<div class="double">
					<p t-dd>An avatar helps personalize your account</p>
				</div>
				<div class="PF-avatar ddbg-avatar circle" rgdd></div>
			</div>
		</div>

		<div class="item ripple" onclick="windowdd('./pantallas/account/form/basic-info/name.php', 'fit');">
			<div class="left">
				<span t-dd>Name</span>
			</div>
			<div class="right">
				<p user-data-fullname><?=$usuario_mismo_fullname?></p>
			</div>
		</div>

		<div class="item ripple" onclick="windowdd('./pantallas/account/form/basic-info/state.php', 'fit');">
			<div class="left">
				<span t-dd>State</span>
			</div>
			<div class="right">
				<p user-data-state><?=replaceemojis($usuario_mismo_estado)?></p>
			</div>
		</div>

		<div class="item ripple">
			<div class="left">
				<span t-dd>Birth</span>
			</div>
			<div class="right">
				<p user-data-birth><?=$usuario_mismo_birth?></p>
			</div>
		</div>
		
		<div class="item ripple" onclick="windowdd('./pantallas/account/form/basic-info/gender.php', 'fit');">
			<div class="left">
				<span t-dd>Gender</span>
			</div>
			<div class="right">
				<p user-data-gender><?=$usuario_mismo_gender?></p>
			</div>
		</div>

		<div class="item ripple" onclick="windowdd('./pantallas/account/form/basic-info/color.php', 'fit');">
			<div class="left">
				<span t-dd>Color</span>
			</div>
			<div class="right">
				<p user-data-color><?=ucfirst($usuario_mismo_color)?></p>
			</div>
		</div>

	</div>
</div>


<div class="PF PF-page">
	<header>
		<div class="info">
			<h1 t-dd>Contact info</h1>
		</div>
	</header>
	<div class="grid-info">


		<div class="item ripple" onclick="windowdd('./pantallas/account/form/basic-info/email.php', 'fit');">
			<div class="left">
				<span t-dd>Email</span>
			</div>
			<div class="right">
				<p user-data-email><?=$usuario_mismo_email?></p>
			</div>
		</div>

		<div class="item ripple" onclick="windowdd('./pantallas/account/form/basic-info/phone.php', 'fit');">
			<div class="left">
				<span t-dd>Phone</span>
			</div>
			<div class="right">
				<p user-data-phone><?=$usuario_mismo_phone?></p>
			</div>
		</div>

		<div class="item ripple" onclick="windowdd('./pantallas/account/form/basic-info/web.php', 'fit');">
			<div class="left">
				<span t-dd>Website</span>
			</div>
			<div class="right">
				<p user-data-web><?=$usuario_mismo_web?></p>
			</div>
		</div>

	</div>
</div>

<div class="PF PF-page">
	<header>
		<div class="info">
			<h1 t-dd>DumDarac ID y Privacy</h1>
			<p t-dd>DumDarac ID is designed to protect your information so you can choose what to share.</p>
			<br/>
			<p t-dd>We work hard to protect your privacy and collect only the data we need to improve your experience.</p>
			<br/>
			<p t-dd>When you sign in DumDarac stores certain usage data for security, support, and reporting purposes, such as your IP address, time, security level, and log history.</p>
		</div>
	</header>
	<div class="grid-info"></div>
</div>