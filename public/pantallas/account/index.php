<html>

<head>
	<style>

  html {
    height: 100%;
  }

  body {
    overflow: hidden;
  }

  #wrapper {
    overflow: auto;
  }
	
		.account-container {
			flex: 1;
		}
	</style>
</head>

<body>

	<div class="account-container">
		
		<div class="PF PF-content full noborder center between">
			<div class="container">
				<div class="PF-image PF-avatar ddbg-avatar circle" rgdd onclick="windowdd('./resources/window/cambiar-avatar.php');">
					<div class="PF PF-icon ripple"><i class="material-icons">cloud_upload</i></div>
				</div>
				<h1 user-data-fullname><?=$usuario_mismo_nombre?> <?=$usuario_mismo_apellidos?></h1>
				<p t-dd>Manage your info, privacy, and security to make DumDarac better for you</p>
			</div>
		</div>
		
		<div class="PF-tabs">
    <div class="PF PF-tabbar shadow">
      <div class="container">
        <ul>
        	<li class="ripple active" data-for="home-accounttab" data-taburl="./pantallas/account/pantallas/home.php?<?=$server_querystring?>"><span t-dd>Home</span></li>
          <li class="ripple" data-for="personalinfo-accounttab" data-taburl="./pantallas/account/pantallas/personal-info.php?<?=$server_querystring?>"><span t-dd>Personal Info</span></li>
          <li class="ripple" data-for="datapersonalization-accounttab" data-taburl="./pantallas/personalization/index.php?<?=$server_querystring?>"><span t-dd>Data & Personalization</span></li>
          <li class="ripple" data-for="security-accounttab" data-taburl="./pantallas/account/pantallas/security.php?<?=$server_querystring?>"><span t-dd>Security</span></li>
          <li class="ripple" data-for="peoplesharing-accounttab"><span t-dd>People & Sharing</span></li>
          <li class="ripple" data-for="paymentssubscriptions-accounttab"><span t-dd>Payments & Subscriptions</span></li>
        </ul>
        <div class="slider"></div>
      </div>
    </div>
    <div class="tabs">
    	<div class="tab" data-name="home-accounttab"></div>
      <div class="tab" data-name="personalinfo-accounttab"></div>
      <div class="tab" data-name="datapersonalization-accounttab"></div>
      <div class="tab" data-name="security-accounttab"></div>
      <div class="tab" data-name="peoplesharing-accounttab"></div>
      <div class="tab" data-name="paymentssubscriptions-accounttab"></div>
    </div>
  </div>
		
	</div>

	<?php if($usuario_mismo_color){?>
		<script> $('body').addClass('PFC-<?=$usuario_mismo_color?>'); </script>
	<?}?>

</body>

</html>