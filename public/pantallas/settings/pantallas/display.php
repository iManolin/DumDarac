<div class="PF PF-form" style="padding: 1rem;">
	<label class="PF-switch">
		<input type="checkbox" darkmode-checkbox>
		<span>Dark mode</span>
	</label>
</div>

<script>

	if($(body).hasClass('PF-dark')){
		$('[darkmode-checkbox]').click();
	}

	$(document).on('change', '[darkmode-checkbox]', function() {
		if($('[darkmode-checkbox]').is(':checked')){
			$('body').removeClass('PF-light');
			$('body').addClass('PF-dark');
			document.cookie = 'darkmode=1';
		} else {
			$('body').removeClass('PF-dark');
			$('body').addClass('PF-light');
			document.cookie = 'darkmode=0';
		}
	});
</script>