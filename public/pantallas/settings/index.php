<div class="PF PF-toolbar">
    <div class="PF PF-icon ripple closewindowdd"><i class="material-icons">&#xE5CD;</i></div>
    <h1 t-dd>Settings</h1>
  </div>

<div class="PF-tabs">
    <div class="PF PF-tabbar shadow">
      <div class="container">
        <ul>
        	<li class="ripple active" data-for="home-settingstab" data-taburl="./pantallas/settings/pantallas/home.php?<?=$server_querystring?>" preload><span t-dd>Home</span></li>
          <li class="ripple" data-for="display-settingstab" data-taburl="./pantallas/settings/pantallas/display.php?<?=$server_querystring?>" preload><span t-dd>Display</span></li>
          <li class="ripple" data-for="about-settingstab" data-taburl="./pantallas/settings/pantallas/about.php?<?=$server_querystring?>" preload><span t-dd>About DumDarac</span></li>
        </ul>
        <div class="slider"></div>
      </div>
    </div>
    <div class="tabs">
    	<div class="tab" data-name="home-settingstab"></div>
      <div class="tab" data-name="display-settingstab"></div>
      <div class="tab" data-name="about-settingstab"></div>
    </div>
  </div>