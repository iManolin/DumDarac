<html>

<head>
  <style>
    .feedback-support-container {
      max-width: 50rem;
      margin: auto;
    }

    .feedback-support-container .PF-page.known, .feedback-support-container .PF-page.solved, .feedback-support-container .PF-page.last {
      background-color: rgba(var(--PF-color-primary), 0.4);
    }
  </style>
</head>

<body>

  <div class="feedback-support-container">
    <div class="PF PF-content full noborder center transparent">
      <div class="container">
        <div class="PF-image" style="background-image:url('./pantallas/bugs-report/bugs-report.svg');"></div>
        <h1 t-dd>Report a bug</h1>
      </div>
    </div>

    <div class="PF PF-card border">
      <div class="info">
        <h1 t-dd>Send a report</h1>
        <p t-dd>
          For now we only have the email of our project in Gitlab to report bugs, but soon we will add a form.
        </p>
      </div>
      <div class="footer">
        <a class="PF PF-button transparent ripple PFC-red" href="mailto:incoming+DumDarac/DumDarac@incoming.gitlab.com">
          <div class="inside">
            <p t-dd>Report a bug</p>
          </div>
        </a>
      </div>
    </div>

    <div class="PF PF-page known">
      <header>
        <div class="info">
          <h1 t-dd>Known issues</h1>
        </div>
      </header>
      <ul class="PF grid-info">
        <?php
        $json = file_get_contents("https://gitlab.com/api/v4/projects/11605236/issues?private_token=pxBZgoP8iot4jM7zaYWC&labels=Public&state=opened");
        $array = json_decode($json, true);
        foreach($array as $key => $content){?>
        <li class="PF PF-card">
          <div class="info" >
            <h1><?=preg_replace("/\([^)]+\)/","",$content[title])?></h1>
            <p><?=$content[description]?></p>
          </div>
          <div class="footer">
            <p><span t-dd>Created</span> <span prettydate><?=$content[created_at]?></span> / <span t-dd>Last update</span> <span prettydate><?=$content[updated_at]?></span> - <span t-dd>State: <?=$content[state]?></span></p>
          </div>
          <?php
            $issueid = $content[iid];
        $json = file_get_contents("https://gitlab.com/api/v4/projects/11605236/issues/$issueid/notes?private_token=pxBZgoP8iot4jM7zaYWC");
        $array = json_decode($json, true);
        foreach($array as $key => $content){
        if (strpos($content[body], "changed weight to ") === false){?>
        <div class="PF PF-card">
          <div class="info" >
            <h1><?=$content[body]?></h1>
            <p></p>
          </div>
          <div class="footer">
            <p><span t-dd>Comment created by</span> <?=$content[author][name]?> <span prettydate><?=$content[created_at]?></span> / <span t-dd>Last update</span> <span prettydate><?=$content[updated_at]?></span></p>
          </div>
        </div>
        <?} }?>
        </li>
        <?}?>
      </ul>
    </div>

    <div class="PF PF-page solved PFC-green">
      <header>
        <div class="info">
          <h1 t-dd>Solved issues</h1>
        </div>
      </header>
      <ul class="PF grid-info">
        <?php
        $json = file_get_contents("https://gitlab.com/api/v4/projects/11605236/issues?private_token=pxBZgoP8iot4jM7zaYWC&labels=Public,Solved&state=closed");
        $array = json_decode($json, true);
        foreach($array as $key => $content){?>
        <li class="PF PF-card">
          <div class="info" >
            <h1><?=preg_replace("/\([^)]+\)/","",$content[title])?></h1>
            <p><?=$content[description]?></p>
            </div>
            <?php
            $issueid = $content[iid];
        $json = file_get_contents("https://gitlab.com/api/v4/projects/11605236/issues/$issueid/notes?private_token=pxBZgoP8iot4jM7zaYWC");
        $array = json_decode($json, true);
        foreach($array as $key => $content){
        if($content[body] != 'closed' and strpos($content[body], "changed weight to ") === false){?>
        <div class="PF PF-card">
          <div class="info" >
            <h1><?=$content[body]?></h1>
            <p></p>
          </div>
          <div class="footer">
            <p><span t-dd>Comment created by</span> <?=$content[author][name]?></p>
          </div>
        </div>
        <?}}?>
            </li>
            <?}?>
      </ul>
    </div>

    <div class="PF PF-page last PFC-orange">
      <header>
        <div class="info">
          <h1 t-dd>Last issues</h1>
        </div>
      </header>
      <ul class="PF grid-info">
        <?php
        $json = file_get_contents("https://gitlab.com/api/v4/projects/11605236/issues?private_token=pxBZgoP8iot4jM7zaYWC&state=opened&per_page=3");
        $array = json_decode($json, true);
        foreach($array as $key => $content){?>
        <li class="PF PF-card">
          <div class="info" >
            <h1><?=preg_replace("/\([^)]+\)/","",$content[title])?></h1>
            <p><?=$content[description]?></p>
          </div>
          <div class="footer">
            <p><span t-dd>Created</span> <span prettydate><?=$content[created_at]?></span> / <span t-dd>Last update</span> <span prettydate><?=$content[updated_at]?></span> - <span t-dd>State: <?=$content[state]?></span></p>
          </div>
          <?php
            $issueid = $content[iid];
        $json = file_get_contents("https://gitlab.com/api/v4/projects/11605236/issues/$issueid/notes?private_token=pxBZgoP8iot4jM7zaYWC");
        $array = json_decode($json, true);
        foreach($array as $key => $content){
        if(strpos($content[body], "changed weight to ") === false){?>
        <div class="PF PF-card">
          <div class="info" >
            <h1><?=$content[body]?></h1>
            <p></p>
          </div>
          <div class="footer">
            <p><span t-dd>Comment created by</span> <?=$content[author][name]?> <span prettydate><?=$content[created_at]?></span> / <span t-dd>Last update</span> <span prettydate><?=$content[updated_at]?></span></p>
          </div>
        </div>
        <?} }?>
        </li>
        <?}?>
      </ul>
    </div>

    <div class="PF PF-card border">
      <div class="info">
        <h1>Donate</h1>
        <p>If you like DumDarac - and we sincerely hope you do - consider donating. It will help the development process and make it easier for us to improve it for everyone.</p>

      </div>
      <div class="footer">
        <a class="PF PF-button transparent ripple" opendd-href="?p=donate">
          <div class="inside">
            <p t-dd>Donate</p>
          </div>
        </a>
      </div>
    </div>

  </div>

</body>

</html>