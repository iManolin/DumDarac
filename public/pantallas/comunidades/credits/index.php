<html><head>
<style>
html {
  box-sizing: border-box;
}

*,
*:before,
*:after {
  box-sizing: inherit;
}

  .contenedor * {
    font-family:Coves;
  }

.contenedor {
  margin: 0;
  font-family: DumDarac Logo;
  background-color: black;
  color: white;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  height: calc(100% - 64px);
  width: 100%;
  font-size: 1.5vw;
}

section {
  margin: auto;
  text-align: center;
}

.slide {
  font-size: 4em;
}
.slide--label {
    display: inline-block;
    background-color: white;
    color: black;
    font-size: 0.5em;
    padding: 0.5rem;
    margin-bottom: 0.35em;
    font-weight: 900;
}

.twows--thewolf {
  font-size: 6em;
}
.twows--of {
  font-size: 1.5em;
  position: relative;
}
.twows--of:before, .twows--of:after {
  content: '';
  position: absolute;
  width: 7em;
  top: 0.25em;
  height: 0.5em;
  background-color: currentColor;
}
.twows--of:before {
  left: 0;
}
.twows--of:after {
  right: 0;
}
.twows--wallstreet {
  padding-top: 0.1em;
  font-size: 4.33em;
}

.i {
  text-transform: none;
}

section {
  position: absolute;
  visibility: hidden;
}

section:nth-child(1) {
  -webkit-animation: showslide 5s linear 0s 1 none;
          animation: showslide 5s linear 0s 1 none;
}

section:nth-child(2) {
  -webkit-animation: showslide 5s linear 5s 1 none;
          animation: showslide 5s linear 5s 1 none;
}

section:nth-child(3) {
  -webkit-animation: showslide 5s linear 10s 1 none;
          animation: showslide 5s linear 10s 1 none;
}

section:nth-child(4) {
  -webkit-animation: showslide 5s linear 15s 1 none;
          animation: showslide 5s linear 15s 1 none;
}

section:nth-child(5) {
  -webkit-animation: showslide 5s linear 20s 1 none;
          animation: showslide 5s linear 20s 1 none;
}

section:nth-child(6) {
  -webkit-animation: showslide 5s linear 25s 1 none;
          animation: showslide 5s linear 25s 1 none;
}

section:nth-child(7) {
  -webkit-animation: showslide 5s linear 30s 1 none;
          animation: showslide 5s linear 30s 1 none;
}

section:nth-child(8) {
  -webkit-animation: showslide 5s linear 35s 1 none;
          animation: showslide 5s linear 35s 1 none;
}

section:nth-child(9) {
  -webkit-animation: showslide 5s linear 40s 1 none;
          animation: showslide 5s linear 40s 1 none;
}

section:nth-child(10) {
  -webkit-animation: showslide 5s linear 45s 1 none;
          animation: showslide 5s linear 45s 1 none;
}

section:nth-child(11) {
  -webkit-animation: showslide 5s linear 50s 1 none;
          animation: showslide 5s linear 50s 1 none;
}

section:nth-child(12) {
  -webkit-animation: showslide 5s linear 55s 1 none;
          animation: showslide 5s linear 55s 1 none;
}

section:nth-child(13) {
  -webkit-animation: showslide 5s linear 60s 1 none;
          animation: showslide 5s linear 60s 1 none;
}

section:nth-child(14) {
  -webkit-animation: showslide 5s linear 65s 1 none;
          animation: showslide 5s linear 65s 1 none;
}

section:nth-child(15) {
  -webkit-animation: showslide 5s linear 70s 1 none;
          animation: showslide 5s linear 70s 1 none;
}

section:nth-child(16) {
  -webkit-animation: showslide 5s linear 75s 1 none;
          animation: showslide 5s linear 75s 1 none;
}

section:nth-child(17) {
  -webkit-animation: showslide 5s linear 80s 1 none;
          animation: showslide 5s linear 80s 1 none;
}

section:nth-child(18) {
  -webkit-animation: showslide 5s linear 85s 1 none;
          animation: showslide 5s linear 85s 1 none;
}

section:nth-child(19) {
  -webkit-animation: showslide 5s linear 90s 1 none;
          animation: showslide 5s linear 90s 1 none;
}

@-webkit-keyframes showslide {
  from, to {
    position: static;
    visibility: visible;
  }
}

@keyframes showslide {
  from, to {
    position: static;
    visibility: visible;
  }
}
</style></head><body>
<section class="twows">
  <div class="twows--thewolf" style="font-family: DumDarac Logo;">DumDarac</div>
  <div class="twows--of">www.dumdarac.com</div>
  <div class="twows--wallstreet">Plataforma Social Libre</div>
</section>

<section class="slide">
  <div class="slide--label">Creado, fundado y actualmente desarrollado por</div>
  <div class="slide--name">Albert Isern Alvarez</div>
</section>

<section class="slide">
  <div class="slide--label">Desarrollador web</div>
  <div class="slide--name">Pablo Prieto Gomez</div>
</section>

<section class="slide">
  <div class="slide--label">Colaborador en finanzas sin liquidez</div>
  <div class="slide--name">Enric Duran</div>
</section>

<section class="slide">
  <div class="slide--label">Agradecimientos a</div>
  <div class="slide--name">FairCoop</div>
  <div class="slide--name">Chip Chap</div>
</section>
<!-----------
<section class="slide">
  <div class="slide--label">Produced by</div>
  <div class="slide--name">Riza Aziz, <small class="i">p.g.a.</small></div>
  <div class="slide--name">Joey M<span class="i">c</span>Farland, <small class="i">p.g.a.</small></div>
</section>

<section class="slide">
  <div class="slide--label">Produced by</div>
  <div class="slide--name">Emma Tillinger Koskoff, <small class="i">p.g.a.</small></div>
</section>

<section class="slide">
  <div class="slide--label">Executive Producer</div>
  <div class="slide--name">Georgia Kacandes</div>
</section>

<section class="slide">
  <div class="slide--label">Executive Producers</div>
  <div class="slide--name">Alexandra Milchan</div>
  <div class="slide--name">Rick Yorn</div>
  <div class="slide--name">Irwin Winkler</div>
</section>

<section class="slide">
  <div class="slide--label">Executive Producers</div>
  <div class="slide--name">Danny Dimbort</div>
  <div class="slide--name">Joel Gotler</div>
</section>

<section class="slide">
  <div class="slide--label">Director of Photography</div>
  <div class="slide--name">Rodrigo Prieto, <small>ASC</small>, <small>AMC</small></div>
</section>

<section class="slide">
  <div class="slide--label">Production Designer</div>
  <div class="slide--name">Bob Shaw</div>
</section>

<section class="slide">
  <div class="slide--label">Edited by</div>
  <div class="slide--name">Thelma Schoonmaker, <small>A.C.E.</small></div>
</section>

<section class="slide">
  <div class="slide--label">Costume Designer</div>
  <div class="slide--name">Sandy Powell</div>
</section>

<section class="slide">
  <div class="slide--label">Executive Music Producer</div>
  <div class="slide--name">Robbie Robertson</div>
</section>

<section class="slide">
  <div class="slide--label">Musical Supervison</div>
  <div class="slide--name">Randall Poster</div>
</section>

<section class="slide">
  <div class="slide--label">Visual Effects Supervisor</div>
  <div class="slide--name">Rob Legato</div>
</section>

<section class="slide">
  <div class="slide--label">Co-Producers</div>
  <div class="slide--name">Adam Somner</div>
  <div class="slide--name">Richard Baratta</div>
  <div class="slide--name">Ted Griffin</div>
</section>

<section class="slide">
  <div class="slide--label">Casting By</div>
  <div class="slide--name">Ellen Lewis</div>
</section>

---------->

</body></html>