<?php
$back=$_GET['back'];
$antiguo=$_GET['antiguo'];
$continuar=$_GET['continuar'];
if(stripos($back, '?') !==false) {
  $pregory='&';
} else {
  $pregory='?';
}?>

<html>

<head>
  <style>
    .language_container {
      width: 100%;
      margin: auto;
      max-width: 45rem;
      overflow: auto;
      text-align: center;
    }
    
    .language_container * {
      transition: 0.5s;
    }
    
    .language_container .language_head {
      width: 100%;
      background-size: auto 90%;
      background-position: bottom right;
      background-repeat: no-repeat;
      /* background-color: #f4f4f7; */
      background-image: url(./pantallas/languages/cover.svg);
      display: inline-flex;
      align-items: center;
      text-align: left;
      border-radius: 1em;
      padding: 1rem;
    }
    
    .language_container .language_head div {
      padding: 2rem;
      padding-top: 2.5rem;
      width: 100%;
    }
    
    .language_container .language_head div h1 {
      color: var(--PF-color-original-default);
    }
    
    .language_container .language_head div p {
      color: #bcbcbc;
      font-size: 1.2rem;
    }
    
    .language_container .language_lists {
      margin: auto;
      padding: 0.5rem;
      display: inline-block;
      column-width: 15rem;
      column-count: auto;
      column-gap: 0;
      max-width: 100%;
      width: 100%;
      text-align: left;
    }
    
    .language_container .language_lists .list {
      width: calc(100% - 1rem);
      padding: 0.5rem;
      display: inline-flex;
      flex-direction: column;
      margin: 0.5rem;
      border-radius: 1em;
      background: rgb(var(--PF-color-surface));
    }
    
    .language_container .language_lists .list .bandera {
      min-width: 2rem;
      height: 2rem;
      background: var(--PF-color-original-default);
      margin-right: 0.5rem;
      border-radius: 50px;
      background-size: cover;
      background-position: center;
    }
    
    .language_container .language_lists .list .titulo {
      color: var(--PF-color-original-default);
      font-size: 24px;
      padding: 0.5rem;
    }
    
    .language_container .language_lists .list .language {
      width: 100%;
      display: inline-flex;
      align-items: center;
      padding: 0.5rem;
    }
    
    .language_container .language_lists .list .language .info {
      width: 100%;
    }
    
    .language_container .language_lists .list .language .info h1 {
      color: var(--PF-color-original-default);
      font-size: 17px;
      min-height: auto;
    }
    
    .language_container .language_lists .list .language .info p {
      font-size: 0.8rem;
    }
  </style>
</head>

<body>
  <div class="language_container">
    <div class="language_head">
      <div>
        <h1><span t-dd>The current language is</span> <b language-name><?=$language?></b></h1>
      </div>
    </div>
    <?php if($usuario_mismo_id){?>
      <div class="PF-card" >
        <div class="info" >
          <h1 t-dd >Find other people who speak your language</h1>
          <p t-dd >Many people who speak your language would like to connect with you, why not explore a little?<br> (Don't be annoying with people if they don't want to talk to you).</p>
          </div>
        <button class="PF PF-button ripple text" opendd-href="?p=people&qlanguage=<?=$usuario_mismo_language?>">
          <div class="inside">
            <p t-dd >Find people</p>
          </div>
        </button>
      </div>
    <?}?>
    <div class="language_lists">
      
      <?php 
        $languages_continents=mysqli_query($con,"SELECT * FROM languages_continents WHERE enabled = 1 ORDER BY name ASC");
        while($row_languages_continents=mysqli_fetch_array($languages_continents)) {
          $languages_continent_code=$row_languages_continents["code"];
          $languages_continent_name=$row_languages_continents["name"];
        ?>
        <div class="PF list shadow">
          <h1 class="titulo"><?=$languages_continent_name?></h1>

          <?php
          $languages_countries=mysqli_query($con,"SELECT * FROM languages_countries WHERE continent='$languages_continent_code' AND enabled = 1 ORDER BY native ASC");
          while($row_languages_countries=mysqli_fetch_array($languages_countries)) {
            $languages_country_code = $row_languages_countries["code"];
            $languages_country_code_icon = strtolower($languages_country_code);
            $languages_country_name = $row_languages_countries["name"];
            $languages_country_native = $row_languages_countries["native"];
            $languages_country_language = $row_languages_countries["languages"];

            $languages_country_language_array = json_decode($languages_country_language, true);
            $languages_country_language_comma = implode("','", $languages_country_language_array);
            
          $languages_languages=mysqli_query($con,"SELECT * FROM languages WHERE code IN ('$languages_country_language_comma') AND enabled = 1 ORDER BY native_name ASC");
          while($row_languages_languages=mysqli_fetch_array($languages_languages)) {
            $languages_language_code = $row_languages_languages["code"];
            $languages_language_name = $row_languages_languages["name"];
            $languages_language_native = $row_languages_languages["native_name"];
          ?>
          <a class="language" href="<?=$back?><?php if($continuar != ''){?>&continuar=<?=$continuar?><?}?><?=$pregory?>language=<?=$languages_language_code?>-<?=$languages_country_code?>">
            <div class="bandera" style="background-image:url('//img.dumdarac.com/flags/<?=$languages_country_code_icon?>.png?not');" rgdd></div>
            <div class="info">
              <h1><?=$languages_language_native?></h1>
              <p><?=$languages_country_native?></p>
            </div>
          </a>
          <?}}?>
        </div>
      <?}?>
      
    </div>
  </div>

</body>

</html>