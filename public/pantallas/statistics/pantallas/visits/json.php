<?php
header('Content-Type: application/json');

$data = array(
	labels => array(),
	datasets => array());


$count_row = 0;
$data_visit = array();
$result = mysqli_query($con,"SELECT * FROM public_statistics GROUP BY language ORDER BY MONTH(date) ASC, YEAR(date) ASC");
while($row_visits = mysqli_fetch_array($result))
  {
  	$count_row = ++$count_row;

  	$language_visits = $row_visits['language'];

  	$result_data = mysqli_query($con,"SELECT *, COUNT(*) as total, MONTH(date) as month, YEAR(date) as year FROM public_statistics WHERE language='$language_visits' GROUP BY MONTH(date), YEAR(date) ORDER BY MONTH(date) ASC, YEAR(date) ASC");
  	while($row_visits_data = mysqli_fetch_array($result_data))
  	{

			$dateObj = DateTime::createFromFormat('!m', $row_visits_data['month']);

  		if(!in_array($dateObj->format('F'), $data['labels'])){
  			$data['labels'][] = $dateObj->format('F');
  		}
  		
  		$data_visit[] = $row_visits_data['total'];

  	}

  	$data['datasets'][] = array(
  		label => $row_visits['language'],
  		data => $data_visit);

  		$data_visit = array();
  		
  	
  }



echo json_encode($data, JSON_PRETTY_PRINT);
?>