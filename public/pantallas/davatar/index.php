<html>

<head>
	<style>
		body:not(.PF-dark) .dd_screen {
			background-image: url(//img.dumdarac.com/backgrounds/city_banner_background.svg);
			background-repeat: no-repeat;
			background-position: center bottom;
			background-attachment: fixed;
		}
		
		.davatar_container {
			display: flex;
			flex-direction: column;
			margin: auto;
			max-width: 50em;
		}
		
		.davatar_container>.PF-content>.text {
			margin: 0;
			padding: .5rem;
		}
	</style>
</head>

<body>
	<div class="davatar_container">
		<div>
			<div class="PF PF-content noborder center transparent">
				<div class="container">
					<div class="PF-image PF-avatar ddbg-avatar circle" rgdd onclick="windowdd('./resources/window/cambiar-avatar.php');">
						<div class="PF PF-icon ripple"><i class="material-icons">cloud_upload</i></div>
					</div>
					<h1><?=$usuario_mismo_nombre?><span t-dd>, this is your Davatar</span></h1>
				</div>
				</d>
			</div>
			<div class="PF PF-content" style="text-align: left;">
				<div class="container">
					<p class="text" t-dd>Your Davatar is an image that appears next to your name when you write comments or news on a blog. Avatars help identify your posts on blogs and forums, why not anywhere?</p>
					<p class="text" t-dd>An "avatar" is an image that represents you online—a little picture that appears next to your name when you interact with websites.</p>
					<p class="text" t-dd>A Davatar is a Globally Avatar. You upload it and create your profile just once, and then when you participate in any Davatar-enabled site, your Davatar image will automatically follow you there.</p>
					<p class="text" t-dd>Davatar is a free service for site owners, developers, and users.</p>
				</div>
			</div>
		</div>
</body>

</html>