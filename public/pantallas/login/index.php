<html>

<head>
</head>

<body>
  <div class="internal_container PF shadow">
    <progress class="PF-progress linear loading" style="position: absolute; top: 0px; left: 0px; display: none;"></progress>
    <div class="box_container">
      <div class="box">
        <form id="loginform" method="post" action="./pantallas/login/login-access.php" autocomplete="off" class="PF PF-form">
          <div class="overlay"></div>
          <div class="processSteps"></div>
          <div class="steps">
            <div class="content step">
              <div class="logo_container">
                <div class="logo" title="DumDarac" alt="DumDarac"></div>
              </div>
              <div class="presentation">
                <h1 t-dd>Sign in</h1>
                <?php if($usuario_mismo_id){?>
                <p t-dd>Add another DumDarac Account</p>
                <?} else {?>
                <p t-dd>with your DumDarac Account</p>
                <?}?>
              </div>
              <div class="block">
                <label class="PF-textfield filled">
                <input placeholder=" " type="text" name="username" pattern=".{3,60}" required id="estein" >
                <span>DumDarac ID</span>
              </label>
                <a opendd-href="?p=recover&type=email" t-dd>Forgot email?</a>
              </div>
              <div class="block">
                <p><span t-dd>What is DumDarac? </span> <a onclick="openddgo('?p=about');" t-dd>Learn more</a></p>
                <!-----------<p><span t-dd>Not your computer? Use Anonymous mode to sign in privately.</span> <a onclick="openddgo('?p=anonymous'); $('body').addClass('PF-dark');" t-dd>Learn more</a></p>----->
              </div>
              <div class="block">
                <div class="PF PF-buttons flex full">
                  <?php if(!$usuario_mismo_id){?>
                  <div class="PF-button text" opendd-href="?p=register" t-dd>Create account</div>
                  <?}?>
                  <div class="space"></div>
                  <button type="button" class="PF-button next" t-dd>Next</button>
                </div>
              </div>
            </div>

            <div class="content step">
              <div class="logo_container">
                <div class="logo" title="DumDarac"></div>
              </div>
              <div class="presentation">
                <h1><span t-dd>Hi</span> <span data-value="user_name"></span></h1>
                <p t-dd>with your DumDarac Account</p>
              </div>
              <div class="block">

                <label class="PF-textfield filled">
                <input placeholder=" " type="password" name="usuario_clave" pattern=".{4,60}" required>
                <span t-dd >Password</span>
              </label>
                <p><span t-dd>Not</span> <span data-value="user_fullname"></span>? <a class="back" t-dd>Go back</a></p>
              </div>
              <div class="block">
                <div class="PF PF-buttons flex full">
                  <div class="PF-button text" opendd-href="?p=recover&type=password" t-dd>Forgot password?</div>
                  <div class="space"></div>
                  <button type="button" class="PF-button next" t-dd>Next</button>
                </div>
              </div>
            </div>

            <div class="content step auto">
              <div class="logo_container">
                <div class="logo" title="DumDarac"></div>
              </div>
              <div class="presentation">
                <h1><span t-dd>Welcome</span> <span data-value="user_name"></span></h1>
                <p t-dd>Logging in... In a few moments you will be redirected.</p>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  
  <div style="display: none;">
 <input type="text" id="PreventChromeAutocomplete" 
  name="PreventChromeAutocomplete" autocomplete="address-level4" />
</div>

  <?php include(__DIR__."/js.php"); ?>

</body>

</html>